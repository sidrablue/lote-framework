<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Application\App;

use SidraBlue\Lote\Application\Bootstrap\Component;
use SidraBlue\Lote\Application\Config as AppConfig;
use SidraBlue\Lote\View\Transform\Html\Base as BaseHtmlView;
use SidraBlue\Lote\View\Transform\Json;

/**
 *  */
class Bootstrap extends Component
{

    /**
     * @var AppConfig $config
     * */
    private $config;


    /**
     * Setup the access namespaces for this system
     * @return void
     * */
    public function setupViewPath()
    {
        $params = func_get_args();
        if (isset($params[2]) && $params[2] instanceof BaseHtmlView) {
            $view = $params[2];
        } else {
            $view = $this->getState()->getView();
        }
        if (isset($params[0]) && $params[0] && isset($params[1]) && $params[1] && $view && $view instanceof BaseHtmlView) {
            $systemRef = $params[0];
            $skinDir = $params[1];
            $path = LOTE_APP_PATH . $this->config->getReference() . "/view/{$systemRef}/{$skinDir}/";
            if (file_exists($path)) {
                $view->paths[] = $path;
            } else {
                $path = LOTE_APP_PATH . $this->config->getReference() . "/view/_base/{$skinDir}/";
                if (file_exists($path)) {
                    $view->paths[] = $path;
                }
            }
        }
    }

    public function setConfig($config)
    {
        $this->config = $config;
    }

    protected function getConfig(){
        return $this->config;
    }
}
