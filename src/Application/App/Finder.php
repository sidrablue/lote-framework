<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Application\App;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class Finder
{

    public function getAppEntityClasses($reference)
    {
        $result = [];
        foreach ($this->getEntityFiles(LOTE_CORE_PATH . "app/{$reference}/src/Entity/") as $path) {
            if ($entityClass = $this->getEntityClass(LOTE_LIB_PATH . "{$reference}/src/Entity", "Lote\\App\\{$reference}\\Entity\\", $path)) {
                $result[] = $entityClass;
            }
        }
        return $result;
    }

    function getEntityFiles($folder)
    {
        $result = [];
        $path = realpath($folder);
        if($path) {
            foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path)) as $filename) {
                if (preg_match("#\.php$#", $filename->getPathname(), $matches)) {
                    $result[] = $filename->getPathname();
                }
            }
        }
        return $result;
    }

    public function getProjectEntityClasses()
    {
        $result = [];
        foreach ($this->getEntityFiles(LOTE_PROJECT_PATH . "src/Entity/") as $path) {
            if ($entityClass = $this->getEntityClass(LOTE_PROJECT_PATH . "src/Entity", APP_NAMESPACE.'Entity\\', $path)) {
                $result[] = $entityClass;
            }
        }
        return $result;
    }

    private function getEntityClass($baseClassPath, $classPath, $path)
    {
        $result = false;
        if (file_exists($path)) {
            require_once $path;
            $info = pathinfo($path);
            $dirname = str_replace("\\", '/', $info['dirname']);
            $subPath = str_replace($baseClassPath, '', $dirname);
            if($subPath) {
                $subPath = substr($subPath, 1);
                $subPath = str_replace("/", '\\', $subPath).'\\';
            }
            $classPath = $classPath . $subPath. $info['filename'];
            $object = new $classPath();
            $class = new \ReflectionClass($classPath);
            $className = $class->getName();
            if (is_subclass_of($object,'SidraBlue\Lote\Object\Entity\Base')) {
                $result = $className;
            }
        }
        return $result;
    }

}
