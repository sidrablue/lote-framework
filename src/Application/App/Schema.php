<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Application\App;

use Doctrine\DBAL\Schema\Schema as DbalSchema;
use ReflectionClass;
use zpt\anno\Annotations;

class Schema
{

    public function getSchema(DbalSchema $toSchema, $className, $isMaster)
    {
        if (class_exists($className)) {
            $classAnno = new Annotations(new ReflectionClass($className));
            if ($classAnno->hasAnnotation("dbEntity") && $classAnno->offsetGet("dbEntity") === true) {
                $classReflector = new ReflectionClass($className);
                if ((!$isMaster && $this->isEntityClass($classReflector)) || ($isMaster && $this->isEntityMasterClass($classReflector))) {
                    $table = $this->getClassTable($toSchema, $className);
                    $this->addAnnotatedColumns($classReflector, $table);
                    $this->setDefaultTablePrimaryKey($table);
                    $this->addClassIndexes($table, $classAnno);
                    $this->addClassUniqueIndexes($table, $classAnno);
                }
            }
        }
    }

    /**
     * @param DbalSchema $toSchema
     * @param $className
     * @return \Doctrine\DBAL\Schema\Table $table
     */
    private function getClassTable(DbalSchema $toSchema, $className)
    {
        $classObject = new $className();
        /** @var \SidraBlue\Lote\Object\Entity\Base $classObject */
        $tableName = $classObject->getTableName();
        if ($toSchema->hasTable($tableName)) {
            $table = $toSchema->getTable($tableName);
        } else {
            $table = $toSchema->createTable($tableName);
        }
        return $table;
    }

    private function addAnnotatedColumns(ReflectionClass $classReflector, \Doctrine\DBAL\Schema\Table $table)
    {
        foreach ($classReflector->getProperties() as $methodReflector) {
            if ($this->propertyBelongsToEntityOrEntityBaseClasses($methodReflector, $classReflector)) {
                $anno = new Annotations($methodReflector);
                if ($anno->hasAnnotation('dbcolumn') && $anno->hasAnnotation('dbtype')) {
                    $column = $anno->offsetGet("dbcolumn");
                    if(!$table->hasColumn($column)) {
                        $type = $anno->offsetGet("dbtype");
                        $options = [];
                        if ($anno->hasAnnotation("dboptions")) {
                            $options = $anno->offsetGet("dboptions");
                        }
                        $table->addColumn($column, $type, $options);
                        if ($anno->hasAnnotation("dbindex") && $anno->offsetGet("dbindex") === true) {
                            $table->addIndex([$column]);
                        }
                    }
                }
            }
        }
    }

    private function setDefaultTablePrimaryKey(\Doctrine\DBAL\Schema\Table $table)
    {
        if (!$table->getPrimaryKey() && $table->hasColumn("id")) {
            $table->setPrimaryKey(['id']);
        }
    }

    private function addClassIndexes(\Doctrine\DBAL\Schema\Table $table, Annotations $classAnnotation)
    {
        $this->addClassIndexBase($table, $classAnnotation, 'dbIndex', 'addIndex');
    }

    private function addClassUniqueIndexes(\Doctrine\DBAL\Schema\Table $table, Annotations $classAnnotation)
    {
        $this->addClassIndexBase($table, $classAnnotation, 'dbUniqueIndex', 'addUniqueIndex');
    }

    private function addClassIndexBase(\Doctrine\DBAL\Schema\Table $table, Annotations $classAnnotation, $annotationName = 'dbIndex', $functionName = 'addIndex')
    {
        if ($classAnnotation->hasAnnotation($annotationName)) {
            $dbIndex = $classAnnotation->offsetGet($annotationName);
            if (is_array($dbIndex) && count($dbIndex) > 0) {
                $table->{$functionName}($dbIndex);
            }
        }
    }

    private function propertyBelongsToEntityOrEntityBaseClasses(\ReflectionProperty $property, ReflectionClass $classReflector)
    {
        return is_subclass_of($classReflector->getName(), '\SidraBlue\Lote\Object\Entity\Base') || $classReflector->getParentClass()->getName() == 'SidraBlue\Lote\Object\Entity\Base';
    }

    private function isEntityClass(ReflectionClass $reflectionClass, $directChild = false)
    {
        return $reflectionClass->getParentClass()->getName() == 'SidraBlue\Lote\Object\Entity\Base' || is_subclass_of($reflectionClass->getParentClass()->getName(), '\SidraBlue\Lote\Object\Entity\Base');
    }

    private function isEntityMasterClass(ReflectionClass $reflectionClass)
    {
        return $reflectionClass->isSubclassOf('SidraBlue\Lote\Object\Entity\BaseMaster');
    }

}
