<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Application\Bootstrap;

/**
 *  */
class Admin extends Component
{

    protected function getParentSystem()
    {
        $result = false;
        $route = $this->getState()->getRoute();
        if (isset($route['_handler']['parent'])) {
            $result = $route['_handler']['parent'];
        }
        return $result;
    }

}

