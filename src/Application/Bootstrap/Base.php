<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Application\Bootstrap;

use \SidraBlue\Lote\State\Base as State;
use SidraBlue\Lote\State\Web;
use SidraBlue\Lote\Routing\RouteCollection;

class Base
{

    /**
     * @var State $_state
     * The state object
     */
    private $_state;

    /**
     * @var string $_reference
     * The system reference
     */
    private $_reference;

    /**
     * The default constructor
     * @param Base $state
     * @access public
     */
    public function __construct($state)
    {
        $this->_state = $state;
    }

    /**
     */
    protected function onCreate()
    {

    }

    /**
     * @param State $state
     */
    public function setState($state)
    {
        $this->_state = $state;
    }

    /**
     * @return Web
     */
    public function getState()
    {
        return $this->_state;
    }

    public function setupRoutes($router)
    {
        //do nothing
    }

    public function setupSystem($state)
    {
        //do nothing
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->_reference;
    }

    /**
     * @param string $reference
     */
    public function setReference($reference)
    {
        $this->_reference = $reference;
    }

    /**
     * Setup the routes for this system
     * @param string $url
     * @param RouteCollection $routes
     * @return boolean
     */
    protected function staticUrlExists($url, $routes) {
        $result = false;
        foreach($routes as $route) {
            /** @var \Symfony\Component\Routing\Route $route */
            if($route->getPath()==$url) {
                $result = true;
            }
        }
        return $result;
    }

    /**
     * Get a variable from either the route parameters or from the request data
     *
     * @access protected
     * @param string $key - the variable name
     * @param mixed $default - the default value for the variable
     * @return mixed
     * */
    protected function getVar($key, $default = null)
    {
        $route = $this->getState()->getRoute();
        if (isset($route[$key])) {
            $result = $route[$key];
        } else {
            $result = $this->getState()->getRequest()->get($key, $default);
        }
        return $result;
    }
}