<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Application\Bootstrap;

use ReflectionClass;
use SidraBlue\Lote\Console\Application;
use SidraBlue\Lote\Console\Command;
use SidraBlue\Lote\Routing\Route;
use SidraBlue\Lote\Routing\RouteCollection;
use SidraBlue\Lote\Auth\Acl\Context;
use SidraBlue\Lote\State\Base as StateBase;

/**
 *  */
class Component extends Base
{

    /**
     * @var RouteCollection $routeCollection
     * @var array $defaults
     * @var string $routeNamePrefix
     * @var string $folderReference
     * */
    protected function setupBaseHomeRoutes(
        RouteCollection $routeCollection,
        $defaults,
        $routeNamePrefix,
        $folderReference
    ) {
        $defaults['_method'] = 'home';
        $route = new Route('/' . $folderReference, $defaults);
        $routeCollection->add("$routeNamePrefix Home", $route);
    }

    /**
     * @var RouteCollection $routeCollection
     * @var array $defaults
     * @var string $routeNamePrefix
     * @var string $folderReference
     * */
    protected function setupBaseFolderRoutes(
        RouteCollection $routeCollection,
        $defaults,
        $routeNamePrefix,
        $folderReference
    ) {
        $requirements = ['id' => '([0-9])+', 'format' => '(\.[a-z]+)?'];
        $route = new Route('/' . $folderReference . '/folder/{id}{format}', $defaults, $requirements);
        $routeCollection->add("$routeNamePrefix Folder View", $route);

        $defaults['_method'] = 'folderTree';
        $requirements = ['parent_id' => '([a-zA-Z0-9\-])+', 'format' => '(\.[a-z]+)?'];
        $route = new Route('/' . $folderReference . '/tree-folders{format}{params}', $defaults, $requirements);
        $routeCollection->add("$routeNamePrefix Folders Only Tree", $route);

        $defaults['_method'] = 'tree';
        $requirements = ['parent_id' => '([a-zA-Z0-9\-])+', 'format' => '(\.[a-z]+)?'];
        $route = new Route('/' . $folderReference . '/tree/{parent_id}{format}', $defaults, $requirements);
        $routeCollection->add("$routeNamePrefix Folder Tree", $route);

        $defaults['_method'] = 'tree';
        $requirements = ['format' => '(\.[a-z]+)?'];
        $route = new Route('/' . $folderReference . '/tree{format}{params}', $defaults, $requirements);
        $routeCollection->add("$routeNamePrefix Folder Tree Root", $route);
    }

    /**
     * @var RouteCollection $routeCollection
     * @var array $defaults
     * @var string $routeNamePrefix
     * @var string $folderReference
     * @deprecated
     * */
    protected function setupBaseItemRoutes(
        RouteCollection $routeCollection,
        $defaults,
        $routeNamePrefix,
        $itemReference
    ) {
        $requirements = ['format' => '(\.[a-z]+)?'];
        $defaults['_method'] = 'history';

        $route = new Route('/' . $itemReference . '/history/{id}{format}', $defaults, $requirements);
        $routeCollection->addOverride("$routeNamePrefix history", $route);

        $requirements = ['format' => '(\.[a-z]+)?'];
        $defaults['_method'] = 'revision';

        $route = new Route('/' . $itemReference . '/revision/{id}{format}', $defaults, $requirements);
        $routeCollection->addOverride("$routeNamePrefix revision", $route);

        $requirements = ['format' => '(\.[a-z]+)?', 'parameters' => '(.*)'];
        $defaults['_method'] = 'rollback';

        $route = new Route('/' . $itemReference . '/rollback{format}{parameters}', $defaults, $requirements);
        $routeCollection->addOverride("$routeNamePrefix rollback", $route);

        $requirements = ['id' => '([0-9])+', 'format' => '(\.[a-z]+)?'];
        $defaults['_method'] = 'itemView';

        $route = new Route('/' . $itemReference . '/{id}{format}', $defaults, $requirements);
        $routeCollection->addOverride("$routeNamePrefix View", $route);
    }

    /**
     * @var RouteCollection $routeCollection
     * @var array $defaults
     * @var string $routeNamePrefix
     * @var string $folderReference
     * @deprecated
     * */
    protected function setupBaseEditRoutes(
        RouteCollection $routeCollection,
        $defaults,
        $routeNamePrefix,
        $folderReference
    ) {
        $requirements = ['id' => '([0-9])+', 'format' => '(\.[a-z]+)?'];

        $defaults['_method'] = 'view';
        $route = new Route('/' . $folderReference . '/add{data}', $defaults, ['data' => '.*']);
        $routeCollection->addOverride("$routeNamePrefix Add", $route);

        $route = new Route('/' . $folderReference . '/edit{data}', $defaults, ['data' => '.*']);
        $routeCollection->addOverride("$routeNamePrefix Edit", $route);

        $defaults['_method'] = 'validate';
        $route = new Route('/' . $folderReference . '{format}', $defaults, $requirements);
        $routeCollection->addOverride("$routeNamePrefix Manage", $route);
    }

    /**
     * Setup the access namespaces for this system
     * @param Context $context
     * @return array
     * */
    public function setupAccess(Context $context)
    {

    }

    /**
     * Setup the hooks for this system
     * @return void
     * */
    public function setupClosures()
    {

    }

    public function setupUser()
    {

    }

    /**
     * Setup any new state DI objects
     * @param StateBase $state
     * @return array
     * */
    public function setupState(StateBase $state)
    {
        //do nothing
    }

    /**
     * Setup the login routes for this system
     * @param \SidraBlue\Lote\Routing\RouteCollection $router
     * @param string $systemReference - the reference of the system
     * @param string $controller - the name of the login controller
     * @param string $urlPrefix - the URL prefix for login routes
     * @param array $baseDefaults - Base defaults to use
     * @internal Route $route
     */
    public function setupSystemLoginRoutes($router, $systemReference, $controller, $urlPrefix = '', $baseDefaults = [])
    {
        if($system = $this->getState()->getApps()->getSystem($systemReference)) {
            $defaults = ['_handler' => ['system' => $systemReference]];
            $defaults['controller'] = $controller;
            $defaults['_method'] = 'login';

            $defaults = array_merge($defaults, $baseDefaults);

            $route = new Route('/'.$urlPrefix.'/login', $defaults);
            $router->add($system->getName().' Login', $route);
            $route = new Route('/'.$urlPrefix.'/login/redirect/{redirect}', $defaults, ['redirect' => '.+']);
            $router->add($system->getName().' Login Redirect', $route);

            $requirements = ['format' => '(\.[a-z]+)?'];
            $defaults['_method'] = 'validate';
            $route = new Route('/'.$urlPrefix.'/login/validate{format}', $defaults, $requirements);
            $router->add($system->getName().' Login Validate', $route);

            $requirements = ['format' => '(\.[a-z]+)?'];
            $defaults['_method'] = 'refreshToken';
            $route = new Route('/'.$urlPrefix.'/login/refresh-token{format}', $defaults, $requirements);
            $router->add($system->getName().' Login Refresh Token', $route);

            $defaults['_method'] = 'logout';
            $route = new Route('/'.$urlPrefix.'/logout', $defaults);
            $router->add($system->getName().' Logout', $route);

            $defaults['_method'] = 'retrieve';
            $route = new Route('/'.$urlPrefix.'/retrieve', $defaults);
            $router->add($system->getName().' Login retrieve', $route);

            $defaults['_method'] = 'resetPassword';
            $route = new Route('/'.$urlPrefix.'/reset-password', $defaults);
            $router->add($system->getName().' Reset Password Page', $route);

            $defaults['_method'] = 'updatePassword';
            $route = new Route('/'.$urlPrefix.'/updatePassword{format}', $defaults, $requirements);
            $router->add($system->getName().' Update Password', $route);
        }
    }

    /**
     * Get the name of a Twig skin for a system by checking the settings and determining if an override exists
     * @access public
     * @param string $system
     * @param string $suffix
     * @param string $default
     * @return string
     * */
    protected function getSkinName($system, $suffix = '', $default = 'default') {
        $result = $default;
        if($this->getState()->getSettings()->get("$system.skin", false)) {
            $result = $this->getState()->getSettings()->get("$system.skin", false);
            if($suffix) {
                $result .= $suffix;
            }
        }
        return $result;
    }

    /**
     * Setup the console commands
     * the installed systems and modules
     * @param Application $consoleApplication
     * @return void
     */
    public function setupConsole(Application $consoleApplication)
    {
        $reflector = new ReflectionClass(get_class($this));
        $path = dirname($reflector->getFileName());
        $path = str_replace("\\","/", $path).'/Command';
        if(is_dir($path)) {
            $consoleApplication->addCommandsFromPath($path, $reflector->getNamespaceName().'\Command');
        }
    }

}
