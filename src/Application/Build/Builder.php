<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Application\Build;

use DirectoryIterator;
use SidraBlue\Util\Dir;

class Builder
{

    public function build()
    {
        Dir::make(LOTE_BASE_PATH."/core/app");
        $config = json_decode(file_get_contents(LOTE_BASE_PATH."/lote.json"), true);
        $this->updateOrInstallApps($config);
        //$this->uninstallRemovedApps($config);

    }

    private function updateOrInstallApps($config)
    {
        foreach($config['apps'] as $k=>$v) {
            chdir(LOTE_BASE_PATH."/core/app");
            if(file_exists("$k")) {
                $this->updateApp($k, $v);
            }
            else {
                $this->installApp($k, $v);
            }
        }
    }

    private function uninstallRemovedApps($config)
    {
        $expectedApps = [];// array_keys($config['apps']);
        $currentApps = $this->getCurrentInstalledApps();
        $appsToRemove = array_diff($currentApps, $expectedApps);
        foreach($appsToRemove as $app) {
            $this->uninstallApp($app, $config['apps'][$app]);
        }
    }

    private function getCurrentInstalledApps() {
        $dirs = [];
        foreach (new DirectoryIterator(LOTE_APP_PATH) as $file) {
            if ($file->isDir() && !$file->isDot()) {
                $dirs[] = $file->getFilename();
            }
        }
        return $dirs;
    }

    private function installApp($appReference, $appData)
    {
        echo("Installing $appReference\n");
        if (isset($appData['source-url']) && isset($appData['source-branch'])) {
            shell_exec("git clone {$appData['source-url']} {$appReference} --branch={$appData['source-branch']}");
        }
    }

    private function updateApp($appReference, $appData) {
        echo("Updating $appReference\n");
        chdir(LOTE_BASE_PATH."/core/app/$appReference");
        shell_exec("git pull");
        echo("Updated $appReference\n");
    }

    private function uninstallApp($appReference, $appData) {
        Dir::delete(LOTE_APP_PATH.'/'.$appReference);
        die("Removed one...{$appReference}\n");
    }

}
