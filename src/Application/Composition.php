<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Application;

use Lote\App\TemplateBuilder\Entity\TemplateBuilder;
use function PHPSTORM_META\type;
use SidraBlue\Lote\Auth\Acl\Context;
use SidraBlue\Lote\Console\Application;
use \SidraBlue\Lote\State\Base;

class Composition
{

    /**
     * @var array $apps
     * */
    private $apps;

    /**
     * @var array $allApps
     * */
    private $allApps;

    /**
     * @var Base $state
     * */
    private $state;

    /**
     * @var boolean systemSetupCompleted
     * */
    private $systemSetupCompleted;


    /**
     * Default constructor for the site Configuration
     * @param \SidraBlue\Lote\State\Base $state
     * @param boolean $loadApps - true if the apps should be auto loaded
     * @access public
     * @return Composition
     * */
    public function __construct($state, $loadApps = true)
    {
        $this->state = $state;
        if ($loadApps) {
            $this->loadApps();
        }
    }

    /**
     * Force a reload of the apps
     * */
    public function reloadApps()
    {
        $this->apps = [];
        $this->loadApps();
    }

    private function loadApps()
    {
        global $loader;
        $loader->add("Lote\\System\\", LOTE_SRC_PATH . 'component/');
        $loader->add("Lote\\Module\\", LOTE_SRC_PATH . 'component/');
        $apps = $this->state->getReadDb()->query("select * from sb__app where enabled = 1 order by priority asc")->fetchAll(\PDO::FETCH_ASSOC);
        $this->addBaseApp();
        foreach ($apps as $v) {
            if ($v['type'] == "system") {
                $this->addSystem($v);
            } elseif ($v['type'] == "module") {
                $this->addModule($v);
            } elseif ($v['type'] == "app") {
                $this->addApp($v);
            }
        }
    }

    private function addBaseApp()
    {
        global $loader;
        if(defined('APP_NAMESPACE') && file_exists(LOTE_PROJECT_PATH.'/src/Bootstrap.php')) {
            $s = new App\Config();
            $s->setReference(APP_NAMESPACE);
            $s->setDirectory(LOTE_PROJECT_PATH);
            $s->setNamespace(APP_NAMESPACE);
            $this->apps['app']['base'] = $s;
            $this->allApps['base'] = $s;
            $loader->addPsr4(APP_NAMESPACE, LOTE_PROJECT_PATH.'/src/');
        }
    }

    private function addApp($app)
    {
        global $loader;
        $dir = LOTE_APP_PATH . '/' . $app['reference'] . '/';
        if (is_dir($dir)) {
            $s = new App\Config();
            $s->setReference($app['reference']);
            $s->setDirectory($dir);
            $s->setNamespace($namespace = '\Lote\App\\' . $app['reference'].'\\');
            $s->setData($app);
            $this->apps['app'][strtolower($app['reference'])] = $s;
            $this->allApps[strtolower($app['reference'])] = $s;
            $loader->addPsr4("Lote\\App\\" . $app['reference'] . "\\", LOTE_CORE_PATH . 'app/' . $app['reference'] . '/src');
            //$loader->addClassMap(["Lote\\App\\" . $app['reference'] . '\Bootstrap' => $s->getBootstrap()]);
        }
    }

    private function addModule($module)
    {
        global $loader;
        $dir = LOTE_SRC_PATH . 'Lote/Module/' . ucfirst($module['reference']) . '/';
        if (is_dir($dir)) {
            $s = new Module\Config();
            $s->setReference($module['reference']);
            $s->setVersion($module['version']);
            $s->setDirectory($dir);
            $s->setData($module);
            $this->apps['module'][$module['reference']] = $s;
            $this->allApps[strtolower($module['reference'])] = $s;
            $moduleFolder = $s->getModuleFolder();
            $loader->addClassMap(["Lote\\Module\\" . $moduleFolder . '\Bootstrap' => $s->getBootstrap()]);
            //$loader->add("Lote\Module\\".$moduleFolder, $s->getDirectory().'src/');
        }
    }

    private function addSystem($sys)
    {
        global $loader;
        $dir = LOTE_SRC_PATH . 'Lote/System/' . ucfirst($sys['reference']) . '/';
        if (is_dir($dir)) {
            $s = new System\Config();
            $s->setReference($sys['reference']);
            $s->setVersion($sys['version']);
            $s->setDirectory($dir);
            $s->setData($sys);
            $this->apps['system'][strtolower($sys['reference'])] = $s;
            $this->allApps[strtolower($sys['reference'])] = $s;
            $systemFolder = $s->getSystemFolder();
            $loader->addClassMap(["Lote\\System\\" . $systemFolder . '\Bootstrap' => $s->getBootstrap()]);
            //$loader->add("Lote\System\\".$systemFolder, $s->getDirectory().'');
        }
    }

    public function getSystems()
    {
        return $this->getAppsByType('system');
    }

    public function getModules()
    {
        return $this->getAppsByType('module');
    }

    /**
     * Get all of the 'app' type apps in the system.
     *
     * Eventually all systems and modules will be converted into an 'app'
     * @access public
     * @return array
     * */
    public function getApps()
    {
        return $this->getAppsByType('app');
    }

    private function getAppsByType($type)
    {
        $result = [];
        if (isset($this->apps[$type])) {
            $result = $this->apps[$type];
        }
        return $result;
    }

    /**
     * @param string $reference
     * @return System\Config
     */
    public function getSystem($reference)
    {
        $result = false;
        if (isset($this->apps['system'][$reference])) {
            $result = $this->apps['system'][$reference];
        }
        return $result;
    }

    /**
     * @param string $reference
     * @return System\Config
     */
    public function getAppOrSystem($reference)
    {
        $result = false;
        if (isset($this->apps['system'][$reference])) {
            $result = $this->apps['system'][$reference];
        }
        if(!$result && isset($this->apps['app'][$reference]) ) {
            $result = $this->apps['app'][$reference];
        }
        return $result;
    }

    public function hasSystem($reference)
    {
        return $this->getSystem($reference) instanceof Config;
    }

    public function hasModule($reference)
    {
        return $this->getModule($reference) instanceof Config;
    }

    /**
     * Check if this system has a specified app
     * @access public
     * @param string $reference
     * @return boolean
     * */
    public function hasApp($reference)
    {
        return $this->getApp($reference) instanceof Config;
    }

    public function getModule($reference)
    {
        $result = false;
        if (isset($this->apps['module'][$reference])) {
            $result = $this->apps['module'][$reference];
        }
        return $result;
    }

    /**
     * Get an app by its reference
     * @access public
     * @param string $reference
     * @return boolean
     * */
    public function getApp($reference)
    {
        $result = false;
        if (isset($this->apps['app'][strtolower($reference)])) {
            $result = $this->apps['app'][strtolower($reference)];
        }
        return $result;
    }

    public function getAllApps()
    {
        return $this->allApps;
    }

    /**
     * Setup the routes for the system, by calling all of the setupRoutes functions in the bootstraps of
     * the installed systems and modules
     * @param
     * @return void
     */
    public function setupRoutes($router)
    {
        $this->callAllSystems("setupRoutes", [$router]);
        $this->callAllModules("setupRoutes", [$router]);
        $this->callAllApps("setupRoutes", [$router]);
    }

    /**
     * Setup the routes for the system, by calling all of the setupRoutes functions in the bootstraps of
     * the installed systems and modules
     * @param
     * @return void
     */
    public function setupData()
    {
        $this->callAllSystems("setupData", null, true);
        $this->callAllModules("setupData", null, true);
        $this->callAllApps("setupData", null, true);
    }

    /**
     * Setup the route view paths for the system, by calling all of the setupRoutes functions in the bootstraps of
     * the installed systems and modules
     * @param
     * @return void
     */
    public function setupViewPath()
    {
        $this->callAllSystems("setupViewPath");
        $this->callAllModules("setupViewPath");
        $this->callAllApps("setupViewPath", func_get_args());
    }

    /**
     * Setup the signals for the system, by calling all of the setupSignals functions in the bootstraps of
     * the installed systems and modules
     * @param \Aura\Signal\Manager $signalManager
     * @return void
     */
    public function setupSignals($signalManager)
    {
        $this->callAllSystems("setupSignals", [$signalManager]);
        $this->callAllModules("setupSignals", [$signalManager]);
        $this->callAllApps("setupSignals", [$signalManager]);
    }

    /**
     * Setup the console commands
     * the installed systems and modules
     * @param Application $consoleApplication
     * @return void
     */
    public function setupConsole(Application $consoleApplication)
    {
        $this->callAllSystems("setupConsole", [$consoleApplication]);
        $this->callAllModules("setupConsole", [$consoleApplication]);
        $this->callAllApps("setupConsole", [$consoleApplication]);
    }

    /**
     * Setup the access namespaces for the system, by calling all of the setupAccess functions in the bootstraps of
     * the installed systems and modules
     * @param Context $access - an array of accesses
     * @return void
     */
    public function setupAccess(Context $access)
    {
        $this->callAllSystems("setupAccess", [$access]);
        $this->callAllModules("setupAccess", [$access]);
        $this->callAllApps("setupAccess", [$access]);
    }

    /**
     * Setup the access namespaces for the system, by calling all of the setupAccess functions in the bootstraps of
     * the installed systems and modules
     * @param Context $access - an array of accesses
     * @return void
     */
    public function setupState()
    {
        $this->callAllSystems("setupState", [$this->state]);
        $this->callAllModules("setupState", [$this->state]);
        $this->callAllApps("setupState", [$this->state]);
    }

    /**
     * Setup the access namespaces for the system, by calling all of the setupAccess functions in the bootstraps of
     * the installed systems and modules
     * @return void
     */
    public function setupClosures()
    {
        $this->callAllSystems("setupClosures", [$this->state]);
        $this->callAllModules("setupClosures", [$this->state]);
        $this->callAllApps("setupClosures", [$this->state]);
    }

    /**
     * Setup the plugins
     * @return void
     */
    public function setupPlugins()
    {
        $this->callAllSystems("setupPlugins");
        $this->callAllModules("setupPlugins");
        $this->callAllApps("setupPlugins");
    }

    private function callAllSystems($function, $params = [], $useAppData = false)
    {
        foreach ($this->getSystems() as $sys) {
            /** @var \SidraBlue\Lote\Application\System\Config $sys */
            if (file_exists($sys->getBootstrap())) {
                if($useAppData) {
                    $params = $sys->data;
                }
                $k = $sys->getSystemFolder();
                $this->callFunction('system', $k, $function, $params);
            }
        }
    }

    private function callAllModules($function, $params = [], $useAppData = false)
    {
        foreach ($this->getModules() as $mod) {
            /** @var \SidraBlue\Lote\Application\Module\Config $mod */
            if (file_exists($mod->getDirectory() . 'Bootstrap.php')) {
                if($useAppData) {
                    $params = [];
                    $params[] = $mod->data;
                }
                $k = $mod->getModuleFolder();
                $this->callModuleBootstrapFunction($k, $function, $params);
            }
        }
    }

    private function callAllApps($function, $params = [], $useAppData = false)
    {
        foreach ($this->getApps() as $app) {
            /** @var \SidraBlue\Lote\Application\App\Config $app */
            if (file_exists($app->getDirectory() . 'src/Bootstrap.php')) {
                if($useAppData) {
                    $params = $app->data;
                }
                $k = $app->getAppFolder();
                $this->callAppBootstrapFunction($app, $k, $function, $params);
            }
        }
    }

    private function callModuleBootstrapFunction($module, $function, $params = [])
    {
        $namespace = '\Lote\Module\\' . $module . '\Bootstrap';
        /** @var $class \SidraBlue\Lote\Application\System\Bootstrap */
        $class = new $namespace($this->state);
        if (method_exists($class, $function)) {
            call_user_func_array([$class, $function], $params);
        }
    }

    private function callAppBootstrapFunction($app, $ref, $function, $params = [])
    {
        $namespace = $app->getNamespace().'Bootstrap';
        /** @var $class \SidraBlue\Lote\Application\App\Bootstrap */
        if(class_exists($namespace)) {
            $class = new $namespace($this->state);
            $class->setConfig($app);
            if (method_exists($class, $function)) {
                call_user_func_array([$class, $function], $params ? $params : []);
            }
        }

    }

    public function callModuleFunction($module, $function, $params = [])
    {
        return $this->callFunction('module', $module, $function, $params);
    }

    public function callSystemFunction($system, $function, $params = [])
    {
        return $this->callFunction('system', $system, $function, $params);
    }


    private function callFunction($type, $system, $function, $params = [])
    {
        $result = null;
        $namespace = '\Lote\\' . ucfirst($type) . '\\' . $system . '\Bootstrap';
        $class = new $namespace($this->state);
        if (method_exists($class, $function)) {
            $result = call_user_func_array([$class, $function], $params);
        }
        return $result;
    }

    /**
     * Setup the view data for rendering
     * @param \SidraBlue\Lote\View\Base $view - the view Reference
     * @param string $reference - the view Reference
     * @access public
     * @return void
     * */
    public function setupView($view, $reference)
    {
        $this->callAllSystems("setupView", [$view, $reference]);
        $this->callAllModules("setupView", [$view, $reference]);
        $this->callAllApps("setupView", [$view, $reference]);
    }

    /**
     *  */
    public function setupSystem($state, $system)
    {
        /**
         * @var \SidraBlue\Lote\Application\System\Config $system
         * */
        if (!$this->systemSetupCompleted && $system = $this->getSystem($system)) {
            $this->callFunction('system', $system->getSystemFolder(), 'setupSystem', [$state]);
            $this->systemSetupCompleted = true;
        }

    }

    public function setupNamespaces()
    {

    }

}