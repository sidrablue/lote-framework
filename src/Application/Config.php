<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Application;

/**
 * Class to define a systems base parameters
 *
 * @package SidraBlue\Lote\Application\Module
 * */
class Config
{

    /**
     * The version of this system
     * @var string $_version
     * */
    protected $_version;

    /**
     * The reference of this system
     * @var string $_reference
     * */
    protected $_reference;

    /**
     * The directory base of this system
     * @var string $_directory
     * */
    protected $_directory;

    /**
     * The database data base of this system
     * @var string $data
     * */
    public $data;

    /**
     * The namespace base of this app
     * @var string $namespace
     * */
    protected $namespace;

    /**
     * @param string $directory
     * @return void
     */
    public function setDirectory($directory)
    {
        $this->_directory = $directory;
    }

    /**
     * Set the app namespace
     * @access public
     * @param string $namespace
     * @return void
     */
    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;
    }

    /**
     * Get the app namespace
     * @access public
     * @return string
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * @param array $data
     * @return void
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getDirectory()
    {
        return $this->_directory;
    }

    /**
     * @return string
     */
    public function getBootstrap()
    {
        return $this->getDirectory().'Bootstrap.php';
    }

    /**
     * @param string $reference
     */
    public function setReference($reference)
    {
        $this->_reference = $reference;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->_reference;
    }

    /**
     * @param mixed $version
     */
    public function setVersion($version)
    {
        $this->_version = $version;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->_version;
    }

}
