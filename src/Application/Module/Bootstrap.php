<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Application\Module;

use SidraBlue\Lote\Application\Bootstrap\Component;

/**
 *  */
class Bootstrap extends Component
{

    /**
     * Perform any admin tree setup required for this component
     * @param string $systemReference - the reference of the system that we are setting up nodes for
     * @return void
     * */
    public function getAdminMenu($systemReference)
    {
        //implement in child
    }

}
