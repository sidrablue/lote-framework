<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Application\Module;

use SidraBlue\Lote\Application\Config as BaseConfig;

/**
 * Class to define a systems base parameters
 *
 * @package SidraBlue\Lote\Application\Module
 * */
class Config extends BaseConfig
{

    /**
     * Get the name of folder for this systems.
     *
     * This function allows for the main system of _admin to be prefixed with an _ but still apply the same
     * namespacing conventions
     * @access public
     * @return string
     * */
    public function getModuleFolder()
    {
        return ucfirst($this->_reference);
    }

    /**
     * Get the view path for this module
     * @access public
     * @return string
     * @todo - this one does not make sense
     * */
    public function getViewPath()
    {
        return LOTE_CORE_PATH.'component/module/'.$this->getReference().'/view/default/';
    }

}