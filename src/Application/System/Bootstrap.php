<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Application\System;

use SidraBlue\Lote\Application\Bootstrap\Admin;

/**
 *  */
class Bootstrap extends Admin
{

    /**
     * Call a controller action, typically for use in a signal/slot scenario
     * @param string $className - the class name of the controller
     * @param string $function - the function to call
     * @return void
     */
    protected function callControllerFunction($className, $function)
    {
        if(class_exists($className)) {
            $c = new $className($this->getState());
            if(method_exists($c, $function)) {
                $c->{$function}();
            }
        }
    }

    protected function getThemePath($system = 'admin')
    {
        return 'theme/_admin/'.$this->getState()->getSettings()->get($system.'.theme','bluetree').'/';
    }

}
