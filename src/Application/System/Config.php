<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Application\System;

use SidraBlue\Lote\Application\Config as BaseConfig;


/**
 * Class to define a systems base parameters
 *
 * @package SidraBlue\Lote\Application\System
 * */
class Config extends BaseConfig
{

    /**
     * Get the name of folder for this systems.
     *
     * This function allows for the main system of _admin to be prefixed with an _ but still apply the same
     * namespacing conventions
     * @access public
     * @return string
     * */
    public function getSystemFolder()
    {
        return ucfirst(preg_replace("/^(_)?(.*)/",'\2',$this->_reference));
    }

    /**
     * Get the name of this system
     * @return string
     * */
    public function getName()
    {
        $result = '';
        if($this->data && isset($this->data['name'])) {
            $result = $this->data['name'];
        }
        return $result;
    }

}
