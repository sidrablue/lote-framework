<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Auth\Acl;

use Doctrine\DBAL\Connection;
use SidraBlue\Lote\Entity\User;
use SidraBlue\Lote\Event\Data;
use SidraBlue\Lote\Object\Entity\Base as BaseEntity;
use SidraBlue\Lote\Object\State;
use SidraBlue\Lote\Object\Model\Base as BaseModel;

class Access extends State
{

    /**
     * @var array $groupAccess - The group access permissions
     * */
    protected $groupAccess = false;

    /**
     * @var array $accessSequence - the hierarchical array of permissions and their level. The higher the level the greater the access
     * */
    protected $accessSequence = ['none' => 0, 'view' => 1, 'edit' => 2, 'administer' => 3];

    /**
     * Check if a user has access to a specific context
     * @param $path
     * @param $right
     * @param $ignoreAdmin
     * @return boolean
     */
    public function hasAccess($path, $right, $ignoreAdmin = false, $checkAppAccess = true)
    {
        $result = true;
        if (!$this->getState()->getUser()->is_admin || $ignoreAdmin === true) {
            $this->getState()->getApps(); //ensure that the apps are loaded
            if ($this->getState()->getContext()->isRestricted($path)) {
                if (!$this->getState()->getUser()->id) {
                    $result = false;
                } else {
                    $result = $this->checkAccess($path, $right, $ignoreAdmin, $checkAppAccess);
                }
            }
        }
        return $result;
    }

    /**
     * Check if a user has access for a specific route in a given path
     * @access private
     * @param string $path - the path, such as "admin.user"
     * @param string $right - the right, such as "view", "edit", "administer"
     * @param boolean $ignoreAdmin - True if access checks ignore admin status
     * @param boolean $checkAppAccess - True if app access levels are checked
     * @return boolean
     * */
    private function checkAccess($path, $right, $ignoreAdmin, $checkAppAccess)
    {
        $result = false;
        if($checkAppAccess) {
            $result = $this->checkAppAccess($path, $right, $ignoreAdmin);
        }
        $this->loadGroupAccess();
        while ($path && !$result) {
            if (isset($this->groupAccess[$path])) {
                $permission = $this->groupAccess[$path];
                if ($right == $permission || ($right == 'view' && $permission != 'none') || ($right == 'edit' && $permission == 'administer')) {
                    $result = true;
                }
                break;
            } else {
                $path = substr($path, 0, strrpos($path, '.'));
            }
        }
        return $result;
    }

    /**
     * Check if a user has access for a specific route in a given path via event dispatch
     *
     * @access private
     * @param string $path - the path, such as "admin.user"
     * @param string $right - the right, such as "view", "edit", "administer"
     * @param boolean $ignoreAdmin - True if access checks ignore admin status
     * @return boolean
     * */
    private function checkAppAccess($path, $right, $ignoreAdmin)
    {
        $data = new Data();
        $data->setData([
            'path' => $path,
            'right' => $right,
            'ignoreAdmin' => $ignoreAdmin,
        ]);
        $this->getState()->getEventManager()->dispatch('app.has_access', $data);

        return $data->getData('has_access', false);
    }

    /**
     * Load the group access for the current user, which is the access of all of that users groups combined
     * @access private
     * @return void
     * */
    private function loadGroupAccess()
    {
        if ($this->groupAccess === false && $this->getState()->getGroupService()->getGroupIds()) {
            $this->groupAccess = [];
            $q = $this->getState()->getReadDb()->createQueryBuilder();
            $q->select("a.*")
                ->from("sb__group_rights", "a")
                ->leftJoin("a", "sb__group", "g", "g.id = a.group_id")
                ->where("g.id in (:groups)")
                ->setParameter("groups", $this->getState()->getGroupService()->getGroupIds(), Connection::PARAM_INT_ARRAY)
                ->andWhere("a.lote_deleted is null")
                ->andWhere("g.lote_deleted is null")
                ->addOrderBy("g.sort_order");
            $allRows = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
            foreach ($allRows as $row) {
                if ($this->isHigherAccess($row)) {
                    $this->groupAccess[$row['context']] = $row['mode'];
                }
            }
        }
    }

    /**
     * Check if a given row has higher access than is currently defined for the context within that row
     * @access private
     * @param array $row
     * @return boolean
     * */
    private function isHigherAccess(array $row)
    {
        if ($result = isset($row['context'])) {
            if (isset($this->groupAccess[$row['context']])) {
                $currentAccess = $this->groupAccess[$row['context']];
                if (isset($this->accessSequence[$currentAccess]) && isset($this->accessSequence[$row['mode']])) {
                    $result = $this->accessSequence[$row['mode']] > $this->accessSequence[$currentAccess];
                }
            }
        }
        return $result;
    }

    /**
     * Check if the current user has access to a given entity
     * @access public
     * @param BaseEntity $entity
     * @param string $accessLevel
     * @param User $userEntity
     * @return boolean
     * @todo - update $accessLevel to be a constant
     * */
    public function hasEntityAccess($entity, $accessLevel = "view", $userEntity = null)
    {
        if(!$userEntity) {
            $userEntity = $this->getState()->getUser();
        }
        $return = true;
        if (is_null($entity->lote_access) || $this->getState()->isCli()) {

        } elseif ($entity->lote_access == 0) {
            if (!$userEntity->id) {
                $return = false;
            }
        } elseif ($entity->lote_access == -1) {
            if (!$userEntity->is_admin) {
                $obj = new BaseModel($this->getState());
                $count = $obj->checkAccessForEntity($entity, $accessLevel, $userEntity);
                if ($count == 0) {
                    $return = false;
                }
            }
        } elseif ($entity->lote_access == -2 && !$userEntity->is_admin) {
            $return = false;
        }
        return $return;
    }

    /**
     * Check if the current user has access to a given entity
     * @access public
     * @var BaseEntity $entity
     * @return boolean
     * @todo - update $accessLevel to be a constant
     * */
    public function addEntityPermissions($entity)
    {
        $userAccess = ['view' => false, 'edit' => false];
        $userAccess['edit'] = $this->hasEntityAccess($entity, 'edit');
        $userAccess['view'] = $userAccess['edit'] || $this->hasEntityAccess($entity, 'view');
        $entity->_permissions = $userAccess;
    }

}
