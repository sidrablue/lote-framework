<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Auth\Login\Credentials;

/**
 * HTTP Digest authentication credentials class
 * @package SidraBlue\Lote\Auth\Credentials
 * */
class HttpDigest extends Base
{

    /**
     * Load and determine the username and password from HTTP Digest parameters
     * @throws \Exception
     * @return void
     */
    protected function loadCredentials()
    {
        parent::loadCredentials();//throws exception for now
    }

}
