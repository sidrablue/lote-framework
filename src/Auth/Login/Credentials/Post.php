<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Auth\Login\Credentials;

/**
 * HTTP POST authentication credentials class
 * @package SidraBlue\Lote\Auth\Credentials
 * */
class Post extends Base
{

    /**
     * Load and determine the username and password from POST parameters
     * @return void
     */
    protected function loadCredentials()
    {
        $this->username = $this->request->request->get('username', '');
        $this->password = $this->request->request->get('password', '');
        $this->userId = $this->request->request->get('_lote_login_user_id', '');
        $this->twoFactorCode = $this->request->request->get('two_factor_code', '');
    }

}
