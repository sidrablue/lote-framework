<?php

/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Auth\Login\Method;

use SidraBlue\Lote\Model\UserSession;
use SidraBlue\Lote\Entity\User;
use SidraBlue\Util\Time;

/**
 * Lote login method, via database validation of the users credentials
 * @package SidraBlue\Lote\Auth\Login\Method
 * */
class Auto extends Base
{

    /**
     * @var User
     * */
    private $user;

    /**
     * Validate that sufficient login parameters have been provided to attempt a login
     * @access protected
     * @return boolean
     */
    protected function validParams()
    {
        return $this->credentials->getUserId();
    }


    /**
     * Validate a login against the database for this user
     * @access protected
     * @return boolean
     */
    protected function validateLogin()
    {
        $result = false;
        $this->user = new User($this->state);
        $this->user->load($this->credentials->getUserId());
        if ($this->user->id) {
            if (true || $this->user->active) {
                $s = $this->state->getUserSession();
                if($s->username !=$this->user->username) {
                    $m = new UserSession($this->state);
                    $m->delete($s->id);
                }
                $result = $this->saveLogin();
            } else {
                $this->loginError = 4; //inactive
            }
        }
        return $result;
    }

    /**
     * Save the current login
     * @param $updateHash
     * @access protected
     * @return bool
     * */
    protected function saveLogin($updateHash = true)
    {
        $result = false;
        $s = $this->state->getUserSession();
        $m = new UserSession($this->state);
        if ($loginData = $m->saveLogin($s, $this->user->id, $this->user->username, $this->credentials->getIp(), $this->siteId, $this->credentials->getSessionId(),'local', $this->credentials->getMasqueradeUserId(), $this->credentials->getTwoFactorCompleted())) {
            $session = $this->state->getRequest()->getSession();
            $this->state->getUser()->setData($this->user->getData());
            $this->state->getUser()->updateProperty('last_login', Time::getUtcNow());
            $session->set('_lote_login_user_id', $this->user->id);
            $session->set('username', $this->user->username);
            if ($updateHash) {
                $hashKey = $this->getLoginHashKey($s->token);
                $session->set('hash', password_hash($hashKey, PASSWORD_DEFAULT));
            }
            $session->set('ip', $this->credentials->getIp());
            $result = true;
        }
        return $result;
    }

    /**
     * @return Boolean
     * */
    public function passwordResetRequired()
    {
        return false;
    }

    /**
     * Get the key that is used to generate a login hash from
     * @param string $loginToken - the unique token assigned to this login
     * @access private
     * @return string
     * */
    private function getLoginHashKey($loginToken)
    {
        return $this->user->token . $loginToken . $this->user->password;
    }

    /**
     * Perform a system logout
     * @access public
     * @return void
     * */
    public function logout()
    {
        $s = new UserSession($this->state);
        $s->removeLogin($this->credentials->getSessionId(), $this->credentials->getUsername(), $this->siteId);
    }

    /**
     * Check if a user is currently logged in
     * @access public
     * @return boolean
     * */
    public function isLoggedIn()
    {
        return $this->user->isLoggedIn();
    }

}
