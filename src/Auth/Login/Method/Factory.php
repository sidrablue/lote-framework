<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Auth\Login\Method;

use SidraBlue\Lote\Auth\Login\Credentials\Base as Credentials;
use SidraBlue\Lote\State\Web as State;

/**
 * Factory for generating an instance of a login method
 * @package SidraBlue\Lote\Auth\Method
 * @see SidraBlue\Lote\Auth\Method\Base
 * */
class Factory
{

    /**
     * Get the method for a login, based on a reference
     * @param State $state
     * @param string $reference - the method credentials
     * @param Credentials $credentials - the request credentials
     * @return Base
     */
    public static function getInstance(State $state, $reference, Credentials $credentials = null)
    {
        $result = new Base($state, $credentials);
        if ($reference == 'lote') {
            $result = new Lote($state, $credentials);
        } elseif ($reference == 'ldap') {
            $result = new Ldap($state, $credentials);
        } elseif ($reference == 'auto') {
            $result = new Auto($state, $credentials);
        }
        return $result;
    }

}
