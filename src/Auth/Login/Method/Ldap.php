<?php

/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Auth\Login\Method;

use Adldap\Exceptions\AdldapException;
use Adldap\Models\Group;
use SidraBlue\Lote\Auth\Login\Credentials\Session;
use SidraBlue\Lote\Entity\User;
use SidraBlue\Lote\Model\UserSession;
use SidraBlue\Util\Time;

/**
 * LDAP login class
 * @package SidraBlue\Lote\Auth\Login\Method
 * */
class Ldap extends Base
{

    protected $userData;

    /**
     * @var User
     * */
    private $user;


    /**
     * Validate a login against the database for this user
     * @access protected
     * @return boolean
     */
    protected function validateLogin()
    {
        $result = false;
        $this->user = new User($this->state);
        if ($this->credentials instanceof Session) {
            $this->user->loadByUsername($this->credentials->getUsername());
            //if this is a session instance, then we need to verify the session hash
            $s = new UserSession($this->state);
            $data = $s->getLogin($this->credentials->getUsername(), $this->credentials->getIp(), $this->siteId, $this->credentials->getSessionId());
            $loginHash = $this->getLoginHashKey($this->state->getUser(), $data['token']);
            if (password_verify($loginHash, $this->credentials->getPassword())) {
                $result = $this->saveLogin(false);
            } else {
                $this->loginError = 2; //invalid session
            }
        } else {
            $this->state->getLdap()->ldapConnect($this->loginError);
            if ($this->state->getLdap()->getConnection()) {
                $username = $this->credentials->getUsername();
                $password = $this->credentials->getPassword();
                try {
                    $result = $this->state->getLdap()->getConnection()->getDefaultProvider()->auth()->attempt($username, $password, true);
                    if(!$result) {
                        throw new AdldapException("Unable to bind user");
                    }

                    $this->state->getLdap()->ldapConnect($this->loginError);
                    if($username != null) {
                        $this->userData = $this->state->getLdap()->getConnection()->getDefaultProvider()->search()->users()->where('samAccountName', '=', $username)->firstOrFail(['*']);
                        $result = $this->saveLogin();
                    } else {
                        $this->loginError = 1; //invalid credentials
                    }
                } catch(AdldapException $ex) {
                    $this->state->getLdap()->ldapConnect($this->loginError);
                    if ($this->state->getLdap()->requiresPasswordReset($username)) {
                        $this->state->getLdap()->unsetPasswordResetRequirement($username);
                        try {
                            $result = $this->state->getLdap()->getConnection()->getDefaultProvider()->auth()->attempt($username, $password, true);
                            if(!$result) {
                                throw new AdldapException("Unable to bind user");
                            }
                            $this->state->getLdap()->ldapConnect($this->loginError);
                            $this->state->getLdap()->setPasswordResetRequirement($username);
                            if($username != null) {
                                $this->userData = $this->state->getLdap()->getConnection()->getDefaultProvider()->search()->users()->where('samAccountName', '=', $username)->firstOrFail(['*']);
                                $result = $this->saveLogin();
                            } else {
                                $this->loginError = 1; //invalid credentials
                            }
                        } catch(AdldapException $ex) {
                            $this->loginError = 1; //invalid credentials
                        }
                    } else {
                        $this->loginError = 1; //invalid credentials
                    }
                }
            } else {
                $this->loginError = 1; //invalid credentials
            }

        }
        return $result;
    }



    /**
     * @return Boolean
     * */
    public function passwordResetRequired()
    {
        //return $this->user->passwordResetRequired();
    }

    /**
     * Get the key that is used to generate a login hash from
     * @param User $u - the user entity
     * @param string $loginToken - the unique token assigned to this login
     * @access private
     * @return string
     * */
    private function getLoginHashKey($u, $loginToken)
    {
        return $u->token . $loginToken . $u->password;
    }

    /**
     * Save the current login
     * @param $updateHash - true if the session hash needs to be updated, generally meaning that this is a new login
     * @access protected
     * @return bool
     * */
    protected function saveLogin($updateHash = true)
    {
        $result = false;
        $this->user = new User($this->state);
        $data = $this->user->loadByUsername($this->credentials->getUsername());

        if($this->userData instanceof \Adldap\Models\User) {
            $newUserData = $this->getUserDetailsFromLdap($this->userData);
            $groups = $this->userData->getGroups();
            $userGroups = [];
            foreach($groups as $group) {
                if($group instanceof Group) {
                    $userGroups[] = $group->getCommonName();
                }
            }

            $data = $data ? $data : $this->user->loadByEmail($newUserData['email']);
            if (!$data) {
                $newUserData['password'] = password_hash($this->credentials->getPassword(), PASSWORD_DEFAULT);
                $this->user->createFromLdap($newUserData, $userGroups);
                $this->user->loadByUsername($this->credentials->getUsername());
            } else {
                if ($updateHash && $this->state->getLdap()->getConnection()) {
                    $this->user->updateFromLdap($data['id'], $newUserData, $userGroups);
                }
            }
        }

        $s = $this->state->getUserSession();
        $m = new UserSession($this->state);
        if ($loginData = $m->saveLogin($s, $this->credentials->getUserId(), $this->credentials->getUsername(), $this->credentials->getIp(), $this->siteId, $this->credentials->getSessionId())) {
            $session = $this->state->getRequest()->getSession();
            $this->state->getUser()->load($s->user_id);
            $this->state->getUser()->updateProperty('last_login', Time::getUtcNow());

            $session->set('ip', $this->credentials->getIp());
            $session->set('_lote_login_user_id', $this->credentials->getUserId());
            $session->set('username', $this->credentials->getUsername());

            if ($updateHash) {
                $hashKey = $this->getLoginHashKey($this->state->getUser(), $s->token);
                $session->set('hash', password_hash($hashKey, PASSWORD_DEFAULT));
            }
            $result = true;
        }
        return $result;
    }

    /**
     * Retrieve user details from Ldap into usable myCARE user data
     *
     * @access private
     * @param \Adldap\Models\User $user - Ldap user model
     * @return array
     */
    private function getUserDetailsFromLdap($user)
    {
        $details = [];
        $details['first_name'] = $user->getFirstName();
        $details['last_name'] = $user->getLastName();
        $details['username'] = $user->getAccountName();
        $details['email'] = $user->getEmail();
        $timeLastSet = $user->getPasswordLastSet();
        $details['password_lastupdate'] = Time::ldapTimestampToDateString($timeLastSet);
        return $details;
    }

    public function updatePassword($username, $password, &$errorMessage)
    {
        $result = false;
        $this->state->getLdap()->ldapConnect($this->loginError);
        if ($this->state->getLdap()->getConnection()) {
            $result = $this->state->getLdap()->updatePassword($username, $password, $errorMessage);
        }
        return $result;
    }

}
