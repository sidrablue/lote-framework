<?php

/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Auth\Login\Method;

use SidraBlue\Lote\Auth\Login\Credentials\Session;
use SidraBlue\Lote\Entity\User;
use SidraBlue\Lote\Event\Data;
use SidraBlue\Lote\Model\Auth\Authenticator;
use SidraBlue\Lote\Model\User\Password;
use SidraBlue\Lote\Model\UserSession;
use SidraBlue\Lote\Service\Jwt;
use SidraBlue\Util\Time;
use Whitelist\Check as WhitelistCheck;
use SidraBlue\Lote\Auth\Login\Throttle;

/**
 * Lote login method, via database validation of the users credentials
 * @package SidraBlue\Lote\Auth\Login\Method
 * */
class Lote extends Base
{

    /**
     * Validate a login against the database for this user
     * @access protected
     * @return boolean
     */
    protected function validateLogin()
    {
        $result = false;
        $m = new User($this->state);
        if ($this->credentials->getToken()) {
            $result = $this->validateLoginByToken($m);
        } elseif ($this->credentials->getUsername() || $this->credentials->getUserId()) {
            if ($this->credentials->getUsername()) {
                $m->loadForLogin($this->credentials->getUsername());
            } elseif ($this->credentials->getUserId()) {
                $m->load($this->credentials->getUserId());
            }
            $result = $this->validateLoginByUserCredentials($m);
        }
        return $result;
    }

    /**
     * Validate login by token
     * @param User $m
     * @return boolean
     * */
    protected function validateLoginByToken(User $m) {
        $result = false;
        $token = $this->credentials->getToken();
        $jwtService = new Jwt();
        if($jwtService->getTokenId($token)) {
            //the DB is linked to the database, so we have to validate
        }
        else {
            $tokenKey = $this->state->getSettings()->getSettingOrConfig("login.jwt.key",false);
            if($jwtService->validateTokenString($token, $this->state->getUrl()->getBaseUrl(), $tokenKey)) {
                if($userId = $jwtService->getTokenUserId($token)) {
                    if($m->load($userId)) {
                        $this->state->getUser()->setData($m->getData());
                        $result = true;
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Validate login by user credentials, as this user was loaded by a username and/or user ID
     * @param User $m
     * @return boolean
     * */
    protected function validateLoginByUserCredentials(User $m) {
        $result = false;
        $throttle = new Throttle($this->state);
        if ($m->id) {
            if ($m->active) {
                $this->credentials->setUserId($m->id);
                $throttle->load($m->id);

                if ($this->credentials instanceof Session) {
                    //if this is a session instance, then we need to verify the session hash
                    $s = new UserSession($this->state);
                    if($data = $s->getLogin($m->id, $this->credentials->getIp(), $this->siteId, $this->credentials->getSessionId())) {
                        $loginHash = $this->getLoginHashKey($m, $data['token']);
                        if (password_verify($loginHash, $this->credentials->getPassword())) {
                            if($result = $this->saveLogin(false)) {
                                $this->credentials->setTwoFactorCompleted(boolval($data['is_two_factor_verified']));
                                if(!$data['is_two_factor_verified'] && $this->requiresTwoFactor()) {
                                    $this->state->getView()->twoFactorRequired();
                                }
                                else {
                                    $this->state->getUser()->setData($m->getData());
                                }
                            }
                        } else {
                            $this->loginError = 2; //invalid session
                        }
                    }
                    else {
                        $this->loginError = 5; //login expired
                    }
                } elseif ($this->verifyPassword($m, $this->credentials->getPassword()) && !$throttle->isSuspended() && !$throttle->isBanned()) {
                    if($result = $this->saveLogin()) {
                        $throttle->resetThrottle($m->id);

                        $this->state->getUser()->setData($m->getData());
                        if($this->twoFactorEnabled() && !$this->credentials->getTwoFactorCompleted()) {
                            $e = new \SidraBlue\Lote\Entity\UserSession($this->state);
                            $e->loadByFields(["user_id" => $m->id, "session_id" => $this->credentials->getSessionId()]);
                            $this->state->getView()->two_factor_verified = boolval($e->is_two_factor_verified);
                            if ($e->id && !$e->is_two_factor_verified) {
                                if ($this->credentials->getTwoFactorCode()) {
                                    $authenticatorModel = new Authenticator($this->state);
                                    if ($authenticatorModel->authenticate($m->id, $this->credentials->getTwoFactorCode())) {
                                        $e->is_two_factor_verified = true;
                                        $e->save();
                                        $this->state->getView()->two_factor_verified = true;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    $this->loginError = 1; //invalid credentials
                    $this->loginError = $throttle->check();
                }
            } else {
                $this->loginError = 4; //inactive
            }
        }

        return $result;
    }

    /**
     * @param User $u
     * @param $password
     * @return boolean
     */
    private function verifyPassword(User $u, $password)
    {
        if(!$result = password_verify($password, $u->password)) {
            //get users additional passwords
            $pm = new Password($this->state);
            if($passwordId = $pm->isValidPassword($password, $u->id)) {
                $pm->recordUsage($passwordId);
                $result = true;
            }
        }
        return $result;
    }

    /**
     * Save the current login
     * @param $updateHash - true if the session hash needs to be updated, generally meaning that this is a new login
     * @access protected
     * @return bool
     * */
    protected function saveLogin($updateHash = true)
    {
        $s = $this->state->getUserSession();
        $m = new UserSession($this->state);
        if ($m->saveLogin($s, $this->credentials->getUserId(), $this->credentials->getUsername(), $this->credentials->getIp(), $this->siteId, $this->credentials->getSessionId(), null, $this->credentials->getTwoFactorCompleted())) {
            $session = $this->state->getRequest()->getSession();
            $this->state->getUser()->load($s->user_id);
            $this->state->getUser()->updateProperty('last_login', Time::getUtcNow());
            $session->set('ip', $this->credentials->getIp());
            $session->set('_lote_login_user_id', $this->credentials->getUserId());
            $session->set('username', $this->credentials->getUsername());
            if ($updateHash) {
                $hashKey = $this->getLoginHashKey($this->state->getUser(), $s->token);
                $session->set('hash', password_hash($hashKey, PASSWORD_DEFAULT));
            }
        }
        return $s->id;
    }

    /**
     * Check if two step verification is required
     */
    private function requiresTwoFactor()
    {
        $result = false;
        $route = $this->state->getRoute();
        if (isset($route['_context']) && isset($route['_context']['path']) && $this->state->getContext()->isRestricted($route['_context']['path'])) {
            if (!$this->credentials->getTwoFactorCompleted() && $this->twoFactorEnabled() && !$this->loginIpWhitelisted()) {
                if (!isset($route['is_two_factor']) || !$route['is_two_factor']) {
                    $result = true;
                } else {
                    $eventData = new Data(['two_factor_required' => false]);
                    $this->state->getEventManager()->dispatch("login.two_factor_check", $eventData);
                    $result = boolval($eventData->getData("two_factor_required"));
                }
            }
        }
        return $result;
    }

    private function twoFactorEnabled()
    {
        return $this->state->getSettings()->getSettingOrConfig("login.two_factor.enabled", false);
    }

    private function loginIpWhitelisted()
    {
        $result = false;
        if ($whiteList = $this->state->getSettings()->getSettingOrConfig("login.two_factor.ip_whitelist", false)) {
            $checker = new WhitelistCheck();
            try {
                $checker->whitelist(explode(",", $whiteList));
                foreach($this->state->getRequest()->getClientIps() as $clientIp) {
                    if($checker->check($clientIp)) {
                        $result = true;
                        break;
                    }
                }
            }
            catch (InvalidArgumentException $e) {
                // thrown when an invalid definition is encountered
            }
        }
        return $result;
    }

    /**
     * @return Boolean
     * */
    public function passwordResetRequired()
    {
        return false;
    }

    /**
     * Get the key that is used to generate a login hash from
     * @param User $u - the user entity
     * @param string $loginToken - the unique token assigned to this login
     * @access private
     * @return string
     * */
    private function getLoginHashKey($u, $loginToken)
    {
        return $u->token . $loginToken . $u->password;
    }

    /**
     * Perform a system logout
     * @access public
     * @return void
     * */
    public function logout()
    {
        $sessionEntity = new \SidraBlue\Lote\Entity\UserSession($this->state);
        $sessionEntity->loadByFields(['user_id' => $this->credentials->getUserId(), 'session_id' => $this->credentials->getSessionId()]);
        if($sessionEntity->masquerade_user_id) {
            $this->state->getLogin()->autoLogin($sessionEntity->masquerade_user_id);
        }
        else {
            $s = new UserSession($this->state);
            $s->removeLogin($this->credentials->getSessionId(), $this->credentials->getUsername(), $this->siteId);
            $this->state->getRequest()->getSession()->invalidate();
        }
    }

    /**
     * Check if a user is currently logged in
     * @access public
     * @return boolean
     * */
    public function isLoggedIn()
    {
        return $this->state->getUserSession()->isValidLogin();
    }

}
