<?php

/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Auth\Login;

use SidraBlue\Lote\Auth\Login\Credentials\Base as CredentialBase;
use SidraBlue\Lote\Object\Model\Base;
use SidraBlue\Lote\Entity\UserLoginThrottle;
use SidraBlue\Lote\Entity\UserLoginAttempt;
use SidraBlue\Util\Time;

class Throttle extends Base
{
    protected $data;

    public function check()
    {
        $result = 1;

        if ($this->isSuspended()) {
            $stObj = \DateTime::createFromFormat('Y-m-d H:i:s', $this->getSuspensionTime());
            $stObj->add(new \DateInterval('PT' . $this->getSuspendTime() . 'M'));
            $now = \DateTime::createFromFormat('Y-m-d H:i:s', Time::getUtcNow());

            if ($now > $stObj) {
                $this->removeSuspension($this->data['user_id']);
            } else {
                $result = 6;
            }
        }

        if ($this->isBanned()) {
            $result = 7;
        }

        return $result;
    }

    public function getAttempt()
    {
        return isset($this->data['attempts']) ? $this->data['attempts'] : 0;
    }

    public function getAttemptLimit()
    {
        return $this->getState()->getSettings()->get('security.enabled.login.attempt_limit', 5);
    }

    public function getAttemptPeriod()
    {
        return $this->getState()->getSettings()->get('security.enabled.login.attempt_period', 10);
    }

    public function getSuspendTime()
    {
        return $this->getState()->getSettings()->get('security.enabled.login.lockout_time', 30);
    }

    public function load($userId)
    {
        $lt = new UserLoginThrottle($this->getState());
        $lt->loadByField('user_id', $userId);
        $this->data = $lt->getData();
    }

    public function isBanned()
    {
        return isset($this->data['banned']) && $this->data['banned'] ? true : false;
    }

    public function isSuspended()
    {
        return isset($this->data['suspended']) && $this->data['suspended'] ? true : false;
    }

    public function addFailedLogin(CredentialBase $credential)
    {
        if (!$this->isSuspended() && !$this->isBanned()) {
            $userId = $credential->getUserId();

            $la = new UserLoginAttempt($this->getState());
            $la->user_id = $credential->getUserId();
            $la->username = $credential->getUsername();
            $la->password = $credential->getPassword();
            $la->ip_address = $credential->getIp();
            $la->status = 'fail';
            $la->attempt = Time::getUtcNow();
            $la->save();

            if ($userId > 0) {
                $attemptPeriod = \DateTime::createFromFormat('Y-m-d H:i:s', Time::getUtcNow());
                $attemptPeriod->sub(new \DateInterval('PT' . $this->getAttemptPeriod() . 'M'));

                $q = $this->getReadDb()->createQueryBuilder();
                $q->select('count(*) as attempts')
                    ->from('sb__user_login_attempt', 'la')
                    ->where('la.user_id = :user_id')
                    ->setParameter('user_id', $userId)
                    ->andWhere('la.status = "fail"')
                    ->andWhere('la.attempt > :attempt')
                    ->setParameter('attempt', $attemptPeriod->format('Y-m-d H:i:s'))
                    ->andWhere('la.lote_deleted is null');
                $row = $q->execute()->fetch(\PDO::FETCH_ASSOC);

                $lt = new UserLoginThrottle($this->getState());
                if(!$lt->loadByField('user_id', $userId)) {
                    $this->setDefaultThrottleValues($lt, $userId);
                }
                $lt->ip_address = $credential->getIp();
                $lt->attempts = $row['attempts'];
                $lt->last_attempt = Time::getUtcNow();
                $lt->save();

                if ($row['attempts'] >= $this->getAttemptLimit() && !$this->isSuspended()) {
                    $this->suspend($userId);
                    $this->data = $lt->getData();
                }
            }
        }
    }

    public function suspend($userId)
    {
        $lt = new UserLoginThrottle($this->getState());
        if(!$lt->loadByField('user_id', $userId)) {
            $this->setDefaultThrottleValues($lt, $userId);
        }
        $lt->suspended = 1;
        $lt->suspended_time = Time::getUtcNow();
        $lt->save();
    }

    public function ban($userId)
    {
        $lt = new UserLoginThrottle($this->getState());
        if(!$lt->loadByField('user_id', $userId)) {
            $this->setDefaultThrottleValues($lt, $userId);
        }
        $lt->banned = 1;
        $lt->banned_time = Time::getUtcNow();
        $lt->save();
    }

    public function removeBan($userId)
    {
        $lt = new UserLoginThrottle($this->getState());
        if(!$lt->loadByField('user_id', $userId)) {
            $this->setDefaultThrottleValues($lt, $userId);
        }
        $lt->banned = 0;
        $lt->banned_time = null;
        $lt->save();
    }

    public function removeSuspension($userId)
    {
        $lt = new UserLoginThrottle($this->getState());
        if(!$lt->loadByField('user_id', $userId)) {
            $this->setDefaultThrottleValues($lt, $userId);
        }
        $lt->suspended = 0;
        $lt->suspended_time = null;
        $lt->save();

        $this->deleteLoginAttempts($userId);
    }

    public function resetThrottle($userId)
    {
        $lt = new UserLoginThrottle($this->getState());
        if(!$lt->loadByField('user_id', $userId)) {
            $this->setDefaultThrottleValues($lt, $userId);
        }
        $lt->attempts = 0;
        $lt->suspended = 0;
        $lt->suspended_time = null;
        $lt->save();

        $this->deleteLoginAttempts($userId);
    }
    
    private function deleteLoginAttempts($userId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('la.*')
            ->from('sb__user_login_attempt', 'la')
            ->where('user_id = :user_id')
            ->setParameter('user_id', $userId)
            ->andWhere('lote_deleted is null');
        $rows = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);

        foreach ($rows as $attempt) {
            $la = new UserLoginAttempt($this->getState());
            $la->load($attempt['id']);
            $la->delete();
        }
    }

    private function setDefaultThrottleValues(UserLoginThrottle $lt, $userId)
    {
        $lt->user_id = $userId;
        $lt->suspended = false;
        $lt->banned = false;
    }

    public function getSuspension()
    {
        return $this->data['suspended'];
    }

    public function getSuspensionTime()
    {
        return $this->data['suspended_time'];
    }

    public function getBan()
    {
        return $this->data['banned'];
    }

    public function getBanTime()
    {
        return $this->data['banned_time'];
    }


}