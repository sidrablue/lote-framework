<?php

/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Auth;

use SidraBlue\Lote\Entity\Token;
use SidraBlue\Lote\Model\User;
use SidraBlue\Lote\Entity\UserSession;
use SidraBlue\Lote\Model\UserSession as UserSessionModel;
use SidraBlue\Lote\Model\UserSessionData;
use SidraBlue\Lote\Service\Jwt;
use SidraBlue\Lote\State\Web as WebState;
use SidraBlue\Lote\Auth\Login\Method;
use SidraBlue\Lote\Auth\Login\Credentials\Base as CredentialsBase;
use SidraBlue\Lote\Auth\Login\Credentials\Auto as CredentialsAuto;
use SidraBlue\Lote\Auth\Login\Credentials\Factory as CredentialsFactory;
use SidraBlue\Util\Time;


class Session
{

    /**
     * @var WebState $state
     */
    private $state;

    /**
     * @var Method\Base $state
     */
    private $method;

    /**
     * @var boolean $isNewLogin
     */
    private $isNewLogin;

    /**
     * @var CredentialsBase $credentials
     */
    private $credentials;

    /**
     * @var UserSessionData $userSessionData
     */
    private $userSessionData;

    public function __construct(WebState $state)
    {
        $this->state = $state;
        $this->userSession = new UserSession($state);
        $this->userSessionData = new UserSessionData($state);
    }

    /**
     * @param boolean $acceptInput - true if the login check is meant to account for a new login attempt
     * @return boolean
     * */
    public function login($acceptInput = false)
    {
        $m = new User($this->state);
        $this->isNewLogin = $acceptInput;
        $result = false;
        $this->method = Method\Factory::getInstance($this->state, '');
        if($this->credentials = CredentialsFactory::getInstance($this->state->getRequest(), $acceptInput)) {
            $this->method = Method\Factory::getInstance($this->state, 'lote', $this->credentials);
            if($this->method->login()) {
                $result = true;
            }
            elseif($this->credentials->getUsername() && !$m->usernameExists($this->credentials->getUsername()) &&
                $this->state->getSettings()->get('login.ldap', false)) {
                $this->method = Method\Factory::getInstance($this->state, 'ldap', $this->credentials);
                return $this->method->login();
            }
        }
        return $result;
    }

    /**
     * Generate a login token to use in future login requests
     * @param int|bool $userId - the ID of the user that this token is for
     * @param bool $linkToDb - true to create a corresponding DB entry for this token
     * @param int $expires - when this token expires
     * @return string
     * */
    public function generateToken($userId = false, $linkToDb = false, $expires = 28880)
    {
        $jwtService = new Jwt();
        $tokenJwtId = false;
        $tokenKey = $this->state->getSettings()->getSettingOrConfig("login.jwt.key",false);
        if(!$userId) {
            $userId = $this->state->getUser()->id;
        }
        if(false && $linkToDb) {
            $t = new Token($this->state);
            $now = Time::getUtcObject('now');
            $now->add(new \DateInterval('PT'.$expires.'S'));
            $t->expires = $now->format("Y-m-d H:i:s");
            $t->public_key = uniqid();
            $t->private_key = uniqid();
            $t->save();
            $tokenJwtId = $t->public_key;
            $tokenKey = $t->private_key;
            $t->reference_key = 'sb__user.login';
            $t->reference_id = $userId;
        }
        $token = $jwtService->generateTokenString($userId,
            $this->state->getUrl()->getBaseUrl(),
            $expires,
            $tokenKey,
            $tokenJwtId);
        return $token;
    }

    /**
     * Perform a login to the system as the specified user ID
     * @access public
     * @param int $userId - the users ID
     * @param int $masqueradeUserId - the user ID to log back in as, after the logout of hte current session
     * @param bool $twoFactorCompleted - true if two factor login has already been completed for this session
     * @return boolean - true if the login succeeded
     * */
    public function autoLogin($userId, $masqueradeUserId = 0, $twoFactorCompleted = false)
    {
        $result = false;
        $credentials = new CredentialsAuto($this->state->getRequest());
        $credentials->setUserId($userId);
        $credentials->setMasqueradeUserId($masqueradeUserId);
        $credentials->setTwoFactorCompleted($twoFactorCompleted);
        $this->method = Method\Factory::getInstance($this->state, 'auto', $credentials);
        if($this->method->login()) {
            $result = true;
        }
        return $result;
    }

    /**
     *
     * */
    public function passwordResetRequired() {
        $result = false;
        if($this->method) {
            $result = $this->method->passwordResetRequired();
        }
        return $result;
    }

    /**
     * @todo - the update password function has moved and needs to be updated.
     * */
    public function updateLoggedInPassword($password, &$errorMessage, $userData = false)
    {
        $result = false;
        if(!$userData) {
            $userData = $this->state->getUser()->getData();
        }
        if(isset($userData) && isset($userData['source']) && $userData['source']=='ldap') {
            $method = Method\Factory::getInstance($this->state, 'ldap', null);
            if($method->updatePassword($userData['username'], $password, $errorMessage)){
                $this->state->getUser()->updatePassword($password, $errorMessage, $userData['username']);
                $result = true;
            }
        }
        else {
            $errorMessage = 'Passwords can only be updated for LDAP users. This user is a local user.';
        }

        return $result;
    }

    /**
     * @return boolean
     * */
    public function logout()
    {
        $result = false;
        if($credentials = CredentialsFactory::getInstance($this->state->getRequest(), false)) {
            $this->method = Method\Factory::getInstance($this->state, 'lote', $credentials);
            if($this->method->logout()) {
                $result = true;
            }
        }
        return $result;
    }

    /**
     * @return string
     * */
    public function getLoginError()
    {
        return $this->method->getErrorMessage();
    }

    /**
     * @return int|string
     * */
    public function getLoginErrorCode()
    {
        return $this->method->getErrorCode();
    }

    /**
     * @return string
     * */
    public function getUsername()
    {
        return $this->method->getUsername();
    }

    /**
     * @return string
     * */
    public function getPassword()
    {
        return $this->method->getPassword();
    }

    /**
     * @return CredentialsBase
     * */
    public function getCredentials()
    {
        return $this->credentials;
    }

    public function getLoginSession()
    {
        $result = [];
        if($this->state->getUser()->id) {
            $s = new UserSessionModel($this->state);
            if($data = $s->getLogin($this->state->getUser()->id, $this->credentials->getIp(), 1, $this->credentials->getSessionId())) {
                $result = $data;
            }
        }
        return $result;
    }

    /**
     * @return boolean
     * */
    public function isNewLogin()
    {
        return $this->isNewLogin;
    }

    /**
     * @return UserSessionData
     * */
    public function getUserSessionData()
    {
        return $this->userSessionData;
    }

}
