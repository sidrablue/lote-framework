<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Bootstrap;

use SidraBlue\Lote\State;
use SidraBlue\Lote\Routing\Route;

class Base
{

    /**
     * @var \SidraBlue\Lote\State\Web
     */
    private $state;

    /**
     * Default constructor for the Bootstrap object
     * @access public
     * @param null $state
     */
    public function __construct($state = null)
    {
        $this->state = $state;
        if (!$this->state) {
            $this->state = State\Factory::newInstance();
        }
        if(php_sapi_name()!='cli') {
            $this->setupCoreRoutes();
            if(!$this->getCoreStaticRoute()) {
                $this->getState()->getApps()->setupRoutes($this->getState()->getRouter());
            }
        }
    }

    /**
     * Check if the route is a core static one that does not require the loading of the app routes to deduce it.
     *
     * This is used to prevent an un-needed database connection
     * @access private
     * @return array|false
     * */
    protected function getCoreStaticRoute()
    {
        $result = false;
        $match = $this->getState()->hasStaticMatch($this->getState()->getRequestUrl());
        if($match && isset($match['_static']) && $match['_static'] == true) {
            $result = $match;
        }
        return $result;
    }

    /**
     * Setup the core routes of the system
     * */
    private function setupCoreRoutes()
    {
        $r = $this->getState()->getRouter();
        $defaults = ['_handler' => ['system' => 'core']];
        $defaults['controller'] = 'SidraBlue\Lote\Controller\Core\Image';
        $defaults['_method'] = 'renderImageById';
        $defaults['_static'] = true;
        $route = new Route('/_img/{id}', $defaults, ['id' => '([0-9]+)']);
        $r->add("Image View and Thumbnail", $route);

        /** @see \SidraBlue\Lote\Controller\Core\Image::renderImageByReference() */
        $defaults['_method'] = 'renderImageByReference';
        $route = new Route('/_image/{reference}', $defaults, ['reference' => '.*']);
        $r->add("Image View and Thumbnail By Reference", $route);

        /** @see \SidraBlue\Lote\Controller\Core\Image::renderImageByReference */
        $defaults['_method'] = 'renderImageByReference';
        $route = new Route('/_icdn/{timestamp}/{reference}', $defaults, ['reference' => '.*', 'timestamp' => '(\d+)']);
        $r->add("core.image.thumbnail.view_by_cdn_url", $route);

        $defaults = ['_handler' => ['system' => 'website']];

        $defaults['controller'] = 'SidraBlue\Lote\Controller\Core\File';

        /** @see \SidraBlue\Lote\Controller\Core\File::download */
        $defaults['_method'] = 'download';
        $route = new Route('_d/{id}', $defaults);
        $r->add('core.download.by_id', $route);

        /** @see \SidraBlue\Lote\Controller\Core\File::downloadByReference */
        $defaults['_method'] = 'downloadByReference';
        $route = new Route('_file/{reference}', $defaults, ['reference' => '.*']);
        $r->add('core.download.by_reference', $route);

        /** @see \SidraBlue\Lote\Controller\Core\File::attachmentDownloadByReference */
        $defaults['_method'] = 'attachmentDownloadByReference';
        $route = new Route('_download/{reference}', $defaults, ['reference' => '.*']);
        $r->add('core.download.by_reference_attachment', $route);

        unset($defaults['_static']);

        $requirements = ['format' => '(\.[a-z]+)?'];
        $defaults = ['controller' => 'Lote\System\Website\Controller\Util', '_method' => 'ping'];
        $route = new Route('_util/keep-alive{format}', $defaults, $requirements);
        $r->add("core.keep_alive", $route);

        $defaults['controller'] = 'SidraBlue\Lote\Controller\Login';
        $requirements = ['login_hash' => '([a-zA-Z0-9]+)', 'user_hash' => '([a-zA-Z0-9]+)'];
        $defaults['_method'] = 'autoLogin';
        $route = new Route('/_l/{login_hash}/{user_hash}', $defaults, $requirements);
        $r->add(uniqid('Auto Login Redirect'), $route);

        /** @see \SidraBlue\Lote\Controller\Access\Url\Manage::urlAccess() */
        $defaults['controller'] = 'SidraBlue\Lote\Controller\Access\Url\Manage';
        $requirements = ['encoded_url' => '(.+)'];
        $defaults['_method'] = 'urlAccess';
        $route = new Route('/_access/password-restricted/{encoded_url}', $defaults, $requirements);
        $r->add(uniqid('Url Access Verification'), $route);

        /** @see \SidraBlue\Lote\Controller\Access\Url\Form\Password */
        $defaults['_method'] = 'validate';
        $defaults['controller'] = 'SidraBlue\Lote\Controller\Access\Url\Form\Password';
        $route = new Route('/_access/password-validate{format}', $defaults, ['format' => '(\.[a-z]+)?']);
        $r->add(uniqid('Url Access Verification Password Check'), $route);
    }

    /**
     * Get the state of this bootstrap
     * @access public
     * @return \SidraBlue\Lote\State\Web
     * */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Determine if the current user has access to the specified context
     * @param array $route
     * @return boolean
     */
    protected function hasAccess($route)
    {
        $result = true;
        if (!$this->getState()->getUser()->isAdmin() && isset($route['_context']) && isset($route['_context']['right']) && isset($route['_context']['path'])) {
            $path = $route['_context']['path'];
            $right = $route['_context']['right'];
            $result = $this->getState()->getAccess()->hasAccess($path, $right);
        } elseif (!$this->getState()->getUser()->isAdmin() && isset($route['_context_array']) && is_array($route['_context_array'])) {
            foreach ($route['_context_array'] as $context) {
                $path = $context['path'];
                $right = $context['right'];
                if ($result = $this->getState()->getAccess()->hasAccess($path, $right)) {
                    break;
                }
            }
        } elseif(!$this->getState()->getUser()->isAdmin() && isset($route['_context_function']) && is_array($route['_context_function']) ) {
            $result = false;
            $class = $route['_context_function']['class'];
            $function = $route['_context_function']['function'];
            $params = isset($route['_context_function']['params'])?$route['_context_function']['params']:[];

            $reflectedClass = new \ReflectionClass($route['_context_function']['class']);
            if($reflectedClass->isInstantiable()) {
                $object = new $class($this->getState());
                if(is_callable([$object, $function])) {
                    $result = $object->$function($params);
                }
            }
        }
        return $result;
    }

}
