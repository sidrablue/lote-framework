<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Bootstrap;

use SidraBlue\Lote\Controller\Api as ApiController;
use SidraBlue\Lote\Controller\Request as RequestController;
use SidraBlue\Lote\Model\SiteSkin as SkinModel;
use SidraBlue\Lote\Model\UrlPassword as UrlPasswordModel;
use SidraBlue\Lote\View\Transform\Html\Base as HtmlViewBase;
use SidraBlue\Util\Strings;
use Symfony\Component\HttpFoundation\RedirectResponse;


class Web extends Base
{

    /**
     * Get the state of this bootstrap
     * @access protected
     * @return \SidraBlue\Lote\State\Web
     * */
    public function getState()
    {
        return parent::getState();
    }

    /**
     * Get the controller for a requested based on the provided route data
     * @param array $route
     * @param \SidraBlue\Lote\State\Web $state
     * @return Base
     */
    private function getController($route, $state)
    {
        if (isset($route['_is_api']) && $route['_is_api']) {
            $c = new ApiController($this->getState());
            if ($controllerClass = $c->getController($route)) {
                $controller = new $controllerClass($state);
            } else {
                $controller = new ApiController($state);
            }
        } else if (isset($route['controller']) && class_exists('' . $route['controller'])) {
            $controller = new $route['controller']($state);
        } else {
            if (Strings::startsWith("_api/", $this->getState()->getRequest()->getUri())) {
                $controller = new ApiController($state);
            } else {
                $controller = new RequestController($state);
            }
        }
        return $controller;
    }

    /**
     * @todo - find a more elegant way to do this
     * */
    private function setupUser()
    {
        $this->getState()->getLogin()->login(false);
    }

    public function execute()
    {
        if ($proxies = $this->getState()->getConfig()->get("security.trusted_proxies", false)) {
            $this->getState()->getRequest()->setTrustedProxies(explode(",", $proxies));
        }

        $this->getState()->getSignal();

        $this->getState()->getLoggers()->getAccountLogger("base")->info("Begin Bootstrap Web Execution");
        if ($route = $this->getCoreStaticRoute()) {
            $this->executeMatchedCoreStaticRoute($route);
        } elseif ($route = $this->getState()->getRoute()) {
            if ($this->checkDynamicRoutePassword($route)) {
                $this->executeMatchedRoute($route);
            }
        } else {
            $this->executeNoMatch();
        }
    }

    private function executeMatchedCoreStaticRoute($route)
    {
        if ($controller = $this->getController($route, null)) {
            /** @var \SidraBlue\Lote\Controller\Core\Base $controller */
            $controller->setState($this->getState());
            if ($controller->requiresFullFramework($route)) {
                $this->executeMatchedRoute($route);
            } else {
                $controller->runLite($route);
                $this->getState()->getResponse()->send();
                $this->getState()->getResponse()->closeOutputBuffers(0, true);
            }
        }
    }

    private function checkDynamicRoutePassword($route)
    {
        $result = true;
        if (isset($route['_dynamic']) && isset($route['_passwords']) && count($route['_passwords']) > 0) {
            $bag = $this->getState()->getRequest()->getSession()->get('route/password', []);
            $uri = trim($this->getState()->getRequest()->getPathInfo(), '/');
            if (!isset($bag[$uri]) || !array_intersect($bag[$uri], $route['_passwords'])) {
                $response = new RedirectResponse($this->getState()->getRequest()->getSchemeAndHttpHost() . "/_access/password-restricted/" . base64_encode($uri));
                $response->send();
            }
        }
        return $result;
    }

    /**
     * Execute a reply where a route was matched
     * @access private
     * @param array $route - the route definition
     * @return void
     * */
    private function executeMatchedRoute($route)
    {
        $this->setupUser();
        $this->getState()->getLoggers()->getAccountLogger("base")->info('Route matched', $route);
        //check permissions
        if ($this->hasAccess($route)) {
            /** @var \SidraBlue\Lote\Controller\Base $controller */
            if ($controller = $this->getController($route, $this->getState())) {
                $this->getState()->getLoggers()->getAccountLogger("base")->info("Controller Found", [get_class($controller)]);
                $controller->run($route);
                $this->getState()->getResponse()->send();
                $this->getState()->getResponse()->closeOutputBuffers(0, true);
            } else {
                $this->getState()->getLoggers()->getAccountLogger("base")->notice("No valid Controller", [$route]);
            }
        } else {
            if ($this->getState()->getUser()->id > 0) {
                $this->getState()->getLoggers()->getAccountLogger("base")->notice("No permissions for context", [$route]);
                $this->getState()->getView()->accessDenied();
                //$this->getState()->getView()->accessDenied(); @todo An access denied function
            } else {
                $this->getState()->getLoggers()->getAccountLogger("base")->notice("Login required to access resource", [$route]);
                $this->getState()->getView()->loginRequired();
            }
        }
    }

    /**
     * Execute a reply where no matched route was found
     * @access private
     * @return void
     * */
    private function executeNoMatch()
    {
        $this->setupUser();
        $this->getState()->getLoggers()->getAccountLogger("base")->notice("No valid route", ['uri' => $this->getState()->getRequest()->getRequestUri()]);
        $this->getState()->getSignal()->send($this, 'error.404');
        //Assertion : No additional function has overwritten the 404
        $this->set404Handler($this->getState()->getRequest()->getRequestUri());
        $system = $this->getState()->getSystemByUrl($this->getState()->getRequest()->getRequestUri());
        if (!$this->getState()->getContext()->isRestricted($system) || $this->getState()->getUser()->id) {
            if (Strings::startsWith("/_api/", $this->getState()->getRequest()->getRequestUri())) {
                $controller = new ApiController($this->getState());
                $controller->run($this->getState()->getRoute());
            } else {
                //Load default skin and apply it
                $s = new SkinModel($this->getState());
                $default_skin = $s->getDefaultSkin();
                if (isset($default_skin[0]['id'])) {
                    $this->getState()->getSettings()->setSkinId($default_skin[0]['id']);
                    $this->getState()->getView()->setConfigVar('_skin', $default_skin[0]['skin']);
                }
                $this->getState()->getView()->setRenderFile('_error/404');
                $this->getState()->getView()->setStatusCode(404);
                $this->getState()->getResponse()->setStatusCode("404");
            }
            $this->getState()->getSignal()->send($this, 'after.action');
            $this->getState()->getSignal()->send($this, 'before.render');
            $this->getState()->getResponse()->setContent($this->getState()->getView()->render());
            $this->getState()->getResponse()->send();
        } else {
            $this->getState()->getView()->loginRequired();
            $this->getState()->getResponse()->closeOutputBuffers(0, true);
        }
    }

    private function set404Handler($url)
    {
        if ($this->getState()->getView() instanceof HtmlViewBase) {
            do {
                $url = substr($url, 0, strrpos($url, '/'));
                if ($match = $this->getState()->hasStaticMatch($url)) {
                    if (isset($match['_handler']['system'])) {
                        $this->getState()->getView()->setConfigVar('_route_handler', $match['_handler']);
                        break;
                    }
                }
            } while ($url);
        }
    }


    /**
     * Echoes an exception and all its previous exceptions.
     * @param \Exception $e The exception to echo.
     * @return void
     */
    protected function echoException(\Exception $e = null)
    {
        if ($e) {
            echo $e . PHP_EOL;
            $this->echoException($e->getPrevious());
        }
    }

}
