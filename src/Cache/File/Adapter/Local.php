<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Cache\File\Adapter;

use SidraBlue\Lote\Cache\File\Base;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local as LocalAdapter;
use SidraBlue\Util\Dir;

/**
 * Base controller for Core Controllers
 * @package SidraBlue\Lote\Controller\Core
 * */
class Local extends Base
{

    /**
     * @return \League\Flysystem\Adapter\AbstractAdapter
     * */
    protected function getAdapter()
    {
        return new LocalAdapter(LOTE_ASSET_PATH . 'cache/' . $this->getState()->accountReference . '/media/thumb/');
    }

}
