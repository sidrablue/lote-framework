<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Cache\File\Adapter;

use League\Flysystem\Adapter\NullAdapter as NullFlysystemAdapter;
use SidraBlue\Lote\Cache\File\Base;

/**
 * Base controller for Core Controllers
 * @package SidraBlue\Lote\Controller\Core
 * */
class NullAdapter extends Base
{

    /**
     * @return \League\Flysystem\Adapter\AbstractAdapter
     * */
    protected function getAdapter()
    {
        return new NullFlysystemAdapter();
    }

    /**
     * Check if an image has already been cached
     * @access public
     * @param string $reference
     * @param boolean $autoCache
     * @param int $width
     * @param int $height
     * @param int $size
     * @param int $timestamp
     * @param string $mode
     * @return boolean
     * */
    public function imageIsCached($reference, $autoCache = false, $width = 0, $height = 0, $size = 0, $timestamp = 0, $mode = '')
    {
        return false;
    }

}
