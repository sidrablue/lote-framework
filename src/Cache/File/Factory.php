<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Cache\File;

/**
 * Base controller for Core Controllers
 * @package SidraBlue\Lote\Controller\Core
 * */
class Factory
{

    /**
     * Create an instance of field data
     * @param $state \SidraBlue\Lote\State\Base
     * @param string $reference
     * @return \League\Flysystem\Filesystem
     * */
    public static function createInstance($state, $reference = "")
    {
        if(!$reference) {
            $reference = $state->getSettings()->getConfigOrSetting("media.cache.adapter", 'local');
        }
        $referenceClass = __NAMESPACE__ . '\Adapter\\'.ucfirst($reference);
        if (class_exists($referenceClass)) {
            $className = $referenceClass;
        }
        else {
            $className = __NAMESPACE__ . '\Adapter\Local';
        }
        /**
         * @var \SidraBlue\Lote\Cache\File\Base $adapterClass
         * */
        return new $className($state);
    }

}
