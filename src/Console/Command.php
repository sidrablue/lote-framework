<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Console;

use SidraBlue\Lote\Interfaces\StateInterface;
use SidraBlue\Lote\State\Base as BaseState;
use Symfony\Component\Console\Command\Command as SymfonyCommand;

class Command extends SymfonyCommand implements StateInterface
{

    /** @var BaseState $state */
    private $state;

    /**
     * Set the state object
     * @access public
     * @param BaseState $state
     * @return void
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * Get the state object
     * @access public
     * @return BaseState
     */
    public function getState()
    {
        return $this->state;
    }

}
