<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Console\Command\Cache;

use SidraBlue\Lote\Console\Command;
use SidraBlue\Util\Dir;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Base class for all entities
 */
class ClearTwig extends Command
{

    protected function configure()
    {
        $this->setName('core:clear-twig-cache')
            ->setDescription('Clear the Twig cache')
            ->setHelp('This command clears the twig cache for all accounts in this system...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $baseDir = LOTE_BASE_PATH . "/core/asset/cache/";
        $directories = glob($baseDir . '*', GLOB_ONLYDIR);
        foreach ($directories as $d) {
            $twigCacheDir = $d . '/twig';
            $twigCacheDir = realpath($twigCacheDir);
            if ( $twigCacheDir && is_dir($twigCacheDir)) {
                Dir::delete($twigCacheDir);
                $output->writeln(["Cleared cache for " . basename($d)]);
            }
        }
    }

}
