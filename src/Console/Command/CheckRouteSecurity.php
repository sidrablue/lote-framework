<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Console\Command;

use SidraBlue\Lote\Console\Command;
use SidraBlue\Lote\State\Cli;
use SidraBlue\Lote\State\Web;
use SidraBlue\Util\Dir;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Base class for all entities
 */
class CheckRouteSecurity extends Command
{

    protected function configure()
    {
        $this->setName('core:check-route-security')
            ->setDescription('Find insecure admin routes')
            ->setHelp('This command clears the twig cache for all accounts in this system...')
            ->addArgument('account', InputArgument::REQUIRED, 'Please specify the account reference');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $unrestrictedContexts = $unrestrictedPaths = [];
        $io = new SymfonyStyle($input, $output);
        $reference = $input->getArgument("account");
        $s = new Web($reference);
        $s->getApps()->setupRoutes($s->getRouter());
        foreach ($s->getRouter()->all() as $r) {
            /** @var SidraBlue\Lote\Routing\Route $r */
            if (strpos($r->getPath(), '/_admin') === 0) {
                $defaults = $r->getDefaults();
                if (isset($defaults['_context']) && isset($defaults['_context']['path']) && !isset($defaults['_context_array'])) {
                    if (!$s->getContext()->isRestricted($defaults['_context']['path'])) {
                        $unrestrictedContexts[] = [$r->getPath(), $defaults['_context']['path']];
                    }
                } elseif (isset($defaults['_context_array']) && is_array($defaults['_context_array'])) {
                    $foundRestrictedContextPath = false;
                    $contextArrayPaths = [];
                    foreach($defaults['_context_array'] as $arr) {
                        if(isset($arr['path']) && $s->getContext()->isRestricted($arr['path'])) {
                            $foundRestrictedContextPath = true;
                        }
                        $contextArrayPaths[] = $arr['path'];
                    }
                    if(!$foundRestrictedContextPath) {
                        $unrestrictedContexts[] = [$r->getPath(), implode("\n", $contextArrayPaths)];
                    }
                } elseif (!isset($defaults['_context_function'])) {
                    $unrestrictedPaths[] = [$r->getPath()];
                }
            }
        }
        if ($unrestrictedContexts) {
            $io->warning("Unresricted Contexts");
            $io->table(['Path', 'Context'], $unrestrictedContexts);
        }
        if ($unrestrictedPaths) {
            $io->warning("Admin path with no restriction");
            $io->table(["Path"], $unrestrictedPaths);
        }
    }

}
