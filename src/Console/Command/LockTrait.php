<?php

namespace SidraBlue\Lote\Console\Command;

use SidraBlue\Lote\Registry\Config;
use Symfony\Component\Lock\Store\RedisStore;
use Symfony\Component\Lock\Factory;

trait LockTrait
{

    private $lock;

    public function lock(Config $config)
    {
        $host = $config->get("queue.lock.redis.host");
        $port = $config->get("queue.lock.redis.port");
        $store = new RedisStore(new \Predis\Client("tcp://{$host}:{$port}"));
        $factory = new Factory($store);
        $this->lock = $factory->createLock('cron');
        return $this->lock->acquire();
    }

    public function release()
    {
        $this->lock->release();
    }

}
