<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Controller\Access\Url\Form;

use SidraBlue\Lote\Controller\Form\Standard as FormBase;
use SidraBlue\Lote\Entity\Url;
use SidraBlue\Lote\Model\UrlPassword;
use SidraBlue\Util\Input\Filter;

/**
 * Class for Custom Object Field Groups
 */
class Password extends FormBase
{

    /**
     * @var array $formFields - Recorded fields for a user
     * */
    protected $formFields = ['url', 'object_id', 'object_ref', 'url_password'];

    /**
     * Setup the form filter for validating a user
     *
     * @access protected
     * @param Filter $filter
     * @return void
     */
    protected function setupEditForm(Filter $filter)
    {
        $filter->setRule(
            'url_password',
            'Please specify a password',
            function ($value) {
                $return = false;
                if (!empty($value)) {
                    if (trim($value) == $value) {
                        $return = true;
                    }
                }
                return $return;
            }
        );
        $filter->setRule(
            'url_password',
            'Invalid Password',
            function ($value, $fields) {
                $m = new UrlPassword($this->getState());
                if(!$fields->object_ref) {
                    $result = $m->isValidPassword($fields->url, $value);
                }
                else {
                    $result = $m->isValidObjectPassword($fields->object_ref, $fields->object_id, $value);
                }
                return $result;
            }
        );
        $filter->setRule(
            'url',
            'Invalid Url',
            function ($value, $fields) {
                if(!$fields->object_ref) {
                    $e = new Url($this->getState());
                    $result = $e->loadByField('uri', $value);
                }
                else {
                    $result = $this->getState()->hasStaticMatch($value) || ($fields->object_id && $fields->object_ref);
                }
                return $result;
            }
        );
    }

    /**
     * Function used to save
     *
     * @access protected
     * @param array $data
     * @return bool|int
     */
    protected function save($data, $additionalData = null)
    {
        if($data['object_ref']) {
            $bagName = "route/password/{$data['object_ref']}/{$data['object_id']}";
            $bag = $this->getState()->getRequest()->getSession()->get($bagName, []);
            $bag[] = $data['url_password'];
            $bag = array_unique($bag);
            $this->getState()->getRequest()->getSession()->set($bagName, $bag);
        }
        else {
            $uri = trim($data['url'], '/');
            $bag = $this->getState()->getRequest()->getSession()->get('route/password', []);
            $bag[$uri][] = $data['url_password'];
            $this->getState()->getRequest()->getSession()->set('route/password', $bag);
        }
        return true;
    }

}
