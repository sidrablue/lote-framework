<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Controller\Core;

use SidraBlue\Lote\Controller\Rest;

/**
 * Base controller for Core Controllers
 * @package SidraBlue\Lote\Controller\Core
 * */
abstract class Base extends Rest
{

    /**
     * Abstract function which must be implemented by the child to determine whether or not the current request
     * can be met without requiring the full framework state to be loaded
     * @access public
     * @param array $route - the static route
     * @return boolean
     * */
   abstract public function requiresFullFramework($route);

}
