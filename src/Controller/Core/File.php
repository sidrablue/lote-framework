<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Controller\Core;

use SidraBlue\Lote\Controller\Rest;
use SidraBlue\Lote\Model\File as FileModel;

class File extends Rest
{

    protected function download()
    {
        $id = $this->getVar('id');
        $f = new FileModel($this->getState());
        $f->download($id);
        //should not get to here
    }

    /**
     * Download a file by reference, using the default rendering method of "inline"
     * @access protected
     * @return void
     * */
    protected function downloadByReference()
    {
        $reference = $this->getVar('reference');
        $f = new FileModel($this->getState());
        $f->downloadByReference($reference);
    }

    /**
     * Force a file to be downloaded as an attachment, attempting to prevent rendering in the browser
     * @access protected
     * @return void
     * */
    protected function attachmentDownloadByReference()
    {
        $reference = $this->getVar('reference');
        $f = new FileModel($this->getState());
        $f->downloadByReference($reference,"attachment");
    }

    /**
     * Open a file in the browser forcing inline, allowing the browser to choose if it will download it or render it
     * @access protected
     * @return void
     * */
    protected function inlineDownloadByReference()
    {
        $reference = $this->getVar('reference');
        $f = new FileModel($this->getState());
        $f->downloadByReference($reference,"inline");
    }

}
