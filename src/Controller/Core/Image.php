<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Controller\Core;

use SidraBlue\Lote\Entity\File;
use SidraBlue\Lote\Event\Data;
use SidraBlue\Lote\Service\Thumb;
use SidraBlue\Util\Strings;
use SidraBlue\Util\Time;

/**
 * Image view controller of the framework
 * @package SidraBlue\Lote\Controller\Core
 * */
class Image extends Base
{

    /**
     * Render an image by its ID
     *
     * @access protected
     * @return void
     * */
    protected function renderImageById()
    {
        $e = new File($this->getState());
        $e->load($this->getVar('id'));
        $this->renderImageByEntity($e);
    }

    /**
     * Render an image from a given reference
     *
     * @access protected
     * @return void
     * */
    protected function renderImageByReference()
    {
        $keys = $this->getCacheKeys();
        $e = new File($this->getState());
        if ($this->isCached($keys)) {
            $this->renderFromCache($keys);
        } else {
            $e = new File($this->getState());
            if ($e->loadByField('location_reference', $this->getReference())) {
                $data = ['file_id' => $e->id];
                $eventData = new Data($data);
                $this->getState()->getEventManager()->dispatch('file.download.has_access', $eventData);
                if ($eventData->getData()['has_access'] === false) {
                    die();
                }
                $this->renderImageByEntity($e);
            } elseif ($e->loadByField('reference', $this->getReference()) && $e->reference != $e->location_reference) {
                $data = ['file_id' => $e->id];
                $eventData = new Data($data);
                $this->getState()->getEventManager()->dispatch('file.download.has_access', $eventData);
                if ($eventData->getData()['has_access'] === false) {
                    die();
                }
                $this->renderImageByEntity($e);
            } else {
                $t = new Thumb($this->getState());
                $t->displayBlankImage();
            }
        }

    }

    /**
     * Output an image by its enity
     *
     * @access private
     * @param File $e - the file entity to render from
     * @return void
     * */
    private function renderImageByEntity(File $e)
    {
        if ($e->file_type == "image" && !$this->isPrivateAndInaccessible($e)) {
            $this->getState()->getEventManager()->dispatch('core.image.render', new Data(['entity' => $e]));
            $t = new Thumb($this->getState(), $e);
            $t->setWidth($this->getWidth());
            $t->setHeight($this->getHeight());
            $t->setSize($this->getSize());
            $t->setAlgorithm($this->getAlgorithm());
            $t->render();
        } else {
            $t = new Thumb($this->getState());
            $t->displayBlankImage();
        }
        die;
    }

    /**
     * Check if a file is private and inaccessible due to the user nor being logged in
     *
     * @access private
     * @param File $e - the file entity to check for
     * @return boolean
     * */
    private function isPrivateAndInaccessible(File $e)
    {
        $result = false;
        if (!$e->is_public && $e->lote_access != null && $e->lote_access != 1 && !$this->getState()->getUser()->id) {
            $result = true;
        }
        return $result;
    }

    /**
     * Render a file from the cache
     *
     * @access private
     * @param array $keys
     * @return void
     * */
    private function renderFromCache($keys)
    {
        if ($this->getState()->getFileCache()->has($keys)) {
            $content = $this->getState()->getFileCache()->get($keys);
            $this->getLoteUpdatedTimestamp($this->getReference());
            if ($this->getTimestamp()) {
                $eTagFile = md5($this->getReference() . json_encode($keys));
                $eTagHeader = (isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false);

                header("Etag: $eTagFile");

                $loteUpdated = $this->getLoteUpdatedTimestamp($this->getReference());
                //get the last-modified-date of this very file
                if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $loteUpdated || $eTagHeader == $eTagFile) {
                    header("HTTP/1.1 304 Not Modified");
                    header('Content-Type:image');
                    header("Last-Modified: " . gmdate("D, d M Y H:i:s", $loteUpdated));
                    die;
                } else {
                    header('Content-Type:image');
                    header("Last-Modified: " . gmdate("D, d M Y H:i:s", $loteUpdated));
                    header("Cache-Control: private, max-age=604800");
                    header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 604800) . ' GMT');
                }

            } else {
                header('Content-Type:image');
                header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
                header("Cache-Control: post-check=0, pre-check=0", false);
                header("Pragma: no-cache");
            }
            header('Content-Length: ' . strlen($content));
            echo($content);
        }
        die;
    }

    /**
     * Get the time that a file was last updated
     *
     * @access private
     * @param string $fileRef - the reference of the file in question
     * @return string|int
     * */
    private function getLoteUpdatedTimestamp($fileRef)
    {
        $result = false;
        if ($this->getState()->getRedisDbClient()->exists("filecache:media.file." . Strings::generateHtmlId($fileRef))) {
            $result = $this->getState()->getRedisDbClient()->get("filecache:media.file." . Strings::generateHtmlId($fileRef));
        } else {
            $e = new File($this->getState());
            if ($e->loadByField('location_reference', $this->getReference())) {
                $result = Time::dateToTimestamp($e->lote_updated);
                $this->getState()->getRedisDbClient()->set("filecache:media.file." . Strings::generateHtmlId($fileRef),
                    $result);
            }
        }
        return $result;
    }

    /**
     * Get the keys to use in accessing the cache for this image
     *
     * @access private
     * @param bool $isPrivate
     * @return array
     * */
    private function getCacheKeys($isPrivate = false)
    {
        return $this->getState()->getFileCache()->getKeyArray($isPrivate, $this->getReference(), $this->getWidth(),
            $this->getHeight(), $this->getSize());
    }

    /**
     * Get the reference to use for determining the image to be rendered
     *
     * @access private
     * @return string
     * */
    private function getReference()
    {
        return strtok($this->getVar('reference'), '?');
    }

    /**
     * Get the width to use for the current image thumbnail
     *
     * @access private
     * @return int
     * */
    private function getWidth()
    {
        $result = $this->getVar('Width', 0);
        if (!$result) {
            $result = $this->getVar('width', 0);
        }
        return $result;
    }

    /**
     * Get the height to use for the current image thumbnail
     *
     * @access private
     * @return int
     * */
    private function getHeight()
    {
        $result = $this->getVar('Height', 0);
        if (!$result) {
            $result = $this->getVar('height', 0);
        }
        return $result;
    }

    /**
     * Get the timestamp to use for showing the current image
     *
     * @access private
     * @return string|bool
     * */
    private function getTimestamp()
    {
        $result = $this->getVar('ts', false);
        if (!$result) {
            $result = $this->getVar('timestamp', false);
        }
        return $result;
    }

    /**
     * Get the size to use for the current image thumbnail
     *
     * @access private
     * @return int
     * */
    private function getSize()
    {
        $result = $this->getVar('size', 0);
        if (!$result) {
            $result = $this->getVar('Size', 0);
        }
        return $result;
    }

    /**
     * Get the algorithm to use for resizing the image if required
     *
     * @access private
     * @return string|bool
     * */
    private function getAlgorithm()
    {
        return $this->getVar('algorithm', 'auto');
    }

    /**
     * Determine whether or not the current request can be met without requiring the full framework state to be loaded
     *
     * @access public
     * @param array $route - the static route
     * @return boolean
     * */
    public function requiresFullFramework($route)
    {
        return !$route['_method'] || $route['_method'] != 'renderImageByReference' || !$this->isCached($this->getCacheKeys(false));
    }

    /**
     * Check if the current image is cached, which would only occur if it is public
     *
     * @access private
     * @param array $keys
     * @return bool
     * */
    private function isCached($keys)
    {
        return $this->getState()->getFileCache()->has($keys);
    }

}
