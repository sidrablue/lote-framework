<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Controller\Core;;

use SidraBlue\Lote\Controller\Rest;

class Util extends Rest
{

    protected function ping(){
        $this->getState()->getView()->success = true;
        $this->getState()->getView()->user = ['id' => $this->getState()->getUser()->id];
    }

}