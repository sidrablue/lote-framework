<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Controller;

use SidraBlue\Lote\Entity\Dispatch as DispatchEntity;
use SidraBlue\Lote\Model\Dispatch as DispatchModel;
use SidraBlue\Util\Time;

/**
 * Base form controller class
 * */
class Dispatcher extends Base
{

    /**
     *
     * */
    function processAction()
    {
        $m = new DispatchModel($this->getState());
        if(!$m->isProcessing(true)) {
            $e = new DispatchEntity($this->getState());
            $e->process_id = getmypid();
            $e->status = 'processing';
            $e->save();
            $this->process($e);
            $e->status = 'completed';
            $e->completed = Time::getUtcNow();
            $e->save();
        }
    }

    function process(DispatchEntity $e)
    {
        $this->getState()->getSignal()->send($this, 'dispatch.before');
        $this->getState()->getSignal()->send($this, 'dispatch.event', $e);
        $this->getState()->getSignal()->send($this, 'dispatch.after');
    }

}
