<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Controller\Form;

use SidraBlue\Lote\Object\Model;
use SidraBlue\Lote\Entity;
use SidraBlue\Lote\File\Upload\Validator;
use SidraBlue\Lote\Controller\Rest;

/**
 * Base form controller class
 * */
class FileUpload extends Rest
{

    /**
     * @var int $id - the id of the file in the sb__file table
     * */
    protected $id;

    /**
     * @var string $fileReference - the reference of the file in the $_FILES array
     * */
    protected $fileReference;

    /**
     * @var Validator $validator - the file validator to use
     * */
    protected $validator;

    /**
     * On create handler, to create a validator
     * @access protected
     * @return void
     * */
    protected function onCreate()
    {
        parent::onCreate();
        $this->validator = new Validator($this->fileReference, true);
    }

    /**
     * Generic validation function for the class
     * @access protected
     * @return void
     * */
    protected function validate()
    {
        $this->getState()->getView()->success = false;
        if ($this->validateFile()) {
            if ($this->id = $this->save()) {
                $this->getState()->getView()->success = true;
                $this->getState()->getView()->id = $this->id;
            } else {
                $this->getState()->getView()->error = 'Could not complete form save';
            }
        } else {
            $this->getState()->getView()->errors = $this->validator->getValidationErrors();
        }
    }

    /**
     * Validate the uploaded file
     * @access protected
     * @return boolean
     */
    protected function validateFile()
    {
        return false;
    }

    /**
     * Save the file to the file store, and return its file ID
     * @access protected
     * @return int - the file_id
     * */
    protected function save()
    {

    }

}
