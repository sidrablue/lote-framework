<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Controller\Install;

use Doctrine\DBAL\Schema\Schema;
use SidraBlue\Lote\Application\App\Finder;
use SidraBlue\Lote\Application\Config;
use SidraBlue\Lote\Controller\Rest;
use SidraBlue\Lote\Entity\App;

define('LOTE_VERSION_PATH', LOTE_CORE_PATH . 'res/release/');

/**
 * Image view controller of the framework
 * @package SidraBlue\Lote\Controller\Core
 * */
class Update extends Rest
{

    protected function runHotfix()
    {
        global $loader;
        $loader->addPsr4("Lote\\Script\\Hotfix\\", LOTE_CORE_PATH.'../scripts/hotfix/');
        $route = $this->getState()->getRoute();
        if(isset($route['fix'])) {
            $class = 'Lote\Script\Hotfix\\'.$route['fix'];
            if(class_exists($class)) {
                $updateObject = new $class($this->getState());
                if (method_exists($updateObject, 'run')) {
                    $updateObject->run();
                    echo("Fix finished for {$this->getState()->getReadDb()->getDatabase()}\n");
                }
                else {
                    echo("Could not find method 'run'\n");
                }
            }
            else {
                echo("Could not find class {$route['fix']}\n");
            }
        }
        else {
            echo("No fix specified\n");
        }
    }

    protected function check()
    {
        $this->sync(true);
    }

    protected function update()
    {
        $this->sync(false);
    }

    protected function sync($simulate = true)
    {
        if ($this->getState()->isInstalled()) {
            $messages = [];
            $db = $this->getState()->getWriteDb();
            $schemaTemplate = $this->buildUpdateSchema($this->getState()->getApps()->hasSystem("master"));
            $currentSchema = $db->getSchemaManager()->createSchema();
            $migration = $currentSchema->getMigrateToSql($schemaTemplate, $db->getDatabasePlatform());
            if (is_array($migration) && count($migration) > 0) {
                foreach ($migration as $sql) {
                    if ($simulate) {
                        echo($sql . "\n");
                    } else {
                        $db->exec($sql);
                    }
                }
                if($simulate) {
                    $messages[] = "[Warning] DB Schema updates required";
                }
                else {
                    $messages[] = 'DB Schema updated';
                }
            } else {
                $messages[] = "System schema is up to date";
            }
            $messages = array_merge($messages, $this->syncSettings($simulate));

            foreach($messages as $m) {
                echo("\n$m");
            }
            echo("\n");
        } else {
            die('system is not installed');
        }
    }

    private function syncSettings($simulate = true)
    {
        $messages = [];
        $schemaManager = $this->getState()->getWriteDb()->getSchemaManager();
        if($schemaManager->tablesExist("sb__setting")) {
            $settings = [];
            $settings[] = ['reference' => 'system.version.number', 'value' => LOTE_VERSION_NUMBER];
            $settings[] = ['reference' => 'system.version.date', 'value' => LOTE_VERSION_DATE];
            $settings[] = ['reference' => 'system.version.build', 'value' => LOTE_BUILD_NUMBER];
            foreach ($settings as $s) {
                $currentValue = $this->getState()->getSettings()->get($s['reference']);
                if ($currentValue != $s['value']) {
                    if ($simulate) {
                        $messages[] = "[Warning] Setting update required for '{$s['reference']}', current value = '$currentValue', new value = '{$s['value']}'";
                    } else {
                        $this->getState()->getSettings()->set($s['reference'], $s['value']);
                        $messages[] = "Setting '{$s['reference']}' updated to '{$s['value']}'";
                    }
                }
            }
            if (empty($messages)) {
                $messages[] = 'Settings are up to date';
            }
        }
        else {
            $messages[] = '[Error] Settings table does not exist';
        }
        return $messages;
    }

    public function install()
    {
        if (!$this->getState()->isInstalled()) {
            $db = $this->getState()->getWriteDb();
            $schemaTemplate = $this->buildInstallSchema();
            $currentSchema = $db->getSchemaManager()->createSchema();
            $migration = $currentSchema->getMigrateToSql($schemaTemplate, $db->getDatabasePlatform());
            foreach ($migration as $sql) {
                $db->exec($sql);
            }
            $this->insertSeedData();
        } else {
            $route = $this->getState()->getRoute();
            $modules = $route['module'];
            foreach ($modules as $m) {
                $this->installModule($m);
            }
            $system = $route['system'];
            foreach ($system as $s) {
                $this->installSystem($s);
            }
        }
    }

    public function uninstall()
    {
        if ($this->getState()->isInstalled()) {
            $route = $this->getState()->getRoute();
            $modules = $route['module'];
            foreach ($modules as $m) {
                $this->uninstallModule($m);
            }
            $systems = $route['system'];
            foreach ($systems as $s) {
                $this->uninstallSystem($s);
            }
        }
    }

    private function uninstallModule($moduleRef)
    {
        $version = $this->getLatestInstallVersion(LOTE_VERSION_PATH . 'component/module/' . $moduleRef);
        $this->downData($version);
        $this->removeAppEntry($moduleRef);
        $this->update();
    }

    private function uninstallSystem($systemRef)
    {
        $version = $this->getLatestInstallVersion(LOTE_VERSION_PATH . 'component/system/' . $systemRef);
        $this->downData($version);
        $this->removeAppEntry($systemRef);
        $this->update();
    }


    private function installSystem($systemRef)
    {
        $version = $this->getLatestInstallVersion(LOTE_VERSION_PATH . 'component/system/' . $systemRef);
        $this->updateAppEntry($version . 'info.json');
        $this->update();
        $this->upData($version);
    }

    private function removeAppEntry($reference) {
        $this->getState()->getWriteDb()->delete('sb__app', ['reference' => $reference]);
    }

    private function installModule($moduleRef)
    {
        $version = $this->getLatestInstallVersion(LOTE_VERSION_PATH . 'component/module/' . $moduleRef);
        $this->updateAppEntry($version . 'info.json');
        $this->update();
        $this->upData($version);
    }

    private function executeData($version, $type = "Module", $method){
        if ($class = $this->getFileClassName($version . 'Seed.php')) {
            require_once($version . 'Seed.php');
            $className = '\\Lote\\Release\\'.$type.'\\' . $class;
            $c = new $className($this->getState());
            if (method_exists($c, $method)) {
                $data = [];
                $c->{$method}($data);
            }
        }
    }

    private function upData($version, $type = "Module"){
        $this->executeData($version, $type, 'up');
    }

    private function downData($version, $type = "Module"){
        $this->executeData($version, $type, 'down');
    }

    private function buildUpdateSchema($isMaster = false)
    {
        $toSchema = $this->getCoreSchema();
        $this->getProjectSchema($isMaster, $toSchema);

        /**
         * @var Config $v
         * */

        $this->getState()->getApps()->reloadApps();
        $systems = $this->getState()->getApps()->getSystems();
        foreach ($systems as $v) {
            $this->addSystemSchema($toSchema, $v->getReference(), $v->getVersion(), $isMaster);
        }
        $modules = $this->getState()->getApps()->getModules();
        foreach ($modules as $v) {
            $this->addModuleSchema($toSchema, $v->getReference(), $v->getVersion(), $isMaster);
        }
        $apps = $this->getState()->getApps()->getApps();
        foreach ($apps as $v) {
            $this->addAppSchema($toSchema, $v->getReference(), $isMaster);
        }
        return $toSchema;
    }

    /**
     * @return Schema|false
     * */
    private function getProjectSchema($isMaster, Schema $schema)
    {
        if(!$isMaster) {
            $f = new Finder();
            foreach ($classes = $f->getProjectEntityClasses() as $class) {
                $s = new \SidraBlue\Lote\Application\App\Schema();
                $s->getSchema($schema, $class, $isMaster);
            }
        }
        return $schema;
    }

    /**
     * @return void
     * */
    private function insertSeedData()
    {
        $route = $this->getState()->getRoute();
        $allData = [];

        $this->coreSeed($allData);

        foreach ($route['module'] as $v) {
            $this->insertModuleSeed($allData, $v);
        }
        foreach ($route['system'] as $v) {
            $this->insertSystemSeed($allData, $v);
        }
    }

    /**
     * @return Schema|false
     * */
    private function coreSeed($data)
    {
        $result = new Schema();
        if ($version = $this->getLatestInstallVersion(LOTE_VERSION_PATH . 'sidrablue')) {
            if ($class = $this->getFileClassName($version . 'Seed.php')) {
                require_once($version . 'Seed.php');
                $className = '\\Lote\\Release\\SidraBlue\\Core\\' . $class;
                $c = new $className($this->_state);
                if (method_exists($c, 'up')) {
                    $c->up($data);
                }
                $this->updateAppEntry($version . 'info.json');
            }
        }
        return $result;
    }


    protected function updateAppEntry($fileName)
    {
        if (file_exists($fileName)) {
            $json = json_decode(file_get_contents($fileName), true);
            if ($this->validJsonInfoSchema($json)) {
                $a = new App($this->getState());
                $a->loadByField('reference', $json['reference']);
                $a->type = $json['type'];
                $a->name = $json['name'];
                $a->description = $json['description'];
                $a->reference = $json['reference'];
                $a->enabled = true;
                $a->version = $json['version'];
                $a->save();
            }
            else {
                dump("INVALID SCHEMA");
            }
        }
        else {
            dump("NO APP DATA: ".$fileName);
        }
    }

    private function validJsonInfoSchema($json)
    {
        $result = false;
        if (is_array($json) && count($json) > 0) {
            $result = isset($json['type']) && isset($json['name']) && isset($json['reference']) && isset($json['version']) && isset($json['description']);
        }
        return $result;
    }

    private function insertModuleSeed(&$allData, $moduleReference, $moduleVersion = false)
    {
        if ($moduleVersion) {
            $version = LOTE_VERSION_PATH . 'component/module/' . $moduleReference . '/' . $moduleVersion . '/';
        } else {
            $version = $this->getLatestInstallVersion(LOTE_VERSION_PATH . 'component/module/' . $moduleReference);
        }
        if ($class = $this->getFileClassName($version . 'Seed.php')) {
            require_once($version . 'Seed.php');
            $className = '\\Lote\\Release\\Module\\' . $class;
            if(class_exists($className)) {
                $c = new $className($this->getState());
                if (method_exists($c, 'up')) {
                    $c->up($allData);
                }
            }
        }
        $this->updateAppEntry($version . 'info.json');
    }

    private function insertSystemSeed(&$allData, $systemReference, $systemVersion = false)
    {
        if ($systemVersion) {
            $version = LOTE_VERSION_PATH . 'component/system/' . $systemReference . '/' . $systemVersion . '/';
        } else {
            $version = $this->getLatestInstallVersion(LOTE_VERSION_PATH . 'component/system/' . $systemReference);
        }
        if ($class = $this->getFileClassName($version . 'Seed.php')) {
            require_once($version . 'Seed.php');
            $className = '\\Lote\\Release\\System\\' . $class;
            if(class_exists($className)) {
                $c = new $className($this->getState());
                if (method_exists($c, 'up')) {
                    $c->up($allData);
                }
            }
        }
        $this->updateAppEntry($version . 'info.json');
    }

    /**
     * @return Schema
     * */
    private function buildInstallSchema()
    {
        $route = $this->getState()->getRoute();
        $toSchema = $this->getCoreSchema();

        foreach ($route['module'] as $v) {
            $this->addModuleSchema($toSchema, $v);
        }

        foreach ($route['system'] as $v) {
            $this->addSystemSchema($toSchema, $v);
        }
        return $toSchema;
    }

    private function addAppSchema($toSchema, $appReference, $isMaster = false)
    {
        $f = new Finder($this->_state);
        foreach($classes = $f->getAppEntityClasses($appReference) as $class) {
            $s = new \SidraBlue\Lote\Application\App\Schema($this->_state);
            $s->getSchema($toSchema, $class, $isMaster);
        }
    }

    private function addModuleSchema($toSchema, $moduleReference, $moduleVersion = false, $isMaster = false)
    {
        if ($moduleVersion) {
            $version = LOTE_VERSION_PATH . 'component/module/' . $moduleReference . '/' . $moduleVersion . '/';
        } else {
            $version = $this->getLatestInstallVersion(LOTE_VERSION_PATH . 'component/module/' . $moduleReference);
        }
        if ($class = $this->getFileClassName($version . 'Schema.php')) {
            require_once($version . 'Schema.php');
            $className = '\\Lote\\Release\\Module\\' . $class;
            $c = new $className();
            if (!$isMaster && method_exists($c, 'up')) {
                $c->up($toSchema);
            }
            if ($isMaster && method_exists($c, 'upMaster')) {
                $c->upMaster($toSchema);
            }
        }
    }

    private function addSystemSchema($toSchema, $systemReference, $systemVersion = false, $isMaster = false)
    {
        if ($systemVersion) {
            $version = LOTE_VERSION_PATH . 'component/system/' . $systemReference . '/' . $systemVersion . '/';
        } else {
            $version = $this->getLatestInstallVersion(LOTE_VERSION_PATH . 'component/system/' . $systemReference);
        }
        if ($class = $this->getFileClassName($version . 'Schema.php')) {
            require_once($version . 'Schema.php');
            $className = '\\Lote\\Release\\System\\' . $class;
            $c = new $className();
            if (!$isMaster && method_exists($c, 'up')) {
                $c->up($toSchema);
            }
            if ($isMaster && method_exists($c, 'upMaster')) {
                $c->upMaster($toSchema);
            }
        }
    }

    private function getFileClassName($filePath)
    {
        $result = false;
        if (file_exists($filePath)) {
            $phpSrc = file_get_contents($filePath);
            $classes = $this->getPhpClasses($phpSrc);
            if (isset($classes) && is_array($classes) && isset($classes[0])) {
                $result = $classes[0];
            }
        }
        return $result;
    }

    private function getPhpClasses($phpSrc)
    {
        $classes = [];
        $tokens = token_get_all($phpSrc);
        $count = count($tokens);
        for ($i = 2; $i < $count; $i++) {
            if ($tokens[$i - 2][0] == T_CLASS
                && $tokens[$i - 1][0] == T_WHITESPACE
                && $tokens[$i][0] == T_STRING
            ) {

                $class_name = $tokens[$i][1];
                $classes[] = $class_name;
            }
        }
        return $classes;
    }


    /**
     * @return Schema|false
     * */
    private function getCoreSchema()
    {
        $result = new Schema();
        if ($version = $this->getLatestInstallVersion(LOTE_VERSION_PATH . 'sidrablue')) {
            if ($class = $this->getFileClassName($version . 'Schema.php')) {
                require_once($version . 'Schema.php');
                $className = '\\Lote\\Release\\SidraBlue\\Core\\' . $class;
                $c = new $className();
                if (method_exists($c, 'up')) {
                    $c->up($result);
                }
            }
        }
        return $result;
    }

    private function getLatestInstallVersion($baseFolder)
    {
        $result = false;
        if (is_dir($baseFolder)) {
            $d = new \DirectoryIterator($baseFolder);
            foreach ($d as $v) {
                if ($v->getFileName() != '.' && $v->getFileName() != '..') {
                    $installBase = str_replace('\\', '/', $v->getPathname()) . '/';
                    if (file_exists($installBase . 'Schema.php')) {
                        $result = $installBase;
                    }
                }
            }
        }
        return $result;
    }

}
