<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Controller;

use Lote\System\Schoolzine\Model\Link;
use Lote\System\Schoolzine\Model\School;
use SidraBlue\Lote\Auth\Login\Throttle;
use SidraBlue\Lote\Entity\Token as TokenEntity;
use SidraBlue\Lote\Entity\User;
use SidraBlue\Lote\Event\Data;
use SidraBlue\Lote\Model\UserAutoLogin;
use SidraBlue\Lote\Service\Email as EmailService;
use SidraBlue\Lote\State\Web;
use SidraBlue\Util\Time;
use SidraBlue\Lote\Model\Auth\Authenticator;
use SidraBlue\Lote\Entity\UserSession;
use SidraBlue\Lote\Entity\Auth\Authenticator as AuthEntity;
use SidraBlue\Lote\Model\Auth\Authenticator as AuthModel;
use SidraBlue\Lote\View\Transform\Html\Service as HtmlService;
use SidraBlue\Lote\Service\Content as ContentService;

/**
 * Default REST based controller for the Lote Framework
 * @package SidraBlue\Lote\Controller
 * @todo - break this up into a Rest and RestPhpcrItem controller
 * */
class Login extends Rest
{

    protected function login()
    {
        $this->getState()->getRequest()->cookies->set('lote_cookie_test', true);
        $this->getState()->getView()->cookieEnabled = $this->getState()->getRequest()->cookies->get('lote_cookie_test');
        $this->getState()->getView()->setRenderFile('login/login');
    }

    protected function twoFactorRequired()
    {
        $twoFactorAuth = new AuthEntity($this->getState());

        if (!$twoFactorAuth->loadByField('object_id', $this->getState()->getUser()->id)) {
            $this->getState()->getView()->two_factor_setup_required = true;
        }

        $this->getState()->getView()->setRenderFile('login/two_factor');
    }

    protected function twoFactorSetup()
    {
        $token = $this->getVar('token');
        $te = new TokenEntity($this->getState());
        if ($te->loadByFields(['public_key' => $token])) {
            $tokenData = $te->getData();
            $userId = $tokenData['reference_id'];
            if ($userId > 0 && $tokenData['reference_key'] == 'two_factor_qrcode_setup') {

                $auth = new AuthModel($this->getState());
                $auth->generate($userId, true);

                // if we are in www then get the auth details and save to child 
                $sysRef = $this->getState()->getSettings()->get('system.reference');
                if ($sysRef == 'www') {
                    $ue = new User($this->getState());
                    $ue->load($userId);

                    $twoFactorAuth = new AuthEntity($this->getState());
                    $twoFactorAuth->loadByField('object_id', $ue->id);

                    // get the schools
                    $lm = new Link($this->getState()->getSuperParentState());
                    $schools = $lm->getUserSchools($userId);
                    if (!empty($schools)) {
                        foreach ($schools as $item) {
                            if (isset($item['school_id'])) {
                                $m = new School($this->getState());
                                $prefix = $m->getSchoolPrefixById($item['school_id']);
                                if ($prefix) {
                                    $csState = new Web($prefix);
                                    $csUe = new User($csState);
                                    $csUe->loadByEmail($ue->email);

                                    $csTwoFactorAuth = new AuthEntity($csState);
                                    $csTwoFactorAuth->loadByField('object_id', $csUe->id);
                                    $csTwoFactorAuth->object_ref = $twoFactorAuth->object_ref;
                                    $csTwoFactorAuth->object_id = $csUe->id;
                                    $csTwoFactorAuth->secret = $twoFactorAuth->secret;
                                    $csTwoFactorAuth->save();
                                }
                            }
                        }
                    }
                }

                $content = HtmlService::renderView(
                    $this->getState(),
                    'authenticate/generate',
                    [
                        'qr' => $auth->getQrCode($userId),
                        'secret' => $auth->getSecret($userId)
                    ],
                    [LOTE_CORE_PATH . 'view/admin/default/']
                );

                $this->getState()->getView()->qr_code = $content;
            }

            $this->getState()->getView()->setRenderFile('login/two_factor_qr_setup');
        }
    }

    protected function twoFactorSendEmail()
    {
        $userId = $this->getVar('id');
        $redirect = $this->getVar('redirect');

        $u = new User($this->getState());
        $u->load($userId);
        $userData = $u->getData();

        // Create Token
        $tokenData = [];
        $tokenData['public_key'] = uniqid();
        $tokenData['private_key'] = uniqid();
        $tokenData['reference_id'] = $userData['id'];
        $tokenData['reference_key'] = 'two_factor_qrcode_setup';
        $tokenData['activated'] = gmdate("Y-m-d H:i:s", time());
        // Default expiry is 24 hrs. 24*60*60 = 86400 seconds
        $tokenData['expires'] = gmdate("Y-m-d H:i:s", (time() + 86400));

        $obj = new TokenEntity($this->getState());
        $obj->setData($tokenData);
        $tokenId = $obj->save();

        // Send email
        $result = false;
        if ($tokenId) {
            $content = '';
            if ($this->getState()->getApps()->hasSystem("edm")) {
                $tm = new \Lote\System\Edm\Model\Template($this->getState());
                $template = $tm->getDefaultTemplate();

                if (strpos($template['content_html'], "%%__content__%%") !== false) {
                    $content = HtmlService::renderView(
                        $this->getState(),
                        'login/two_factor_email',
                        [
                            'user_data' => $userData,
                            'token_data' => $tokenData,
                            'urlBase' => $this->getState()->getSites()->getCurrentSiteUrl(),
                            'redirect' => $redirect ? base64_encode($redirect) : ''
                        ]
                    );
                    $content = str_replace("%%__content__%%", $content, $template['content_html']);
                    $content = str_replace('%%__email_subject__%%', 'Two Factor Setup', $content);
                }
            }

            if (!$content) {
                // Get the default edm template
                $content = HtmlService::renderView(
                    $this->getState(),
                    'login/two_factor_email',
                    [
                        'user_data' => $userData,
                        'token_data' => $tokenData,
                        'urlBase' => $this->getState()->getSites()->getCurrentSiteUrl(),
                        'redirect' => $redirect ? base64_encode($redirect) : ''
                    ]
                );
            }

            $contentSvc = new ContentService($this->getState());
            $content = $contentSvc->renderContent($content, $u);
            $content = $contentSvc->removeEmptyWildcards($content);

            $e = new EmailService($this->getState());
            $senderEmail = $this->getState()->getSettings()->get('system.email.address');
            $senderName = $this->getState()->getSettings()->get('system.email.from');
            $subject = $this->getState()->getSettings()->get('system.name') . ' Two Factor Setup';
            $recipientEmail = $userData['email'];

            $result = $e->sendEmail($subject, $content, $recipientEmail, $senderEmail, $senderName);
        }

        $this->getState()->getView()->success = $result;
    }

    protected function twoFactorValidate()
    {
        $code = $this->getVar("code");
        $authModel = new Authenticator($this->getState());
        if ($authModel->authenticate($this->getState()->getUser()->id, $code)) {
            $e = new UserSession($this->getState());
            if($e->loadByFields(["user_id" => $this->getState()->getUser()->id, 'session_id' => $this->getState()->getRequest()->getSession()->getId()])) {
                $e->is_two_factor_verified = true;
                $e->save();
            }
            $this->getState()->getView()->success = true;
        } else {
            $this->setError("Invalid code");
        }
    }

    protected function validUsernameSubmitted()
    {
        $result = false;
        $username = $this->getVar("username");
        $u = new User($this->getState());
        if ($username) {
            $u->loadByUsername($username);
            if (!$u->id) {
                $u->reset();
                $u->loadByEmail($username);
                $result = $u->id > 0;
            } else {
                $result = true;
            }
        }
        return $result;
    }

    protected function getValidUser()
    {
        $result = false;
        $username = $this->getVar("username");
        $u = new User($this->getState());
        if ($username) {
            $u->loadByUsername($username);
            if (!$u->id) {
                $u->reset();
                $u->loadByEmail($username);
                $result = $u->getViewData();
            } else {
                $result = true;
            }
        }
        return $result;
    }

    protected function googleValidate()
    {
        $this->getState()->getView()->success = false;
        $token = $this->getVar("googleAccessToken");

        if (!is_null($token)) {
            $tokenCheckEndpoint = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=' . $token;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $tokenCheckEndpoint);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $response = curl_exec($ch);
            $response = json_decode($response, true);
            curl_close($ch);

            $clientId = $this->getState()->getSettings()->getSettingOrConfig("system.login.google.client_id");
            if (!empty($response) && !isset($response["error_description"]) && $response["aud"] == $clientId) {
                if (isset($response["email"])) {
                    $u = new User($this->getState());
                    if ($u->loadByEmail($response["email"])) {
                        if ($this->getState()->getLogin()->autoLogin($u->id, 0, true)) {
                            $this->getState()->getView()->success = true;
                        }
                    }
                }
            }
        }
    }

    protected function office365Validate()
    {
        $curlRedirectUri = $this->getState()->getRequest()->getSchemeAndHttpHost() . '/_admin/login/office365-validate';

        unset($_SESSION['access_token']);

        if (!isset($_SESSION['access_token'])) {
            $office365Token = $_SESSION['access_token'] = $this->getOffice365AccessToken($curlRedirectUri);
        } else {
            $office365Token = $_SESSION['access_token'];
        }

        if (isset($office365Token)) {
            if ($u = $this->getUserFromOffice365($office365Token)) {
                if ($this->getState()->getLogin()->autoLogin($u->id, 0, true)) {
                    $this->redirect('/_admin');
                } else {
                    $this->setError("Could not log perform Office365 auto login.");
                }
            } else {
                $this->setError("Your Office365 email could not be matched to a user in this system.");
            }
        } else {
            $this->setError("Invalid Office365 token. Please contact an administrator");
        }
        $this->loginError();
    }

    protected function loginError()
    {
        $this->getState()->getView()->setRenderFile("login/error");
    }


    protected function getOffice365AccessToken($curlRedirectUri)
    {
        $result = null;
        $clientId = $this->getState()->getSettings()->getSettingOrConfig("system.login.office365.client_id");
        $clientSecret = urlencode($this->getState()->getSettings()->getSettingOrConfig("system.login.office365.client_secret"));
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://login.microsoftonline.com/common/oauth2/token");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            "code=" . $_GET['code'] . "&client_id=" . $clientId . "&redirect_uri=" . $curlRedirectUri . "&grant_type=authorization_code&resource=https://graph.microsoft.com&client_secret=" . $clientSecret
        );
        $data = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($data);
        if (isset($data->access_token)) {
            $result = $data->access_token;

        }
        return $result;
    }

    protected function getUserFromOffice365($office365Token)
    {
        $result = false;
        if ($email = $this->getEmailFromOffice365($office365Token)) {
            $u = new User($this->getState());
            if ($u->loadByEmail($email)) {
                $u->office_365_last_validated = date("Y-m-d H:i:s");
                $u->save();
                $result = $u;
            }
        }
        return $result;
    }

    protected function getEmailFromOffice365($office365Token)
    {
        $result = false;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://graph.microsoft.com/v1.0/me");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer $office365Token",
            'Content-Type: application/json;odata.metadata=minimal;odata.streaming=true'
        ]);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        $data = curl_exec($ch);
        curl_close($ch);
        // Get the data from the JSON response
        $data = json_decode($data, true);
        if (isset($data['userPrincipalName'])) {
            $result = $data['userPrincipalName'];
        }
        return $result;
    }

    function windowsValidate()
    {
        $clientKey = '0000000040160F76';
        $clientSecret = 'LfzZM0kbfWpp9upBKgVBvw2gpi83NsPP';
        $redirectUri = $this->getState()->getRequest()->getSchemeAndHttpHost() . '/_admin/login/windows-validate';

        if (!isset($_SESSION['access_token'])) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://login.live.com/oauth20_token.srf");
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt(
                $ch,
                CURLOPT_POSTFIELDS,
                "code=" . $_GET['code'] . "&client_id=" . $clientKey . "&client_secret=" . $clientSecret . "&redirect_uri=" . $redirectUri . "&grant_type=authorization_code"
            );
            $data = curl_exec($ch);
            curl_close($ch);
            $data = json_decode($data);
            if (isset($data->access_token)) {
                $windowsLiveToken = $data->access_token;
                $_SESSION['access_token'] = $data->access_token;
            }
        } else {
            $windowsLiveToken = $_SESSION['access_token'];
        }

        if (isset($windowsLiveToken)) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://apis.live.net/v5.0/me?access_token=" . $windowsLiveToken);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $data = curl_exec($ch);
            curl_close($ch);
            // Get the data from the JSON response
            $data = json_decode($data, true);
            if (isset($data['emails']) && isset($data['emails']['account'])) {
                $u = new User($this->getState());
                if ($u->loadByEmail($data['emails']['account'])) {
                    if ($this->getState()->getLogin()->autoLogin($u->id)) {
                        $this->redirect('/_admin');
                    }
                }
            }
        }
    }

    protected function validate()
    {
        $this->validateBase(false);
    }

    protected function validateBase($generateToken = false)
    {
        if ($this->getState()->getLogin()->login(true)) {
            if ($this->getState()->getMasterDb() && $this->getState()->getAccount() && $this->getState()->getAccount()->isLoginLocked()) {
                $this->setVar("success", false);
                $this->setVar("error", "This account is inactive");
            } else {
                $this->saveLastLoggedIn();
                $this->setVar("success", true);
                if ($generateToken) {
                    $this->setVar("user_login_token",
                        $this->getState()->getLogin()->generateToken($this->getState()->getUser()->id));
                }
                $isCentralLogin = false;
                if($this->getState()->getAccount()->reference == 'www'){
                    $isCentralLogin = true;
                }
                $eventData = new Data();
                $eventData->setData(['is_central_login' => $isCentralLogin]);
                $this->getState()->getEventManager()->dispatch('lote.login.validate.true', $eventData);
                if($eventData->getData('is_redirect')) {
                    $this->getState()->getView()->redirect($eventData->getData('path'));
                }
            }
        } else {
            $throttle = new Throttle($this->getState());
            $throttle->load($this->getState()->getLogin()->getCredentials()->getUserId());
            $throttle->addFailedLogin($this->getState()->getLogin()->getCredentials());

            $this->setVar("success", false);
            $this->setVar("error", $this->getState()->getLogin()->getLoginError());
        }
    }

    protected function autoLogin()
    {
        $m = new UserAutoLogin($this->getState());
        if ($l = $m->getAutoLogin($this->getVar('login_hash'), $this->getVar('user_hash'))) {
            if ($this->getState()->getLogin()->autoLogin($l->user_id, null, $l->two_factor_completed)) {
                $this->redirect($l->redirect);
            }
        }
    }

    protected function parentLogin()
    {
        $m = new UserAutoLogin($this->getState());
        if ($l = $m->getAutoLogin($this->getVar('login_hash'), $this->getVar('user_hash'))) {
            if ($this->getState()->getLogin()->autoLogin($l->user_id)) {
                $this->redirect($l->redirect);
            }
        }
    }

    protected function logout()
    {
        $this->getState()->getLogin()->logout();
        if($redirectURl = $this->getVar("redirect", false)) {
            $url = $redirectURl;
            if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $url)) {
                $url = base64_decode($url);
            }
        }
        else {
            $url = $this->getRouteSystemPrefix('', true) . '/login';
        }
        $this->redirect($url);
    }

    protected function retrieve()
    {
        $this->getState()->getView()->setRenderFile('login/retrieve');
    }

    private function saveLastLoggedIn()
    {
        if ($this->getState()->getUser()->isAdmin() && $this->getState()->getMasterDb()) {
            $clientRef = $this->getState()->getSettings()->get('system.reference');

            if (!empty($clientRef)) {
                $obj = new \Lote\System\Client\Entity\Master\Account($this->getState());
                $accountDetails = $obj->loadByField('reference', $clientRef);
                if (!empty($accountDetails['id'])) {
                    $obj->last_logged_in = Time::getUtcNow();
                    $obj->save();
                }
            }
        }
        return true;
    }


    public function masquerade()
    {
        $this->getState()->getView()->success = false;
        if ($this->getState()->getUser()->isAdmin()) {
            $currentSession = $this->getState()->getLogin()->getLoginSession();
            if ($currentSession['masquerade_user_id'] > 0) {
                die('Already in masquerade mode');
            } else {
                $currentUserId = $this->getState()->getUser()->id;
                $userId = $this->getVar("user_id");
                $ue = new User($this->getState());
                if ($ue->load($userId)) {
                    $this->getState()->getView()->success = $this->getState()->getLogin()->autoLogin($userId, $currentUserId);
                    $this->getState()->getView()->user_id = $userId;
                    if ($redirect = $this->getVar("redirect")) {
                        $this->redirect(base64_decode($redirect));
                    }
                }
            }
        }
    }

}