<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Controller\Queue;

use SidraBlue\Lote\State\Cli;

/**
 * Base queue worker class
 */
abstract class Worker
{

    protected function tearDown()
    {

    }

    abstract public function perform();

}
