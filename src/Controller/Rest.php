<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Controller;

use SidraBlue\Lote\Object\Entity\Base as BaseEntity;
use SidraBlue\Lote\Entity;

/**
 * Default REST based controller for the Lote Framework
 * @package SidraBlue\Lote\Controller
 * @todo - break this up into a Rest and RestPhpcrItem controller
 * */
class Rest extends Request
{

    /**
     * Get the action name of this request, based on the HTTP request type and/or the _method parameter
     * @param array $route - the route data
     * @access protected
     * @return string
     * */
    protected function getActionName($route)
    {
        $this->executedAction = 'unknownAction';
        if(php_sapi_name()=='cli') {
            if(isset($route['action'])) {
                $this->executedAction = $route['action'];
            }
        }
        else {
            /**
             * @var \Symfony\Component\HttpFoundation\Request $r
             * */
            $r = $this->getState()->getRequest();
            $this->executedAction = $r->get("_method");

            if(isset($this->_route['_method']) && !empty($this->_route['_method'])) {
                $this->executedAction = $this->_route['_method'];
            }
            elseif(isset($function) && preg_match('/^[a-z]\w+$/i', $function)) {
                $this->executedAction = strtolower($r->getMethod()).ucfirst($function);
            }
            else {
                if(method_exists($this, $r->getMethod().$this->_className)){
                    $this->executedAction = strtolower($r->getMethod()).$this->_className;
                }
                elseif(method_exists($this, $r->getMethod())){
                    $this->executedAction = strtolower($r->getMethod());
                }
            }
        }
        return $this->executedAction;
    }

    protected function validateEntityEditAccess(BaseEntity $entity) {
        if(!$this->getState()->getAccess()->hasEntityAccess($entity, "edit")) {
            $this->getState()->getView()->accessDenied();
        }
    }

    protected function validateEntityViewAccess(BaseEntity $entity) {
        if(!$this->getState()->getAccess()->hasEntityAccess($entity, "view")) {
            $this->getState()->getView()->accessDenied();
        }
    }

    /**
     * Update the access for an object
     * @access protected
     * @param int $objectId
     * @param string $objectReference
     * @param array $groups
     * @return void
     * @todo - create an entity for the access entity instead of using normal sql
     */
    protected function updateAccess($objectId, $objectReference, $groups)
    {
        if(is_array($groups)) {
            foreach($groups as $gId => $accessLevel) {
                $ae = new Entity\Access\Entity($this->getState());
                $ae->loadByFields(['object_ref' => $objectReference, 'object_id' => $objectId, 'access_type' => 'group', 'access_type_id' => $gId]);
                if($ae->id && $accessLevel=='') {
                    $ae->delete();
                } elseif($accessLevel) {
                    $ae->object_ref = $objectReference;
                    $ae->object_id = $objectId;
                    $ae->access_type = 'group';
                    $ae->access_type_id = $gId;
                    $ae->access_level = $accessLevel;
                    $ae->save();
                }
            }
        }
    }

    /**
     * Save access groups with selected Ids
     *
     * @access protected
     * @param object $obj - Object
     * @param array $groups_selected - Selected Groups
     * @param int $parentId
     * @param string $kind
     * @return void
     */
    protected function saveAccessGroups($obj, $groups_selected, $parentId = 0, $kind = '')
    {
        // access groups
        $g = new \SidraBlue\Lote\Model\Group($this->getState());
        $access_groups = $g->getAllGroups($parentId, $kind);

        $lote_access = [];
        if (!empty($access_groups)) {
            foreach ($access_groups as $v) {
                if (!empty($groups_selected) && in_array($v['id'], $groups_selected)) {
                    $lote_access[$v['id']] = 'view';
                } else {
                    $lote_access[$v['id']] = '';
                }
            }
        }

        $this->updateAccess($obj->id, $obj->getTableName(), $lote_access);
    }

}
