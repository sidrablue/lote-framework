<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity\Access;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for a process
 * @package Lote\System\Crm\Entity
 */
class Entity extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__access_entity';

    /**
     * @var string $access_type - The access type, e.g. 'user' or 'group'
     */
    public $access_type;

    /**
     * @var string $access_type - The access type ID, the ID of the access type such as the users ID
     */
    public $access_type_id;

    /**
     * @var string $access_level - The access level, e.g. 'read', 'write'
     */
    public $access_level;

    /**
     * @var string $object_ref - The object reference that this access applies to
     */
    public $object_ref;

    /**
     * @var int $object_id - The object ID that this access applies to
     */
    public $object_id;

    /**
     * @var string $field_ref - The object field reference that this access applies to
     */
    public $field_ref;


}
