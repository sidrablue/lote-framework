<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Class Address
 * @package SidraBlue\Lote\Entity
 */
class Address extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__address';

    /**
     * @var boolean $isAudited
     * True if this entity is audited
     * */
    protected $isAudited = true;

    /**
     * @var string $object_ref - the object reference
     */
    public $object_ref;

    /**
     * @var int $object_id - The object ID
     */
    public $object_id;

    /**
     * @var int $parent_id - The object ID
     */
    public $parent_id;

    /**
     * @var string $priority -
     */
    public $location;

    /**
     * @var string $name -
     */
    public $name;

    /**
     * @var string $address_type - address type
     */
    public $address_type;

    /**
     * @var string $company_name -
     */
    public $company_name;

    /**
     * @var string $street1 -
     */
    public $street1;

    /**
     * @var string $street2 -
     */
    public $street2;

    /**
     * @var string $city -
     */
    public $city;

    /**
     * @var int $postcode -
     */
    public $postcode;

    /**
     * @var string $state -
     */
    public $state;

    /**
     * @var string $country -
     */
    public $country;

    /**
     * @var boolean $active - True if the group is active
     */
    public $active;

    /**
     * @var string $phone - phone
     */
    public $phone;

    /**
     * @var string $mobile - mobile
     */
    public $mobile;

}
