<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for all entities
 */
class App extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__app';

    /**
     * @var string $type - the type of app, 'system' or 'module'
     */
    public $type;

    /**
     * @var string $name - the name of the app
     */
    public $name;

    /**
     * @var string $reference - the reference of the app
     */
    public $reference;

    /**
     * @var int $version - the version of the app
     */
    public $version;

    /**
     * @var string $description - The description of the app
     */
    public $description;

    /**
     * @var string $name - The priority of the app
     */
    public $priority;

    /**
     * @var boolean $enabled - The enabled status of the app
     */
    public $enabled;

    /**
     * @var boolean $hidden - True if this app is hidden from the user
     */
    public $hidden;

    /**
     * @var string $license - The type of license that this app has
     */
    public $license;

}
