<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity\Audit;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Audit object entity
 */
class FieldLog extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__audit_field_log';

    /**
     * @var int $object_id - The object ID
     */
    public $object_id;

    /**
     * @var string $field_name - The field that has been changed
     */
    public $field_name;

    /**
     * @var string $field_type - The field type, 'core' or 'custom'
     */
    public $field_type;

    /**
     * @var string $value_old - The old value
     */
    public $value_old;

    /**
     * @var string $value_new - The new value
     */
    public $value_new;

}