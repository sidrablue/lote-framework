<?php

/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity\Audit;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Audit object entity
 */
class FileLog extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__audit_file_log';

    /**
     * @var int $log_id - The log ID that this corresponds to
     */
    public $log_id;

    /**
     * @var int $object_log_id - The object log ID that this corresponds to
     */
    public $object_log_id;

    /**
     * @var string $object_ref - The object reference
     */
    public $object_ref;

    /**
     * @var integer $object_id - The object id
     */
    public $object_id;

    /**
     * @var string $field_reference - The field that this file reference is for
     */
    public $field_reference;

    /**
     * @var string $file_store - The store that the content is saved in
     */
    public $store;

    /**
     * @var string $reference - The reference of the file content
     */
    public $location_reference;

    /**
     * @var string $archive_reference - The archive reference of the file content
     */
    public $archive_reference;

}
