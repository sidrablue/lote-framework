<?php

/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity\Audit;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Audit object entity
 */
class LinkLog extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__audit_link_log';

    /**
     * @var int $log_id - The log ID that this corresponds to
     */
    public $log_id;

    /**
     * @var int $object_log_id - The object log ID that this corresponds to
     */
    public $object_log_id;

    /**
     * @var string $object_ref_from - The source object reference
     */
    public $object_ref_from;

    /**
     * @var string $object_ref_to - The destination object reference
     */
    public $object_ref_to;

    /**
     * @var int $object_id_from - The object ID
     */
    public $object_id_from;

    /**
     * @var int $object_id_to - The object ID
     */
    public $object_id_to;

    /**
     * @var string $operation - The operation type
     */
    public $operation;

}
