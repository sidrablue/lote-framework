<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity\Audit;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Audit log entity
 */
class Log extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__audit_log';

    /**
     * @var int $parent_id - the id of any parent log
     */
    public $parent_id;

    /**
     * @var string $controller - the $controller being called
     */
    public $controller;

    /**
     * @var string $method - the method being undertaken
     */
    public $method;

    /**
     * @var string $status - the status of the log entry
     */
    public $status;

    /**
     * @var string $reference - the reference of the log
     */
    public $reference;

    /**
     * @var string $description - the description of the log
     */
    public $description;

    /**
     * @var string $data - data for the log
     */
    public $data;

}
