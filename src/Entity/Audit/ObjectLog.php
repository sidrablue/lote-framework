<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity\Audit;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Audit object field entity
 */
class ObjectLog extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__audit_object_log';

    /**
     * @var string $object_ref - the object reference
     */
    public $object_ref;

    /**
     * @var int $object_id - The object ID
     */
    public $object_id;

    /**
     * @var int $log_id - The log ID
     */
    public $log_id;

    /**
     * @var int $operation - the operation
     */
    public $operation;

    /**
     * @var int $description - the description of the change
     */
    public $description;

}
