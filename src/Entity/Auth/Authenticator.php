<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity\Auth;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Group class
 * @package SidraBlue\Lote\Entity
 * @dbEntity true
 *
 * @SWG\Definition(required={"tableName", "object_id", "object_ref","secret", @SWG\Xml(name="Authenticator")})
 */

class Authenticator extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_authenticator';

    /**
     * @dbColumn object_id
     * @dbType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int
     */
    public $object_id;

    /**
     * @dbColumn object_ref
     * @dbType string
     * @dbOptions notnull = false
     * @var string $object_ref
     * @SWG\Property()
     * @var string
     */
    public $object_ref;

    /**
     * @dbColumn secret
     * @dbType string
     * @dbOptions notnull = false
     * @var string $secret - The secret code for the account
     * @SWG\Property()
     * @var string
     */
    public $secret;

}
