<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Data\Field\Factory;
use SidraBlue\Lote\Object\Data\Field\Type\Base as BaseFieldType;
use SidraBlue\Lote\Object\Entity\Base;
use SidraBlue\Lote\Model\CfField as FieldModel;

/**
 * Base class for all entities
 */
class CfFieldOption extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__cf_field_option';

    /**
     * @var int $field_id - the ID of the field that the option belongs to
     */
    public $field_id;

    /**
     * @var int $parent_id - the ID of the parent option of this option
     */
    public $parent_id;

    /**
     * @var string $reference - the reference of the field
     */
    public $reference;

    /**
     * @var string $value - The value of the field
     */
    public $value;

    /**
     * @var int $sort_order - the sort order of this field in its group
     */
    public $sort_order;

    /**
     * @var int $weighting - the weighting of the field used in the sorting of the options
     */
    public $weighting;

    /**
     * @var string $direction - the direction of the option
     */
    public $direction;

    /**
     * @var int $selection_limit - the number of times that this option can be selected
     */
    public $selection_limit;

    /**
     * @var boolean $is_assessable - true if it can be selected
     */
    public $is_selectable;

    /**
     * @var boolean $is_hidden - true if this field is hidden
     */
    public $is_hidden;

    /**
     * @var boolean $is_active - true if this field is active
     */
    public $is_active;

    /**
     * @var boolean $is_correct - true if this field is a correct answer
     */
    public $is_correct;

}
