<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Model\CfGroup as GroupModel;
use SidraBlue\Lote\Model\CfGroup as CfGroupModel;
use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for all entities
 */
class CfGroup extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__cf_group';

    /**
     * @var string $reference - the reference of the group
     */
    public $reference;

    /**
     * @var int $object_id - the object that the group applies to
     */
    public $object_id;

    /**
     * @var string $name - The name of the field
     */
    public $name;

    /**
     * @var string $description - The description of the field
     */
    public $description;

    /**
     * @var int $sort_order - the sort order of this group for its object
     */
    public $sort_order;

    /**
     * @var boolean $active - true if this group is active
     */
    public $active = true;

    /**
     * @var boolean $hidden - true if this group is hidden
     */
    public $hidden = false;

    /**
     * The list of fields that make up this group
     * */
    public $fields = [];

    /**
     * The list of child fields that belong to this group
     * */
    public $childFields = [];

    /**
     * @access protected
     * @return void
     * */
    protected function beforeCreate()
    {
        $this->privateProperties[] = 'childFields';
        $this->privateProperties[] = 'fields';
    }

    /**
     * Set the name of this field
     * @access public
     * @param string $column - the column name
     * @throws \Exception
     * @return void
     */
    public function setName($column)
    {
        if (is_string($column) && preg_match("/^[\w]+$/", $column) > 0) {
            $this->_column = $column;
        } else {
            throw new \Exception('Column name must be alpha numeric');
        }
    }

    /**
     * Set the data for the fields in this group
     * @param array $fieldData
     * @param bool $loadValues
     * @return void
     * */
    public function setFieldData($fieldData, $loadValues = true)
    {
        $this->childFields = [];
        foreach ($fieldData as $d) {
            $f = new CfField($this->getState(), $d);
            if ($loadValues) {
                if(isset($d['value_id'])) {
                    $d['id'] = $d['value_id'];
                }
                $v = new CfValue($this->getState(), $d);
                $v->setTableName(str_replace("_group", "", $this->getTableName()));
                $f->setValueObject($v);
            }
            if ($f->config_data) {
                $f->config_data = json_decode($f->config_data, true);
            }
            $this->childFields[] = $f;
        }
    }

    /**
     * Load all of the fields for this group
     * @access public
     * @return void
     *  */
    public function loadAllFields($active_only = false)
    {
        if ($this->id) {
            $m = new CfGroupModel($this->getState());
            $this->setFieldData($m->getCfFields($this->id, $active_only));
        }
    }

    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeInsert($data)
    {
        $m = new GroupModel($this->getState());
        $data['sort_order'] = $m->getNextSortOrder($this->object_id);
        return $data;
    }

    /**
     * Function to call before updating data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeUpdate($data)
    {
        //unset($data['reference']);
        unset($data['object_id']);
        return $data;
    }

    /**
     * Get the custom fields belonging to this group
     * @access public
     * @return array
     * */
    public function getChildFields()
    {
        return $this->childFields;
    }

    /**
     * Get the exportable custom fields belonging to this group
     * @access public
     * @return array
     * */
    public function getExportFields()
    {
        $result = [];
        foreach ($this->childFields as $v) {
            /**
             * @var CfField $v
             * */
            if ($v->getDataDefinition()->supportsExporting()) {
                $result[] = $v;
            }
        }
        return $result;
    }

}
