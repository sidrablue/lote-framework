<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;
use SidraBlue\Util\Arrays;

/**
 * Base class for all entities
 */
class CfObject extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__cf_object';

    /**
     * @var string $reference - the reference of the group
     */
    public $reference;

    /**
     * @var string $reference_id - the id of the reference
     */
    public $reference_id;

    /**
     * @var string $name - The name of the field
     */
    public $name;

    /**
     * @var string $description - The description of the field
     */
    public $description;

    /**
     * @var int $display_order - the sort order of this group for its object
     */
    public $display_order;

    /**
     * @var boolean $active - true if this group is active
     */
    public $active = true;

    /**
     * @var boolean $hidden - true if this group is hidden
     */
    public $hidden = false;

    /**
     * @var boolean $is_core - true if this is a core object
     */
    public $is_core = false;

    /**
     * @var array $groups - The list of groups that make up this object
     * */
    protected $cfGroups = [];

    /**
     * Before create event handler to be implemented in the child
     * @access protected
     * @return void
     * */
    protected function beforeCreate()
    {
        $this->privateProperties[] = 'cfGroups';
    }

    /**
     * Set the data for the groups in this object
     * @param array $groupData
     * @return void
     * */
    public function setGroupData($groupData)
    {
        $this->cfGroups = [];
        foreach($groupData as $d) {
            $g = new CfGroup($this->getState(), $d);
            $g->setFieldData($g);
            $this->cfGroups[] = $g;
        }
    }

    /**
     * Get the groups in this object
     * @return array
     * */
    public function getGroups()
    {
        return $this->cfGroups;
    }

    /**
     * Get the groups in this object
     * @return array
     * */
    public function getActiveGroups($accessLevel = false)
    {
        $result = [];
        foreach($this->cfGroups as $g) {
            if($g->active) {
                if($accessLevel) {
                    if($this->getState()->getAccess()->hasEntityAccess($g, $accessLevel)) {
                        $result[] = $g;
                    }
                }
                else {
                    $result[] = $g;
                }
            }
        }
        return $result;
    }

    /**
     * Get the list of groups in this object that are accessible to the currently logged in user
     * @return array
     * */
    public function getAccessibleGroups()
    {
        $result = $this->getActiveGroups();
        if(!$this->getState()->isCli()) {
            $data = $result;
            $result = [];
            foreach($data as $v) {
                if($this->getState()->getAccess()->hasEntityAccess($v)) {
                    $result[] = $v;
                }
            }
        }
        return $result;
    }

    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeInsert($data)
    {
        $data['display_order'] = $this->getNextSortOrder();
        return $data;
    }

    /**
     * Get the next sort order for an object
     * @access public
     * @return int
     * */
    public function getNextSortOrder()
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('max(o.display_order) + 1')
            ->from('sb__cf_object', 'o');
        return max(1,$data->execute()->fetchColumn());
    }

    /**
     * Function to call before updating data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeUpdate($data)
    {
        unset($data['is_core']);
        unset($data['user_id']);
        return $data;
    }

    /**
     * Load the object by its ID or reference
     * @param int|string $id
     * @param bool $loadGroups - true if the groups for this object are to be loaded
     * @param bool $includeDeleted - Whether or not to include objects that have been deleted
     * @return Object
     * @todo - create a prent function where you can pass in a field to load by, instead of the ID
     */
    public function load($id, $loadGroups = false, $includeDeleted = false, $active_only = false)
    {
        if(is_numeric($id)) {
            parent::load($id, $includeDeleted);
        }
        elseif(is_string($id) && $id) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('t.*')
                ->from($this->getTableName(),'t')
                ->where('reference = :reference')
                ->setParameter('reference', $id);

            if(!$includeDeleted) {
                $q->andWhere('lote_deleted is null');
            }
            if($active_only){
                $this->loadGroups("t.active = 1");
            }

            $s = $q->execute();
            $this->setData($s->fetch(\PDO::FETCH_ASSOC));
        }
        if($loadGroups) {
            $this->loadGroups($active_only);
        }
        return $this->id;
    }

    /**
     * Load the object by its reference and reference ID or reference
     * @param string $reference
     * @param int $referenceId
     * @param bool $loadGroups - true if the groups for this object are to be loaded
     * @return Object
     */
    public function loadByReferences($reference, $referenceId, $loadGroups = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*')
            ->from($this->getTableName(),'t')
            ->where('reference = :reference')
            ->setParameter('reference', $reference)
            ->andWhere('reference_id = :reference_id')
            ->setParameter('reference_id', $referenceId);
        $s = $q->execute();
        $this->setData($s->fetch(\PDO::FETCH_ASSOC));
        if($loadGroups) {
            $this->loadGroups();
        }
        return $this->id;
    }

    public function loadGroups($active_only = false)
    {
        if($this->id) {
            $m = new \SidraBlue\Lote\Model\CfObject($this->getState());
            $groups = $m->getCfGroups($this->id);
            foreach($groups as $group) {
                $g = new CfGroup($this->getState());
                $g->setData($group);
                $g->loadAllFields($active_only);
                if ($active_only) {
                    if ($g->active == 1) {
                        $this->cfGroups[] = $g;
                    }
                } else {
                    $this->cfGroups[] = $g;
                }
            }
        }
    }

    /**
     * Retrieves map of all custom fields tied to this CfObject
     *
     * @param bool $groupByGroups - Groups fields within their CfGroups in the resultant array
     */
    public function getCustomFieldsMap($groupByGroups = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('f.*')
            ->from($this->getTableName(), 't')
            ->leftJoin('t', 'sb__cf_group', 'g', 'g.object_id = t.id')
            ->leftJoin('g', 'sb__cf_field', 'f', 'f.group_id = g.id')
            ->andWhere('t.id = :id')
            ->setParameter('id', $this->id)
            ->andWhere('t.lote_deleted is null')
            ->andWhere('g.lote_deleted is null')
            ->andWhere('f.lote_deleted is null')
            ->orderBy('g.sort_order, f.sort_order');

        $gm = new \SidraBlue\Lote\Model\CfGroup($this->getState());

        $gm = new \SidraBlue\Lote\Model\CfGroup($this->getState());
        $gm->addLoteAccessViewClauses($q, 'g', $gm->getTableName());

        $result = $q->execute();
        $data = [];

        if ($groupByGroups) {
            $cfGroups = [];
            if(!$this->cfGroups) {
                $this->loadGroups();
            }
            $cfGroupObjects = $this->getAccessibleGroups();
            foreach($cfGroupObjects as $v) {
                $cfGroups[] = $v->getData();
            }
            $data = Arrays::indexArrayByField($cfGroups, 'id');
            while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
                $data[$row['group_id']]['_fields'][$row['reference']] = $row;
            }
        } else {
            while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
                $data[$row['reference']] = $row;
            }
        }

        return $data;
    }

}
