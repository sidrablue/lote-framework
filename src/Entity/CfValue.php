<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Class CfValue
 * @package SidraBlue\Lote\Entity
 * @dbEntity true
 * @dbIndex [field_id, value_date]
 */
class CfValue extends Base
{
    /**
     * @dbColumn object_id
     * @dbType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $object_id - the id of the object that the value belongs to
     */
    public $object_id;

    /**
     * @dbColumn field_id
     * @dbType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $field_id - Field ID that the value belongs to
     */
    public $field_id;

    /**
     * @dbColumn value_string
     * @dbType string
     * @dbOptions length = 1500, notnull = false
     * @dbIndex true
     * @var string $value_string - The string value of the field
     */
    public $value_string;

    /**
     * @dbColumn value_number
     * @dbType decimal
     * @dbOptions precision = 10, scale = 0, notnull = false
     * @dbIndex true
     * @var double $value_number - Number value of the fields
     */
    public $value_number;

    /**
     * @dbColumn value_date
     * @dbType datetime
     * @dbOptions notnull = false
     * @var \DateTime $value_date - Date value of the field
     */
    public $value_date;

    /**
     * @dbColumn value_text
     * @dbType text
     * @dbOptions notnull = false
     * @var string $value_text - The text value of the field
     */
    public $value_text;

    /**
     * @dbColumn value_boolean
     * @dbType boolean
     * @dbOptions length = 1, notnull = false
     * @var boolean $value_boolean - The boolean value of the field
     */
    public $value_boolean;

    /**
     *
     * @param int $objectId
     * @param int $fieldId
     * @return boolean
     * */
    public function loadByObjectAndField($objectId, $fieldId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->getTableName(), 't')
            ->where('t.object_id = :object_id')
            ->setParameter('object_id', $objectId)
            ->andWhere('t.field_id = :field_id')
            ->setParameter('field_id', $fieldId)
            ->andWhere('t.lote_deleted is null');
        $s = $q->execute();
        if($row = $s->fetch(\PDO::FETCH_ASSOC)) {
            $this->setData($row);
        }
        return $this->id;
    }

}
