<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Class CfValueOptionSelected
 * @package SidraBlue\Lote\Entity
 * @dbEntity true
 */
class CfValueOptionSelected extends Base
{

    /**
     * @dbColumn value_id
     * @dbType integer
     * @dbOptions length = 11, notnull = false
     * @dbIndex true
     * @var int $value_id - the ID of the group that the field belongs to
     */
    public $value_id;

    /**
     * @dbColumn option_id
     * @dbType integer
     * @dbOptions length = 11, notnull = false
     * @dbIndex true
     * @var string $option_id - the id of the option that was selected
     */
    public $option_id;

    /**
     * @dbColumn option_id_col
     * @dbType integer
     * @dbOptions length = 11, notnull = false
     * @dbIndex true
     * @var integer $option_id_col - the id of the option column that was selected for a 2D selection
     */
    public $option_id_col;

    /**
     * @dbColumn value_string
     * @dbType string
     * @dbOptions length = 1500, notnull = false
     * @var string $description - The description of the field
     */
    public $value_string;

    /**
     * @dbColumn value_string_col
     * @dbType string
     * @dbOptions length = 1500, notnull = false
     * @var string $value_string_col - The value of the field
     */
    public $value_string_col;

    /**
     * @dbColumn value_number
     * @dbType decimal
     * @dbOptions precision = 10, scale = 0, notnull = false
     * @var double $value_number - Number value of field
     */
    public $value_number;

    /**
     * @dbColumn value_date
     * @dbType datetime
     * @dbOptions notnull = false
     * @var \DateTime $value_date - Date value of field
     */
    public $value_date;

    /**
     * @dbColumn value_text
     * @dbType text
     * @dbOptions notnull = false
     * @var string $value_text - Text value of the field
     */
    public $value_text;

    /**
     * @dbColumn value_boolean
     * @dbType boolean
     * @dbOptions length = 1, notnull = false
     * @var string $value_boolean - Boolean value of field
     */
    public $value_boolean;

}
