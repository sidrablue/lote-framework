<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\CustomField;

/**
 * Base class for companies
 */
class Company extends CustomField
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__company';


    /**
     * @var int $id - The companies unique ID
     */
    public $id;

    /**
     * @var string $name - The companies name
     */
    public $name;

    /**
     * @var string $email - The companies contact email address
     */
    public $email;

    /**
     * @var string $phone - The companies contact phone number
     */
    public $phone_number;

    /**
     * @var string $street - The first line of the companies street address
     */
    public $street;

    /**
     * @var string $street2 - The first line of the companies street address
     */
    public $street2;

    /**
     * @var string $city - The city of the companies street address
     */
    public $city;

    /**
     * @var string $state - The state of the companies street address
     */
    public $state;

    /**
     * @var string $postcode - The postcode of the companies street address
     */
    public $postcode;

    /**
     * @var string $country - THe country of the companies street address
     */
    public $country;

    /**
     * @var boolean $active - True if the company is active
     */
    public $active;

    /**
     * @var string $business_number - The business number of the company
     */
    public $business_number;

    /**
     * @var string $business_number_type - The type of business that the company has
     */
    public $business_number_type;

    /**
     * @var String $trading_name - The trading name of the company
     */
    public $trading_name;

    /**
     * @var boolean $isAudited
     * True if this entity is audited
     * */
    protected $isAudited = true;

    /**
     * Load a company by their name
     * @param string $name
     * @return boolean
     */
    public function loadByCompanyName($name)
    {
        return $this->loadByField('name', $name);
    }

    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
}