<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for all entities
 */
class Dispatch extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__dispatch';

    /**
     * @var string $reference - the reference of the current job
     */
    public $reference;

    /**
     * @var int $process_id - the system process id of the current job
     */
    public $process_id;

    /**
     * @var string $status - the status of the job
     */
    public $status;

    /**
     * @var string $comment - A comment of the job
     */
    public $comment;

    /**
     * @var \DateTime $completed - the time that this job completed
     */
    public $completed;

}
