<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;
use SidraBlue\Util\Time;

/**
 * Base class for all entities
 */
class DispatchAction extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__dispatch_action';

    /**
     * @var int $dispatch_id - the ID of the dispatch
     */
    public $dispatch_id;

    /**
     * @var string $reference - the status of the action
     */
    public $reference;

    /**
     * @var string $object_ref - the object reference
     */
    public $object_ref;

    /**
     * @var string $object_id - The object ID
     */
    public $object_id;

    /**
     * @var string $status - the status of the action
     */
    public $status;

    /**
     * @var string $comment - A comment of the action
     */
    public $comment;

    /**
     * @var \DateTime $executed - the time that this action was executed
     */
    public $executed;

    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeInsert($data)
    {
        if(!$data['executed']) {
            $data['executed'] = Time::getUtcNow();
        }
        return $data;
    }

    /**
     * Load the last (most recent) execution of an action based on its action reference, object reference and object ID
     * @param $actionRef
     * @param string $objectRef
     * @param string $objectId
     * @return int|bool
     */
    public function loadLastExecution($actionRef, $objectRef = '', $objectId = '')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->getTableName(), 't')
            ->where('t.reference = :action_ref')
            ->setParameter("action_ref", $actionRef);
        if($objectRef) {
            $q->andWhere('t.object_ref = :object_ref')
                ->setParameter("object_ref", $objectRef);
        }
        if($objectId) {
            $q->andWhere('t.object_id = :object_id')
                ->setParameter("object_id", $objectId);
        }
        $q->orderBy("t.executed" ,"desc")
            ->setMaxResults(1);
        $s = $q->execute();
        if($row = $s->fetch(\PDO::FETCH_ASSOC)) {
            $this->setData($row);
        }
        return $this->id;
    }

}
