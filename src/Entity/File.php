<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;
use SidraBlue\Lote\Service\ImageServer;

/**
 * Base class for all entities
 */
class File extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__file';

    /**
     * @var string $store - the reference of the adapter used to store the file
     */
    public $store;

    /**
     * @var string $location_reference - the reference of the file in the given store
     */
    public $location_reference;

    /**
     * @var string $status - the status of the file
     */
    public $status;

    /**
     * @var boolean $is_public - the privacy status of the file
     */
    public $is_public;

    /**
     * @var string $object - the reference of the object that this file is for
     */
    public $object_ref;

    /**
     * @var int $object_id - the ID of the object that this file is for
     */
    public $object_id;

    /**
     * @var int $object_key - the key of the object if one is required, such as a field name
     */
    public $object_key;

    /**
     * @var int $sub_path - the sub path of the file if one is required
     */
    public $sub_path;

    /**
     * @var string $reference - the reference of the file
     */
    public $reference;

    /**
     * @var string $size - the size of the file
     */
    public $size;

    /**
     * @var int $filename - the ID of the group that the field belongs to
     */
    public $filename;

    /**
     * @var int $extension - the extension of the file as it was uploaded as
     */
    public $extension;

    /**
     * @var int $mime_type - the mime type of the file
     */
    public $mime_type;

    /**
     * @var int $file_type - the file type of the file
     */
    public $file_type;

    /**
     * @var string $checksum - the checksum of the file
     */
    public $checksum;
    /**
     * @var int $is_saved - true if the file has been saved to its applicable locations
     */
    public $is_saved;

    /**
     * @var string $lote_access - file access
     */
    public $lote_access;

    /**
     * @var boolean $isAudited
     * True if this entity is audited
     * */
    protected $isAudited = true;


    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeInsert($data)
    {
        if (!isset($data['sub_path']) || $data['sub_path'] == '') {
            $data['sub_path'] = '/';
        } elseif (isset($data['sub_path'])) {
            $data['sub_path'] = strtolower($data['sub_path']);
        }
        if (!isset($data['is_saved']) || $data['is_saved'] === null) {
            $data['is_saved'] = true;
        }
        return $data;
    }

    /**
     * Function to call before updating data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeUpdate($data)
    {
        if (!isset($data['sub_path']) || $data['sub_path'] == '') {
            $data['sub_path'] = '/';
        } elseif (isset($data['sub_path'])) {
            $data['sub_path'] = strtolower($data['sub_path']);
        }
        //check if the file content has updated?
        return $data;
    }

    /**
     * Load an entity by its reference
     * @access public
     * @param string $reference - the reference of the object
     * @return bool
     * */
    public function loadByReference($reference)
    {
        $reference = strtolower($reference);
        if ($this->getState()->getData()->hasValue("entity:sb__file", $reference)) {
            $this->setData($this->getState()->getData()->getValue("entity:sb__file", $reference));
            $result = $this->id;
        } elseif ($this->getState()->getRedisDbClient()->exists('entity:sb__file:' . $reference)) {
            $data = $this->getState()->getRedisDbClient()->get('entity:sb__file:' . $reference);
            $this->setData($data);
            $this->updateDataCache();
            $result = $this->id;
        } else {
            if ($result = $this->loadByField('reference', $reference)) {
                $this->getState()->getRedisDbClient()->set('entity:sb__file:' . $reference, $this->getData());
                $this->updateDataCache();
            }
        }
        return $result;
    }

    /**
     * Load an entity by its reference
     * @access public
     * @param string $locationReference - the location reference of the object
     * @return bool
     * */
    public function loadByLocationReference($locationReference)
    {
        $reference = strtolower($locationReference);
        if ($this->getState()->getData()->hasValue("entity:sb__file:location_reference", $reference)) {
            $this->setData($this->getState()->getData()->getValue("entity:sb__file", $reference));
            $result = $this->id;
        } elseif ($this->getState()->getRedisDbClient()->exists('entity:sb__file:location_reference:' . $reference)) {
            $data = $this->getState()->getRedisDbClient()->get('entity:sb__file:location_reference:' . $reference);
            $this->setData($data);
            $this->updateDataCache();
            $result = $this->id;
        } else {
            if ($result = $this->loadByField('location_reference', $reference)) {
                $this->getState()->getRedisDbClient()->set('entity:sb__file:location_reference:' . $reference, $this->getData());
                $this->updateDataCache();
            }
        }
        return $result;
    }

    /**
     * Load the latest non archived version of the entity by its ID by first retrieving its reference
     * @access public
     * @param string $id - the reference of the object
     * @return bool
     * */
    public function loadLatestById($id)
    {
        $result = false;
        $tempFile = new File($this->getState());
        if ($tempFile->load($id)) {
            if (!$tempFile->lote_deleted) {
                $this->setData($tempFile->getData());
                $result = $this->id;
            } else {
                $result = $this->loadByReference($tempFile->reference);
            }
        }
        return $result;
    }

    /**
     * Get the web URL of a file
     * @access public
     * @param int $width
     * @param int $height
     * @param int $size
     * @param int $timestamp
     * @return string
     * */
    public function getWebUrl($width = 0, $height = 0, $size = 0, $timestamp = 0)
    {
        $i = new ImageServer($this->getState());
        return $i->getImageUrl($this->reference, $width, $height, $size, $timestamp);
    }

    /**
     * Load an entity by its reference
     * @access public
     * @param string $objectRef
     * @param int $objectId
     * @param string $objectKey
     * @param string $filename
     * @param string $subPath
     * @return void
     */
    public function loadByObjectParams($objectRef, $objectId, $objectKey = '', $filename = '', $subPath = '')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->getTableName(), 't')
            ->where('t.object_ref = :object_ref')
            ->setParameter('object_ref', $objectRef);
        if($objectId) {
            $q->andWhere('t.object_id = :object_id')->setParameter('object_id', $objectId);
        }
        if ($objectKey) {
            $q->andWhere('t.object_key = :object_key')->setParameter('object_key', $objectKey);
        }
        if ($filename) {
            $q->andWhere('t.filename = :filename')->setParameter('filename', $filename);
        }
        if ($subPath && $subPath != '/') {
            $q->andWhere('t.sub_path = :sub_path')->setParameter('sub_path', strtolower(trim($subPath, '\\')));
        }
        $s = $q->execute();
        if ($row = $s->fetch(\PDO::FETCH_ASSOC)) {
            $this->setData($row);
            $this->updateDataCache();
        }
    }

    /**
     * Load an entity by its filename and reference
     * @access public
     * @param string $filename
     * @param string $objectRef
     * @param string $subPath
     * @return void
     */
    public function loadByFilename($filename, $objectRef, $subPath = '/')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->getTableName(), 't')
            ->where('t.object_ref = :object_ref')
            ->setParameter('object_ref', $objectRef);
        if ($filename) {
            $q->andWhere('t.filename = :filename')
                ->setParameter('filename', $filename);
        }
        if ($subPath && $subPath != '/') {
            $q->andWhere('t.sub_path = :sub_path')
                ->setParameter('sub_path', strtolower(trim('\\', $subPath)));
        }
        $q->orderBy('t.object_id', 'desc');
        $s = $q->execute();
        if ($row = $s->fetch(\PDO::FETCH_ASSOC)) {
            $this->setData($row);
            $this->updateDataCache();
        }
    }

    /**
     * Delete this object
     * @param bool $strongDelete - Whether or not to delete the table object completely
     * @return boolean
     */
    public function delete($strongDelete = false)
    {
        $fileId = $this->id;
        $fileReference = $this->reference;
        $result = parent::delete($strongDelete);
        if ($fileId && $fileReference) {
            $vars = ['reference' => $this->getState()->accountReference];
            $vars['file_id'] = $fileId;
            $vars['file_reference'] = $fileReference;
            $this->getState()->getQueue()->add("media-uncache", '\Lote\Module\Media\Queue\Processor\UnCache', $vars);
        }
        return $result;
    }

    /**
     * Archive this file
     * @access public
     * @return void
     * @todo - put this in a sub folder instead
     * */
    public function archive()
    {
        if (true || $this->id && $this->status != 'archived') {
            $this->status = 'archived';
            $this->reference = $this->makeArchiveReference();
            $this->save();
        }
    }

    /**
     * Make a reference to use for archiving this current file
     * @access public
     * @return string
     * */
    public function makeArchiveReference()
    {
        $baseName = explode('.', basename($this->location_reference));
        count($baseName) ==1?$offset=0:$offset=count($baseName)-2;
        $baseName[$offset] .= uniqid("_");
        $baseName = implode(".", $baseName);
        return dirname($this->reference) . '/_archive/' . $baseName;
    }

}
