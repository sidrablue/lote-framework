<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for all entities
 */
class FileImage extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__file_image';

    /**
     * @var int $file_id - The ID of the image
     */
    public $file_id;

    /**
     * @var int $width
     */
    public $width;

    /**
     * @var int $height
     */
    public $height;

}
