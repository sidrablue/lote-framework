<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for all entities
 */
class FileRemote extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__file_remote';

    /**
     * @var string $store - the reference of the adapter used to store the file
     */
    public $store;

    /**
     * @var string $status - the status of the file
     */
    public $status;

    /**
     * @var string $object - the reference of the object that this file is for
     */
    public $object_ref;

    /**
     * @var int $object_id - the ID of the object that this file is for
     */
    public $object_id;

    /**
     * @var string $object_key - the key of the object if one is required, such as a field name
     */
    public $object_key;

    /**
     * @var string $source_path - the full path of the remote file
     */
    public $source_path;

    /**
     * @var string $embed_path - the embeddable path of the remote file
     */
    public $embed_path;

    /**
     * @var string $key_path - the id key of the remote file
     */
    public $key_path;

    /**
     * @var string $reference - the reference of the file
     */
    public $reference;

    /**
     * @var int $filename - the ID of the group that the field belongs to
     */
    public $filename;

    /**
     * @var int $mime_type - the mime type of the file
     */
    public $mime_type;

    /**
     * @var string $account_ref - the account reference used for this file
     */
    public $account_ref;

    /**
     * @var string $embed_code - the embed code that can be used to as a pre-generated embeddable iframe link
     */
    public $embed_code;

    /**
     * @var boolean $isAudited
     * True if this entity is audited
     * */
    protected $isAudited = true;

}
