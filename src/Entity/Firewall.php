<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Firewall Entity
 */
class Firewall extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__firewall';

    /**
     * @var string $object_ref - the object reference
     */
    public $object_ref;

    /**
     * @var string $object_id - The object ID
     */
    public $object_id;

    /**
     * @var string $field_ref - the field refrence
     */
    public $field_ref;

    /**
     * @var string $type - type
     */
    public $type;

    /**
     * @var string $ip_version - Ip version
     */
    public $ip_version;

    /**
     * @var string $ip_address - Ip address
     */
    public $ip_address;

    /**
     * @var string $ip_subnet - Ip subnet
     */
    public $ip_subnet;

}
