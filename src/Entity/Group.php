<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base entity for groups
 */
class Group extends Base
{

    const GROUP_TYPE_LIST = 'list';

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__group';

    /**
     * @var string $name - the group name
     */
    public $name;

    /**
     * @var string $description - The group description
     */
    public $description;

    /**
     * @var int $priority - The priority of the group
     */
    public $priority;

    /**
     * @var string $parent_id - The parent ID of the group
     */
    public $parent_id;

    /**
     * @var string $sort_order - The visual sort order
     */
    public $sort_order;

    /**
     * @var string $ldap_path - The LDAP path
     */
    public $ldap_path;

    /**
     * @var boolean $active - True if the group is active
     */
    public $active;

    /**
     * @var string $group_type - 'static' or 'query', which determines if the user list is fixed or determined via
     *                           a custom query
     */
    public $group_type = 'static';

    /**
     * @var string $kind - User group kind - either 'user' or 'admin'
     */
    public $kind;

    /**
     * @var string $weighting - Group weighting (affects sort order)
     */
    public $weighting;

    /**
     * @var boolean $is_public True if the group is publicly visible
     */
    public $is_public = 0;

    /**
     * @var boolean $is_parent_account_accessible True if the group is publicly visible
     */
    public $is_parent_account_accessible = 0;

    /**
     * @var int $query_id - The query ID to get the group members from, if the group type is 'query'
     */
    public $query_id;

    /**
 * @var int $validate_members - To check if the members need to be validated
 */
    public $validate_members = 0;

    /**
     * @var string $code - code
     */
    public $code;

    /**
     * @var int $lote_path - lote path
     */
    public $lote_path;

    /**
     * @var boolean $is_protected
     * True if this entity is deletion protected
     * */
    public $is_protected;

    /**
     * @var boolean $isAudited
     * True if this entity is audited
     * */
    protected $isAudited = true;


    public $dashboard_redirect;

    /**
     * @var boolean $is_broadcast_group - True if entity is a broadcast group
     */
    public $is_broadcast_group;

    /**
     * @var string $broadcast_ref - Broadcast reference
     */
    public $broadcast_ref;

    /**
     * @var string $list_sql - List SQL
     */
    public $list_sql;

    /**
     * @var \DateTime $members_generated_at - Last time that the members list of this group was generated
     */
    public $members_generated_at;

    /**
     * @var string $external_id - External ID
     */
    public $external_id;

    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeInsert($data)
    {
        if(!isset($data['parent_id']) || !$data['parent_id'] > 0 ) {
            $data['parent_id'] = 0;
        }
        if(!isset($data['active']) || $data['active'] !='0') {
            $data['active'] = 1;
        }
        return $data;
    }

}
