<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity\Group;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base entity for groups user totals
 */
class UserTotal extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__group_user_total';

    /**
     * @var string $group_id - The ID of hte group
     */
    public $group_id;

    /**
     * @var string $total - The parent users in the group
     */
    public $total = 0;

    /**
     * @var string $total_active - The total active users in the group
     */
    public $total_active = 0;

    /**
     * @var string $total_subscribed - The total subscribed users in the group
     */
    public $total_subscribed = 0;

    /**
     * @var string $total_active_subscribed - The total subscribed active users in the group
     */
    public $total_active_subscribed = 0;

    /**
     * @var string $total_subscribed_edm - The total subscribed users in the group with a valid email
     */
    public $total_subscribed_edm = 0;

    /**
     * @var string $total_subscribed_sms - The total subscribed users in the group with a valid sms
     */
    public $total_subscribed_sms = 0;

    /**
     * @var string $total_active_subscribed_edm - The total subscribed active users in the group with a valid email
     */
    public $total_active_subscribed_edm = 0;

    /**
     * @var string $total_active_subscribed_sms - The total subscribed active users in the group with a valid sms
     */
    public $total_active_subscribed_sms = 0;

    /**
     * @var string $total_unsubscribed_temporary - The total subscribed active users in the group
     */
    public $total_unsubscribed_temporary = 0;

    /**
     * @var string $total_active_subscribed - The total subscribed active users in the group
     */
    public $total_active_unsubscribed_temporary = 0;

    /**
     * @var string $total_subscribed_edm - The total subscribed users in the group with a valid email
     */
    public $total_unsubscribed_temporary_edm = 0;

    /**
     * @var string $total_subscribed_sms - The total subscribed users in the group with a valid sms
     */
    public $total_unsubscribed_temporary_sms = 0;

    /**
     * @var string $total_active_subscribed_edm - The total subscribed active users in the group with a valid email
     */
    public $total_active_unsubscribed_temporary_edm = 0;

    /**
     * @var string $total_active_subscribed_sms - The total subscribed active users in the group with a valid sms
     */
    public $total_active_unsubscribed_temporary_sms = 0;

    /**
     * @var string $total_active_subscribed - The total subscribed active users in the group
     */
    public $total_unsubscribed_permanent = 0;

    /**
     * @var string $total_active_subscribed - The total subscribed active users in the group
     */
    public $total_active_unsubscribed_permanent = 0;

    /**
     * @var string $total_subscribed_edm - The total subscribed users in the group with a valid email
     */
    public $total_unsubscribed_permanent_edm = 0;

    /**
     * @var string $total_subscribed_sms - The total subscribed users in the group with a valid sms
     */
    public $total_unsubscribed_permanent_sms = 0;

    /**
     * @var string $total_active_subscribed_edm - The total subscribed active users in the group with a valid email
     */
    public $total_active_unsubscribed_permanent_edm = 0;

    /**
     * @var string $total_active_subscribed_sms - The total subscribed active users in the group with a valid sms
     */
    public $total_active_unsubscribed_permanent_sms = 0;

    /**
     * Set the group_id of this entity
     * @access public
     * @param int $groupId
     * @return int
     * */
    public function setGroupId($groupId)
    {
        $this->group_id = $groupId;
        return $this->loadByField("group_id", $groupId);
    }

    /**
     * Set the group_id of this entity
     * @access public
     * @param array $data
     * @return array
     * */
    public function beforeInsert($data)
    {
        $this->getWriteDb()->delete($this->getTableName(), ['group_id' => $this->group_id]);
        if(!$data['total']) :  $data['total'] = 0; endif;
        if(!$data['total_active']) :  $data['total_active'] = 0; endif;
        if(!$data['total_subscribed']) :  $data['total_subscribed'] = 0; endif;
        if(!$data['total_active_subscribed']) :  $data['total_active_subscribed'] = 0; endif;
        if(!$data['total_subscribed_edm']) :  $data['total_subscribed_edm'] = 0; endif;
        if(!$data['total_subscribed_sms']) :  $data['total_subscribed_sms'] = 0; endif;
        if(!$data['total_active_subscribed_edm']) :  $data['total_active_subscribed_edm'] = 0; endif;
        if(!$data['total_active_subscribed_sms']) :  $data['total_active_subscribed_sms'] = 0; endif;
        if(!$data['total_unsubscribed_temporary']) :  $data['total_unsubscribed_temporary'] = 0; endif;
        if(!$data['total_active_unsubscribed_temporary']) :  $data['total_active_unsubscribed_temporary'] = 0; endif;
        if(!$data['total_unsubscribed_temporary_edm']) :  $data['total_unsubscribed_temporary_edm'] = 0; endif;
        if(!$data['total_unsubscribed_temporary_sms']) :  $data['total_unsubscribed_temporary_sms'] = 0; endif;
        if(!$data['total_active_unsubscribed_temporary_edm']) :  $data['total_active_unsubscribed_temporary_edm'] = 0; endif;
        if(!$data['total_active_unsubscribed_temporary_sms']) :  $data['total_active_unsubscribed_temporary_sms'] = 0; endif;
        if(!$data['total_unsubscribed_permanent']) :  $data['total_unsubscribed_permanent'] = 0; endif;
        if(!$data['total_active_unsubscribed_permanent']) :  $data['total_active_unsubscribed_permanent'] = 0; endif;
        if(!$data['total_unsubscribed_permanent_edm']) :  $data['total_unsubscribed_permanent_edm'] = 0; endif;
        if(!$data['total_unsubscribed_permanent_sms']) :  $data['total_unsubscribed_permanent_sms'] = 0; endif;
        if(!$data['total_active_unsubscribed_permanent_edm']) :  $data['total_active_unsubscribed_permanent_edm'] = 0; endif;
        if(!$data['total_active_unsubscribed_permanent_sms']) :  $data['total_active_unsubscribed_permanent_sms'] = 0; endif;
        return $data;
    }

}
