<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */


namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Class GroupOwner
 * @package SidraBlue\Lote\Entity
 */
class GroupOwner extends Base
{
    /**
     * @var string $tableName
     * The name of the table
     * */
    protected $tableName = 'sb__group_owners';

    /**
     * @var string $group_id - group id
     */
    public $group_id;

    /**
     * @var string $user_id - user id
     */
    public $user_id;

}