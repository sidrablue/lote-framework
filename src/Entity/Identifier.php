<?php

/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

class Identifier extends Base
{

    /**
     * The reference of the object for which this identifier exists
     * @var string $object
     * */
    public $object;

    /**
     * The id of the object for which this identifier points to
     * @var string $object_id
     * */
    public $object_id;

    /**
     * The reference specifying a subtype for the object
     * @var string $reference
     * */
    public $reference;

    /**
     * The access for this identifier
     * @var string $access
     * */
    public $access;

    /**
     * The table name related to this model
     * @var string $tableName
     * */
    protected $tableName = 'sb__identifier';

    /**
     *   */
    public function save()
    {
        $result = 0;
        if(!($result = $this->exists(['object' => $this->object, 'object_id' => $this->object_id]))) {
            $result = parent::save();
        }
        return $result;
    }

}
