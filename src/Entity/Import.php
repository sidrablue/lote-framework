<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Model\Import as ImportModel;
use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for all entities
 */
class Import extends Base implements QueueJobInterface
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__import';

    /**
     * @var string $object_id - the reference id of the field
     */
    public $object_id;

    /**
     * @var string $object_ref - the reference of the field
     */
    public $object_ref;

    /**
     * @var int $file_id - the ID of the file that was uploaded for the import
     */
    public $file_id;

    /**
     * @var string $status - The name of the field
     */
    public $status;

    /**
     * @var string $subscribed_status - The subscription status of the import
     */
    public $subscribed_status;

    /**
     * @var boolean $skip_first_row - true to skip the first row of an import file
     */
    public $skip_first_row;

    /**
     * @var string $field_map - The field map for this import
     */
    public $field_map;

    /**
     * @var string $mode - The mode of the import, such as 'update', 'ignore', 'replace'
     */
    public $mode;

    /**
     * @var string $data - The list of misc data for import to pass to process row. Should be a json string
     */
    public $data;

    /**
     * @var int $total_processed - the total number of rows processed for the file
     */
    public $total_processed;

    /**
     * @var int $total_inserted - the total number of rows inserted for the file
     */
    public $total_inserted;

    /**
     * @var int $total_updated - the total number of rows updated for the file
     */
    public $total_updated;

    /**
     * @var int $total_skipped - the total number of rows that were skipped, generally due to being empty
     */
    public $total_skipped;

    /**
     * @var int $total_error - the total number of rows that resulted in errors
     */
    public $total_error;

    /**
     * @var int $total_ignored - the total number of rows ignored for the file
     */
    public $total_ignored;

    /**
     * @var int $total_lines - the total number of lines for the file
     */
    public $total_lines;

    /**
     * @var int $session_id - id of the session keeper session if it exists
     */
    public $session_id;

    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeInsert($data)
    {
        $this->defaultTo($data, 'total_processed', 0);
        $this->defaultTo($data, 'total_inserted', 0);
        $this->defaultTo($data, 'total_updated', 0);
        $this->defaultTo($data, 'total_ignored', 0);
        $this->defaultTo($data, 'total_skipped', 0);
        $this->defaultTo($data, 'total_error', 0);
        $this->defaultTo($data, 'total_lines', 0);
        return $data;
    }

    /**
     * Determine if a variable has been defined, and if not then default it to a specified value
     * @param array $data - the data of this entity
     * @param string $name - the name of the variable
     * @param string $default - the default of the variable
     * */
    private function defaultTo(&$data, $name, $default)
    {
        if(!isset($data[$name])) {
            $data[$name] = $default;
        }
    }


    /**
     * Check if this import is still incomplete
     * @access public
     * @return boolean
     * */
    public function isIncomplete()
    {
        return $this->status=='new' || $this->status=='processing' || $this->status=='suspended';
    }

    /**
     * Add to the inserted count for this import
     * @access public
     * @param boolean $clearError - true if this is a resubmission to the import and thus the removal of an error
     * @return void
     * */
    public function addInsertedCount($clearError = false)
    {
        $this->total_inserted++;
        if($clearError) {
            $this->total_error = max(0, $this->total_error - 1);
        }
    }

    /**
     * Add to the updated count for this import
     * @access public
     * @param boolean $clearError - true if this is a resubmission to the import and thus the removal of an error
     * @return void
     * */
    public function addUpdatedCount($clearError = false)
    {
        $this->total_updated++;
        if($clearError) {
            $this->total_error = max(0, $this->total_error - 1);
        }
    }

    /**
     * Add to the ignored count for this import
     * @access public
     * @param boolean $clearError - true if this is a resubmission to the import and thus the removal of an error
     * @return void
     * */
    public function addIgnoredCount($clearError = false)
    {
        $this->total_ignored++;
        if($clearError) {
            $this->total_error = max(0, $this->total_error - 1);
        }
    }

    /**
     * Add to the skipped count for this import
     * @access public
     * @param boolean $clearError - true if this is a resubmission to the import and thus the removal of an error
     * @return void
     * */
    public function addSkippedCount($clearError = false)
    {
        $this->total_skipped++;
        if($clearError) {
            $this->total_error = max(0, $this->total_error - 1);
        }
    }



    /**
     * QueueJobInterface Implementation
     */

    public function startJob()
    {
        // Set status using Queue status options
        $this->status = Queue::STATUS_PROCESSING;
        $this->save();
        // Enqueue job
        $m = new ImportModel($this->getState());
        $m->queueJob($this->id, $this->mode);
    }

    public function stopJob()
    {
        // Set status using Queue status options
        $this->status = Queue::STATUS_SUSPENDED;
        $this->save();
    }

    public function cancelJob()
    {
        // Set status using Queue status options
        $this->status = Queue::STATUS_CANCELLED;
        $this->save();
    }

}
