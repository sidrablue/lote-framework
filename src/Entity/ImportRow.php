<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for all entities
 */
class ImportRow extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__import_row';

    /**
     * @var string $import_id - the import ID
     */
    public $import_id;

    /**
     * @var string $status - The name of the field
     */
    public $status;

    /**
     * @var string $note - The note
     */
    public $note;

    /**
     * @var string $data - The data of the row
     */
    public $data;

    /**
     * Remove the user_id field from this table as it is not required
     * @param array $data
     * @return array
     * */
    protected function beforeInsert($data)
    {
        unset($data['user_id']);
        return $data;
    }

}
