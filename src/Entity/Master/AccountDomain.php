<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */


namespace SidraBlue\Lote\Entity\Master;


use SidraBlue\Lote\Object\Entity\Base;

/**
 * Class AccountDomain
 * @package SidraBlue\Lote\Entity\Master
 */
class AccountDomain extends Base
{
    /**
     * @var string $tableName
     */
    protected $tableName = 'sb_master_account_domain';

    /**
     * @var string $account_ref - Account reference
     */
    public $account_ref;

    /**
     * @var string $domain - Domain
     */
    public $domain;

    /**
     * @var bool $is_active - True if entry is active
     */
    public $is_active;
}