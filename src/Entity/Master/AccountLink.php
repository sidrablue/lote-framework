<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity\Master;

use SidraBlue\Lote\Object\Entity\BaseMaster;

/**
 * Base class for all entities
 */
class AccountLink extends BaseMaster
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb_master_account_link';

    /**
     * @var string $reference_from - the account "from" reference
     */
    public $reference_from;

    /**
     * @var string $reference_to - the account "to" reference
     */
    public $reference_to;

    /**
     * @var string $link_type - the link type
     */
    public $link_type;

}
