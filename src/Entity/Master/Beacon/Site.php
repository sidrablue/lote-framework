<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity\Master\Beacon;

use SidraBlue\Lote\Object\Entity\BaseMaster;


class Site extends BaseMaster
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb_beacon_site';

    /**
     * @var string $account_ref - account
     */
    public $account_ref;

    /**
     * @var string $name - name
     */
    public $name;

    /**
     * @var string $description - description
     */
    public $description;

    /**
     * @var string $location - location
     */
    public $location;

    /**
     * @var string $latitude - latitude
     */
    public $latitude;

    /**
     * @var string $longitude - longitude
     */
    public $longitude;

    /**
     * @var int $active - active
     */
    public $active;

    /**
     * @var int $parent_id - parent id
     */
    public $parent_id;

    /**
     * @var int $email - email
     */
    public $email;

    /**
     * @var int $phone - phone
     */
    public $phone;
    
    /**
     * @var string $lote_path - path
     */
    public $lote_path;

    /**
     * @var string $url - Site's Website URL 
     */
    public $url;

    /**
     * @var string $url - Site's Facebook Page ID / URL path
     */
    public $facebook_id;

    /**
     * @var int $allow_fb_check_in - Flag for whether to allow Facebook Check-ins at this Site.
     */
    public $allow_fb_check_in;

    /**
     * Only allow a $allow_fb_check_in to be true if a Facebook ID is present
     *
     * @param $data
     * @return bool
     */
    private function validate($data)
    {
        if ($data['allow_fb_check_in'] && empty($data['facebook_id'])) {
            throw new \InvalidArgumentException("Facebook Check-in cannot be allowed if no Facebook ID is provided.");
        }
        return true;
    }
    protected function beforeInsert($data)
    {
        $this->validate($data);
        return parent::beforeInsert($data);
    }
    protected function beforeUpdate($data)
    {
        $this->validate($data);
        return parent::beforeUpdate($data);
    }

}