<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity\Master\Beacon;

use SidraBlue\Lote\Object\Entity\BaseMaster;


class SiteCategory extends BaseMaster
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb_beacon_site_category';

    /**
     * @var string $name - name
     */
    public $name;

    /**
     * @var string $description - description
     */
    public $description;

    /**
     * @var int $active - active
     */
    public $active;

    /**
     * @var int $parent_id - parent in
     */
    public $parent_id;

}