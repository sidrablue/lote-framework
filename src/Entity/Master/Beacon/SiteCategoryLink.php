<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity\Master\Beacon;

use SidraBlue\Lote\Object\Entity\BaseMaster;


class SiteCategoryLink extends BaseMaster
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb_beacon_site_category_link';

    /**
     * @var int $site_id - site id
     */
    public $site_id;

    /**
     * @var int $category_id - category id
     */
    public $category_id;

    /**
     * @var int $sort_order - sort order
     */
    public $sort_order;

    /**
     * @var int $is_primary - primary
     */
    public $is_primary;

}