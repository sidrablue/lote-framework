<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity\Master\Beacon;

use SidraBlue\Lote\Object\Entity\BaseMaster;


class SiteImage extends BaseMaster
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb_beacon_site_image';

    /**
     * @var int $site_id - site id
     */
    public $site_id;

    /**
     * @var string $name - name
     */
    public $name;

    /**
     * @var string $description - description
     */
    public $description;

    /**
     * @var int $url - url
     */
    public $url;

    /**
     * @var int $sort_order - sort order
     */
    public $sort_order;

    /**
     * @var int $is_primary - primary
     */
    public $is_primary;

}