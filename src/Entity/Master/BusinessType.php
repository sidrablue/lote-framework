<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity\Master;

use SidraBlue\Lote\Object\Entity\BaseMaster;

/**
 * Base class for all entities
 */
class BusinessType extends BaseMaster
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__business_type';

    /**
     * @var integer $id
     */
    public $id;

    /**
     * @var integer $industry_type_id
     */
    public $industry_type_id;

    /**
     * @var string $value
     */
    public $value;

}
