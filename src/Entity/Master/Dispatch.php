<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity\Master;

use SidraBlue\Lote\Object\Entity\BaseMaster;

/**
 * Base class for all entities
 */
class Dispatch extends BaseMaster
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb_master_dispatch';

    /**
     * @var int $account_ref - the account reference
     */
    public $account_ref;

    /**
     * @var int $object_ref - the object reference
     */
    public $object_ref;

    /**
     * @var string $message - the message
     */
    public $message;

    /**
     * @var int $object_id - the object ID
     */
    public $object_id;

    /**
     * @var int $account_ref - the account reference
     */
    public $parent_account_ref;

    /**
     * @var int $object_parent_ref - the parent object's  reference
     */
    public $object_parent_ref;

    /**
     * @var int $object_parent_id - the parent object's  ID
     */
    public $object_parent_id;

    /**
     * @var bool $priority - true if this is a priority
     */
    public $priority;

    /**
     * @var \DateTime $process_at - the time that this message should be processed
     */
    public $process_at;

    /**
     * @var string $status - the status of this message
     */
    public $status;

    /**
     * @var string $data - any additional data for the message
     */
    public $data;

    /**
     *
     * @param \Lote\System\Edm\Entity\Campaign\Campaign $c
     * @return void
     * */
    public function setFromCampaign($c, $priority = false) {
        $this->account_ref = $this->getState()->getSettings()->get('system.reference');
        $this->object_ref = 'sb_edm_campaign';
        $this->priority = $priority;
        $this->object_id = $c->id;
        if($c->scheduled) {
            $this->process_at = $c->scheduled;
        }
        $this->status = "new";
    }

    /**
     * Get a single object by specific fields
     *
     * @param array $fieldNameValues
     * @param bool $includeDeleted - Whether or not to include objects that have been deleted
     * @return boolean
     */
    public function loadByFields($fieldNameValues, $includeDeleted = false)
    {
        $q = $this->queryFromFields($fieldNameValues, $includeDeleted);

        $s = $q->execute();
        $data = $s->fetch(\PDO::FETCH_ASSOC);
        $this->setData($data);
        return $data;
    }

    /**
     * Load a queue entry by its object reference, id, and optional
     * @param string $accountRef
     * @param string $objectRef
     * @param int|string $objectId
     * @return int|bool
     */
    public function loadByAccountObjectAndId($accountRef, $objectRef, $objectId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("d.*")
            ->from("sb_master_dispatch", "d")
            ->where("d.account_ref = :account_ref")
            ->setParameter("account_ref", $accountRef)
            ->andWhere("d.object_ref = :object_ref")
            ->setParameter("object_ref", $objectRef)
            ->andWhere("d.object_id = :object_id")
            ->setParameter("object_id", $objectId)
            ->andWhere("lote_deleted is null");
        $res = $q->execute();
        if($res->rowCount()==1) {
            $data = $res->fetch(\PDO::FETCH_ASSOC);
            $this->setData($data);
        }
        return $this->id;
    }

}
