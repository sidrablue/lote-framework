<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity\Master;

use SidraBlue\Lote\Object\Entity\BaseMaster;
use SidraBlue\Lote\Entity\Master\BusinessType as BusinessTypeEntity;

/**
 * Base class for all entities
 */
class IndustryType extends BaseMaster
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__industry_type';

    /**
     * @var integer $id
     */
    public $id;

    /**
     * @var string $value
     */
    public $value;

    /**
     * Gets business types associated with this industry.
     *
     * @return array - Business Types (id=>value)
     */
    public function getBusinessTypes()
    {
        $businessDetails = [];
        if($this->id){
            $businessObj = new BusinessTypeEntity($this->getState());
            $data = $businessObj->listByField('industry_type_id', $this->id);
            foreach($data as $busi){
                $businessDetails[$busi['id']] = $busi['value'];
            }
        }
        return $businessDetails;
    }
}
