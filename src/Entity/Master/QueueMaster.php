<?php
/**
 * Created by PhpStorm.
 * User: Jono
 * Date: 18-Apr-16
 * Time: 2:11 PM
 */

namespace SidraBlue\Lote\Entity\Master;


use Lote\System\Edm\Entity\Campaign\Campaign;
use SidraBlue\Lote\Entity\Queue;
use SidraBlue\Util\Time;

class QueueMaster extends Queue
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb_master_queue';

    /**
     * @var string $account_ref - account reference
     */
    public $account_ref;

    /**
     * @var string $parent_account_ref - parent account reference
     */
    public $parent_account_ref;

    /**
     * @var string $parent_object_ref - parent object reference
     */
    public $parent_object_ref;

    /**
     * @var int $parent_object_id - parent object id
     */
    public $parent_object_id;

    /**
     * @var string $queue_class - Queue controller class
     */
    public $queue_class;

    /**
     * @var string $queue_args - Queue args
     */
    public $queue_args;


    /**
     * Define the read DB function so that it can be overwritten
     * @access protected
     * @return \Doctrine\DBAL\Connection
     * */
    protected function getReadDb()
    {
        return $this->getState()->getMasterDb();
    }

    /**
     * Define the write DB function so that it can be overwritten
     * @access protected
     * @return \Doctrine\DBAL\Connection
     * */
    protected function getWriteDb()
    {
        return $this->getState()->getMasterDb();
    }


    /**
     * Save or update the queue entry, taking into consideration if it is already there
     * */
    public function saveOrUpdate()
    {
        $params = [
            'account_ref' => $this->account_ref,
            'object_ref' => $this->object_ref,
            'object_id' => $this->object_id
        ];
        if ($this->isMasterPending()) {
            $this->getWriteDb()->delete($this->getTableName(), $params);
            $result = $this->save();
        } elseif ($this->isMasterQueued()) {
            $result = false;
        } else {
            $result = $this->save();
        }
        return $result;
    }

    private function isMasterPending()
    {
        return $this->isMasterQueuedStatus('0');
    }

    private function isMasterQueued()
    {
        return $this->isMasterQueuedStatus('1');
    }

    private function isMasterQueuedStatus($status) {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*')
            ->from($this->getTableName(), 't')
            ->where("account_ref = :account_ref")
            ->setParameter("account_ref", $this->account_ref)
            ->andWhere("object_ref = :object_ref")
            ->setParameter("object_ref", $this->object_ref)
            ->andWhere("object_id = :object_id")
            ->setParameter("object_id", $this->object_id)
            ->andWhere("queued = :queued_status")
            ->setParameter("queued_status", $status);
        return $q->execute()->fetch(\PDO::FETCH_ASSOC);
    }

    protected function beforeInsert($data)
    {
        if($data['queued']==null) {
            $data['queued'] = '0';
        }
        if($data['status']==null) {
            $data['status'] = 'pending';
        }
        return $data;
    }

    /**
     *
     * @param Campaign $campaignEntity
     * @param Campaign|bool $parentCampaignEntity
     * @return bool
     * */
    public function scheduleFromCampaign($campaignEntity, $parentCampaignEntity = false) {
        $this->account_ref = $this->getState()->accountReference;
        $this->object_ref = 'sb_edm_campaign';
        $this->object_id = $campaignEntity->id;
        $this->queue_args = json_encode(['reference' => $this->getState()->accountReference, 'campaign_id' => $campaignEntity->id]);
        $this->queue_class = '\Lote\System\Edm\Queue\Processor\Campaign\SenderNew';
        $this->queue_name = 'edm-sender';
        $this->title = "Campaign ({$campaignEntity->name})";
        $this->status = 'pending';
        $this->queued = '0';
        $this->process_at = $campaignEntity->scheduled;
        if($parentCampaignEntity) {
            $this->parent_account_ref = $parentCampaignEntity->getState()->accountReference;
            $this->parent_object_id = $parentCampaignEntity->id;
            $this->parent_object_ref = 'sb_edm_campaign';
        }
        $result = $this->saveOrUpdate();
        $this->getState()->getLoggers()->getAccountLogger("edm")->info("EDM Campaign scheduled for future sending", $campaignEntity->getData());
        return $result;
    }

    /**
     *
     * @param Campaign $c
     * @return bool
     * */
    public function setOrScheduleFromCampaign($c) {

        if($c->scheduled <= Time::getUtcNow('Y-m-d H:i:s')) {
            $data = ['reference' => $this->getState()->accountReference, 'campaign_id' => $c->id];
            $c->queue_id = $this->getState()->getQueue()->add('edm-sender', '\Lote\System\Edm\Queue\Processor\Campaign\SenderNew', $data);
            $result = $c->save();
            $this->getState()->getLoggers()->getAccountLogger("edm")->info("EDM Campaign queued for immediate processing", $c->getData());
        }
        else {
            $result = $this->scheduleFromCampaign($c);
        }
        return $result;
    }

}