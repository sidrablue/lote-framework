<?php
/**
 * Created by PhpStorm.
 * User: Jono
 * Date: 18-Apr-16
 * Time: 2:11 PM
 */

namespace SidraBlue\Lote\Entity\Master;


use Lote\System\Edm\Entity\Campaign\Campaign;
use SidraBlue\Lote\Entity\Tag;
use SidraBlue\Util\Time;

class TagMaster extends Tag
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__tag';

    /**
     * @var string $account_ref - the account reference
     */
    public $account_ref;

    /**
     * @var string $value - The string value of the tag
     */
    public $value;

    /**
     * @var string $display_value - The string value of the tag
     */
    public $display_value;

    /**
     * @var string $colour_code - colour code
     */
    public $colour_code;

    /**
     * @var string $weighting - weighting
     */
    public $weighting;

    /**
     * @var boolean $active - is active
     */
    public $active;

    /**
     * @var boolean $restricted - is restricted
     */
    public $restricted;

    /**
     * Define the read DB function so that it can be overwritten
     * @access protected
     * @return \Doctrine\DBAL\Connection
     * */
    protected function getReadDb()
    {
        return $this->getState()->getMasterDb();
    }

    /**
     * Define the write DB function so that it can be overwritten
     * @access protected
     * @return \Doctrine\DBAL\Connection
     * */
    protected function getWriteDb()
    {
        return $this->getState()->getMasterDb();
    }

}