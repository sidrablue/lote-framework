<?php
/**
 * Created by PhpStorm.
 * User: Jono
 * Date: 18-Apr-16
 * Time: 2:11 PM
 */

namespace SidraBlue\Lote\Entity\Master;


use Lote\System\Edm\Entity\Campaign\Campaign;
use SidraBlue\Lote\Entity\TagUsage;
use SidraBlue\Util\Time;

class TagUsageMaster extends TagUsage
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__tag_usage';

    /**
     * @var int $tag_id - The id of the sb__tag that this usage references
     */
    public $tag_id;

    /**
     * @var string $object_ref - The reference of the object for this usage
     */
    public $object_ref;

    /**
     * @var string $field_ref - The reference of the field in the object for this usage
     */
    public $field_ref;

    /**
     * @var int $object_id - The id of the object that this usage is for
     */
    public $object_id;


    /**
     * Define the read DB function so that it can be overwritten
     * @access protected
     * @return \Doctrine\DBAL\Connection
     * */
    protected function getReadDb()
    {
        return $this->getState()->getMasterDb();
    }

    /**
     * Define the write DB function so that it can be overwritten
     * @access protected
     * @return \Doctrine\DBAL\Connection
     * */
    protected function getWriteDb()
    {
        return $this->getState()->getMasterDb();
    }

}