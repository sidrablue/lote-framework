<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Class Message
 * @package SidraBlue\Lote\Entity
 */
class Message extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb_message';

    /**
     * @var string $name - name of message
     */
    public $name;

    /**
     * @var string $description - description of message
     */
    public $description;

    /**
     * @var string $reference - references
     */
    public $reference;

    /**
     * @var string $type - message type
     */
    public $type;

    /**
     * @var string $wildcard - The wildcard of the message
     */
    public $wildcard;

    /**
     * @var string $subject - The subject of the message
     */
    public $subject;

    /**
     * @var string $content - The body text of the message
     */
    public $content;

    /**
     * @var boolean $active - Whether or not the message is active
     */
    public $is_active;

    /**
     * @var string $object_ref - The reference object
     */
    public $object_ref;

    /**
     * @var int $object_id - The ID of the reference object
     */
    public $object_id;

}