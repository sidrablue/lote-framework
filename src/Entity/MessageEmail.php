<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */


namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for Message Emails
 * @package SidraBlue\Lote\Entity
 * @dbEntity true
 */
class MessageEmail extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb_message_email';

    /**
     * @dbColumn email_type
     * @dbType string
     * @dbOptions notnull = true
     * @dbIndex false
     * @var string $email_type - the email type
     */
    public $email_type;

    /**
     * @dbColumn object_id
     * @dbType integer
     * @dbOptions notnull = true
     * @dbIndex false
     * @var integer $object_id - the ID of the object
     */
    public $object_id;

    /**
     * @dbColumn object_ref
     * @dbType string
     * @dbOptions notnull = true
     * @dbIndex false
     * @var string $object_ref - the reference object
     */
    public $object_ref;

    /**
     * @dbColumn data
     * @dbType text
     * @dbOptions notnull = false
     * @dbIndex false
     * @var string $data - the data
     */
    public $data;

    /**
     * @dbColumn processed
     * @dbType datetime
     * @dbOptions notnull = false
     * @dbIndex false
     * @var string $processed - the date processed
     */
    public $processed;

    /**
     * @dbColumn scheduled
     * @dbType datetime
     * @dbOptions notnull = false
     * @dbIndex false
     * @var string $scheduled - the scheduled date
     */
    public $scheduled;

    /**
     * @dbColumn note
     * @dbType text
     * @dbOptions notnull = false
     * @dbIndex false
     * @var string $note - the note
     */
    public $note;

}
