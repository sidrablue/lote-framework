<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;
use SidraBlue\Lote\Object\Entity\Base;

/**
 * Note base class
 *
 * @package SidraBlue\Lote\Entity
 */
class Note extends Base
{

    protected $tableName = 'sb__note';

    /**
     * @var int $id - The ID of the note
     */
    public $id;

    /**
     * @var int $parent_id - The ID of the parent note
     */
    public $parent_id;

    /**
     * @var string $object_ref - The reference object
     */
    public $object_ref;

    /**
     * @var int $object_id - The ID of the reference object
     */
    public $object_id;

    /**
     * @var string $subject - The subject of the note
     */
    public $subject;

    /**
     * @var string $reference - Search or report references
     */
    public $reference;

    /**
     * @var string $summary - A summary of the note
     */
    public $summary;

    /**
     * @var string $content - The body text of the note
     */
    public $content;

    /**
     * @var string $attendees_text - The list of attendees
     */
    public $attendees_text;

    /**
     * @var boolean $active - Whether or not the note is active
     */
    public $active;

    /**
     * @var boolean $has_follow_up - Whether or not the note has a follow up date
     */
    public $has_follow_up;

    /**
     * @var boolean $confidential - True if the file is confidential
     */
    public $confidential;

    /**
     * @var \DateTime $date - date
     */
    public $date;

    /**
     * @var \DateTime $follow_up_date - Follow up date
     */
    public $follow_up_date;

    /**
     * @var string $type - Note type
     */
    public $type;

}