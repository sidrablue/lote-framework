<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Notification base class
 *
 * @package SidraBlue\Lote\Entity
 */
class Notification extends Base
{

    protected $tableName = 'sb__notification';

    /**
     * @var int $id - The ID of the notification
     */
    public $id;

    /**
     * @var int $parent_id - The ID of the parent notification
     */
    public $parent_id;

    /**
     * @var string $object_ref - The reference object
     */
    public $object_ref;

    /**
     * @var int $object_id - The ID of the reference object
     */
    public $object_id;

    /**
     * @var int $user_id - the ID of the user that this notification is for
     */
    public $user_id;

    /**
     * @var string $subject - The subject of the notification
     */
    public $subject;

    /**
     * @var string $description - The $description of the notification
     */
    public $description;

    /**
     * @var string $level - level of the notification, e.g. info, error, warning, alert, critical, etc
     */
    public $level;

    /**
     * @var string $view_url - The URL at which to find more information
     */
    public $view_url;

    /**
     * @var string $download_url - The download URL for the attached file
     */
    public $download_url;

    /**
     * @var \DateTime $date - date that the notification was read
     */
    public $date_read;

    /**
     * @var bool $confirmation_required - true if a confirmation is required for this notifcation
     */
    public $confirmation_required;

    /**
     * @var \DateTime $date_confirmed - date that the notification was confirmed
     */
    public $date_confirmed;

    /**
     * @var string $data - Any data for this notification
     */
    public $data;

    /**
     * Only allow a notification if it has a valid user ID and subject
     *
     * @param $data
     * @return bool
     */
    private function validate($data)
    {
        if (!$this->subject && (isset($data['subject']) && !$data['subject']) && (!$data || !isset($data['subject']))) {
            throw new \InvalidArgumentException("Notifications require a subject");
        }
        return true;
    }
    protected function beforeInsert($data)
    {
        $this->validate($data);
        return parent::beforeInsert($data);
    }
    protected function beforeUpdate($data)
    {
        $this->validate($data);
        return parent::beforeUpdate($data);
    }

}
