<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for a process
 * @package Lote\System\Crm\Entity
 */
class Payment extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__payment';

    /**
     * @var string $success - success
     */
    public $success;

    /**
     * @var double $subtotal - subtotal
     */
    public $subtotal;

    /**
     * @var double $shipping - shipping
     */
    public $shipping;

    /**
     * @var double $tax - tax
     */
    public $tax;

    /**
     * @var double $total - total
     */
    public $total;

    /**
     * @var string $gateway - gateway
     */
    public $gateway;

    /**
     * @var string $status - status
     */
    public $status;

    /**
     * @var string $message - message
     */
    public $message;

    /**
     * @var string $receipt - receipt
     */
    public $receipt;

    /**
     * @var string $po_number - po_number
     */
    public $po_number;

    /**
     * @var string $reference - reference
     */
    public $reference;

    /**
     * @var string $reference_id - reference_id
     */
    public $reference_id;

    /**
     * @var string $data - data
     */
    public $data;


}