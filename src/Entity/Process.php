<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for a process
 * @package Lote\System\Crm\Entity
 */
class Process extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__process';

    /**
     * @var string $name - The process name
     */
    public $name;

    /**
     * @var string $summary - Summary
     */
    public $summary;

    /**
     * @var string $description - Description
     */
    public $description;


}