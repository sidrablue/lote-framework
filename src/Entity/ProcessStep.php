<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for a step
 * @package Lote\System\Crm\Entity
 */
class ProcessStep extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__process_step';

    /**
     * @var int $process_id- process id
     */
    public $process_id;

    /**
     * @var int $user_id- user id
     */
    public $user_id;

    /**
     * @var string $name - The step name
     */
    public $name;

    /**
     * @var string $type - Type
     */
    public $type;

    /**
     * @var string $description - Description
     */
    public $description;

    /**
     * @var int $group_id - group id
     */
    public $group_id;

}