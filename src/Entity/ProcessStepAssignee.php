<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;


class ProcessStepAssignee extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__process_step_assignee';

    /**
     * @var string $step_id- step id
     */
    public $step_id;

    /**
     * @var string $reference_id - The reference
     */
    public $reference_id;

    /**
     * @var string $type - The type
     */
    public $type;

}