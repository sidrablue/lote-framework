<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;


use SidraBlue\Lote\Object\Entity\Base;

class Query extends Base
{

    public $clauseOptions =
        [
            'string' =>
                [
                    'begins_with' => 'Begins With',
                    'ends_with' => 'Ends With',
                    'like' => 'Contains',
                    'equals' => 'Matches',
                    'not_equals' => 'Does Not Match',
                ],
            'boolean' =>
                [
                    'is_true' => 'Is True',
                    'is_false' => 'Is False'
                ],
            'int' =>
                [
                    'equals' => 'Equal To',
                    'not_equals' => 'Not Equal To',
                    'greater_than' => 'Greater Than',
                    'less_than' => 'Less Than',
                ],
            'date' =>
                [
                    'equals' => 'Equal To',
                    'not_equals' => 'Not Equal To',
                    'greater_than' => 'Greater Than',
                    'less_than' => 'Less Than',
                ],
            'array' =>
                [
                    'in' => 'In',
                    'not_in' => 'Not In'
                ]
        ];

    /**
     * @var string tableName - Name of database table
     */
    protected $tableName = 'sb__query';

    /**
     * @var int $id - Query ID
     */
    public $id;

    /**
     * @var string $name - Name
     */
    public $name;

    /**
     * @var string $description - Description
     */
    public $description;

    /**
     * @var string $object_ref - Name of reference object
     */
    public $object_ref;

    /**
     * @var int $object_id - ID of reference object
     */
    public $object_id;

    /**
     * @var string $logic - Logic type of the query
     */
    public $logic;

    /**
     * @access public
     * @var int $lote_access - object access level
     */
    public $lote_access;

    /**
     * @access public
     * @var /DateTime $lote_created - date and time of query creation
     */
    public $lote_created;

    /**
     * @access public
     * @var /DateTime $lote_updated - date and time of last update
     */
    public $lote_updated;

    /**
     * @access public
     * @var /DateTime $lote_author_id - user id of author
     */
    public $lote_author_id;

    /**
     * On create event handler to add in an additional field to ignore
     * @access protected
     * @return void
     * */
    protected function beforeCreate()
    {
        $this->privateProperties[] = 'clauseOptions';
    }
}
