<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;
use SidraBlue\Lote\Entity\User as UserEntity;

class Queue extends Base
{
    const STATUS_COMPLETED = 'completed';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_FAILED = 'failed';
    const STATUS_PENDING = 'pending';
    const STATUS_PROCESSING = 'processing';
    const STATUS_SUSPENDED = 'suspended';
    const STATUS_EMPTY = 'empty';

    protected $tableName = 'sb__queue';

    /**
     * @var string $queue_name - Queue name
     */
    public $queue_name;

    /**
     * @var string $object_ref - object reference
     */
    public $object_ref;

    /**
     * @var int $object_id - object id
     */
    public $object_id;

    /**
     * @var string $reference - account reference
     */
    public $reference;

    /**
     * @var string $title - queue job title
     */
    public $title;

    /**
     * @var int $queued -
     */
    public $queued;

    /**
     * @var string $status - status
     */
    public $status;

    /**
     * @var string $status_message - status
     */
    public $status_message;

    /**
     * @var string $status_data - status
     */
    public $status_data;

    /**
     * @var string $percent_completed - job process progress as a percentage
     */
    public $percent_completed;

    /**
     * @var string $process_at -
     */
    public $process_at;

    /**
     * @var string $process_started - datetime of process start
     */
    public $process_started;

    /**
     * @var string $process_completed - datetime of process completion
     */
    public $process_completed;

    /**
     * @var string $data - data
     */
    public $data;

    /**
     * Begin processing a pending job
     * @return bool|int
     */
    public function begin()
    {
        if ($this->status === $this::STATUS_PENDING) {
            $this->status = $this::STATUS_PROCESSING;
            $this->percent_completed = 0;
            $this->process_started = date('c');
            $result = $this->save();
        } else {
            $result = false;
        }
        return $result;
    }

    /**
     * Resume a suspended job
     * @return int
     */
    public function resume()
    {
        $this->status = $this::STATUS_PROCESSING;
        $result = $this->save();
        return $result;
    }

    /**
     * Suspend a job running job
     * @return int
     */
    public function suspend()
    {
        $this->status = $this::STATUS_SUSPENDED;
        $result = $this->save();
        return $result;
    }

    /**
     * Mark a job as completed
     * @return int
     */
    public function complete()
    {
        $this->status = $this::STATUS_COMPLETED;
        $this->percent_completed = 100;
        $this->process_completed = date('c');
        return $this->save();
    }

    /**
     * Cancel a job permanently
     * @return int
     */
    public function cancel()
    {
        $this->status = $this::STATUS_CANCELLED;
        return $this->save();
    }

    /**
     * Mark a job as failed
     * @return int
     */
    public function fail($errorMsg = null)
    {
        $this->status = $this::STATUS_FAILED;
        $this->status_data = $errorMsg;
        $this->process_completed = date('c');
        return $this->save();
    }

    /**
     * @return bool - is pending
     */
    public function isPending() { return $this->status == $this::STATUS_PENDING; }

    /**
     * @return bool - is suspended
     */
    public function isSuspended() { return $this->status == $this::STATUS_SUSPENDED; }

    /**
     * @return bool - is completed
     */
    public function isCompleted() { return $this->status == $this::STATUS_COMPLETED || $this->percent_completed==100; }

    /**
     * Load a queue entry by its object reference, id, and optional
     * @param string $objectRef
     * @param int|string $objectId
     * @param bool $reference
     * @return int|bool
     */
    public function loadByObjectRefAndId($objectRef, $objectId, $reference = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("q.*")
            ->from("sb__queue", "q")
            ->where("q.object_ref = :object_ref")
            ->setParameter("object_ref", $objectRef)
            ->andWhere("object_id = :object_id")
            ->setParameter("object_id", $objectId)
            ->andWhere("lote_deleted is null");
        if($reference) {
            $q->andWhere("q.reference = :reference")->setParameter("reference", $reference);
        }
        $res = $q->execute();
        if($res->rowCount()==1) {
            $data = $res->fetch(\PDO::FETCH_ASSOC);
            $this->setData($data);
        }
        return $this->id;
    }

    /**
     * Load the latest queue entry by its object reference, id, and optional
     * @param string $objectRef
     * @param int|string $objectId
     * @param bool $reference
     * @return int|bool
     */
    public function loadLatestByObjectRefAndId($objectRef, $objectId, $reference = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("q.*")
            ->from("sb__queue", "q")
            ->where("q.object_ref = :object_ref")
            ->setParameter("object_ref", $objectRef)
            ->andWhere("object_id = :object_id")
            ->setParameter("object_id", $objectId)
            ->andWhere("lote_deleted is null")
            ->orderBy('q.lote_created', 'desc');

        if($reference) {
            $q->andWhere("q.reference = :reference")->setParameter("reference", $reference);
        }

        $res = $q->execute();
        if($data = $res->fetch(\PDO::FETCH_ASSOC)) {
            $this->setData($data);
        }
        return $this->id;
    }

    protected function beforeInsert($data)
    {
        $data['percent_completed'] = 0;
        return parent::beforeInsert($data);
    }

    /**
     * Return true if queue has admin level permissions
     *
     * @access public
     * @return bool
     */
    public function isAdminQueue()
    {
        $isAdmin = false;
        if($this->lote_author_id) {
            $ue = new UserEntity($this->getState());
            if($ue->load($this->lote_author_id)) {
                $isAdmin = $ue->isAdmin();
            }
        }
        return $isAdmin;
    }
}