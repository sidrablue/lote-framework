<?php
/**
 * Created by PhpStorm.
 * User: Jono
 * Date: 3/16/2016
 * Time: 1:49 PM
 */

namespace SidraBlue\Lote\Entity;


class QueueJobFactory
{
    /**
     * @var array $referenceMap - Map of references to QueueJobInterface Implementations
     */
    private static $referenceMap = [
        'sb__import' => '\SidraBlue\Lote\Entity\Import'
    ];

    /**
     * Instantiate a QueueJobInterface implementation based on object ref and ID
     * @param $state
     * @param $objectRef
     * @param $objectId
     * @return QueueJobInterface|null
     */
    public static function createInstance($state, $objectRef, $objectId)
    {
        $jobObj = null;
        if (isset($objectRef) && self::$referenceMap[$objectRef] && isset($objectId)) {
            $className = self::$referenceMap[$objectRef];
            $jobObj = new $className($state, $objectId);
            // Only allow return of QueueJobInterface Implementation
            if (!($jobObj instanceof QueueJobInterface)) {
                unset($jobObj);
                $jobObj = null;
            }
        }
        return $jobObj;
    }
}