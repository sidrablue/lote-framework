<?php
/**
 * Created by PhpStorm.
 * User: Jono
 * Date: 3/16/2016
 * Time: 12:56 PM
 */

namespace SidraBlue\Lote\Entity;


interface QueueJobInterface
{
    /**
     * Actions to perform when a job is queued for processing
     * Should result in the job being re-added to the Resque queue
     * @return mixed
     */
    public function startJob();

    /**
     * Actions to perform when a job is suspended
     * @return mixed
     */
    public function stopJob();

    /**
     * Actions to perform when a job is cancelled
     * @return mixed
     */
    public function cancelJob();
}