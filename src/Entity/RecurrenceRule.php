<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use Recurr\Rule;
use Recurr\Transformer\ArrayTransformer;
use Recurr\Transformer\ArrayTransformerConfig;
use Recurr\Transformer\TextTransformer;
use SidraBlue\Lote\Entity\RecurrenceRuleEvent as RecurrenceRuleEventEntity;
use SidraBlue\Lote\Object\Entity\Base;
use SidraBlue\Lote\Service\Timezone;
use SidraBlue\Util\Time;

/**
 * Recurrence Entity
 */
class RecurrenceRule extends Base
{
    
    const VIRTUAL_LIMIT = 732;

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__recurrence_rule';
    /**
     * @var int $id - ID
     */
    public $id;

    /**
     * @var string $object_ref - The reference of the object for this recurrence rule
     */
    public $object_ref;

    /**
     * @var int $object_id - The id of the object that this recurrence rule is for
     */
    public $object_id;

    /**
     * @var string $timezone - timezone for this recurrence rule
     */
    public $timezone;

    /**
     * @var $start_time - Start time of broadcast period
     */
//    public $start_time;

    /**
     * @var $end_time - End time of broadcast period
     */
//    public $end_time;

    /**
     * @var $end_time_add_days - Days between day of start_time and day of end_time
     */
//    public $end_time_add_days;

    /**
     * @var \DateTime $release_date - Release date of this recurrence rule
     */
//    public $release_date;

    /**
     * @var \DateTime $start_date - Start date of this recurrence rule
     */
    public $start_date;

    /**
     * @var \DateTime $end_date - Date used to calculate duration of event (Duration = end_date - $start_date)
     */
    public $end_date;

    /**
     * @var string $freq - String indicating frequency of recurrence rule
     */
    public $freq;

    /**
     * @var int $interval - Count representing how often an occurrence date of the recurrence rule is considered
     */
    public $recur_interval;

    /**
     * @var boolean $ongoing - Flag for whether this rrule is ongoing i.e. it has no end.
     */
    public $ongoing;

    /**
     * @var \DateTime $until - An ending date beyond which no occurrences are considered. Mutually Exclusive to $count
     */
    public $until_date;

    /**
     * @var int $rule_count - A count of how many occurrences to generate. Mutually Exclusive to $until
     */
    public $recur_count;

    /**
     * @var string $by_second - Rule part that determines which seconds to recur on. Comma delimited list.
     */
    public $by_second;

    /**
     * @var string $by_minute - Rule part that determines which minutes to recur on. Comma delimited list.
     */
    public $by_minute;

    /**
     * @var string $by_hour - Rule part that determines which hours to recur on. Comma delimited list.
     */
    public $by_hour;

    /**
     * @var string $by_day - Rule part that determines which days of the week to recur on. Comma delimited list.
     */
    public $by_day;

    /**
     * @var string $by_month_day - Rule part that determines which days of the month to recur on. Comma delimited list.
     */
    public $by_month_day;

    /**
     * @var string $by_year_day - Rule part that determines which days of the year to recur on. Comma delimited list.
     */
    public $by_year_day;

    /**
     * @var string $by_week_number - Rule part that determines which weeks of the year to recur on. Comma delimited list.
     */
    public $by_week_number;

    /**
     * @var string $by_month - Rule part that determines which months to recur on. Comma delimited list.
     */
    public $by_month;

    /**
     * @var string $week_start - Rule part that determines which day of the week to start on
     */
    public $week_start;

    /**
     * @var string $by_set_position - Determines which occurrences to keep. Comma delimited list
     */
    public $by_set_position;

    /**
     * @var string $ex_dates - Dates to exclude from recurrence rule
     */
    public $ex_dates;

    /**
     * @var string $r_rule - The recurrence rule
     */
    public $r_rule;

    /**
     * @var string $r_string - The recurrence rule explained in natural language
     */
    public $r_string;

    /**
     * @var int $duration - Amount of time each generated event will last for in seconds
     */
    public $duration;

    /**
     * @var int $all_day - Flag indicating whether occurrences are all day.  Purely cosmetic.
     */
    public $all_day;


    /**
     * Upon save, automatically generates new recurrences in DB
     *
     * @return int - ID of table entry
     */
    public function save()
    {
        // Unset old Fields
        if(!empty($this->id)) {
            $this->unsetRulesInDB();
        }

        // Generate rec strings and set to entity fields for saving
        $rule = $this->getRule();
        $this->generateRecurrenceStrings($rule);

        // Save and set $this->id as side-effect
        $id = parent::save();

        // if save is successful, also generate new recurrences
        if (isset($id)) {
            $this->saveRecurrences($rule);
        }

        return $id;
    }

    /**
     * Deletes existing recurrences if any
     * then generates new recurrences
     * and saves to the database
     *
     * @param \Recurr\Rule|null $rule - optional supplied param to reduce overhead of recreating duplicate Rule objects
     * @return void
     * @throws \Recurr\Exception\MissingData
     */
    protected function saveRecurrences($rule = null)
    {
        if(!is_null($this->id)) {

            // Delete Existing Recurrences
            $this->deleteRecurrences();

            // Create new Recurrences
            $recurs = $this->generateRecurrences($rule);

            // Save to DB
            $eve = new RecurrenceRuleEvent($this->getState());
            foreach($recurs as $recur) {
                $eve->reset();
                if ($this->until_date) {
                    $eve->setDataFromRecurrence($recur, $this->id, $this->until_date);
                } else {
                    $eve->setDataFromRecurrence($recur, $this->id);
                }
                $eve->save();
            }
        }
    }

    /**
     * Unsets old values of rules from the DB to prevent them from affecting the new rule
     *
     * @access protected
     */
    protected function unsetRulesInDB()
    {
        $q = $this->getState()->getWriteDb()->createQueryBuilder();

        $q->update($this->tableName, 'rr')
            ->set('until_date', 'NULL')
            ->set('recur_count', 'NULL')
            ->set('ongoing', 'NULL')
            ->set('by_second', 'NULL')
            ->set('by_minute', 'NULL')
            ->set('by_hour', 'NULL')
            ->set('by_day', 'NULL')
            ->set('by_month_day', 'NULL')
            ->set('by_year_day', 'NULL')
            ->set('by_week_number', 'NULL')
            ->set('by_month', 'NULL')
            ->set('by_set_position', 'NULL')
            ->andWhere('rr.id = :id')
            ->setParameter('id', $this->id)
            ->execute();
    }

    protected function generateRecurrenceStrings($rule = null)
    {
        if(!is_null($rule)) {
            $rule = $this->getRule();
        }

        // Generate and set rrule string
        $this->r_rule = $rule->getString();

        // Generate and set human-readable string
        $t = new TextTransformer();
        $this->r_string = $t->transform($rule);
    }

    /**
     * Deletes this Rule and all of the Events linked to it from the DB
     *
     * @param bool $strongDelete
     */
    public function delete($strongDelete = false)
    {
        // First delete RecurrenceEvents belonging to this Rule
        $this->deleteRecurrences($strongDelete);

        // Then delete rule itself
        parent::delete($strongDelete);
    }

    /**
     * Deletes Recurrence Events belonging to this Rule
     */
    protected function deleteRecurrences($strongDelete=false)
    {
        $eve = new RecurrenceRuleEvent($this->getState());
        $data = $eve->listByField('rule_id', $this->id);

        if(is_array($data)) {
            foreach($data as $event) {
                $eve->loadByField('id', $event['id']);
                $eve->delete($strongDelete);
            }
        }
    }

    /**
     * Create Database compatible entity from Rule object.
     *
     * @param \Recurr\Rule $rule
     * @param string $object_ref - Reference table
     * @param int $object_id - Reference ID
     * @param int $id - Table ID
     */
    public function setDataFromRule($rule, $object_ref, $object_id, $id = null)
    {
        // Set ID if modifying an existing rule
        if(isset($id)) {
            $this->id = $id;
        }

        // Set linked object's table name
        $this->object_ref = $object_ref;

        // Set linked object id
        $this->object_id = $object_id;

        // Database entity dates always in UTC
        $this->timezone = 'UTC';
        $utctz = new \DateTimeZone('UTC');

        // Convert date to UTC
        $startDate = $rule->getStartDate();
        $startDate->setTimezone($utctz);
        $this->start_date = $startDate->format('Y-m-d H:i:s');

        // Must create new datetime obj because getEndDate will return a string if rule was constructed using
        $endDate = $rule->getEndDate();
        if(!is_null($endDate)) {
            // Convert date to UTC
            $endDate->setTimezone($utctz);
            $this->end_date = $endDate->format('Y-m-d H:i:s');

            // Set duration as number of seconds between End Date and Start Date
            $this->duration = $endDate->getTimestamp() - $startDate->getTimestamp();
        }

        /*$broadcastParams = $this->calculateBroadcastPeriodParameters($startDate->format('Y-m-d H:i:s'), $endDate->format('Y-m-d H:i:s'));
        $this->release_date = $broadcastParams['release_date'];
        $this->start_time = $broadcastParams['start_time'];
        $this->end_time = $broadcastParams['end_time'];
        $this->end_time_add_days = $broadcastParams['end_time_add_days'];*/

        // Until and Count cannot co-exist
        $untilDate = $rule->getUntil();
        if(!is_null($untilDate)) {
            // Convert date to UTC
            $untilDate->setTimezone($utctz);
            $this->until_date = $untilDate->format('Y-m-d H:i:s');
        } elseif (!is_null($rule->getCount())) {
            $this->recur_count = $rule->getCount();
        }

        // Set Frequency
        $this->freq = $rule->getFreqAsText();

        // Set Interval
        $this->recur_interval = $rule->getInterval();

        // Convert array of values to comma delimited string
        if(is_array($rule->getBySecond())) {
            $this->by_second = implode(",", $rule->getBySecond());
        }

        // Convert array of values to comma delimited string
        if(is_array($rule->getByMinute())) {
            $this->by_minute = implode(",", $rule->getByMinute());
        }

        // Convert array of values to comma delimited string
        if(is_array($rule->getByHour())) {
            $this->by_hour = implode(",", $rule->getByHour());
        }

        // Convert array of values to comma delimited string
        if(is_array($rule->getByDay())) {
            $this->by_day = implode(",", $rule->getByDay());
        }

        // Convert array of values to comma delimited string
        if(is_array($rule->getByMonthDay())) {
            $this->by_month_day = implode(",", $rule->getByMonthDay());
        }

        // Convert array of values to comma delimited string
        if(is_array($rule->getByYearDay())) {
            $this->by_year_day = implode(",", $rule->getByYearDay());
        }

        // Convert array of values to comma delimited string
        if(is_array($rule->getByWeekNumber())) {
            $this->by_week_number = implode(",", $rule->getByWeekNumber());
        }

        // Convert array of values to comma delimited string
        if(is_array($rule->getByMonth())) {
            $this->by_month = implode(",", $rule->getByMonth());
        }

        // Set week start
        $this->week_start = $rule->getWeekStart();

        // Set setpos
        // Convert array of values to comma delimited string
        if(is_array($rule->getBySetPosition())) {
            $this->by_set_position = implode(",", $rule->getBySetPosition());
        }

        // Convert array of values to comma delimited string
        // Conversion of ExclusionDates[] to string modified from SimShaun' Rule->getString method
        $exDates = $rule->getExDates();
        if(is_array($exDates)) {
            foreach ($exDates as $key => $exclusion) {
                $format = 'Ymd';
                if ($exclusion->hasTime) {
                    $format .= '\THis';
                    if ($exclusion->isUtcExplicit) {
                        $format .= '\Z';
                    }
                }
                $exDates[$key] = $exclusion->date->format($format);
            }
            $this->ex_dates = implode(',', $exDates);
        }

        $this->generateRecurrenceStrings($rule);
    }

    /**
     * Get Rule object from database entry
     *
     * @return \Recurr\Rule $rule - Rule constructed form database fields
     * @throws \Recurr\Exception\InvalidRRule
     */
    public function getRule()
    {
        if(!isset($this->week_start)) {
            $this->week_start = 'MO';
        }

        // Set Freq
        $rule['FREQ'] = strtoupper($this->freq);

        // Don't set both Until and Count
        if(!is_null($this->until_date)) {
            $rule['UNTIL'] = $this->until_date;
        } elseif(!is_null($this->recur_count)) {
            $rule['COUNT'] = $this->recur_count;
        }

        if(isset($this->recur_interval)) {
            $rule['INTERVAL'] = $this->recur_interval;
        }

        if(isset($this->by_second)) {
            $rule['BYSECOND'] = $this->by_second;
        }

        if(isset($this->by_minute)) {
            $rule['BYMINUTE'] = $this->by_minute;
        }

        if(isset($this->by_hour)) {
            $rule['BYHOUR'] = $this->by_hour;
        }

        if(isset($this->by_day)) {
            $rule['BYDAY'] = $this->by_day;
        }

        if(isset($this->by_month_day)) {
            $rule['BYMONTHDAY'] = $this->by_month_day;
        }

        if(isset($this->by_year_day)) {
            $rule['BYYEARDAY'] = $this->by_year_day;
        }

        if(isset($this->by_week_number)) {
            $rule['BYWEEKNO'] = $this->by_week_number;
        }

        if(isset($this->by_month)) {
            $rule['BYMONTH'] = $this->by_month;
        }

        if(isset($this->by_set_position)) {
            $rule['BYSETPOS'] = $this->by_set_position;
        }

        if(isset($this->week_start)) {
            $rule['WKST'] = $this->week_start;
        }

        if(isset($this->ex_dates)) {
            $rule['EXDATE'] = $this->ex_dates;
        }

//        $startAndEndDates = $this->calculateRecurrenceRuleStartAndEndDateTimes($this->release_date, $this->start_time, $this->end_time, $this->end_time_add_days);

        // Create the Rule
        $rule = new Rule(
            $rule,
            new \DateTime($this->start_date, new \DateTimeZone($this->timezone)),
            new \DateTime($this->end_date, new \DateTimeZone($this->timezone))
        );

        return $rule;
    }

    /**
     * Generates Recurrences with dates and times adhering to this Recurrence Rule
     *
     * @return \Recurr\RecurrenceCollection
     * @throws \Recurr\Exception\MissingData
     */
    public function generateRecurrences($rule = null, $virtualLimit = null, $timezone = null)
    {
        if(is_null($rule)) {
            $rule = $this->getRule();
        }
        if(is_null($timezone)){
            $timezone = $this->getState()->getSettings()->get('system.timezone', 'UTC');
        }

        $rule = $this->timezoneConvert($rule, $timezone);

        $config = new ArrayTransformerConfig();
        if($virtualLimit) {
            $config->setVirtualLimit($virtualLimit);
        }
        $transformer = new ArrayTransformer($config);
        $recurs = $transformer->transform($rule);

        return $recurs;
    }

    public function getNextOccurrence($fromDate = 'now')
    {
        $fromDate = new \DateTime($fromDate);
        $fromDate = $fromDate->format('Y-m-d h:i:s');
        $e = new RecurrenceRuleEvent($this->getState());
        $tableName = $e->getTableName();

        $query = "
            select e.*
            from $tableName e
            where e.end_date = (
              select min(v.end_date)
              from $tableName v
              where v.end_date > \"$fromDate\"
              and v.rule_id = $this->id
              and v.lote_deleted is null
            )
            and e.rule_id = $this->id
            and e.lote_deleted is null";

        return $this->getReadDb()->query($query)->fetch(\PDO::FETCH_ASSOC);
    }

    public function getLastOccurrence()
    {
        if($this->until_date){
            $result = $this->until_date;
        } elseif ($this->recur_count) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('max(e.end_date) as last_oc')
                ->from('sb__recurrence_rule_event', 'e')
                ->andWhere('e.rule_id = :id')
                ->setParameter('id', $this->id)
                ->andWhere('e.lote_deleted is null');
            $result = $q->execute()->fetch(\PDO::FETCH_ASSOC)['last_oc'];
        } elseif ($this->ongoing){
            $result = 'ongoing';
        }
        return $result;
    }

    public function getTimezoneAdjustedString()
    {
        $result = '';
        try {
            $tz = new Timezone($this->getState());
            date_default_timezone_set($tz->getDisplayTimezone());
            // Create copy to avoid altering the original
            $e = new RecurrenceRule($this->getState());
            $e->load($this->id);

            // Generate and set human-readable string
            $rule = $e->getRule();
            $t = new TextTransformer();
            $result = $t->transform($rule);
        } catch (\Exception $e) {
            // Do nothing
        } finally {
            date_default_timezone_set('UTC');
        }
        return $result;
    }

    /**
     * Convert timezone of a rules datetime fields
     *
     * @param \Recurr\Rule $rule
     * @param string $timezone - Destination Timezone
     */
    protected function timezoneConvert($rule, $timezone)
    {
        // Convert timezones of entity's datetimes
        $tz = new \DateTimeZone($timezone);

        $obj = $rule->getStartDate();
        $obj->setTimezone($tz);
        $rule->setStartDate($obj);

        $obj = $rule->getEndDate();
        if($obj) {
            $obj->setTimezone($tz);
            $rule->setEndDate($obj);
        }

        $obj = $rule->getUntil();
        if($obj) {
            $obj->setTimezone($tz);
            $rule->setUntil($obj);
        }

        $rule->setTimezone($timezone);

        return $rule;
    }

    public function calculateBroadcastPeriodParameters($startDateTime, $endDateTime)
    {
        $result = [];

        if (!($startDateTime instanceof \DateTime)) {
            $startDateTime = new \DateTime($startDateTime);
        }
        if (!($endDateTime instanceof \DateTime)) {
            $endDateTime = new \DateTime($endDateTime);
        }

        if (!is_null($startDateTime) && !is_null($endDateTime)) {
            /*$tz = new Timezone($this->getState());
            $startDateTime->setTimezone(new \DateTimeZone($tz->getDisplayTimezone()));
            $endDateTime->setTimezone(new \DateTimeZone($tz->getDisplayTimezone()));*/
            
            $result['release_date'] = $startDateTime->format('Y-m-d');
            $result['start_time'] = $startDateTime->format('H:i:s');
            $result['end_time'] = $endDateTime->format('H:i:s');
            $dateDiff = $startDateTime->diff($endDateTime);
            $result['end_time_add_days'] = $dateDiff->days;
        }

        return $result;
    }

    public function calculateRecurrenceRuleStartAndEndDateTimes($releaseDate, $startTime, $endTime, $endTimeAddDays = 0)
    {
        $result = [];
        
        $startDateTime = new \DateTime($releaseDate . " " . $startTime);
        /*$startDateTime = new \DateTime($releaseDate . " " . $startTime, new \DateTimeZone($tz->getDisplayTimezone()));
        $startDateTime->setTimezone(new \DateTimeZone('UTC'));*/
        $result['start_date'] = $startDateTime->format('Y-m-d H:i:s');

        $endDateTime = new \DateTime($releaseDate . " " . $endTime);
        /*$endDateTime = new \DateTime($releaseDate . " " . $endTime, new \DateTimeZone($tz->getDisplayTimezone()));
        $endDateTime->setTimezone(new \DateTimeZone('UTC'));*/
        $endDateTime->modify("+ ".$endTimeAddDays.' days');
        $result['end_date'] = $endDateTime->format('Y-m-d H:i:s');

        return $result;
    }

    /**
     * Returns an expiry date if one can be determined.
     * If it is detected that a true expiry date has not yet been generated, the limitReached flag is set to true.
     * If the rule is found to have ongoing scheduling, the ongoing flag is set and the date is omitted.
     *
     * @return array expiry_date -  Array containing expiry date and/or related flags.
     */
    public function getExpiryString()
    {
        $result = [];
        
        $ree = new RecurrenceRuleEventEntity($this->getState());

        if (!empty($this->until_date)) {
            $result['date'] = $this->until_date;
        } elseif ($this->ongoing) {
            $result['message'] = 'ongoing';
        } elseif ($this->recur_count > 0) {
            // Get end date of last recurrence and total count 
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('max(re.end_date) as rule_expiry, count(distinct re.id) as event_cnt')
                ->from($this->getTableName(), 'rr')
                ->innerJoin('rr', $ree->getTableName(), 're', 're.rule_id = rr.id')
                ->where('rr.id = :rule_id')
                ->setParameter('rule_id', $this->id)
                ->andWhere('rr.lote_deleted is null')
                ->andWhere('re.lote_deleted is null');
            $data = $q->execute()->fetch(\PDO::FETCH_ASSOC);

            if ($data['event_cnt'] == self::VIRTUAL_LIMIT) {
                // If count is at limit, it is probably awaiting generation of more events
                $result['message'] = 'More than 2 years away';
            } else {
                // Else, all the events have been generated and the end of the last event is the expiry
                $result['date'] = $data['rule_expiry'];
            }
        }

        return $result;
    }

    /**
     * Calculate the duration of events generated by this rule in hours and minutes
     * @return array
     */
    public function calculateEventDuration()
    {
        /*$addDays = $this->end_time_add_days;
        $addDays = empty($addDays) ? 0 : $addDays;

        $startTime = strtotime($this->start_time, 0);
        $endTime = strtotime($this->end_time, $addDays * 86400);*/

        $startTime = strtotime($this->start_date, 0);
        $endTime = strtotime($this->end_date, 0);
        
        $duration = $endTime - $startTime;

        $hours = intval($duration / 3600);
        $minutes = intval(($duration % 3600) / 60);
        
        return ['hours' => $hours, 'minutes' => $minutes];
    }
}
