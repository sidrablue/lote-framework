<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use Recurr\Recurrence;
use SidraBlue\Lote\Object\Entity\Base;

/**
 * Recurrence Rule Event Entity
 */
class RecurrenceRuleEvent extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__recurrence_rule_event';
    /**
     * @var int $id - ID
     */
    public $id;

    /**
     * @var int $rule_id - id of recurrence rule that generated this event
     */
    public $rule_id;

    /**
     * @var \DateTime $start_date - Date and time this event starts at
     */
    public $start_date;

    /**
     * @var \DateTime $end_date - Date and time this event ends at
     */
    public $end_date;

    /**
     * @param \Recurr\Recurrence $recurrence
     * @param int $ruleId
     * @param int $id
     */
    public function setDataFromRecurrence($recurrence, $ruleId, $ruleExpiry = null)
    {
        // ID set to existing ID for modifying or null for new RecurEvent
        $this->rule_id = $ruleId;

        $tz = new \DateTimeZone('UTC');
        $this->start_date = $recurrence->getStart()->setTimezone($tz)->format('Y-m-d H:i:s');
        $endDate = $recurrence->getEnd()->setTimezone($tz);

        // Do not allow generated events to overflow the expiry date
        if (!is_null($ruleExpiry)) {
            $ruleExpiry = new \DateTime($ruleExpiry);
            if ($ruleExpiry < $endDate) {
                $endDate = $ruleExpiry;
            }
        }
        $this->end_date = $endDate->format('Y-m-d H:i:s');
    }

    public function getRecurrence()
    {
        $recur = new Recurrence(
            new \DateTime($this->start_date, new \DateTimeZone('UTC')),
            new \DateTime($this->end_date, new \DateTimeZone('UTC'))
        );

        return $recur;
    }
}
