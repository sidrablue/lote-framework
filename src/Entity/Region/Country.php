<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity\Region;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Class Country
 * @package SidraBlue\Lote\Entity\Region
 * @dbEntity true
 */
class Country extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__region_country';

    /**
     * @var boolean $isAudited
     * True if this entity is audited
     * */
    protected $isAudited = true;

    /**
     * @dbColumn code
     * @dbType string
     * @dbOptions length = 32, notnull = false
     * @var string $code - code
     */
    public $code;

    /**
     * @dbColumn name
     * @dbType string
     * @dbOptions length = 100, notnull = false
     * @var string $name - name
     */
    public $name;

    /**
     * @dbColumn active
     * @dbType integer
     * @dbOptions length = 10, notnull = false
     * @var int $active - active
     */
    public $active;


}