<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;
use SidraBlue\Lote\Object\Entity\Base;

/**
 * Setting base entity
 *
 * @package SidraBlue\Lote\Entity
 */
class Setting extends Base
{

    protected $tableName = 'sb__setting';

    /**
     * @var int $id - The ID of the note
     */
    public $id;

    /**
     * @var int $parent_id - The ID of the parent note
     */
    public $parent_id;

    /**
     * @var int $site_id - The ID of the site that the setting is for
     */
    public $site_id;

    /**
     * @var string $app_ref - The reference of the app
     */
    public $app_ref;

    /**
     * @var string $setting_type - The setting type
     */
    public $setting_type;

    /**
     * @var string $reference - The setting reference
     */
    public $reference;

    /**
     * @var string $title - The setting title
     */
    public $title;

    /**
     * @var string $description - The setting description
     */
    public $description;

    /**
     * @var string $value_config - Configuration data for this field
     */
    public $value_config;

    /**
     * @var string $value_default - The default value
     */
    public $value_default;

    /**
     * @var string $value_custom - The custom value of the setting
     */
    public $value_custom;

    /**
     * @var boolean content_access - Whether or not the setting is available in the WYSIWYG editor
     */
    public $content_access;

    /**
     * @var boolean $hidden - Whether or not the setting is hidden from the user
     */
    public $hidden;

    /**
     * @var boolean $locked - Whether or not the setting is locked from editing
     */
    public $locked;

    /**
     * @var boolean $active - Whether or not the setting is active
     */
    public $active;

    /**
     * Overwrite the save function
     * @todo - implement duplicate check
     * @return int
     * */
    public function save()
    {
        //check if the setting exists, and if it doesn't then save it
        return parent::save();
    }

    /**
     * @return boolean
     * */
    public function isWritable()
    {
        $result = $this->getState()->getUser()->isAdmin() || $this->lote_access == 1;
        if(!$result) {
            $result = $this->getState()->getUser()->id && ($this->lote_access === '0' || $this->lote_access === 0);
            if(!$result && $this->lote_access == -1 && $this->id) {
                $q = $this->getReadDb()->createQueryBuilder();
                $q->select('count(*)')
                    ->from('sb__access_entity', 'sbe')
                    ->leftJoin('sbe', 'sb__group', 'sbe_g', 'sbe_g.id = sbe.access_type_id')
                    ->leftJoin('sbe_g', 'sb__user_group', 'sbe_u', 'sbe_u.group_id = sbe_g.id')
                    ->andWhere('sbe.object_ref = :table_name')
                    ->andWhere('sbe_u.user_id = :user_id')
                    ->andWhere('sbe.object_id = :entity_id')
                    ->andWhere('sbe.access_level = "write"');
                $q->setParameter('table_name', $this->getTableName());
                $q->setParameter('entity_id', $this->id);
                $q->setParameter('user_id', $this->getState()->getUser()->id);
                $result = $q->execute()->fetchColumn();
            }
        }
        return $result;
    }

}
