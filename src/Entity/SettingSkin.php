<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;
use SidraBlue\Lote\Object\Entity\Base;

/**
 * Setting skin base entity
 *
 * @package SidraBlue\Lote\Entity
 */
class SettingSkin extends Base
{

    protected $tableName = 'sb__setting_skin';

    /**
     * @var int $id - The ID of the setting
     */
    public $id;

    /**
     * @var string $skin_reference - The ID of the parent note
     */
    public $setting_reference;

    /**
     * @var int $site_id - The ID of the site that the setting is for
     */
    public $skin_id;

    /**
     * @var string $value_custom - The custom value of the setting
     */
    public $value_custom;

    /**
     * Load the value by its skin ID and reference
     * @param int $skinId
     * @param string $settingReference
     * @return int|boolean
     */
    public function loadBySkinAndReference($skinId, $settingReference)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*')
            ->from($this->getTableName(),'t')
            ->where('setting_reference = :setting_reference')
            ->setParameter('setting_reference', $settingReference)
            ->andWhere('skin_id = :skin_id')
            ->setParameter('skin_id', $skinId)
            ->andWhere('lote_deleted is null');
        $s = $q->execute();
        $this->setData($s->fetch(\PDO::FETCH_ASSOC));
        return $this->id;
    }

}
