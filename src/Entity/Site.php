<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;
use SidraBlue\Lote\Object\Entity\Base;

/**
 * Site entity
 *
 * @package SidraBlue\Lote\Entity
 */
class Site extends Base
{

    protected $tableName = 'sb__site';

    /**
     * @var int $id - The ID of the note
     */
    public $id;

    /**
     * @var string $reference - The reference of the site
     */
    public $reference;

    /**
     * @var string $name - The name of the site
     */
    public $name;

    /**
     * @var string $description - The description of the site
     */
    public $description;

    /**
     * @var string $theme - The theme of the site
     */
    public $theme;

    /**
     * @var boolean $enabled - True if enabled
     */
    public $enabled;

}
