<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * SiteDomain entity
 *
 * @package SidraBlue\Lote\Entity
 */
class SiteDomain extends Base
{

    protected $tableName = 'sb__site_domain';

    /**
     * @var int $id - The ID of the note
     */
    public $id;

    /**
     * @var int $site_id - The Site ID of the domain
     */
    public $site_id;

    /**
     * @var string $name - The name of the site
     */
    public $name;

    /**
     * @var string $url - The url of the dmain entry
     */
    public $url;

    /**
     * @var boolean $enabled - True if enabled
     */
    public $enabled;

    /**
     * @var boolean $ssl_enabled - Has the support of SSL on this domain
     */
    public $ssl_enabled;

    /**
     * @var boolean $ssl_force - Force the use of SSL on this domain
     */
    public $ssl_force;


    /**
     * @param boolean $preferSsl
     * @return string
     * */
    public function getHttpSchemeAndHost($preferSsl = false)
    {
        $scheme = "http://";
        if ($this->ssl_force || ($preferSsl && $this->ssl_enabled)) {
            $scheme = 'https://';
        }
        return $scheme . trim($this->url, '/') . '/';
    }

}
