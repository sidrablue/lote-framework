<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for staging
 */
class Staging extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__staging';


    /**
     * @var int $id -  ID
     */
    public $id;

    /**
     * @var int $object_id - object id
     */
    public $object_id;

    /**
     * @var string $object_ref - object reference
     */
    public $object_ref;

    /**
     * @var string $status - status
     */
    public $status;

}