<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for staging field
 */
class StagingField extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__staging_field';


    /**
     * @var int $id - id
     */
    public $id;

    /**
     * @var int $staging_id - staging id
     */
    public $staging_id;

    /**
     * @var string $field - field
     */
    public $field;

    /**
     * @var string $value - value
     */
    public $value;

}