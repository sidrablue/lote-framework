<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Entity class for Client Stats
 */
class Stat extends Base
{
    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__stat';

    /**
     * @var string $account_ref - client reference
     */
    public $account_ref;

    /**
     * @var string $app_ref - app reference
     */
    public $app_ref;

    /**
     * @var string $object_ref - object reference
     */
    public $object_ref;

    /**
     * @var int $object_id - object ID
     */
    public $object_id;

    /**
     * @var string $reference - stat reference
     */
    public $reference;

    /**
     * @var string $name - stat name
     */
    public $name;

    /**
     * @var int $latest_value - the latest generated stat value
     */
    public $latest_value;

    /**
     * @var int $latest_value_time - the time that latest generated stat value
     */
    public $latest_value_time;

}
