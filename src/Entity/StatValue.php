<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Entity class for Client Stat Values
 */
class StatValue extends Base
{
    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__stat_value';

    /**
     * @var int $stat_id - client reference
     */
    public $stat_id;

    /**
     * @var float $stat_value - stat value
     */
    public $stat_value;

    /**
     * @var string $stat_range_type - the intended range type of this value, e.g. 'day', 'month', 'hour', etc
     */
    public $stat_range_type;

    /**
     * @var string $datetime - datetime of this stat value
     */
    public $datetime;

}
