<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Tag Entity
 */
class Tag extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__tag';

    /**
     * @var string $value - The string value of the tag
     */
    public $value;

    /**
     * @var string $display_value - The string value of the tag
     */
    public $display_value;

    /**
     * @var string $colour_code - colour code
     */
    public $colour_code;

    /**
     * @var string $weighting - weighting
     */
    public $weighting;

    /**
     * @var boolean $active - is active
     */
    public $active;

    /**
     * @var boolean $restricted - is restricted
     */
    public $restricted;

    /**
     * @var boolean $show_admin_only - display on admin only
     */
    public $display_admin_only;

}
