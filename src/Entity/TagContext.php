<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Tag Usage Entity
 */
class TagContext extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__tag_context';

    /**
     * @var int $tag_id - The id of the sb__tag that this usage references
     */
    public $tag_id;

    /**
     * @var int $object_id - The id of the object that this usage is for
     */
    public $object_ref;

}
