<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Tag Usage Entity
 */
class TagUsage extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__tag_usage';

    /**
     * @var int $tag_id - The id of the sb__tag that this usage references
     */
    public $tag_id;

    /**
     * @var string $object_ref - The reference of the object for this usage
     */
    public $object_ref;

    /**
     * @var string $field_ref - The reference of the field in the object for this usage
     */
    public $field_ref;

    /**
     * @var int $object_id - The id of the object that this usage is for
     */
    public $object_id;

}
