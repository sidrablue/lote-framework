<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for a task
 * @package Lote\System\Crm\Entity
 */
class Task extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__task';

    /**
     * @var string $name - The task name
     */
    public $name;

    /**
     * @var string $date_enabled - The task date enabled
     */
    public $date_enabled;

    /**
     * @var string $date_required - The task date required
     */
    public $date_required;

    /**
     * @var string $date_completed - The task date completed
     */
    public $date_completed;

    /**
     * @var int $group_id - The group task is assigned to
     */
    public $group_id;

    /**
     * @var string $process_id - The process task is assigned to
     */
    public $process_id;

    /**
     * @var string $reference - The reference
     */
    public $reference;

    /**
     * @var string $reference - The reference
     */
    public $reference_id;

    /**
     * @var string $default_image - default image
     */
    public $default_image;

    /**
     * @var int $parent_id - parent id
     */
    public $parent_id;

    /**
     * @var int $priority_id - priority id
     */
    public $priority_id;

    /**
     * @var int $status_id - status id
     */
    public $status_id;

    /**
     * @var int $type_id - type id
     */
    public $type_id;

    /**
     * @var string $description - description
     */
    public $description;

    /**
     * @var int $hours_projected - hours projected
     */
    public $hours_projected;

}