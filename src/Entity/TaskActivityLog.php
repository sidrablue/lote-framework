<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for a task
 * @package Lote\System\Crm\Entity
 */
class TaskActivityLog extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__task_activity_log';

    /**
     * @var string $task_id - task id
     */
    public $task_id;

    /**
     * @var string $event - event
     */
    public $event;

    /**
     * @var string $action - action
     */
    public $action;
    /**
     * @var string $notes - notes
     */
    public $notes;

    /**
     * @var int $status - status
     */
    public $status;
    /**
     * @var int $user_id - user id
     */
    public $user_id;

}
