<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for a task
 * @package Lote\System\Crm\Entity
 */
class TaskAlert extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__task_alert';

    /**
     * @var string $task_id- task id
     */
    public $task_id;

    /**
     * @var string $name - The task name
     */
    public $name;

    /**
     * @var string $date_enabled - The task date enabled
     */
    public $date_enabled;

    /**
     * @var string $date_required - The task date required
     */
    public $date_required;

    /**
     * @var string $date_completed - The task date completed
     */
    public $date_completed;

    /**
     * @var string $active - The task status
     */
    public $active;


}