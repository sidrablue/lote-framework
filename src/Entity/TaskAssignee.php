<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for a task
 * @package Lote\System\Crm\Entity
 */
class TaskAssignee extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__task_assignee';

    /**
     * @var int $task_id- ID of the task
     */
    public $task_id;

    /**
     * @var int $reference_id - ID of the assignee
     */
    public $type_id;

    /**
     * @var string $type_ref - Name of the assignee type
     */
    public $type_ref;

    /**
     * @var int $lote_author_id - ID of author
     */
    public $lote_author_id;

    /**
     * @var \DateTime $lote_updated - Time and Date of last update
     */
    public $lote_updated;

    /**
     * @var \DateTime $lote_created - Time and Date of object creation
     */
    public $lote_created;

    /**
     * @var \DateTime $lote_deleted - Time and Date of object deletion
     */
    public $lote_deleted;

    /**
     * @var array $lote_access - Array containing access parameters
     */
    public $lote_access;

}