<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for a Task Complete
 * @package Lote\System\Crm\Entity
 */
class TaskComplete extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__task_complete';

    /**
     * @var int $task_id - The group task is assigned to
     */
    public $task_id;

    /**
     * @var int $step_id - The group step is assigned to
     */
    public $step_id;
    /**
     * @var string $process_id - The process task is assigned to
     */
    public $user_id;

}