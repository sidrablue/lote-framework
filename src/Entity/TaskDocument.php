<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;
/**
 * Class TaskDocument
 *
 * @package SidraBlue\Lote\Entity
 */
class TaskDocument extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__task_document';

    /**
     * @var int $task_id - task id
     */
    public $task_id;

    /**
     * @var int $file_id - file id
     */
    public $file_id;

    /**
     * @var int $sort_order - sort order
     */
    public $sort_order;

}