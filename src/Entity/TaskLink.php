<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for a task
 * @package Lote\System\Crm\Entity
 */
class TaskLink extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__task_link';

    /**
     * @var string $task_id- task id
     */
    public $task_id;

    /**
     * @var string $link_object
     */
    public $link_object;

    /**
     * @var string $link_object_id
     */
    public $link_object_id;

    /**
     * @var string $link_object_ref
     */
    public $link_object_ref;


}