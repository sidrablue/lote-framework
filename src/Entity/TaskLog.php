<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for a task
 * @package Lote\System\Crm\Entity
 */
class TaskLog extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__task_log';

    /**
     * @var string $task_id- task id
     */
    public $task_id;

    /**
     * @var string $step_id - The step
     */
    public $step_id;

    /**
     * @var string $description - Description
     */
    public $description;

    /**
     * @var string $reference - The reference
     */
    public $reference;

}
