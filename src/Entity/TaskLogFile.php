<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

class TaskLogFile extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__task_log_file';

    /**
     * @var int $id - task log file id
     */
    public $id;

    /**
     * @var string $name - file name
     */
    public $name;

    /**
     * @var string $title - file title
     */
    public $title;

    /**
     * @var boolean $active - file status
     */
    public $active;

    /**
     * @var string $search_tags - file search tags
     */
    public $search_tags;

    /**
     * @var int $file_id - Base file id
     */
    public $file_id;

    /**
     * @var int $log_id - log id
     */
    public $log_id;

    /**
     * @var string $lote_access - file access
     */
    public $lote_access;

    /**
     * @var int $lote_author_id - Author user id
     */
    public $lote_author_id;

    /**
     * @var /DateTime $lote_created - created date
     */
    public $lote_created;

    /**
     * @var /DateTime $lote_updated - date last updated
     */
    public $lote_updated;

    /**
     * @var /DateTime %lote_deleted - date deleted
     */
    public $lote_deleted;

}