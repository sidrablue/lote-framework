<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Class TaskNotes
 * @package SidraBlue\Lote\Entity
 */
class TaskNotes extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__task_notes';

    /**
     * @var int $task_id - task id
     */
    public $task_id;

    /**
     * @var string $notes - notes
     */
    public $notes;
}