<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for a task
 * @package Lote\System\Crm\Entity
 */
class TaskOwner extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__task_owner';

    /**
     * @var string $task_id- task id
     */
    public $task_id;

    /**
     * @var string $user_id - The user
     */
    public $user_id;

    /**
     * @var string $is_primary
     */
    public $is_primary;

}