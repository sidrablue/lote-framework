<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for task status
 */
class TaskPriority extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__task_priority';

    /**
     * @var string $name - name
     */
    public $name;

    /**
     * @var string $description - description
     */
    public $description;

    /**
     * @var int $active - status
     */
    public $active;

    /**
     * @var int $priority_group_id - priority group id
     */
    public $priority_group_id;

    /**
     * @var int $sort_order - sort order
     */
    public $sort_order;

}
