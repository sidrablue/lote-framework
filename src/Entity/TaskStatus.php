<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for task status
 */
class TaskStatus extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__task_status';

    /**
     * @var string $name - name
     */
    public $name;

    /**
     * @var string $description - description
     */
    public $description;

    /**
     * @var int $active - status
     */
    public $active;

    /**
     * @var int $status_group_id - status group id
     */
    public $status_group_id;

    /**
     * @var int $sort_order - sort order
     */
    public $sort_order;

    /**
     * @var string $color - color
     */
    public $color;

}
