<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for a Task Time
 * @package Lote\System\Pm\Entity
 */
class TaskTime extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__task_time';

    /**
     * @var int $task_id - task id
     */
    public $task_id;

    /**
     * @var int $week -  week
     */
    public $week;

    /**
     * @var int $day -  day
     */
    public $day;

    /**
     * @var int $hour -  hour
     */
    public $hour;

    /**
     * @var int $minute -  minute
     */
    public $minute;

    /**
     * @var int $description -  description
     */
    public $description;


    /**
     * @var int $user_id -  user_id
     */
    public $user_id;
}