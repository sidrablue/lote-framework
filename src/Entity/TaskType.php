<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for task type
 */
class TaskType extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__task_type';

    /**
     * @var string $name - name
     */
    public $name;

    /**
     * @var string $description - description
     */
    public $description;

    /**
     * @var int $active - status
     */
    public $active;

    /**
     * @var int $task_group_id - task group id
     */
    public $task_group_id;

    /**
     * @var int $sort_order - sort order
     */
    public $sort_order;

}
