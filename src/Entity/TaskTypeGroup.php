<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for task status group
 */
class TaskTypeGroup extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__task_type_group';

    /**
     * @var string $name - name
     */
    public $name;

    /**
     * @var int $active - status
     */
    public $active;

}
