<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for a Task Watchlist
 * @package Lote\System\Pm\Entity
 */
class TaskWatchlist extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__task_watchlist';

    /**
     * @var int $task_id - task id
     */
    public $task_id;

    /**
     * @var string $user_id -  user id
     */
    public $user_id;

}