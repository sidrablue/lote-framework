<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;
use SidraBlue\Lote\Object\Entity\BaseMaster;

/**
 * Base class for users
 */
class TermsAndConditions extends BaseMaster
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__terms_and_conditions';

    /**
     * @var int $id
     */
    public $id;

    /**
     * @var $content $text - The text of the t&c
     */
    public $content;

    /**
     * @var $version
     */
    public $version;
    
    /**
     * @var $reference
     */
    public $reference;

    /**
     * @var $object_ref
     */
    public $object_ref;

    /**
     * @var $object_id
     */
    public $object_id;

    /**
     * @var $date_applied_from
     */
    public $date_applied_from;

    /**
     * @var $date_applied_to
     */
    public $date_applied_to;

}
