<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for a task
 * @package Lote\System\Crm\Entity
 */
class Token extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__token';

    /**
     * @var string $public_key
     */
    public $public_key;

    /**
     * @var string $private_key
     */
    public $private_key;

    /**
     * @var int $usage_limit
     */
    public $usage_limit;

    /**
     * @var int $usage_count
     */
    public $usage_count;

    /**
     * @var int $reference_id
     */
    public $reference_id;

    /**
     * @var string $reference_key
     */
    public $reference_key;

    /**
     * @var string $activated
     */
    public $activated;

    /**
     * @var string $activation_ip
     */
    public $activation_ip;

    /**
     * @var string $expires
     */
    public $expires;

    public function isValid()
    {
        return $this->expires == null || $this->expires < gmdate("Y-m-d H:i:s", time());
    }

}