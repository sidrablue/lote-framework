<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base entity for types
 */
class Type extends Base
{
    const TYPE_STAFF = 'staff';
    const TYPE_PARENT = 'parent';
    const TYPE_STUDENT = 'student';

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__type';

    /**
     * @var string $name - the type name
     */
    public $name;

    /**
     * @var string $description - The group description
     */
    public $description;

    /**
     * @var string $reference - The user type reference
     */
    public $reference;

    /**
     * @var string $object_ref - The object reference
     */
    public $object_ref;

}
