<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Url Entity
 */
class Url extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__url';

    /**
     * @var int $site_id - The ID of the site that the URL is for
     */
    public $site_id;

    /**
     * @var string $object_ref - The reference of the object that the URL is for
     */
    public $object_ref;

    /**
     * @var int $object_id - The ID of the object that the URL is for
     */
    public $object_id;

    /**
     * @var string $uri - The uri value of the URL
     */
    public $uri;

    /**
     * @var string $title - The name of URL
     */
    public $title;

    /**
     * @var string $keywords - The keywords of URL
     */
    public $keywords;

    /**
     * @var bool $active - Whether or not the URL is active
     */
    public $active;

    /**
     * @var string $published - When the URL is published
     */
    public $published;

    /**
     * @var string $expires - When the URL expires
     */
    public $expires;


    /**
     * @var string $data - Additional Data
     */
    public $data;

    /**
     * Load the entity by its object details
     * @param $objectRef
     * @param $objectId
     * @param int $siteId
     * @return boolean
     */
    public function loadByObject($objectId, $objectRef, $siteId = 0, $includeDeleted=false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->getTableName(), 't')
            ->andWhere('t.object_ref = :object_ref')
            ->setParameter('object_ref', $objectRef)
            ->andWhere('t.object_id = :object_id')
            ->setParameter('object_id', $objectId);
        if (!$includeDeleted) {
            $q->andWhere('lote_deleted is null');
        }
        if($siteId) {
            $q->where('t.site_id = :site_id')
                ->setParameter('site_id', $siteId);
        }
        $s = $q->execute();
        if($row = $s->fetch(\PDO::FETCH_ASSOC)) {
            $this->setData($row);
        }
        return $this->id;
    }

}
