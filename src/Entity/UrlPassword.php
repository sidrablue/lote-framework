<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Url Password Entity
 */
class UrlPassword extends Base
{

    /**
     * @var string $tableName
     * */
    protected $tableName = 'sb__url_password';

    /**
     * @var int $url_id - The ID of the url
     */
    public $url_id;

    /**
     * @var string $object_ref - The reference of the object that the URL password is for
     */
    public $object_ref;

    /**
     * @var int $object_id - The ID of the object that the URL password is for
     */
    public $object_id;

    /**
     * @var string $password - password
     */
    public $password;

    /**
     * @var boolean $never_expires - never expires flag
     */
    public $never_expires;

    /**
     * @var string $start_date - start date
     */
    public $start_date;

    /**
     * @var string $end_date - end date
     */
    public $end_date;

    /**
     * @var string $active - active
     */
    public $active;

}
