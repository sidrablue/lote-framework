<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use Lote\Module\Content\Cms\Model\Holder;
use Lote\System\Admin\Model\Setting as SettingModel;
use Lote\System\Client\Entity\Master\Account;
use SidraBlue\Lote\Model\CfValueGroup as CfValueGroupModel;
use SidraBlue\Lote\Model\Group as GroupModel;
use SidraBlue\Lote\Object\Data\Field\Factory;
use SidraBlue\Lote\Object\Entity\CustomField;
use SidraBlue\Lote\Service\Group as GroupService;
use SidraBlue\Lote\Service\Timezone;
use SidraBlue\Util\Arrays;
use SidraBlue\Util\Strings;
use SidraBlue\Util\Time;
use SidraBlue\Lote\Object\Data\Field\Factory as FieldFactory;
use Lote\System\Admin\Model\App as AppModel;

/**
 * Base class for users
 */
class User extends CustomField
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user';

    /**
     * @var string $source - the source of this user, such as 'local', 'ldap', 'google', 'facebook', etc.
     */
    public $source = 'local';

    /**
     * @var string $username - the users username
     */
    public $username;

    /**
     * @var string $password - The encoded password
     */
    public $password;

    /**
     * @var \DateTime $password_lastupdate - The last time that the password was updated
     */
    public $password_lastupdate;

    /**
     * @var $type - The user type
     */
    public $type;

    /**
     * @var string $token - The encryption token
     */
    public $token;

    /**
     * @var string $title - The users title, such as Mr, Mrs, Miss
     */
    public $title;

    /**
     * @var string $preferred_greeting - The users preferred greeting, such as "Howdy", "Hi", "Hello"
     */
    public $preferred_greeting;

    /**
     * @var string $preferred_name - The users preferred name
     */
    public $preferred_name;

    /**
     * @var string $firstName - The users first name
     */
    public $first_name;

    /**
     * @var string $middle_name - The users middle name
     */
    public $middle_name;

    /**
     * @var string $last_name - The users last name
     */
    public $last_name;

    /**
     * @var string $email - The users email
     */
    public $email;

    /**
     * @var string $email_secondary - The users secondary email
     */
    public $email_secondary;

    /**
     * @var string $phone - The users phone
     */
    public $phone;

    /**
     * @var string $phone_secondary - The users secondary phone
     */
    public $phone_secondary;

    /**
     * @var string $mobile - The users mobile
     */
    public $mobile;

    /**
     * @var string $code - code
     */
    public $code;

    /**
     * @var string $company_name - The users company name
     */
    public $company_name;

    /**
     * @var string $position - The users job title
     */
    public $position;

    /**
     * @var string $fax - fax
     */
    public $fax;

    /**
     * @var string $extension - extension
     */
    public $extension;

    /**
     * @var  int $age - age
     */
    public $age;

    /**
     * @var boolean $gender - gender
     */
    public $gender;

    /**
     * @var boolean $send_email_alerts - send email alerts
     */
    public $send_email_alerts;

    /**
     * @var string $referral - referral
     */
    public $referral;

    /**
     * @var \DateTime $last_login
     */
    public $last_login;

    /**
     * @var string $priority -
     */
    public $address_location;

    /**
     * @var string $address_street -
     */
    public $address_street;

    /**
     * @var string $address_street2 -
     */
    public $address_street2;

    /**
     * @var string $address_suburb -
     */
    public $address_suburb;

    /**
     * @var string $address_city -
     */
    public $address_city;

    /**
     * @var string $address_state -
     */
    public $address_state;

    /**
     * @var int $address_postcode -
     */
    public $address_postcode;

    /**
     * @var string $address_country -
     */
    public $address_country;

    /**
     * @var boolean $is_admin - True if the user is an admin
     */
    public $is_admin;

    /**
     * @var boolean $is_owner - True if the user is the owner
     */
    public $is_owner;

    /**
     * @var boolean $active - True if the user is active
     */
    public $active;

    /**
     * @var boolean $is_approved - True if the user is approved
     */
    public $is_approved;

    /**
     * @var int $subscribed - -1 for unbuscribed permanently, 0 for temporarily unsubscribed, 1 for subscribed
     */
    public $subscribed;

    /**
     * @var boolean $is_suppressed - True if the user is suppressed for edm communication
     */
    public $is_suppressed;

    /**
     * @var int $timezone_id - The users default timezone ID
     */
    public $timezone_id;

    /**
     * @var /Date $date_of_birth - Date of birth
     */
    public $date_of_birth;

    /**
     * @var string $external_id - External reference id
     */
    public $external_id;

    public $notification_device_id;

    public $pin_code;

    public $email_confirmed;

    public $confirm_sent;

    public $display_picture;

    public $display_picture_file_id;

    public $google_id;

    public $facebook_id;

    public $reset_sent;

    public $master_id;

    public $work_phone;
    public $email_double_opt_in;
    public $gdpr_consent;
    public $is_blacklisted;
    public $pending_password_reset;

    /**
     * @var boolean $isAudited
     * True if this entity is audited
     * */
    protected $isAudited = true;



    /**
     * Load a user by their email
     * @param string $email
     * @param boolean $includeDeleted
     * @return boolean
     * */
    public function loadByEmail($email, $includeDeleted = false)
    {
        return $this->loadByField('email', $email, $includeDeleted);
    }

    /**
     * Load a user by their secondary email
     * @param string $email
     * @param boolean $includeDeleted
     * @return boolean
     * */
    public function loadBySecondaryEmail($email, $includeDeleted = false)
    {
        $email = filter_var($email, FILTER_VALIDATE_EMAIL);
        return $this->loadByField('email_secondary', $email, $includeDeleted);
    }

    /**
     * Load a user by their email or mobile
     * @param string $email
     * @param string $mobile
     * @return boolean
     * */
    public function loadByEmailOrMobile($email, $mobile, $includeDeleted = false)
    {
        // Validate arguments
        $email = filter_var($email, FILTER_VALIDATE_EMAIL);
        $mobile = preg_match("/[\+0-9]{0,1}([0-9]*)/", $mobile) ? $mobile : false;

        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("*")
            ->from("sb__user", "u");
        if($email && $mobile) {
            $q->andWhere("(u.email = :email or u.mobile = :mobile)")
                ->setParameter("email", $email)
                ->setParameter("mobile", $mobile);
        }
        elseif($email) {
            $q->andWhere("u.email = :email")
                ->setParameter("email", $email);
        }
        elseif($mobile) {
            $q->andWhere("u.mobile = :mobile")
                ->setParameter("mobile", $mobile);
        }
        if($includeDeleted) {
            $q->addOrderBy("lote_deleted is null", 'desc');
            if ($email && $mobile) {
                $q->addOrderBy("email = :email", 'desc');
            }
        }
        else {
            $q->andWhere("u.lote_deleted is null");
        }
        $q->addOrderBy('id', 'desc');
        if($data = $q->execute()->fetch(\PDO::FETCH_ASSOC)) {
            $this->setData($data);
        }
        $q = null;
        return $this->id > 0;
    }

    /**
     * Load a user by their email or mobile
     * @param string $mobile
     * @param boolean $includeDeleted
     * @return boolean
     * */
    public function loadByMobileAndNoEmail($mobile, $includeDeleted = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("*")
            ->from("sb__user", "u")
            ->where("u.mobile = :mobile")
            ->setParameter("mobile", $mobile)
            ->andWhere("(u.email is null or u.email = '')")
            ->addOrderBy("id", 'asc');
        if(!$includeDeleted) {
            $q->andWhere("lote_deleted is null");
        }
        if($data = $q->execute()->fetch(\PDO::FETCH_ASSOC)) {
            $this->setData($data);
        }
        $q = null;
        return $this->id > 0;
    }

    /**
     * Load a user by their username
     * @param string $username
     * @return boolean
     * */
    public function loadByUsername($username)
    {
        return $this->loadByField('username', $username);
    }

    /**
     * Load a user by their username
     * @param string $username
     * @return boolean
     * */
    public function loadForLogin($username)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("*")
            ->from("sb__user", "u")
            ->where("(u.username = :username or u.email = :email)")
            ->setParameter("username", $username)
            ->setParameter("email", $username)
            ->andWhere("lote_deleted is null");
        if($data = $q->execute()->fetch(\PDO::FETCH_ASSOC)) {
            $this->setData($data);
        }
        return $this->id > 0;
    }

    /**
     * Helper function to get the name of the current user
     * */
    public function getName()
    {
        $result = '';
        if ($this->first_name && $this->last_name) {
            $result = "{$this->first_name} {$this->last_name}";
        } elseif ($this->first_name) {
            $result = $this->first_name;
        } elseif ($this->last_name) {
            $result = $this->last_name;
        }
        return $result;
    }

    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeInsert($data)
    {

        $this->token = $data['token'] = uniqid();
        $isAdmin = $this->getState()->getUser()->isAdmin();
        if(!isset($data['is_admin']) || $data['is_admin'] !='1' || !$isAdmin) {
            $data['is_admin'] = 0;
        }
        if(!isset($data['is_owner']) || $data['is_owner'] !='1') {
            $data['is_owner'] = 0;
        }
        if(!isset($data['active']) || $data['active'] !='0') {
            $data['active'] = 1;
        }
        if(!isset($data['subscribed']) || ($data['subscribed'] !='0' && $data['subscribed'] !='-1')) {
            $data['subscribed'] = 1;
        }
        if($data['password'] && !empty($data['password'])) {
            if(!( strlen($data['password'])==60 && strpos($data['password'], '$2y$10$')===0)) {
                $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
            }
            $data['password_lastupdate'] = Time::getUtcNow();
        }
        if(isset($data['email']) && trim($data['email']) == '') {
            $data['email'] = null;
        }
        if(isset($data['mobile']) && trim($data['mobile']) == '') {
            $data['mobile'] = null;
        }
        elseif(isset($data['mobile']) && $data['mobile'] != '') {
            $data['mobile'] = Strings::removeSpaces($data['mobile']);
        }
        return $data;
    }

    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeUpdate($data)
    {
        $isAdmin = $this->getState()->getUser()->isAdmin();
        if(isset($data['is_admin']) && $data['is_admin'] && !$isAdmin) {
            $data = false;
        } else {
            if(array_key_exists('confirm_password', $data)) {
                unset($data['confirm_password']);
            }
            if(array_key_exists('password', $data) && $data['password']==null )
            {
                unset($data['password']);
                $this->markPropertyUnmodified('password');
            }
            elseif(array_key_exists('password', $data) && $data['password']) {
                if(strlen($data['password'])==60 && strpos($data['password'], '$2y$10$')===0) {
                    unset($data['password']);
                    $this->markPropertyUnmodified('password');
                }
                else {
                    $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
                    $data['password_lastupdate'] = date('Y-m-d H:i:s');
                }
            }
            if(isset($data['email']) && trim($data['email']) == '') {
                $data['email'] = null;
            }
            if(isset($data['mobile']) && trim($data['mobile']) == '') {
                $data['mobile'] = null;
            }
            elseif(isset($data['mobile']) && $data['mobile'] != '') {
                $data['mobile'] = Strings::removeSpaces($data['mobile']);
            }
        }

        return $data;
    }

    /**
     * Check if this user is an admin
     * */
    public function isAdmin()
    {
        return $this->is_admin;
    }

    /**
     * Check if this user is in specified group
     * */
    public function isInGroup($groupId)
    {
        $g = new GroupService($this->getState());
        return $g->isInGroup($groupId);
    }

    /**
     * Get the core list of fields for this database
     * @todo - potentially look at making this dynamic in some way
     * @return array
     * */
    public function getCoreFields()
    {
        $fields = [];
        $fields['first_name'] = 'First Name';
        $fields['last_name'] = 'Last Name';
        $fields['email'] = 'Email';
        $fields['mobile'] = 'Mobile';
        $fields['title'] = 'Title';
        $fields['middle_name'] = 'Middle Name';
        $fields['preferred_name'] = 'Preferred Name';
        $fields['preferred_greeting'] = 'Preferred Greeting';
        $fields['username'] = 'Username';
        $fields['password'] = 'Password';
        $fields['email_secondary'] = 'Secondary Email';
        $fields['company_name'] = 'Company Name';
        $fields['position'] = 'Job Title';
        $fields['phone'] = 'Phone';
        $fields['active'] = 'Active';
        $fields['address_location'] = "Location Address";
        $fields['address_street'] = "Street Address";
        $fields['address_street2'] = "Street Address 2";
        $fields['address_city'] = "City/Suburb";
        $fields['address_state'] = "State";
        $fields['address_postcode'] = "Postcode";
        $fields['address_country'] = "Country";
        $fields['subscribed'] = "Subscription Status";
        $fields['is_suppressed'] = "Is Suppressed";
        $fields['code'] = "Code";
        $fields['lote_created'] = "Created";
        $fields['lote_updated'] = "Updated";
        return $fields;
    }

    /**
     * Set the value of a custom field for this entity
     * @access public
     * @param int $fieldId - the ID of the custom field
     * @param mixed $value - the value of the field
     * @param boolean $updateOnEmpty - true if this value is to tbe updated even if its empty and the existing value
     * @param int $userId - the ID of the User to force using this ID instead of the logged in User ID
     * is not empty.
     * @return void
     * */
    public function setCustomFieldValue($fieldId, $value, $updateOnEmpty = true, $userId = null)
    {
        $objectId = $userId? $userId : $this->id;
        if($objectId) {
            $tablePrefix = 'sb__user__';
            $customField = new CfField($this->getState());
            if ($customField->load($fieldId)) {
                $groupsWithData[] = $customField->group_id;
                $fieldType = FieldFactory::createInstance($this->getState(), $customField->field_type);
                $fieldType->setDefinition($customField);
                $fieldType->updateValue($objectId, $customField->id, $value, $tablePrefix . 'cf_value', $updateOnEmpty, true);
                $this->addFieldGroupEntry($customField->group_id, $objectId);
            }
        }
    }

    /**
     * Add a association for this entity to a custom field group
     * @param int $groupId
     * @param int $objectId
     * @return void
     * */
    public function addFieldGroupEntry($groupId, $objectId)
    {
        $cg = new CfValueGroupModel($this->getState());
        $cg->setTableName('sb__user__cf_value_group');
        $cg->addFieldGroupEntry($groupId, $objectId);
    }

    /**
     * @todo - use value_date, value_text accordingly
     * */
    function getCustomFieldValues($indexById = false)
    {
        if($this->id <= 0) {
            return [];
        }

        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('cff.reference, cff.field_type, cfv.value_string, cfv.value_date, cfv.value_text, cff.id as field_id, cfv.id as value_id')
            ->from('sb__cf_field', 'cff')
            ->leftJoin('cff', 'sb__cf_group', 'cfg', 'cff.`group_id` = cfg.`id`')
            ->leftJoin('cff', 'sb__cf_object', 'cfo', 'cfg.`object_id` = cfo.id')
            ->leftJoin('cff', 'sb__user__cf_value', 'cfv', 'cfv.field_id = cff.id');
        $q->where('cfv.`object_id` = :user_id')
            ->setParameter('user_id', $this->id);
        $q->andWhere('cfo.`reference` = :type')
            ->setParameter('type', 'user');
            $q->andWhere("cfv.lote_deleted is null");

        $result = $q->execute();

        $data = [];

        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $i = Factory::createInstance($this->getState(), $row['field_type']);
            if($indexById) {
                $data[$row['field_id']] = $i->getDisplayValue($row);
            }
            else {
//                $data[$row['reference']] = $row['value_string'];
                $data[$row['reference']] = $i->getDisplayValue($row);
            }
        }
        return $data;
    }

    public function getCustomFieldGroupValues()
    {
        if($this->id <= 0) {
            return [];
        }

        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('cfg.reference,cff.reference as field_reference,cff.name, cfv.value_string')
            ->from('sb__cf_field', 'cff')
            ->leftJoin('cff', 'sb__cf_group', 'cfg', 'cff.`group_id` = cfg.`id`')
            ->leftJoin('cff', 'sb__cf_object', 'cfo', 'cfg.`object_id` = cfo.id')
            ->leftJoin('cff', 'sb__user__cf_value', 'cfv', 'cfv.field_id = cff.id');
        $q->where('cfv.`object_id` = :user_id')
            ->setParameter('user_id', $this->id);
        $q->AndWhere('cfo.`reference` = :type')
            ->setParameter('type', 'user');

        $result = $q->execute();

        $data = [];

        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            //$data[$row['reference']] = $row['value_string'];
            if(!isset($data[$row['reference']])){
                $data[$row['reference']]=[];
            }else{

            }
            array_push($data[$row['reference']],['name'=>$row['name'],'value'=>$row['value_string'],'reference'=>$row['field_reference']]);
            //array_push($data[$row['reference']],$row['value_string']);
        }

        return $data;
    }

    public function getCustomFieldsMap($reference = '')
    {
        if(empty($reference)) {
            return [];
        }
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('cff.reference, cff.name ')
            ->from('sb__cf_field', 'cff')
            ->leftJoin('cff', 'sb__cf_group', 'cfg', 'cff.`group_id` = cfg.`id`')
            ->leftJoin('cff', 'sb__cf_object', 'cfo', 'cfg.`object_id` = cfo.id');
        $q->where('cfo.`reference` = :type')
            ->setParameter('type', $reference);

        $result = $q->execute();

        $data = [];

        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $data[$row['reference']] = $row['name'];
        }

        return $data;
    }

    public function getWildcardNames(
        $wildcardTypes = [
            'user',
            'custom_field',
            'edm',
            'date',
            'content_holder',
            'setting',
            'account'
        ]
    )
    {
        $wildcards = [];

        if(in_array('user', $wildcardTypes))
        {
            $userData = $this->getCoreFields();
            foreach($userData as $k => $v) {
                $wildcards["%%$k%%"] = $v;
            }
        }

        if(in_array('custom_field', $wildcardTypes))
        {
            $customFields = $this->getCustomFieldsMap('user');
            foreach($customFields as $k => $v) {
                $wildcards["%%__cf_$k%%"] = $v;
            }
        }

        if(in_array('edm', $wildcardTypes))
        {
            $wildcards["%%__view_in_browser__%%"] = 'View in Browser Link';
            $wildcards["%%__view_in_browser_url__%%"] = 'View in Browser Link URL';
            $wildcards["%%__latest__%%"] = 'Latest Link';
            $wildcards["%%__unsubscribe_link__%%"] = 'Unsubscribe Link';
            $wildcards["%%__unsubscribe_link_url__%%"] = 'Unsubscribe Link URL';
            $wildcards["%%__manage_profile_link__%%"] = 'Manage Profile Link';
            $wildcards["%%__manage_profile_link_url__%%"] = 'Manage Profile Link URL';
        }

        if(in_array('notification', $wildcardTypes))
        {
            $wildcards["%%__notification_latest__%%"] = 'Latest Link';
        }

        if(in_array('date', $wildcardTypes))
        {
            $wildcards["date"] = 'Date';
        }

        if(in_array('content_holder', $wildcardTypes))
        {
            $wildcards["content_holder"] = 'Content Holder';
        }

        if(in_array('setting', $wildcardTypes))
        {
            $wildcards["setting"] = 'Settings';
        }

        if(in_array('account', $wildcardTypes))
        {
            $wildcards["account"] = 'Account';
        }

        return $wildcards;
    }

    public function getDateWildcards()
    {
        $wildcards = [];

        $wildcards["%%__date_MdY__%%"] = date("F jS Y");
        $wildcards["%%__date_mdY__%%"] = date("M jS Y");
        $wildcards["%%__date_dMY__%%"] = date("jS F Y");
        $wildcards["%%__date_dmY__%%"] = date("jS M Y");
        $wildcards["%%__date_dmy__%%"] = date("d/m/y") . ' - (dd/mm/yy)';
        $wildcards["%%__date_dmY__%%"] = date("d/m/Y") . ' - (dd/mm/yyyy)';
        $wildcards["%%__date_mdy__%%"] = date("m/d/y") . ' - (mm/dd/yy)';
        $wildcards["%%__date_mdY__%%"] = date("m/d/Y") . ' - (mm/dd/yyyy)';
        $wildcards["%%__date_year__%%"] = date("Y") . ' - (Year - Full)';
        $wildcards["%%__date_y__%%"] = date("y") . ' - (Year - Short)';
        $wildcards["%%__date_month__%%"] = date("m") . ' - (Month - Leading 0\'s)';
        $wildcards["%%__date_m0__%%"] = date("n") . ' - (Month - No leading 0\'s)';
        $wildcards["%%__date_M__%%"] = date("F") . ' - (Month - Full name)';
        $wildcards["%%__date_m__%%"] = date("M") . ' - (Month - Short name)';
        $wildcards["%%__date_d00__%%"] = date("d") . ' - (Day - Leading 0\'s)';
        $wildcards["%%__date_d0__%%"] = date("j") . ' - (Day - No leading 0\'s)';
        $wildcards["%%__date_dth__%%"] = date("jS") . ' - (Day - Ordinal)';
        $wildcards["%%__date_day__%%"] = date("l") . ' - (Day - Full name)';
        $wildcards["%%__date_d__%%"] = date("D") . ' - (Day - Short name)';

        return $wildcards;
    }

    public function getContentHolderWildcards()
    {
        //$wildcards = [];

        // get content holders
        $ch = new Holder($this->getState());
        $contentHolders = $ch->getList(1, [], [], 'and');

        return $contentHolders;

        /*if(isset($contentHolders['rows']))
        {
            foreach($contentHolders['rows'] as $contentHolder)
            {
                $wildcards["%%__content_snippet_{$contentHolder['id']}__%%"] = $contentHolder['name'];
            }
        }

        return $wildcards;*/
    }

    public function getSettingWildcards()
    {
        $wildcards = [];

        // get settings
        $s = new SettingModel($this->getState());
        $clauses = $this->settingSearchClauses();
        $options = $this->settingSearchOptions();
        $settings = $s->getList(1, $clauses, $options, 'and', 1000);

        /*if(isset($settings['rows']))
        {
            foreach($settings['rows'] as $setting)
            {
                $wildcards["%%__setting_{$setting['reference']}__%%"] = !empty($setting['title']) ? $setting['title'] : $setting['reference'];
            }
        }*/

        if(isset($settings['rows']))
        {
            foreach($settings['rows'] as $setting)
            {
                $temp = [];
                $temp['key'] = "%%__setting_{$setting['reference']}__%%";
                $temp['value'] = !empty($setting['title']) ? $setting['title'] : $setting['reference'];
                $temp['group'] = !empty($setting['app_ref']) ? $this->getAppName($setting['app_ref']) : 'Core';
                $wildcards[] = $temp;
            }
        }

        return $wildcards;
    }

    public function getAccountWildcards()
    {
        $wildcards = [];

        $columns = [
            'domain',
            //'website', Handled below
            'name',
            'trading_name',
            'business_number',
            'business_number_type',
            'timezone',
            'phone',
            'email',
            'contact_first_name',
            'contact_last_name',
            'street',
            'city',
            'postcode',
            'state',
            'country',
            'fax',
            'mobile'
        ];

        // Only add website_secondary wildcards if the property is present in this System
        foreach (['website', 'website_secondary'] as $websiteProp) {
            if (property_exists($this->getState()->getAccount(), $websiteProp)) {
                $newCols = [
                    "{$websiteProp}",
                    "{$websiteProp}_html_link",
                    "{$websiteProp}_display"
                ];
                array_splice($columns, 4, 0, $newCols);
            }
        }

        foreach($columns as $property) {
            $wildcardName = str_ireplace('_', ' ', $property);
            $wildcards["%%__account_{$property}__%%"] = ucwords($wildcardName);
        }

        return $wildcards;
    }

    private function getAppName($appRef)
    {
        $obj = new AppModel($this->getState());
        $appDetails = $obj->getAppDetailsByReference($appRef);
        return isset($appDetails['name']) && !empty($appDetails['name']) ? $appDetails['name'] : $appRef;
    }

    private function settingSearchClauses()
    {
        $clauses = [];

        $clause = [];
        $clause['field'] = 'content_access';
        $clause['operator'] = 'equals';
        $clause['value'] = true;
        $clause['data_type'] = \PDO::PARAM_BOOL;
        $clauses[] = $clause;

        return $clauses;
    }

    private function settingSearchOptions()
    {
        $options = [];
        $options['app_ref'] = 'asc';
        $options['title'] = 'asc';
        $options['reference'] = 'asc';
        return $options;
    }

    public function getWildcardValues(
        $wildcardTypes = [
            'user',
            'custom_field',
            'edm',
            'date',
            'content_holder',
            'setting',
            'account'
        ]
    )
    {
        $wildcards = [];

        if(in_array('user', $wildcardTypes))
        {
            $data = $this->getData();
            foreach($data as $k=>$v) {
                $wildcards["%%$k%%"] = $v;
            }
        }

        if(in_array('custom_field', $wildcardTypes))
        {
            $data = $this->getCustomFieldValues();
            foreach($data as $k=>$v) {
                $wildcards["%%__cf_$k%%"] = $v;
            }
        }

        if(in_array('date', $wildcardTypes))
        {
            $ts = new Timezone($this->getState());
            $wildcards["%%__date_MdY__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'F jS Y');
            $wildcards["%%__date_mdY__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'M jS Y');
            $wildcards["%%__date_MD__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'F j');
            $wildcards["%%__date_dMY__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'jS F Y');
            $wildcards["%%__date_dmY__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'jS M Y');
            $wildcards["%%__date_dmy__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'd/m/y');
            $wildcards["%%__date_dmY__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'd/m/Y');
            $wildcards["%%__date_mdy__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'm/d/y');
            $wildcards["%%__date_mdY__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'm/d/Y');
            $wildcards["%%__date_year__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'Y');
            $wildcards["%%__date_y__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'y');
            $wildcards["%%__date_month__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'm');
            $wildcards["%%__date_m0__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'n');
            $wildcards["%%__date_M__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'F');
            $wildcards["%%__date_m__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'M');
            $wildcards["%%__date_d00__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'd');
            $wildcards["%%__date_d0__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'j');
            $wildcards["%%__date_dth__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'jS');
            $wildcards["%%__date_day__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'l');
            $wildcards["%%__date_d__%%"] = $ts->convertToDisplay(date("Y-m-d H:i:s"), 'D');
        }

        if(in_array('content_holder', $wildcardTypes))
        {
            // get content holders
            $ch = new Holder($this->getState());
            $contentHolders = $ch->getList(1, [], [], 'and', 1000);
            if(isset($contentHolders['rows']))
            {
                foreach($contentHolders['rows'] as $contentHolder)
                {
                    $wildcards["%%__content_snippet_{$contentHolder['id']}__%%"] = $contentHolder['content'];
                }
            }
        }

        if(in_array('setting', $wildcardTypes))
        {
            if(isset($settings['rows']))
            {
                foreach($this->getState()->getSettings()->getAll() as $s)
                {
                    $value = $this->getState()->getSettings()->get($s['reference']);
                    $wildcards["%%__setting_{$s['reference']}__%%"] = $value;
                }
            }
        }

        if(in_array('account', $wildcardTypes))
        {
            $columns = [
                'domain',
                //'website', handled below
                'name',
                'trading_name',
                'business_number',
                'business_number_type',
                'timezone',
                'phone',
                'email',
                'contact_first_name',
                'contact_last_name',
                'street',
                'city',
                'postcode',
                'state',
                'country',
                'fax',
                'mobile'
            ];

            if($this->getState()->getMasterDb()) {
                $account = new Account($this->getState());
                $account->loadByField('reference', $this->getState()->getSettings()->get('system.reference'));

                if (!empty($account->name)) {
                    foreach ($columns as $property) {
                        $wildcards["%%__account_{$property}__%%"] = $account->$property;
                    }

                    // Create website wildcard variants
                    foreach (['website', 'website_secondary'] as $websiteProp) {
                        $host = '';
                        $url = '';
                        if (!empty($account->{$websiteProp})) {
                            $urlParts = parse_url($account->{$websiteProp});
                            if (isset($urlParts['host'])) {
                                $host = $urlParts['host'];
                                $url = $urlParts['scheme'] . "://" . $host;
                            } elseif (isset($urlParts['path'])) {
                                $host = explode('/', $urlParts['path'])[0];
                                $url = "http://" . $host;
                            }
                        }
                        $wildcards["%%__account_{$websiteProp}__%%"] = $url;
                        $wildcards["%%__account_{$websiteProp}_html_link__%%"] = "<a href='$url'>$host</a>";
                        $wildcards["%%__account_{$websiteProp}_display__%%"] = $host;
                    }
                }
            }
        }

        // These are added OUTSIDE of this function
/*        $wildcards["%%__view_in_browser__%%"] = 'View in Browser Link';
        $wildcards["%%__unsubscribe_link__%%"] = 'Unsubscribe Link';
        $wildcards["%%__manage_profile_link__%%"] = 'Manage Profile Link';*/

        return $wildcards;
    }

    /**
     * Create a user entry for a user from LDAP
     * @param array $userData - the users LDAP data
     * @param array $userGroups - the users LDAP groups
     * @access public
     * @return array - the DB user data
     * */
    public function createFromLdap($userData, $userGroups)
    {
        $userData['source'] = 'ldap';
        $userData['active'] = true;
        $userId = $this->insertUser($userData);
        $this->updateLdapGroups($userId, $userGroups);
    }

    /**
     * Update a user entry for a user from LDAP
     * @param int $userId - the users ID
     * @param array $userData - the users LDAP data
     * @param array $userGroups - the users LDAP groups
     * @access public
     * @return array - the DB user data
     * */
    public function updateFromLdap($userId, $userData, $userGroups)
    {
        $userData['source'] = 'ldap';
        $this->updateUser($userId, $userData);
        $this->updateLdapGroups($userId, $userGroups);
    }

    /**
     *
     *  */
    public function insertUser($detailsArr)
    {
        $e = new User($this->getState());
        $e->setData($detailsArr);
        $e->source = 'ldap';
        return $e->save();
    }

    /**
     *  move to model maybe
     * */
    private function updateUser($userId, $detailsArr)
    {
        $u = new User($this->getState());
        if($u->load($userId)) {
            $u->setData($detailsArr);
            $u->save();
        }
        return $u->id;
    }

    /**
     * @param int $userId
     * @param array $userGroups
     * @return void
     * */
    public function updateLdapGroups($userId, $userGroups)
    {
        $m = new GroupModel($this->getState());
        $groupIds = $m->getIdsFromLdapNames($userGroups);
        $currentIds = Arrays::getFieldValuesFrom2dArray($m->getUserGroups($userId), 'id');
        $allGroups = array_unique(array_merge($groupIds, $currentIds));
        $m->updateGroups($userId, $allGroups);
    }


    /**
     * Updates the local users password
     *
     * @param string $password - The new password
     * @param string $errorMessage - Any error messages sent through
     * @param string $username - Username of the selected user
     * @todo implement function
     */
    public function updatePassword($password, $errorMessage, $username)
    {

    }

    /**
     * @return bool - Whether or not this is user is member to a protected group
     */
    public function isProtected()
    {
        $q = $this->getReadDb()->createQueryBuilder();

        $q->select('max(g.is_protected) as is_protected')
            ->from('sb__group', 'g')
            ->innerJoin('g', 'sb__user_group', 'ug', 'ug.group_id = g.id')
            ->innerJoin('ug', 'sb__user', 't', 'ug.user_id = t.id')
            ->andWhere('t.id = :id')
            ->setParameter('id', $this->id)
            ->andWhere('t.lote_deleted is null')
            ->andWhere('ug.lote_deleted is null')
            ->andWhere('g.lote_deleted is null')
            ->setMaxResults(1);

        return $q->execute()->fetchColumn() ? true : false;
    }

    /**
     * Determines whether this user has opted out
     * Deliberately includes lote_deleted users
     * @param $email
     * @param $mobile
     * @return bool
     */
    public function isOptedOut($email, $mobile)
    {
        // Validate arguments
        $email = filter_var($email, FILTER_VALIDATE_EMAIL);
        $mobile = preg_match("/[\+0-9]{0,1}([0-9]*)/", $mobile) ? $mobile : false;

        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("*")
            ->from("sb__user", "u");
        if($email && $mobile) {
            $q->andWhere("(u.email = :email or u.mobile = :mobile)")
                ->setParameter("email", $email)
                ->setParameter("mobile", $mobile);
        }
        elseif($email) {
            $q->andWhere("u.email = :email")
                ->setParameter("email", $email);
        }
        elseif($mobile) {
            $q->andWhere("u.mobile = :mobile")
                ->setParameter("mobile", $mobile);
        }
        $q->andWhere("u.subscribed = -1");
        $q->andWhere("u.lote_deleted is null");

        $data = $q->execute()->fetch(\PDO::FETCH_ASSOC);

        return isset($data['id']) ? $data['id'] : false;
    }

    /**
     * Undelete a user from the db and update the entity object's fields
     * @return bool success
     */
    public function undelete()
    {
        $result = false;
        if ($this->id > 0) {
            $result = $this->getState()->getWriteDb()->update($this->getTableName(), ['lote_deleted' => null, 'lote_deleted_by' => null], ['id' => $this->id]) > 0;
            if ($result) {
                $this->lote_deleted = null;
                $this->lote_deleted_null = null;
            }
        }
        return $result;
    }
}
