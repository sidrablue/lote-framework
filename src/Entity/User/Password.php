<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity\User;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Password class for users
 * @package SidraBlue\Lote\Entity\User
 * @dbEntity true
 */
class Password extends Base
{

    const USAGE_TYPE_PRIMARY = 'primary';
    const USAGE_TYPE_RANGE = 'range';
    const USAGE_TYPE_LIMIT = 'limit';

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_password';

    /**
     * @dbColumn user_id
     * @dbType integer
     * @dbOptions notnull = false
     * @dbIndex true
     * @var int $user_id - The user ID to which this password belongs
     */
    public $user_id;

    /**
     * @dbColumn password
     * @dbType string
     * @dbOptions length=64, notnull = false
     * @var string $password - The encoded password
     */
    public $password;

    /**
     * @dbColumn sequence
     * @dbType integer
     * @dbOptions notnull = false
     * @var string $sequence - The encoded password
     */
    public $sequence;

    /**
     * @dbColumn token
     * @dbType string
     * @dbOptions length=255, notnull = false
     * @dbIndex true
     * @var string $token - The token for this password
     */
    public $token;

    /**
     * @dbColumn usage_type
     * @dbType string
     * @dbOptions notnull = false, default=false
     * @dbIndex true
     * @var boolean $usage_type - The token for this password
     */
    public $usage_type;

    /**
     * @dbColumn is_primary
     * @dbType boolean
     * @dbOptions notnull = false, default=false
     * @dbIndex true
     * @var boolean $is_primary - The token for this password
     */
    public $is_primary;

    /**
     * @dbColumn valid_from
     * @dbType datetime
     * @dbOptions notnull = false
     * @dbIndex true
     * @var \datetime $is_current - The token for this password
     */
    public $range_from;

    /**
     * @dbColumn range_to
     * @dbType datetime
     * @dbOptions notnull = false
     * @dbIndex true
     * @var \datetime $range_to - The token for this password
     */
    public $range_to;

    /**
     * @dbColumn limit_remaining
     * @dbType integer
     * @dbOptions notnull = false
     * @var int $limit_remaining - The token for this password
     */
    public $limit_remaining;

    /**
     * @var boolean $isAudited
     * True if this entity is audited
     * */
    protected $isAudited = true;

    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeInsert($data)
    {
        if($data['password'] && !empty($data['password'])) {
            if(!( strlen($data['password'])==60 && strpos($data['password'], '$2y$10$')===0)) {
                $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
            }
        }
        return $data;
    }

    /**
     * Function to call before updating data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeUpdate($data)
    {
        if(isset($data['password']) && !empty($data['password'])) {
            if(!( strlen($data['password'])==60 && strpos($data['password'], '$2y$10$')===0)) {
                $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
            }
        }
        return $data;
    }
}