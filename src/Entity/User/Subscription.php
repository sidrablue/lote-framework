<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity\User;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for users
 */
class Subscription extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_subscription';

    /**
     * @var int $user_id - foreign key to the user
     */
    public $user_id;

    /**
     * @var string $source_ref - the source reference of the item unsubscribing the user
     */
    public $source_ref;

    /**
     * @var boolean $is_current - true if this is the current state of the subscription
     */
    public $is_current;

    /**
     * @var string $channel - the channel in question, e.g. 'email' or 'mobile' or 'social'
     */
    public $channel;

    /**
     * @var string $channel_key - The channel key at the given time, e.g. the users email
     */
    public $channel_value;

    /**
     * @var int $subscribed - The subscription status
     */
    public $subscribed;

    /**
     * @var int $subscribed_reason - The subscription change reason
     */
    public $subscribed_reason;

    /**
     * @var int $subscribed_description - The subscription change description
     */
    public $subscribed_description;

}
