<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;
use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for users
 */
class UserAttribute extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_attribute';

    /**
     * @var int $user_id- the user id
     */
    public $user_id;

    /**
     * @var string $attribute_reference- the attribute reference
     */
    public $attribute_reference;

    /**
     * @var string $attribute_type- The attribute type
     */
    public $attribute_type;

    /**
     * @var string $attribute_title- The attribute title
     */
    public $attribute_title;

    /**
     * @var string $attribute_value - The attribute value
     */
    public $attribute_value;

    /**
     * @var string $attribute_data - The attribute data
     */
    public $attribute_data;

    /**
     * @var boolean $is_active- The active
     */
    public $is_active;

    /**
     * @var boolean $is_default - The default
     */
    public $is_default;

    /**
     * @var int $sort_order - The sort order
     */
    public $sort_order;

}
