<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use Hashids\Hashids;
use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for users
 */
class UserAutoLogin extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_auto_login';

    /**
     * @var string $user_hash - the user hash of this link
     */
    public $user_hash;

    /**
     * @var string $login_hash - the login hash of this link
     */
    public $login_hash;

    /**
     * @var string $redirect - the redirect to send the user to upon login completion
     */
    public $redirect;

    /**
     * @var bool $two_factor_completed - true if two factor login has already been completed
     */
    public $two_factor_completed;

    /**
     * @var string $expires - The date that this login link expires
     */
    public $expires;

    /**
     * @var int $user_id - The user ID that this login is for
     */
    public $user_id;

    /**
     * Function to call after inserting the data.
     * This function creates the required hashes
     * @param array $data
     * @return array
     * */
    protected function afterInsertData($data)
    {
        $h = new Hashids();
        $this->user_hash = $h->encode(max(1,($data['user_id'] + $data['id']) % 314159265) );
        $this->login_hash = $h->encode($data['id']);
        $data = ['user_hash' => $this->user_hash, 'login_hash' => $this->login_hash];
        $this->getWriteDb()->update($this->getTableName(), $data, ['id' => $this->id]);
    }

    /**
     * Get the URI for a login link
     * @access public
     * @return string
     * */
    public function getLinkUri()
    {
        return "/_l/{$this->login_hash}/{$this->user_hash}";
    }

    /**
     * Get the URL for a login link
     * @access public
     * @return string
     * */
    public function getLinkUrl()
    {
        if(php_sapi_name()=='cli') {
            $result = $this->getState()->getAccount()->getUrl() . $this->getLinkUri();
        }
        else {
            $result = $this->getState()->getRequest()->getSchemeAndHttpHost() . $this->getLinkUri();
        }
        return $result;
    }

}
