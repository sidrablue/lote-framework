<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;
use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for users
 */
class UserGdprConsentLog extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_gdpr_consent_log';

    /**
     * @var int $user_id - the user id
     */
    public $user_id;

    /**
     * @var string $token - token
     */
    public $token;

    /**
     * @var string $data - The data
     */
    public $data;

    /**
     * @var datetime $processed - The date processed
     */
    public $processed;
}
