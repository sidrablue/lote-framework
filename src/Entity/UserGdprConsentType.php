<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;
use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for user gdpr consent
 */
class UserGdprConsentType extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_gdpr_consent_type';

    /**
     * @var string $name - The consent type name
     */
    public $name;

    /**
     * @var string $description - The description
     */
    public $description;

}
