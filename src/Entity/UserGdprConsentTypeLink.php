<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Entity class for user gdpr consent type
 */
class UserGdprConsentTypeLink extends Base
{
    const CONSENT_SOURCE_REFERENCE_USER = 'import';
    const CONSENT_SOURCE_REFERENCE_FORM = 'form';
    const CONSENT_SOURCE_REFERENCE_ADMIN_USER = 'admin_user';

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_gdpr_consent_type_link';

    /**
     * @var int $user_id - The user id
     */
    public $user_id;

    /**
     * @var int $consent_source_id - The consent source id
     */
    public $consent_source_id;

    /**
     * @var int $consent_type_id - The consent type id
     */
    public $consent_type_id;

    /**
     * @var int $consent_log_id - The consent log id
     */
    public $consent_log_id;

    /**
     * @var datetime $date_confirmed - The date confirmed
     */
    public $date_confirmed;

    /**
     * @var string $description_add - The description for the consent link creation
     */
    public $description_add;

    /**
     * @var string $consent_method - The consent method - internal reference
     */
    public $consent_method;

    /**
     * @var string $description_delete - The description for the consent link deletion
     */
    public $description_delete;

}
