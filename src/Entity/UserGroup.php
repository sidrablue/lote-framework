<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */


namespace SidraBlue\Lote\Entity;


use SidraBlue\Lote\Object\Entity\Base;

/**
 * Class UserGroup
 * @package SidraBlue\Lote\Entity
 */
class UserGroup extends Base
{
    const SUBSCRIPTION_STATUS_PERMANENTLY_UNSUBSCRIBED = -1;
    const SUBSCRIPTION_STATUS_UNSUBSCRIBED = 0;
    const SUBSCRIPTION_STATUS_SUBSCRIBED = 1;
    const SUBSCRIPTION_STATUS_SUBSCRIBED_DAILY = 2;
    const SUBSCRIPTION_STATUS_SUBSCRIBED_WEEKLY = 3;
    const SUBSCRIPTION_STATUS_SUBSCRIBED_MONTHLY = 4;

    protected $tableName = 'sb__user_group';

    /**
     * @var int $user_id - User ID
     */
    public $user_id;

    /**
     * @var int $group_id
     */
    public $group_id;

    /**
     * @var boolean $approved - True if approved
     */
    public $approved;

    /**
     * @var int $approved_by - User id of whom the entity was approved
     */
    public $approved_by;

    /**
     * @var int $pin_code - Pin code
     */
    public $pin_code;

    /**
     * @var int $dispatch_id - Dispatch ID
     */
    public $dispatch_id;

    /**
     * @var int $subscription_status - Subscription status
     */
    public $subscription_status;

}