<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for login throttle
 */
class UserLoginAttempt extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_login_attempt';

    /**
     * @var int $user_id - The user ID that this login is for
     */
    public $user_id;

    /**
     * @var string $ip_address - the ip address
     */
    public $ip_address;

    /**
     * @var string $username - the username tried
     */
    public $username;

    /**
     * @var string $password - the password tried
     */
    public $password;

    /**
     * @var datetime $attempt - the attempt datetime
     */
    public $attempt;

    /**
     * @var string $status - fail
     */
    public $status;

    /**
     * Function to call before inserting data so that data can be manipulated if required
     *
     * @access protected
     * @param array $data
     * @return array
     * */
    protected function beforeInsert($data)
    {
        if ($data['password'] && !empty($data['password'])) {
            if (!(strlen($data['password']) == 60 && strpos($data['password'], '$2y$10$') === 0)) {
                $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
            }
        }
        return $data;
    }

    /**
     * Function to call before inserting data so that data can be manipulated if required
     *
     * @access protected
     * @param array $data - Entity data
     * @return array
     * */
    protected function beforeUpdate($data)
    {
        if ($data['password'] && !empty($data['password'])) {
            if (!(strlen($data['password']) == 60 && strpos($data['password'], '$2y$10$') === 0)) {
                $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
            }
        }
        return $data;
    }
}
