<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for login throttle
 */
class UserLoginThrottle extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_login_throttle';

    /**
     * @var int $user_id - The user ID that this login is for
     */
    public $user_id;

    /**
     * @var string $ip_address - the user ip address
     */
    public $ip_address;

    /**
     * @var int $attempts - number of attempts
     */
    public $attempts;

    /**
     * @var datetime $last_attempt - the last attempt datetime
     */
    public $last_attempt;

    /**
     * @var bool $suspended - is user suspended
     */
    public $suspended;

    /**
     * @var datetime $suspended_time - The date that the user was suspended
     */
    public $suspended_time;

    /**
     * @var bool $banned - is user banned
     */
    public $banned;

    /**
     * @var datetime $banned_time - The date that the user was banned
     */
    public $banned_time;

}
