<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;
use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for users
 */
class UserNextOfKin extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_next_of_kin';

    /**
     * @var int $user_id- the user id
     */
    public $user_id;

    /**
     * @var string $name- the kin name
     */
    public $name;

    /**
     * @var string $relationship- The relationship
     */
    public $relationship;

    /**
     * @var string $address- The users kin address
     */
    public $address;

    /**
     * @var string $mobile - The users kin mobile
     */
    public $mobile;

    /**
     * @var string $phone - The users kin phone
     */
    public $phone;

    /**
     * @var string $phone_secondary - The users kin phone
     */
    public $phone_secondary;

    /**
     * @var string $email - The users kin email
     */
    public $email;

}
