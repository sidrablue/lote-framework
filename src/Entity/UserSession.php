<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;
use SidraBlue\Util\Time;

/**
 * Entity class for the user login session
 */
class UserSession extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_session';

    /**
     * @var int $site_id - the site that this login is for
     */
    public $site_id;

    /**
     * @var string $source - the source of this user, such as 'local', 'ldap', 'google', 'facebook', etc.
     */
    public $source = 'local';

    /**
     * @var string $token - the encryption token for this login
     */
    public $token;

    /**
     * @var string $hash - The encrypted login hash
     */
    public $hash;

    /**
     * @var int $user_id - The user ID
     */
    public $user_id;

    /**
     * @var string $username - The username
     */
    public $username;

    /**
     * @var string $session_id - The web server session id of the current login
     */
    public $session_id;

    /**
     * @var string $ip_address - The IP Address of the current login session
     */
    public $ip_address;

    /**
     * @var boolean $active - true if this session is still active
     */
    public $active = '1';

    /**
     * @var \DateTime $last_activity - The last time that activity occurred for this login session
     */
    public $last_activity;

    /**
     * @var \DateTime $expires - The time that this session expires
     */
    public $expires;

    /**
     * @dbColumn masquerade_user_id
     * @dbType integer
     * @dbOptions notnull = false
     * @var int $masquerade_user_id - a user ID to fall back into and login as afterwards
     */
    public $masquerade_user_id;

    /**
     * @dbColumn is_two_factor_verified
     * @dbType boolean
     * @dbOptions notnull = false, default=false
     * @var bool $is_two_factor_verified
     */
    public $is_two_factor_verified;

    /**
     * Determine if the details of this login session imply that a user is logged in
     * @access public
     * @return bool
     * @todo - validate this, to see if its still working
     * */
    public function isValidLogin()
    {
        return $this->id && $this->expires > Time::getUtcNow() && $this->active;
    }

    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeInsert($data)
    {
        $data = $this->beforeUpdate($data);
        $data['token'] = uniqid();
        return $data;
    }

    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeUpdate($data)
    {
        $time = new \DateTime('now', new \DateTimeZone('UTC'));
        $data['last_activity'] = $time->format("Y-m-d H:i:s");
        $time->add(new \DateInterval('PT5M'));
        $data['expires'] = $time->format("Y-m-d H:i:s");
        return $data;
    }

}
