<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base class for users
 */
class UserSubscriptionConsentSource extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_subscription_consent_source';

    /**
     * @var string $name - the name of the source
     */
    public $name;

    /**
     * @var string $reference - the reference of the source
     */
    public $reference;

    /**
     * @var string $description - the description of the source
     */
    public $description;

}
