<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;
use SidraBlue\Lote\Object\Entity\BaseMaster;

/**
 * Base class for users
 */
class UserTermsAndConditions extends BaseMaster
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_terms_and_conditions';

    /**
     * @var int $id
     */
    public $id;

    /**
     * @var string $account_ref - the account reference
     */
    public $account_ref;

    /**
     * @var int $user_id - the user id
     */
    public $user_id;

    /**
     * @var int $terms_and_conditions_id - the id of the latest tc related user has agreed to
     */
    public $terms_and_conditions_id;

    /**
     * @var int $date_agreed - the date the user agreed to the specified t&c
     */
    public $date_agreed;

}
