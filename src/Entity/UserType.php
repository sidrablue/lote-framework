<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Entity;

use SidraBlue\Lote\Object\Entity\Base;

/**
 * Base entity for types
 */
class UserType extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__user_type';

    /**
     * @var int $type_id - the type id
     */
    public $type_id;

    /**
     * @var int $object_id - The object id
     */
    public $object_id;

}
