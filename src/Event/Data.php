<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Event;

use SidraBlue\Lote\Model\Group as GroupModel;

/**
 * Base class for event dispatch data
 */
class Data
{

    /**
     * @var mixed $data - the event data
     * */
    public $data = null;

    /**
     * @var mixed $params - the event params
     * */
    private $params = null;

    /**
     * Default constructor
     * @param null $data
     * @param null $params
     */
    public function __construct($data = null, $params = null)
    {
        $this->data = $data;
        $this->params = $params;
    }

    /**
     * Get the data for this event
     * @access public
     * @param null|string $key
     * @param null|mixed $default
     * @return mixed
     */
    public function getData($key = null, $default = null)
    {
        $result = $default;
        if($key && array_key_exists($key, $this->data)) {
            $result = $this->data[$key];
        }
        elseif(!$key) {
            $result = $this->data;
        }
        return $result;
    }

    /**
     * Set the data for this event
     * @access public
     * @param mixed $data
     * @return void
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Get the params for this event
     * @access public
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Set the params for this event
     * @access public
     * @param mixed $params
     * @return void
     */
    public function setParams($params)
    {
        $this->params = $params;
    }

}
