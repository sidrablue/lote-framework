<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Event;

use SidraBlue\Lote\Model\Group as GroupModel;
use SidraBlue\Lote\Object\State as BaseState;

/**
 * Base class for all models
 */
class Manager extends BaseState
{

    /**
     * @var array $listeners - the events to observes
     * */
    private $listeners = [];

    /**
     * Check if a user is in a specified group
     * @access public
     * @param string $eventRef
     * @param Data $eventData
     * @return bool
     */
    public function dispatch($eventRef, Data $eventData)
    {
        if (isset($this->listeners[$eventRef]) && is_array($this->listeners[$eventRef])) {
            foreach ($this->listeners[$eventRef] as $listener) {
                $params = array_slice(func_get_args(), 1);
                call_user_func_array($listener, array_merge([$eventData], $params));
            }
        }
    }

    /**
     * Register an interest in an event
     * @access public
     * @param $eventRef
     * @param $callback
     */
    public function listen($eventRef, $callback)
    {
        $this->listeners[$eventRef][] = $callback;
    }

}
