<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Interfaces;

use SidraBlue\Lote\State\Base as BaseState;

/**
 * Interface StateInterface
 *
 * @package SidraBlue\Lote\Framework\Interfaces
 * @author Amel Holic <amel@sidrablue.com.au>
 */
interface StateInterface
{
    /**
     * Get the state object
     * @access public
     * @return BaseState
     */
    public function getState();

    /**
     * Set the state object
     * @access public
     * @param BaseState $state
     * @return void
     */
    public function setState($state);

}
