<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Log;

use SidraBlue\Lote\Registry\Config;
use SidraBlue\Lote\State\Base as BaseState;

/**
 * Primary logging class for the Lote Framework.
 *
 * @package SidraBlue\Lote\Log
 * */
class Set
{

    /**
     * The loggers
     * @var array $loggers - Array of LoteLog loggers
     * */
    private $loggers = [];


    /**
     * The config
     * @var Config $config
     * */
    private $config = [];


    /**
     * The state
     * @var BaseState $state
     * */
    private $state = [];


    /**
     * The suppressed paths
     * @var array $suppressedPaths
     * */
    private $suppressedPaths = [];

    /**
     * Private constructor to create an instance of the Logger
     * @access public
     * @param Config $config
     * @param BaseState $state
     */
    public function __construct(Config $config, BaseState $state)
    {
        $this->config = $config;
        $this->state = $state;
        $this->loggers['account'] = [];
        $this->loggers['master'] = [];
    }

    /**
     * Get an account logger by its reference
     * @access public
     * @param string $reference
     * @return LoteLog
     * */
    public function getAccountLogger($reference = 'log') {
        if(!isset($this->loggers['account']) || !isset($this->loggers['account'][$reference])) {
            $log = new LoteLog($this->state, 'log.account', dirname(LOTE_LOG_PATH).'/'.$this->state->accountReference.'/', $reference.'.txt', $reference);
            $this->addSuppressedPathsToLog($log);
            $this->loggers['account'][$reference] = $log;
        }
        return $this->loggers['account'][$reference];
    }

    /**
     * Get an account logger by its reference
     * @access public
     * @param string $reference
     * @return LoteLog
     * */
    public function getMasterLogger($reference = 'log') {
        if(!isset($this->loggers['master']) || !isset($this->loggers['master'][$reference])) {
            $log = new LoteLog($this->state, 'log.master', dirname(LOTE_LOG_PATH).'/_master/', $reference.'.txt', $reference);
            $this->addSuppressedPathsToLog($log);
            $this->loggers['master'][$reference] = $log;
        }
        return $this->loggers['master'][$reference];
    }

    /**
     * Add a suppressed path, for which no errors are to be logged
     * @access public
     * @param $pathRegex
     * @return void
     */
    public function addSuppressedPath($pathRegex)
    {
        $this->suppressedPaths[] = $pathRegex;
        /** @var LoteLog $log */
        foreach($this->loggers['account'] as $log) {
            $log->addSuppressedPath($pathRegex);
        }
        foreach($this->loggers['master'] as $log) {
            $log->addSuppressedPath($pathRegex);
        }
    }

    /**
     * Add a suppressed path, for which no errors are to be logged
     * @access public
     * @param LoteLog $log
     * @return void
     */
    public function addSuppressedPathsToLog(LoteLog $log)
    {
        foreach($this->suppressedPaths as $path) {
            $log->addSuppressedPath($path);
        }
    }

}
