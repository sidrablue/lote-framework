<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model\Access;

use SidraBlue\Lote\Object\Model\Base;

/**
 * Model class for an Access entity
 *
 * @package SidraBlue\Lote\Model
 */
class Entity extends Base
{

    /**
     * @var string $tableName
     */
    protected $tableName = 'sb__access_entity';

    /**
     * Get the object access defined
     * @param string $objectId
     * @param string $objectReference - the object in question
     * @param string $accessLevel - the access level, defaults to 'write'
     * @param string $accessType - the access type, defaults to 'group'
     * @return array|false
     * */
    public function getObjectAccess($objectId, $objectReference, $accessLevel = 'write', $accessType = 'group')
    {
        $result = false;
        if ($objectId && $objectReference) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select("e.*")
                ->from($this->getTableName(), "e")
                ->where("e.object_ref = :object_ref")
                ->setParameter("object_ref", $objectReference)
                ->andWhere("e.object_id = :object_id")
                ->setParameter("object_id", $objectId);
            if ($accessLevel) {
                $q->andWhere("e.access_level = :access_level")->setParameter("access_level", $accessLevel);
            }
            if ($accessType) {
                $q->andWhere("e.access_type = :access_type")->setParameter("access_type", $accessType);
            }
            $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        }
        return $result;
    }

    public function getUserAccess($access_type, $access_type_id, $object_ref = false)
    {
        $result = false;
        if ($access_type && $access_type_id) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select("e.*")
                ->from($this->getTableName(), "e")
                ->where("e.access_type = :access_type")
                ->setParameter("access_type", $access_type)
                ->andWhere("e.access_type_id = :access_type_id")
                ->setParameter("access_type_id", $access_type_id);
            if ($object_ref) {
                $q->andWhere("e.object_ref = :object_ref")->setParameter("object_ref", $object_ref);
            }
            $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        }
        return $result;
    }


}
