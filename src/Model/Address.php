<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;
use Doctrine\DBAL\Connection;

/**
 * Model class for Importing
 */
class Address extends Base
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__address';

    /**
     * Address search query
     *
     * @access public
     * @param array $params - Search parameters
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    public function getSearchQuery($params)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('a.*')
            ->from($this->getTableName(), 'a')
            ->andWhere("a.lote_deleted is null");

        if(isset($params['object_ref']) && $params['object_ref']) {
            $q->andWhere("a.object_ref = :objectRef")
                ->setParameter('objectRef', $params['object_ref']);

            if(isset($params['object_id']) && $params['object_id']) {
                $q->andWhere("a.object_id = :objectId")
                    ->setParameter('objectId', $params['object_id']);
            }
        }

        if(isset($params['postcode']) && $params['postcode']) {
            $q->andWhere('a.postcode = :postcode')
                ->setParameter('postcode', $params['postcode']);
        }

        if(isset($params['address_type']) && $params['address_type']) {
            $q->andWhere('a.address_type = :addressType')
                ->setParameter('addressType', $params['address_type']);
        }

        if(isset($params['states']) && $params['states']) {
            $q->andWhere('a.state in (:states)')
                ->setParameter('states', $params['states'] , Connection::PARAM_STR_ARRAY);
        }

        return $q;
    }

    /**
     * Delete addresses from id array
     *
     * @access public
     * @param array $ids - ID's to delete
     */
    public function deleteRemovedItems($ids)
    {
        foreach ($ids as $id) {
            $this->delete($id, true);
        }
    }

    public function getExistingItems($userId = '', $objectRef ='')
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('t.id')
            ->from($this->tableName, 't')
            ->andWhere('t.object_id = :object_id')
            ->setParameter('object_id', $userId)
            ->andWhere('t.object_ref = :object_ref')
            ->setParameter('object_ref', $objectRef);
        $query = $data->execute();
        $row = $query->fetchAll(\PDO::FETCH_ASSOC);
        foreach($row as $value){
            $result[] = $value['id'];
        }

        return $result;
    }

    public function getListByObjectId($objectRef = '', $objectId = '')
    {
        $result = false;
        if($objectId > 0) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('o.*')
                ->from($this->getTableName(), 'o')
                ->where('o.object_ref = :ref')
                ->setParameter('ref', $objectRef)
                ->andWhere('o.object_id = :id')
                ->setParameter('id', $objectId);
            $q->andWhere("o.lote_deleted is null");
            $q->orderBy('o.lote_created', 'desc');
            $s = $q->execute();
            $result = $s->fetchAll(\PDO::FETCH_ASSOC);
        }
        return $result;
    }


    public function getAddress($objectId = '',$objectRef = '',$type = '')
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('ce.*')
            ->from($this->tableName, 'ce')
            ->where('ce.object_id = :object_id')
            ->setParameter('object_id', $objectId)
            ->andWhere('ce.object_ref = :object_ref')
            ->setParameter('object_ref', $objectRef);
        if($type){
            $q->andWhere('ce.address_type = :address_type')
                ->setParameter('address_type', $type);
        }
        $q->andWhere("ce.lote_deleted is null");
        $s = $q->execute();
        $result = $s->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function checkAddressBelongsToUser($userId, $addressId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('count(*) as count')
            ->from($this->tableName, 't')
            ->andWhere('t.object_id = :object_id')
            ->setParameter('object_id', $userId)
            ->andWhere('t.object_ref = :object_ref')
            ->setParameter('object_ref', 'admin.user')
            ->andWhere('t.id = :id')
            ->setParameter('id', $addressId);
        $query = $data->execute();
        $row = $query->fetch(\PDO::FETCH_ASSOC);

        return $row['count'] > 0;
    }



    public function getAddressFromPostCode($params)
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('ce.*')
            ->from($this->tableName, 'ce')
            ->Where("ce.lote_deleted is null");

        if(isset($params['address_type']) && $params['address_type']){
            $q->andWhere('ce.address_type = :address_type')
                ->setParameter('address_type', $params['address_type']);
        }
        if(isset($params['object_ref']) && $params['object_ref']){
            $q->andWhere('ce.object_ref = :object_ref')
                ->setParameter('object_ref', $params['object_ref']);
        }
        if(isset($params['postcode']) && $params['postcode']){
            $q->andWhere('ce.postcode = :postcode')
                ->setParameter('postcode', $params['postcode']);
        }
        if(isset($params['cid']) && $params['cid']){
            $q->andWhere('ce.object_id in (:object_ids)')
                ->setParameter('object_ids', $params['cid'] , Connection::PARAM_STR_ARRAY);
        }
        $s = $q->execute();
        $result = $s->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }

    /**
     * Retrieve trip distance and duration data from one address through a list of destinations
     *
     * @access public
     * @param string $originAddress - Origin address
     * @param array $tripAddresses - Trip addresses
     * @param int|string $departureTime - Unix timestamp departure time
     * @param bool $retrieveDirections - True if directions should be retrieved with trip data
     * @return array
     */
    public function getTripDistanceData($originAddress, $tripAddresses, $departureTime = 'now', $retrieveDirections = false) {
        $result = [
            'distance' => 0,
            'duration' => 0,
            'error' => [],
            'success' => true,
        ];
        if($originAddress && is_array($tripAddresses) && count($tripAddresses) > 0) {
            foreach($tripAddresses as $destinationAddress) {
                $mapData = $this->getGoogleDistanceData($originAddress, $destinationAddress, $departureTime);
                if($mapData['success']) {
                    $data = $mapData['response']['rows'][0]['elements'][0];
                    if($data['status'] == 'OK') {
                        $result['distance'] += $data['distance']['value'];
                        $result['duration'] += $data['duration']['value'];

                        if($retrieveDirections) {
                            $routeData = $this->getGoogleDirectionsData($originAddress, $destinationAddress);
                            if($routeData['success']) {
                                $result['directions'][] = $routeData['response']['routes'];
                            }
                        }
                    } else {
                        $result['error'] = $originAddress
                            . ' to '
                            . $destinationAddress
                            . ' is invalid';
                        $result['success'] = false;
                    }
                } else {
                    $result['error'] = $mapData['error'];
                    $result['success'] = false;
                }
                $originAddress = $destinationAddress;
            }
        }
        return $result;
    }

    /**
     * Retrieve google maps distance data between an origin and destination address
     *
     * @access public
     * @param string $originAddress - Origin address
     * @param string $destinationAddress - Destination address
     * @param int|string $departureTime - Unix timestamp departure time
     * @return array
     */
    public function getGoogleDistanceData($originAddress, $destinationAddress, $departureTime = 'now') {
        $apiKey = $this->getState()->getSettings()->get("google.maps.distance.api_key", false);
        if($apiKey) {
            if($originAddress && $destinationAddress) {
                $params = [
                    'units' => 'metric',
                    'origins' => $originAddress,
                    'destinations' => $destinationAddress,
                    'departure_time' => $departureTime,
                    'key' => $apiKey
                ];

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://maps.googleapis.com/maps/api/distancematrix/json?" . http_build_query($params));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HTTPGET, true);
                $response = json_decode(curl_exec($ch), 1);
                curl_close($ch);

                if($response['status'] == 'OK') {
                    $result = [
                        'success' => true,
                        'response' => $response,
                    ];
                } else {
                    $result = [
                        'success' => false,
                        'response' => $response,
                        'error' => 'Invalid request.'
                    ];
                }
            } else {
                $result = [
                    'success' => false,
                    'error' => 'Required addresses not set.'
                ];
            }
        } else {
            $result = [
                'success' => false,
                'error' => 'Invalid API key.'
            ];
        }
        return $result;
    }

    /**
     * Retrieve google maps directions data between an origin and destination address
     *
     * @access public
     * @param string $originAddress - Origin address
     * @param string $destinationAddress - Destination address
     * @return array
     */
    public function getGoogleDirectionsData($originAddress, $destinationAddress) {
        $apiKey = $this->getState()->getSettings()->get("google.maps.directions.api_key", false);
        if($apiKey) {
            if($originAddress && $destinationAddress) {
                $params = [
                    'origin' => $originAddress,
                    'destination' => $destinationAddress,
                    'key' => $apiKey
                ];

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://maps.googleapis.com/maps/api/directions/json?" . http_build_query($params));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HTTPGET, true);
                $response = json_decode(curl_exec($ch), 1);
                curl_close($ch);

                if($response['status'] == 'OK') {
                    $result = [
                        'success' => true,
                        'response' => $response,
                    ];
                } else {
                    $result = [
                        'success' => false,
                        'response' => $response,
                        'error' => 'Invalid request.'
                    ];
                }
            } else {
                $result = [
                    'success' => false,
                    'error' => 'Required addresses not set.'
                ];
            }
        } else {
            $result = [
                'success' => false,
                'error' => 'Invalid API key.'
            ];
        }
        return $result;
    }

    public function getAddressLinkedToUser($userId = null)
    {
        if (is_null($userId)) {
            $userId = $this->getState()->getUser()->id;
        }

        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('address.street1, address.city, address.postcode, address.state')
            ->from($this->tableName, 'address')
            ->where('address.lote_deleted is null')
            ->andWhere('address.address_type = :addressType')
            ->setParameter('addressType', 'physical')
            ->leftJoin('address', 'sb_ymc_organisation_contacts', 'contacts', 'address.object_id = contacts.object_id and address.object_ref = contacts.object_ref ')
            ->andWhere('contacts.lote_deleted is null')
            ->andWhere('contacts.user_id = :userId')
            ->setParameter('userId', $userId);

        $result =  $q->execute()->fetch(\PDO::FETCH_ASSOC);
        return $result['street1'] . ", " . $result['city'] . ", " . $result['state'] . ", " . $result['postcode']; // Could use php implode function here, but order of keys may not be in order
    }
}
