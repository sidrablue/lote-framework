<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;

/**
 * Model class for a file
 *
 * @package Lote\System\Crm\Model
 */
class Api extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__api_key';

    public function updateApi($detailsArr = [])
    {
        $id = isset($detailsArr['id']) ? intval($detailsArr['id']) : false;
        if (!empty($id)) {
            $this->getState()->getWriteDb()->update($this->tableName, $detailsArr, ['id' => $id]);
        }
        return $id;
    }

} 