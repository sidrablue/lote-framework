<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model\Audit;

use SidraBlue\Lote\Object\Model\Base;

/**
 * Model class for audit logs
 */
class Log extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__audit_log';



}
