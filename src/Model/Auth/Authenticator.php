<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model\Auth;

use SidraBlue\Lote\Object\Model\Base;
use Google\Authenticator\GoogleAuthenticator;
use SidraBlue\Lote\Entity\User;
use SidraBlue\Lote\Entity\Auth\Authenticator as AuthEntity;


/**
 * Model class for an Authenticator entity
 *
 * @package SidraBlue\Lote\Model
 */
class Authenticator extends Base
{

    /**
     * @var string $tableName
     */
    protected $tableName = 'sb__user_authenticator';

    public function generate($userId, $force = false)
    {
        $auth = new GoogleAuthenticator();
        $twoFactorAuth = new AuthEntity($this->getState());
        if (!$twoFactorAuth->loadByField('object_id', $userId)) {
            $user = new User($this->getState());
            $twoFactorAuth->secret = $auth->generateSecret();
            $twoFactorAuth->object_id = $userId;
            $twoFactorAuth->object_ref = $user->getTableName();
        } elseif ($force) {
            $twoFactorAuth->secret = $auth->generateSecret();
        }
        $twoFactorAuth->save();
        return true;
    }

    public function getQrCode($userId)
    {
        $auth = new GoogleAuthenticator();
        $hostname = $this->getState()->getUrl()->getHttpHost();
        $user = new User($this->getState());
        $user->load($userId);
        return $auth->getURL($user->username, $hostname, $this->getSecret($userId));
    }

    public function getSecret($userId)
    {
        $twoFactorAuth = new AuthEntity($this->getState());
        $twoFactorAuth->loadByField('object_id', $userId);
        return $twoFactorAuth->secret;
    }

    public function authenticate($userId, $code)
    {
        $auth = new GoogleAuthenticator();
        return $auth->checkCode($this->getSecret($userId), $code);
    }

}
