<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use Doctrine\DBAL\Connection;
use Lote\Module\Form\Entity\Form;
use Lote\Module\Form\Model\Field;
use SidraBlue\Lote\Object\Data\Field\Factory;
use SidraBlue\Lote\Object\Model\Base;
use SidraBlue\Lote\Entity\CfField as FieldEntity;
use SidraBlue\Lote\Model\CfGroup as GroupModel;
use SidraBlue\Lote\Model\CfObject as ObjectModel;
use SidraBlue\Util\Strings;

class CfField extends Base
{

    protected $tableName = 'sb__cf_field';

    /**
     * Get the data for a group that this field belongs to
     * @param int $fieldId
     * @access public
     * @return array|false
     * */
    public function getGroup($fieldId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('g.*')
            ->from('sb__cf_field', 'f')
            ->leftJoin('f', 'sb__cf_group', 'g', 'g.id = f.group_id')
            ->where('f.id = :id')
            ->setParameter('id', $fieldId);
        return $query = $data->execute()->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * Get the next sort order for a field group
     * @param int $groupId
     * @return int
     * */
    public function getNextSortOrder($groupId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('max(f.sort_order) + 1')
            ->from('sb__cf_field', 'f')
            ->where('f.group_id = :id')
            ->setParameter('id', $groupId);
        return max(1, $data->execute()->fetchColumn());
    }

    /**
     * Get field details by group
     * @param int $groupId
     * @access public
     * @return array|false
     * */
    public function getGroupFields($groupId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('f.*')
            ->from('sb__cf_field', 'f')
            ->where('f.group_id = :id')
            ->setParameter('id', $groupId);
        return $query = $data->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Get a list of fields from a given set of ID's
     * @param array $fieldIds
     * @access public
     * @return array|false
     * */
    public function getFieldsByIds($fieldIds)
    {
        $result = [];
        if (is_array($fieldIds) && count($fieldIds) > 0) {
            $q = $this->getReadDb()->createQueryBuilder();
            $data = $q->select('f.*')
                ->from('sb__cf_field', 'f')
                ->where('f.id in (:ids)')
                ->setParameter('ids', $fieldIds, Connection::PARAM_STR_ARRAY);
            if ($data = $data->execute()->fetchAll(\PDO::FETCH_ASSOC)) {
                foreach ($data as $fieldData) {
                    $cfEntity = new \SidraBlue\Lote\Entity\CfField($this->getState());
                    $cfEntity->setData($fieldData);
                    $result[] = $cfEntity;
                }
            }
        }
        return $result;
    }

    /**
     * Get a list of fields that are "option" types, for a specified object reference
     * @access public
     * @param string|array $objectRef
     * @return array
     * */
    public function getOptionFieldsByObjectRef($objectRef)
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('f.*, o.reference as object_reference, g.name as group_name')
            ->from('sb__cf_field', 'f')
            ->leftJoin("f", "sb__cf_group", "g", "g.id = f.group_id")
            ->leftJoin("g", "sb__cf_object", "o", "o.id = g.object_id")
            ->where("f.lote_deleted is null")
            ->andWhere("g.lote_deleted is null")
            ->andWhere("o.lote_deleted is null")
            ->andWhere("f.field_type in (:option_field_types)")
            ->setParameter("option_field_types", $this->getOptionFieldTypes(), Connection::PARAM_STR_ARRAY);
        if (is_array($objectRef)) {
            $q->andWhere("o.reference in (:object_references)")
                ->setParameter("object_references", $objectRef, Connection::PARAM_STR_ARRAY);
        }
        $q->addOrderBy("g.name", "asc")
            ->addOrderBy("f.name", "asc")
            ->addOrderBy("f.id", "asc");
        $s = $q->execute();
        while ($row = $s->fetch()) {
            $result[$row['object_reference']][] = $row;
        }
        return $result;
    }

    /**
     * Get the list of filed types that correspond to "option" type fields
     * @access public
     * @return array
     * */
    public function getOptionFieldTypes()
    {
        return ['single_select', 'multi_select', 'radio_button', 'checkbox'];
    }

    public function addUpdateField($data, $autoManageReference = false, $opts = [])
    {
        $referenceSuffix = !empty($opts['ref_suffix']) ? $opts['ref_suffix'] : '';
        $g = new FieldEntity($this->getState());
        $id = !empty($data['id']) ? $data['id'] : 0;
        $m = new GroupModel($this->getState());
        $group = $m->get($data['group_id']);
        if ($autoManageReference) {
            if ($id && $g->load($data['id'])) {
                $reference = $g->reference;
            } else {
                $reference = $data['name'] . $referenceSuffix;
                if (!empty($data['reference'])) {
                    $reference = $data['reference'] . $referenceSuffix;
                }
                $reference = Strings::generateHtmlId($reference);
            }

            if ($group) {
                $m = new ObjectModel($this->getState());
                while ($m->fieldReferenceExists($reference, $id, false)) {
                    $reference = Strings::generateHtmlId($reference . '_' . substr(uniqid(), -4));
                }
            }
            $data['reference'] = $reference;
        }

        $data['config_data'] = json_encode($data['config_data']);
        $g->setData($data);

        $result = $g->save();
        $field = Factory::createInstance($this->getState(), $g->field_type);
        $field->saveFieldTypeData($result, $data);
        $this->cleanSortOrder(false, $g->group_id);

        return $result;
    }

    /**
     * Function to cleanup the sort order of fields, after an addition or a deletion.
     * This function will also ensure that the GDPR field is always the last on, if one exists.
     *
     * @access public
     * @param int $formId
     * @return void
     * @todo - move this to the object or the group model
     * @todo - check the form-add-page.json url
     * */
    public function cleanSortOrder($objectId = false, $groupId = false)
    {
        //we are given an object ID or a group ID, but ultimately we need the object ID so load it if we don't have it
        if (!$objectId && $groupId) {
            if (!$objectId) {
                $cfGroup = new \SidraBlue\Lote\Entity\CfGroup($this->getState());
                $cfGroup->load($groupId);
                $objectId = $cfGroup->object_id;
            }
        }
        if ($objectId) {
            $cfObjectModel = new CfObject($this->getState());
            $cfGroupModel = new CfGroup($this->getState());

            $groups = $cfObjectModel->getCfGroups($objectId);
            //if gdpr field is not last, make it last
            $gdprField = $cfObjectModel->getGdprConsentField($objectId);
            if ($gdprField) {
                $lastGroup = $groups[count($groups) - 1];
                $lastGroupFields = $cfGroupModel->getCfFields($lastGroup['id']);

                if ($gdprField->group_id != $lastGroup['id'] && count($lastGroupFields) > 0) {
                    $gdprField->group_id = $lastGroup['id'];
                    $gdprField->save();
                } elseif (count($groups) > 1 && count($lastGroupFields) == 1 && $lastGroupFields[0]['db_field'] == 'gdpr_consent') {
                    $secondLastGroup = $groups[count($groups) - 2];
                    $gdprField->group_id = $secondLastGroup['id'];
                    $gdprField->save();
                }
            }
            //iterate through all fields to ensure that they are in the correct order
            foreach ($groups as $g) {
                $fields = $cfGroupModel->getCfFields($g['id']);
                $sortOrder = 1;
                foreach ($fields as $f) {
                    if ($f['db_field'] == 'gdpr_consent') {
                        if ($f['sort_order'] != count($fields)) {
                            $this->setSortOrder($f['id'], count($fields));
                        }
                    }
                    else {
                        if ($sortOrder != $f['sort_order']) {
                            $this->setSortOrder($f['id'], $sortOrder);
                        }
                        $sortOrder++;
                    }
                }
            }
            if ($gdprField && count($groups) > 1) {
                $groups = $cfObjectModel->getCfGroups($objectId);
                $lastGroup = $groups[count($groups) - 1];
            }
        }
    }

    /**
     * Set the sort order of a field
     *
     * @access private
     * @param int $fieldId
     * @param int $sortOrder
     * @return void
     * */
    private function setSortOrder($fieldId, $sortOrder)
    {
        $cfField = new \SidraBlue\Lote\Entity\CfField($this->getState());
        if($cfField->load($fieldId)) {
            $cfField->sort_order = $sortOrder;
            $cfField->save();
        }
    }

}
