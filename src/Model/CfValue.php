<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;

class CfValue extends Base
{

    protected $tableName = 'sb__cf_value';

    /**
     * Set the table name if there is a custom one
     * @access public
     * @param string $newName
     * @return void
     * */
    public function setTableName($newName)
    {
        $this->tableName = $newName;
    }

    /**
     * Retrieve all custom field values with the given field ID
     *
     * @access public
     * @param int $fieldId - Field ID
     * @return array
     */
    public function searchByFieldId($fieldId) {
        $result = [];
        if($fieldId) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('v.*, vo.option_id, vo.value_string as option_value_string, vo.value_number as option_value_number, 
            vo.value_date as option_value_date, vo.value_text as option_value_text, vo.value_boolean as option_value_boolean')
                ->from($this->getTableName(), 'v')
                ->leftJoin('v', "{$this->getTableName()}_option_selected", 'vo', 'vo.value_id = v.id AND vo.lote_deleted IS NULL')
                ->where('v.field_id = :fieldId')
                ->setParameter('fieldId', $fieldId)
                ->andWhere('v.lote_deleted IS NULL');
            $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        }
        return $result;
    }

}
