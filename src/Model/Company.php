<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use Doctrine\DBAL\Connection;
use SidraBlue\Lote\Object\Model\CustomField;
use SidraBlue\Util\Arrays;

/**
 * Model class for Companies
 */
class Company extends CustomField
{
    /**
     * @var string $objectReference
     * The reference of the object
     * */
    protected $objectReference = '_company';


    /**
     * Get all of a companies groups
     * @param  int $companyId
     * @param string $kind - the "kind" of the groups that we are looking at
     * @return array of groups
     */
    public function getCompanyGroups($companyId, $kind = '')
    {
        $result = false;
        if ($companyId) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('g.*')
                ->from('sb__group', 'g')
                ->leftJoin('g', 'sb__company_group', 'l', 'l.group_id = g.id')
                ->where('l.company_id = :company_id')
                ->setParameter('company_id', $companyId);
            if($kind) {
                $q->andWhere('g.kind = :kind')->setParameter('kind', $kind);
            }
            $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        }
        return $result;
    }

    /**
     * Get all of a companies group ID's
     * @param int $companyId
     * @param string $kind - the "kind" of the groups that we are looking at
     * @return array of ID's
     */
    public function getCompanyGroupIds($companyId, $kind = '')
    {
        $groups = $this->getCompanyGroups($companyId, $kind);
        return Arrays::getFieldValuesFrom2dArray($groups, 'id');
    }

    /**
     * Insert a company into a specified set of groups
     * @param int $companyId - the company id for which to insert the groups
     * @param array $companyGroups - the company groups IDs to insert
     * @return void
     * */
    public function insertCompanyGroups($companyId, $companyGroups)
    {
        if ($companyId && is_array($companyGroups) && count($companyGroups) > 0) {
            foreach ($companyGroups as $v) {
                $this->getWriteDb()->insert('sb__company_group', ['company_id' => $companyId, 'group_id' => $v]);
            }
        }
    }

    /**
     * Delete a companies membership of specific groups
     * @param int $companyId - the user id for which to delete the groups
     * @param array $companyGroups - the user groups IDs to delete
     * @return void
     * */
    public function removeCompanyGroups($companyId, $companyGroups)
    {
        if (is_numeric($companyId) && is_array($companyGroups) && count($companyGroups) > 0) {
            foreach ($companyGroups as $g) {
                $this->getWriteDb()->delete('sb__company_group', ['company_id' => $companyId, 'group_id' => $g]);
            }
        }
    }

    /**
     * Delete a company
     *
     * @param bool $strongDelete - Whether or not to delete the table object completely
     * @param int $companyId - the company to delete
     * @return void
     * @todo - implement a trigger on delete for other things to take note of...
     */
    public function delete($companyId, $strongDelete = false)
    {
        $this->getWriteDb()->delete('sb__company_group', ['company_id' => $companyId]);
        parent::delete($companyId, $strongDelete);
    }

    /**
     * Delete's multiple companies
     *
     * @param bool $strongDelete - Whether or not to delete the table object completely
     * @param $company_ids
     */
    public function bulkDelete($company_ids, $strongDelete = false)
    {
        foreach ($company_ids as $companyId)
        {
            $this->delete($companyId, $strongDelete);
        }
    }

    /**
     * Get a companies timezone
     * @param int $timezoneId
     * @return array of timezone details
     */
    public function getCompanyTimezone($timezoneId)
    {
        $result = false;
        if ($timezoneId) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('t.*')
                ->from('sb__timezone', 't')
                ->where('t.id = :id')
                ->setParameter('id', $timezoneId);
            $result = $q->execute()->fetch(\PDO::FETCH_ASSOC);
        }
        return $result;
    }

    /**
     * Get the data for a set of companies defined by an Array of ID's
     * @param array $companyIds - an array of company ids
     * @return array
     * */
    public function getCompanies(Array $companyIds)
    {
        $result = [];
        if (count($companyIds) > 0) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('u.*')->from('sb__company', 'u')->where('u.id in (:ids)')->setParameter(
                'ids',
                $companyIds,
                Connection::PARAM_INT_ARRAY
            );
            $s = $q->execute();
            $rows = $s->fetchAll(\PDO::FETCH_ASSOC);
            if (is_array($rows)) {
                foreach ($rows as $u) {
                    $result[$u['id']] = $u;
                }
            }
        }
        return $result;
    }

    /**
     * Add company data to an existing array that contains entries with a company_id field in each row
     * @param array $data - the data that contains the company_id fields
     * @return array
     * */
    public function addCompanyData($data)
    {
        $values = Arrays::getFieldValuesFrom2dArray($data, 'company_id', true);
        $companies = $this->getCompanies($values);
        $cnt = count($data);
        for ($i = 0; $i < $cnt; $i++) {
            if (isset($companies[$data[$i]['company_id']])) {
                $data[$i]['_company'] = $companies[$data[$i]['company_id']];
            }
        }
        return $data;
    }

    /**
     * Function to check if company name exists
     * @access public
     * @param $companyName
     * @param int $id
     * @return bool
     */
    public function companyNameExists($companyName, $id = 0)
    {
        return $this->valueExists('name', $companyName, $id);
    }

    /**
     * Function to check whether the email address exists within a different company
     *
     * @param $email
     * @param int $id
     * @return bool
     */
    public function emailExists($email, $id = 0)
    {
        return $this->valueExists('email', $email, $id);
    }

    /**
     * Get the unique sources for companies within this system
     * @access public
     * @return array
     * */
    public function getSources() {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('distinct(u.source) as src')->from('sb__company', 'u')->execute();
        $s = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        foreach($s as $row) {
            $s = [];
            $s['id'] = $row['src'];
            if($row['src']=='ldap') {
                $s['name'] = 'LDAP';
            }
            else {
                $s['name'] = ucfirst($row['src']);
            }
            $result[] = $s;
        }
        return $result;
    }



    /**
     * Get the core list of fields for this database
     * @todo - potentially look at making this dynamic in some way
     * @return array
     * */
    public function getCoreFields()
    {
        $fields = [];
        $fields['id'] = 'id';
        $fields['name'] = 'Company Name';
        $fields['email'] = 'Email';
        $fields['phone_number'] = 'Phone';
        $fields['street'] = 'Street Address';
        $fields['street2'] = 'Street Address 2';
        $fields['city'] = 'City';
        $fields['state'] = 'State';
        $fields['postcode'] = 'Postcode';
        $fields['country'] = 'Country';
        $fields['active'] = 'Active';
        $fields['business_number'] = 'Business Number';
        $fields['business_number_type'] = 'Business Number Type';
        $fields['trading_name'] = 'Trade Name of Company';
        $fields['created'] = 'Date Created';
        $fields['updated'] = 'Date Last Updated';
        $fields['user_id'] = 'User ID';
        $fields['lote_access'] = 'Access Variable';

        return $fields;
    }
}