<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;
use SidraBlue\Util\Time;

/**
 * Model class for Dispatch
 */
class Dispatch extends Base
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__dispatch';

    /**
     * Check if there is currently a dispatch in progress
     * @access public
     * @param boolean $closeInvalid - close any invalid processes
     * @return boolean
     * */
    public function isProcessing($closeInvalid = false)
    {
        if($closeInvalid && $this->getProcessingCount() > 0) {
            $this->closeInvalid();
        }
        return $this->getProcessingCount() > 0;
    }

    /**
     * Get the total number of dispatches that are currently processing
     * */
    public function getProcessingCount()
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('count(*) as cnt')
            ->from('sb__dispatch', 'd')
            ->where('completed is null');
        return $q->execute()->fetchColumn();
    }

    /**
     * Get the total number of dispatches that are currently processing
     * */
    public function getProcessingList()
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from('sb__dispatch', 'd')
            ->where('completed is null');
        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Close any invalid processes
     * @access public
     * @return boolean
     * */
    public function closeInvalid()
    {
        $list = $this->getProcessingList();
        foreach($list as $l) {
            $this->closeProcess($l);
        }
    }

    /**
     * Close a process that is not finalised
     * @access public
     * @param array $processData|int - the data of the process
     *
     * */
    public function closeProcess($processData)
    {
        if(isset($processData) && isset($processData['id']) && array_key_exists('completed',$processData) && is_null($processData['completed'])) {
            $this->getWriteDb()->update('sb__dispatch',['completed' => Time::getUtcNow(),'status'=>'aborted'], ['id' => $processData['id']]);
        }
    }

}
