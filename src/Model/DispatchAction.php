<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;
use SidraBlue\Lote\Entity\DispatchAction as DispatchActionEntity;

/**
 * Model class for Dispatch
 */
class DispatchAction extends Base
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__dispatch_action';

    /**
     * Check if there is currently a dispatch in progress
     * @access public
     * @param $actionRef
     * @param string $objectRef
     * @param string $objectId
     * @return bool|DispatchActionEntity
     */
    public function getLastAction($actionRef, $objectRef = '', $objectId = '')
    {
        $result = false;
        $e = new DispatchActionEntity($this->getState());
        if($e->loadLastExecution($actionRef, $objectRef, $objectId)) {
            $result = $e;
        }
        return $result;
    }

    /**
     * Retrieve actions by reference and object
     *
     * @access public
     * @param string $actionRef - Reference
     * @param string $objectRef - Object reference
     * @param string $objectId - Object ID
     * @return array
     */
    public function getActionsByReference($actionRef, $objectRef = '', $objectId = '')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('ar.*')
            ->from($this->getTableName(), 'ar')
            ->where('ar.lote_deleted is null')
            ->andWhere('ar.reference = :actionRef')
            ->setParameter('actionRef', $actionRef);

        if(isset($objectRef) && $objectRef) {
            $q->andWhere('ar.object_ref = :objectRef')
                ->setParameter('objectRef', $objectRef);
        }
        if(isset($objectId) && $objectId) {
            $q->andWhere('ar.object_id = :objectId')
                ->setParameter('objectId', $objectId);
        }

        $actions = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return $actions;
    }

}
