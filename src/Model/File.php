<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use Aws\Result;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use Doctrine\DBAL\Connection;
use League\Flysystem\Adapter\Local;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;
use Lote\Module\Media\Entity\File as MediaFileEntity;
use SidraBlue\Lote\Entity\File as FileEntity;
use SidraBlue\Lote\Entity\FileImage as FileImageEntity;
use SidraBlue\Lote\Event\Data;
use SidraBlue\Lote\Object\Model\Base;
use SidraBlue\Lote\Service\Image;
use SidraBlue\Util\Arrays;
use SidraBlue\Util\Dir;
use SidraBlue\Util\Time;

/**
 * Model class for Files
 */
class File extends Base
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__file';

    /**
     * @var Filesystem[] - the list of filesystems used/created by this model
     *  */
    private $filesystems = [];

    /**
     * The max filesize before streaming is enforced
     * */
    const DOWNLOAD_FULL_DOWNLOAD_LIMIT = 1024 * 1024 * 50;

    /**
     * The size of each part to retrieve when streaming downloads
     * */
    const DOWNLOAD_STREAM_CHUNK_SIZE = 1024 * 1024;

    /**
     * Get the count of the number of lines is a specific file.
     *
     * Typical usage would be for CSV files
     *
     * @access public
     * @param int $fileId - the ID of the file
     * @return int
     *
     * */
    public function getLineCount($fileId)
    {
        ini_set('auto_detect_line_endings', true);
        $result = 0;
        if ($content = $this->getContent($fileId)) {
            $file = new \SplTempFileObject();
            $file->fwrite($content);
            foreach ($file as $lineNumber => $content) {
                $result = ($lineNumber + 1);
            }
        }
        return $result;
    }

    /**
     * Get file as email attachment
     *
     * @access public
     * @param FileEntity $fileEntity - File entity
     * @return array
     * */
    public function getFileAsAttachment($fileEntity)
    {
        $attachment = [];
        //Check users access levels before downloading the file
        $access = $this->checkDownloadAccess($fileEntity);
        if ($access) {
            $fs = $this->getFileSystem('', $fileEntity->is_public);
            $attachment['data'] = base64_encode($fs->read($fileEntity->location_reference));
            if ($fileEntity->mime_type) {
                $attachment['type'] = $fileEntity->mime_type;
            } else {
                $attachment['type'] = $fs->getMimetype($fileEntity->location_reference);
            }
            $attachment['name'] = $fileEntity->filename;
        }
        return $attachment;
    }

    /**
     * Check if a file can be downloaded
     * @param FileEntity $fileEntity
     * @return boolean
     * */
    private function checkDownloadAccess($fileEntity)
    {
        $result = true;
        $functions = $this->getDownloadAccessFunctionArray();
        if (isset($functions[$fileEntity->object_ref])) {
            $f = $functions[$fileEntity->object_ref];
            $result = call_user_func($f, $fileEntity);
        }

        $data = ['file_id' => $fileEntity->id];
        $eventData = new Data($data);
        $this->getState()->getEventManager()->dispatch('file.download.has_access', $eventData);
        if ($eventData->getData()['has_access'] === false) {
            die();
        }

        return $result;
    }

    /**
     * Get the functions for determining access to download a file
     * @access private
     * @return array
     * */
    private function getDownloadAccessFunctionArray()
    {

        $crmFileFunction = function (FileEntity $f) {
            $result = false;
            $m = new \Lote\System\Crm\Entity\File($this->getState());
            if ($m->load($f->object_id)) {
                $result = $this->getState()->getAccess()->hasEntityAccess($m);
            }
            return $result;
        };

        $newsFileFunction = function (FileEntity $f) {
            $result = false;
            $m = new \Lote\Module\News\Entity\Article($this->getState());
            if ($m->load($f->object_id)) {
                $result = $this->getState()->getAccess()->hasEntityAccess($m);
            }
            return $result;
        };

        $mediaFileFunction = function (FileEntity $f) {
            $result = false;
            $m = new MediaFileEntity($this->getState());
            if ($m->load($f->object_id)) {
                $result = $this->getState()->getAccess()->hasEntityAccess($m);
            }
            return $result;
        };

        $fns = [
            "crm.company" => $crmFileFunction,
            "news" => $newsFileFunction,
            "media" => $mediaFileFunction,
            "media.file" => $mediaFileFunction,
            "form" => function (FileEntity $f) {
                return $this->getState()->getAccess()->hasAccess('cms.form', 'edit');
            },
            "import" => function (FileEntity $f) {
                return $this->getState()->getAccess()->hasAccess('admin.import', 'view');
            },
        ];
        return $fns;
    }

    /**
     * @param FileEntity $e
     */
    private function checkFileExpiry(FileEntity $e)
    {
        if ($e->object_ref == 'media' || $e->object_ref == 'sb_media_file_item') {
            if ($this->getState()->getUser()->is_admin || !$this->getState()->getAccess()->hasAccess("cms.media",
                    'view')) {
                if (!$this->getState()->getRequest()->get("filePreviewKey",
                        false) || !$this->getState()->getRedisDbClient()->isEnabled() || !$this->getState()->getRedisDbClient()->get("key:sb__file:preview:" . $this->getState()->getRequest()->get("filePreviewKey"))) {
                    $m = new MediaFileEntity($this->getState());
                    if ($m->load($e->object_id)) {
                        if (method_exists($m, 'isVisible')) {
                            if (!$m->isVisible()) {
                                die('File is unavailable');
                            }
                        } else {
                            if (!$m->isCurrentAndPublished()) {
                                die('File is unavailable');
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Download a file
     * @param int $id - the ID of the file to download
     * @param string $disposition - "inline" to view in browser, "attachment" to force download
     * */
    public function download($id, $disposition = "attachment")
    {
        $fe = new FileEntity($this->getState());
        $fe->load($id);
        $this->downloadFileByEntity($fe, $disposition);
    }

    /**
     * Download a file
     * @param int $reference - the reference of the file to download
     * @param string $disposition - "inline" to view in browser, "attachment" to force download
     * */
    public function downloadByReference($reference, $disposition = "inline")
    {
        $e = new FileEntity($this->getState());
        if(!$e->loadByField('reference', $reference)) {
            $e->loadByField('location_reference', $reference);
        }
        $this->downloadFileByEntity($e, $disposition);
    }

    /**
     * Download a file by its file entity
     * @param FileEntity $fileEntity
     * @param $disposition
     * @return void
     */
    private function downloadFileByEntity(FileEntity $fileEntity, $disposition)
    {
        if ($fileEntity->id > 0 && $this->checkDownloadAccess($fileEntity)) {
            $this->checkFileExpiry($fileEntity);
            $fs = $this->getFileSystem('', $fileEntity->is_public);
            $size = $fs->getSize($fileEntity->location_reference);
            if($size > self::DOWNLOAD_FULL_DOWNLOAD_LIMIT) {
                $this->streamDownload($fileEntity, $fs, $size);
            }
            else {
                $this->fullDownload($fileEntity, $fs, $size, $disposition);
            }
        }
    }

    /**
     * Do a full and instant download, for a file entity whose size is less than DOWNLOAD_FULL_DOWNLOAD_LIMIT
     *
     * @param FileEntity $fileEntity
     * @param Filesystem $fs
     * @param $fileSize
     * @param $disposition
     * @return void
     */
    private function fullDownload(FileEntity $fileEntity, Filesystem $fs, $fileSize, $disposition)
    {
        $contents = $fs->read($fileEntity->location_reference);
        $this->sendDownloadHeaders($fileEntity, $fs, $fileSize, $disposition);
        echo($contents);
        header('Connection: close');
        die;
    }

    /**
     * Send default download headers for a file entity
     * @param FileEntity $fileEntity
     * @param Filesystem $fs
     * @param $fileSize
     * @param $disposition
     * @return void
     */
    private function sendDownloadHeaders(FileEntity $fileEntity, FileSystem $fs, $fileSize, $disposition)
    {
        header('Pragma: public');    // required
        header('Expires: 0');        // no cache
        header('Cache-Control: private', false);
        if ($fileEntity->mime_type) {
            header('Content-Type: ' . $fileEntity->mime_type);
        } else {
            header('Content-Type: ' . $fs->getMimetype($fileEntity->location_reference));
        }
        header('Content-Disposition: ' . $disposition . '; filename="' . $fileEntity->filename . '"');
        //header('Content-Disposition: ' . $disposition . '; filename="' . basename($fileEntity->location_reference) . '"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . $fileSize);
    }

    /**
     * Stream a file download
     * @access public
     * @param FileEntity $fileEntity
     * @param Filesystem $fs
     * @return void
     * */
    private function streamDownload(FileEntity $fileEntity, Filesystem $fs, $fileSize)
    {
        ini_set("max_execution_time", 6000);
        $adapter = $fs->getAdapter();
        if ($adapter instanceof AwsS3Adapter) {
            $partsCount = ceil($fileSize / self::DOWNLOAD_STREAM_CHUNK_SIZE);
            $this->sendDownloadHeaders($fileEntity, $fs, $fileSize, 'attachment');
            for ($i = 0; $i < $partsCount; $i++) {
                echo($this->readAwsFilePart($adapter, $fileEntity, $fileSize, $i+1));
            }
            header('Connection: close');
        }
        die;
    }

    /**
     * Read an object and normalize the response.
     *
     * @param AwsS3Adapter $adapter
     * @param FileEntity $fe
     * @param $fileSize
     * @param $partCount
     * @return string
     */
    private function readAwsFilePart(AwsS3Adapter $adapter, FileEntity $fe, $fileSize, $partCount)
    {
        $rangeFrom = $partCount * self::DOWNLOAD_STREAM_CHUNK_SIZE - self::DOWNLOAD_STREAM_CHUNK_SIZE;
        $rangeTo = min($rangeFrom + self::DOWNLOAD_STREAM_CHUNK_SIZE, $fileSize);
        if($partCount!=1) {
            $rangeFrom++;
        }
        $options = [
            'Bucket' => $adapter->getBucket(),
            'Key' => rtrim($adapter->getPathPrefix(), '/').'/'.ltrim($fe->location_reference, '/'),
            'Range' => "bytes={$rangeFrom}-{$rangeTo}"
        ];
        $command = $adapter->getClient()->getCommand('getObject', $options );
        try {
            /** @var Result $response */
            $response = $adapter->getClient()->execute($command);
        } catch (S3Exception $e) {
            return false;
        }
        /** @var \GuzzleHttp\Psr7\Stream $stream*/
        $stream = $response['Body'];
        return $stream->getContents();
    }

    /**
     * Get the content of a file
     * @param int $fileId - the ID of the file to get
     * @access public
     * @return string
     */
    public function getContent($fileId)
    {
        $result = "";
        $e = new FileEntity($this->getState());
        if ($e->load($fileId)) {
            $result = $this->getContentByEntity($e);
        }
        $e = null;
        return $result;
    }

    /**
     * Get the content of a file by its entity
     * @access public
     * @param FileEntity $e
     * @return string
     * */
    public function getContentByEntity(FileEntity $e)
    {
        $result = '';
        if ($e->id) {
            $fs = $this->getFileSystem($e->store, $e->is_public);
            if ($fs->has($e->location_reference)) {
                $result = $fs->read($e->location_reference);
            }
        }
        $fs = null;
        return $result;
    }

    /**
     * Get a resource stream of a file by its entity
     * @access public
     * @param FileEntity $e
     * @return resource
     * */
    public function getContentStreamByEntity(FileEntity $e)
    {
        $result = '';
        if ($e->id) {
            $fs = $this->getFileSystem($e->store, $e->is_public);
            if ($fs->has($e->location_reference)) {
                $result = $fs->readStream($e->location_reference);
            }
        }
        $fs = null;
        return $result;
    }

    /**
     * Get the content of a file
     * @param int $fileReference - the reference of the file to get
     * @access public
     * @return string
     */
    public function getContentByReference($fileReference)
    {
        $e = new FileEntity($this->getState());
        $e->loadByReference($fileReference);
        return $this->getContentByEntity($e);
    }

    /**
     * Get the content of a file
     * @param int $fileId - the ID of the file to get
     * @param string $content
     * @access public
     * @return boolean
     */
    public function setContent($fileId, $content)
    {
        $result = false;
        $e = new FileEntity($this->getState());
        $e->load($fileId);
        $fs = $this->getFileSystem($e->store, $e->is_public);
        if ($fs->has($e->location_reference)) {
            if ($fs->has($e->location_reference)) {
                $fs->delete($e->location_reference);
            }
            $result = $fs->write($e->location_reference, $content);
        }
        if($e->id) {
            $this->removeFromCache($e->location_reference);
        }
        return $result;
    }

    /**
     * Get an array list of the data for one or more files by object id
     * @param string $objectRef - the reference of the object
     * @param int $objectId - the ID of the object
     * @param string $objectKey - the optional key of the file
     * @param string $subPath - the subpath of the file
     * @return array|false
     * */
    public function getListByObjectId($objectRef, $objectId, $objectKey = '', $subPath = '')
    {
        $result = false;
        $subPath = $this->cleanSubPath($subPath);
        if ($objectId > 0) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('o.*')
                ->from($this->getTableName(), 'o')
                ->where('o.object_ref = :ref')
                ->setParameter('ref', $objectRef)
                ->andWhere('o.object_id = :id')
                ->setParameter('id', $objectId);
            $q->andWhere("o.lote_deleted is null");
            if ($objectKey) {
                $q->andWhere("o.object_key = :key")
                    ->setParameter('key', $objectKey);
            }
            if ($subPath) {
                $q->andWhere("o.sub_path = :sub_path")
                    ->setParameter('sub_path', $subPath);
            }

            $q->orderBy('o.lote_created', 'desc');

            $s = $q->execute();
            $result = $s->fetchAll(\PDO::FETCH_ASSOC);
        }
        return $result;
    }

    /**
     * Delete a file by its entity
     * @access public
     * @param FileEntity $e
     * @param bool $strongDelete
     * @return void
     */
    public function deleteEntity(FileEntity $e, $strongDelete = false)
    {
        $this->removeFromCache($e->location_reference);
        $this->archiveFileContent($e);
        $e->delete();
        parent::delete($e->id, $strongDelete);
    }

    /**
     * Clean the sub-path variable as it sometimes may be just '\'
     */
    private function cleanSubPath($subPath)
    {
        $result = $subPath;
        if ($subPath == '\\') {
            $result = '';
        }
        return strtolower($result);
    }

    /**
     * On upload of a new file or of a delete of an existing file, remove any cached entries for it
     * @access public
     * @param string $reference - the reference of the file
     * return void
     * */
    public function removeFromCache($reference)
    {
        $this->getState()->getFileCache()->deleteDir(dirname($reference));
    }

    /**
     * Make a reference for the file, to use in saving the file.
     * @param string $object - the reference of the object that this file is for
     * @param int $objectId - the ID of the object that the file is for
     * @param string $objectKey - the key of the object ID if one is required, such as a field name
     * @param string $subPath
     * @param string $name - the name of the file if it is known
     * @param bool $cleanName - If true the file name will be cleaned, if false it will remain as is
     * @access private
     * @return FileEntity|false;
     * */
    public function getFile($object, $objectId, $objectKey = '', $subPath = '', $name = '', $cleanName = true)
    {
        $name = $this->cleanName($name, $cleanName);
        $result = false;
        $e = new FileEntity($this->getState());
        $e->loadByObjectParams($object, $objectId, $objectKey, $name, $subPath);
        if ($e->id) {
            $result = $e;
        }
        return $result;
    }

    /**
     * Set a file to be public, by updating its is_public variable and moving it to the public store
     * @access public
     * @param FileEntity $e
     * @return void
     * */
    public function setPublic(FileEntity $e)
    {
        if (!$e->is_public) {
            $fromFileSystem = $this->getFileSystem($e->store, false);
            $e->is_public = true;
            $toFileSystem = $this->getFileSystem($this->getStore(true), true);
            $this->changeFileSystems($e->location_reference, $fromFileSystem, $toFileSystem);
            $e->save();
        }
    }

    /**
     * Set a file to be private, by updating its is_public variable and moving it to the private store
     * @access public
     * @param FileEntity $e
     * @return void
     * */
    public function setPrivate(FileEntity $e)
    {
        if ($e->is_public) {
            $fromFileSystem = $this->getFileSystem($e->store, true);
            $e->is_public = false;
            $toFileSystem = $this->getFileSystem($this->getStore(false), false);
            $this->changeFileSystems($e->location_reference, $fromFileSystem, $toFileSystem);
            $e->save();
        }
    }

    /**
     * Change the file system for a specific file reference
     *
     * @access private
     * @param $fromReference
     * @param Filesystem $fromFileSystem
     * @param Filesystem $toFileSystem
     * @return void
     */
    private function changeFileSystems($fromReference, Filesystem $fromFileSystem, Filesystem $toFileSystem)
    {
        if ($fromFileSystem !== $toFileSystem) {
            if ($fromFileSystem->has($fromReference)) {
                $toFileSystem->put($fromReference, $fromFileSystem->readAndDelete($fromReference));
            }
        }
    }

    /**
     * Get the Filesystem to use for file archives
     * @access private
     * @return Filesystem|false
     * */
    private function getArchiveFilesystem()
    {
        $result = false;
        if ($this->getState()->getSettings()->get("media.archive.is_enabled", true)) {
            $result = $this->getFileSystem($this->getStore(false), false);
        }
        return $result;
    }

    /**
     * Archive the content of a file, by putting it into the Archive file system
     * @access private
     * @param $item
     * @return void
     */
    private function archiveFileContent(FileEntity $item)
    {
        if ($item) {
            if ($destinationFileSystem = $this->getArchiveFilesystem()) {
                $existingFileSystem = $this->getFileSystem($item->store, $item->is_public);
                if ($existingFileSystem->has($item->location_reference)) {
                    $archivePath = $item->makeArchiveReference();
                    $destinationFileSystem->put($archivePath, $existingFileSystem->read($item->location_reference));
                    $this->getState()->getAudit()->addFileLog($this->getStore(true), $item->location_reference,
                        $archivePath, $this->getTableName(), $item->id, "");
                }
            }
        }
    }

    /**
     * Delete a file from a bucket as it is being updated with new content
     *
     * @access private
     * @param $item
     * @return void
     */
    private function deleteExistingContent($item)
    {
        if ($item->id) {
            $existingFileSystem = $this->getFileSystem($item->store, $item->is_public);
            if ($existingFileSystem->has($item->location_reference)) {
                $existingFileSystem->delete($item->location_reference);
            }
        }
    }

    /**
     * Save a file
     *
     * @param string $newPath - the temporary path of the new file to save
     * @param string $newName - the name of the new file
     * @param bool $isPublic - true if this file is public
     * @param bool $isSingle - true if this file is intended to be a single file for the given object parameters, so
     *                         override any file that exists for the combination instead of creating an additional entry
     * @param string $object - the reference of the object that the file is for
     * @param int $objectId - the ID of the object in question
     * @param string $objectKey - the key for the object, if its for a specific field or part of an array
     * @param string $subPath - the sub path of the file
     * @param bool $saveByStream - True is file should be saved by file stream, false if file should be saved by content string
     * @param bool $cleanName - If true the file name will be cleaned, if false it will remain as is
     * @access public
     * @return FileEntity|false
     * */
    public function save(
        $newPath,
        $newName,
        $isPublic,
        $isSingle,
        $object,
        $objectId,
        $objectKey = '',
        $subPath = '',
        $saveByStream = false,
        $cleanName = true

    ) {
        $newName = $this->cleanName($newName, $cleanName);
        $enableRotate = $this->getState()->getSuperParentState()->getSettings()->get("rotate.images", false);
        if ($enableRotate) {
            $this->prepareFile($newPath);
        }
        $subPath = $this->cleanSubPath($subPath);
        $itemByName = $item = $this->getFile($object, $objectId, $objectKey, $subPath, $newName, false);
        $item ? null : $item = $this->getFile($object, $objectId, $objectKey, $subPath, '', false);

        if ($isSingle && ($item || $itemByName)) {
            $itemByName ? $tempItem = $itemByName : $tempItem = $item;
            $result = $this->saveByEntity($tempItem, $newPath, $newName, $isPublic, $saveByStream, false);
        } elseif ($itemByName) {
            $result = $this->saveByEntity($itemByName, $newPath, $newName, $isPublic, $saveByStream, false);
        } else {
            $itemByObjectRef = $this->getFile($object, '', $objectKey, $subPath, $newName, false);
            if ($isSingle && $itemByObjectRef) {
                $result = $this->saveByEntity($itemByObjectRef, $newPath, $newName, $isPublic, $saveByStream, false);
            } else {
                $result = $this->saveByParams($newPath, $newName, $isPublic, $object, $objectId, $objectKey, $subPath,
                    $saveByStream);
            }
        }
        return $result;
    }

    private function cleanName($name, $cleanName = true)
    {
        $result = $name;
        if($cleanName) {
            $name = trim(preg_replace("/[^a-zA-Z0-9\.\_]/", "_", $name));
            $result = preg_replace('!_+!', '_', $name);
        }
        return $result;
    }

    /**
     * Save a file to a store
     *
     * @access public
     * @param FileEntity $e - the file entity in question
     * @param string $newPath - the temporary path of the new file to save
     * @param string $newName - the name of the new file
     * @param bool $isPublic - true if this file is private
     * @param bool $saveByStream - True is file should be saved by file stream, false if file should be saved by content string
     * @param bool $cleanName - If true the file name will be cleaned, if false it will remain as is
     * @access public
     * @return FileEntity
     * */
    public function saveByEntity(FileEntity $e, $newPath, $newName, $isPublic = false, $saveByStream = false, $cleanName = true)
    {
        $newName = $this->cleanName($newName, $cleanName);

        $this->archiveFileContent($e);
        $this->deleteExistingContent($e);

        $location_reference = $this->makeReferenceByEntity($e, $newName);
        if ($size = $this->saveToFileSystem($location_reference, $newPath, $isPublic, $saveByStream)) {
            $this->removeFromCache($e->location_reference);
            $mimeType = \SidraBlue\Util\File::getMimeType($newPath);
            $e->location_reference = $location_reference;
            $e->is_public = boolval($isPublic);
            $e->filename = $newName;
            $e->extension = strtolower(pathinfo($newName, PATHINFO_EXTENSION));
            $e->mime_type = $mimeType;
            $e->size = $size;
            $e->file_type = \SidraBlue\Util\File::getFileCategory($mimeType);
            if ($e->save()) {
                $this->updateImageSize($e, $newPath);
            }
        }
        return $e;
    }

    /**
     * Save a file to a store by its file ID
     *
     * @access public
     * @param int $fileId - the ID of the file in question
     * @param string $newPath - the temporary path of the new file to save
     * @param string $newName - the name of the new file
     * @param bool $isPublic - true if this file is public
     * @param bool $cleanName - If true the file name will be cleaned, if false it will remain as is
     * @access public
     * @return FileEntity|false
     * */
    public function saveById($fileId, $newPath, $newName, $isPublic, $cleanName = true)
    {
        $newName = $this->cleanName($newName, $cleanName);
        $result = false;
        $f = new FileEntity($this->getState());
        if ($f->load($fileId)) {
            $result = $this->saveByEntity($f, $newPath, $newName, $isPublic, false, false);
        }
        return $result;
    }

    /**
     * Save a file with a given set of parameters
     *
     * @param string $newPath - the temporary path of the new file to save
     * @param string $newName - the name of the new file
     * @param bool $isPublic - true if this file is public
     * @param string $object - the reference of the object that the file is for
     * @param int $objectId - the ID of the object in question
     * @param string $objectKey - the key for the object, if its for a specific field or part of an array
     * @param string $subPath - the sub path of the file
     * @param bool $saveByStream - True is file should be saved by file stream, false if file should be saved by content string
     * @access public
     * @return FileEntity|false
     * */
    private function saveByParams(
        $newPath,
        $newName,
        $isPublic,
        $object,
        $objectId,
        $objectKey = '',
        $subPath = '',
        $saveByStream = false
    ) {
        $result = false;
        $item = new FileEntity($this->getState());
        $item->reference = $this->makeReference($object, $objectId, $objectKey, $subPath, $newName);
        $item->location_reference = $this->makeReference($object, $objectId, $objectKey, $subPath, $newName, true);
        $item->store = $this->getStore($isPublic);
        $size = $this->saveToFileSystem($item->location_reference, $newPath, $isPublic, $item->store, $saveByStream);
        if ($size > 0) {
            $mimeType = \SidraBlue\Util\File::getMimeType($newPath);
            $item->object_ref = $object;
            $item->object_id = $objectId;
            $item->object_key = $objectKey;
            $item->sub_path = $this->cleanSubPath($subPath); //@todo - Move this function into the entity maybe?
            $item->filename = $newName;
            $item->is_public = $isPublic;
            $item->extension = strtolower(pathinfo($newName, PATHINFO_EXTENSION));
            $item->mime_type = $mimeType;
            $item->size = $size;
            $item->file_type = \SidraBlue\Util\File::getFileCategory($mimeType);
            if ($item->save()) {
                $this->updateImageSize($item, $newPath);
                $result = $item;
            }
        }
        return $result;
    }

    /**
     * Save blank file with a given set of parameters
     *
     * @param string $newName - the name of the new file
     * @param bool $isPublic - true if this file is public
     * @param string $object - the reference of the object that the file is for
     * @param int $objectId - the ID of the object in question
     * @param string $objectKey - the key for the object, if its for a specific field or part of an array
     * @param string $subPath - the sub path of the file
     * @access public
     * @return FileEntity|false
     * */
    public function savePlaceholderFile($newName, $isPublic, $object, $objectId, $objectKey = '', $subPath = '')
    {
        $newName = $this->cleanName($newName);

        $result = false;
        $item = new FileEntity($this->getState());
        $item->reference = $this->makeReference($object, $objectId, $objectKey, $subPath, $newName);
        $item->location_reference = $this->makeReference($object, $objectId, $objectKey, $subPath, $newName, true);
        $item->store = $this->getStore($isPublic);
        $size = 0;

        $item->object_ref = $object;
        $item->object_id = $objectId;
        $item->object_key = $objectKey;
        $item->sub_path = $this->cleanSubPath($subPath); //@todo - Move this function into the entity maybe?
        $item->filename = $newName;
        $item->is_public = $isPublic;
        $item->extension = strtolower(pathinfo($newName, PATHINFO_EXTENSION));
        $item->size = $size;
        $item->is_saved = false;
        if ($item->save()) {
            $result = $item;
        }
        return $result;
    }

    /**
     * Update the image size of a file, if it is an image.
     *
     * This function will create or delete image size entries based on the filetype.
     *
     * @param FileEntity $item
     * @param string $tempPath
     * @return void
     * @todo - make the file_type check a constant
     */
    private function updateImageSize(FileEntity $item, $tempPath)
    {
        $fileImageEntity = new FileImageEntity($this->getState());
        $fileImageEntity->loadByField("file_id", $item->id);
        if ($item->file_type == "image") {
            if ($size = Image::getImageDimensionsByContent(file_get_contents($tempPath))) {
                $fileImageEntity->file_id = $item->id;
                $fileImageEntity->width = $size && isset($size['width']) ? $size['width'] : 0;
                $fileImageEntity->height = $size && isset($size['height']) ? $size['height'] : 0;
                $fileImageEntity->save();
            } elseif ($fileImageEntity->id) {
                $fileImageEntity->delete();
            }
        } elseif ($fileImageEntity->id) {
            $fileImageEntity->delete();
        }
    }

    /**
     * Initliase the image dimensions as if they have not been set previously
     *
     * @access public
     * @param FileEntity $file
     * @return false|FileImageEntity
     *  */
    public function initImageSize(FileEntity $file)
    {
        $result = false;
        $fileImageEntity = new FileImageEntity($this->getState());
        $fileImageEntity->loadByField("file_id", $file->id);
        if ($file->file_type == "image") {
            if($content = $this->getContentByEntity($file)) {
                if ($size = Image::getImageDimensionsByContent($content)) {
                    $fileImageEntity->file_id = $file->id;
                    $fileImageEntity->width = $size && isset($size['width']) ? $size['width'] : 0;
                    $fileImageEntity->height = $size && isset($size['height']) ? $size['height'] : 0;
                    $fileImageEntity->lote_deleted = null;
                    if($fileImageEntity->save()) {
                        $result = $fileImageEntity;
                    }
                }
            }
        } elseif ($fileImageEntity->id) {
            $fileImageEntity->delete();
        }
        return $result;
    }

    /**
     * Save a file, and if it exists archive the previous one and create a new entry for this one
     *
     * @param $content - the content of the file
     * @param $filename - the filename
     * @param bool $isPublic - true if this file is public
     * @param bool $isSingle - true if this file is intended to be a single file for the given object parameters, so
     *                         override any file that exists for the combination instead of creating an additional entry
     * @param $object - the object that the file belongs to
     * @param $objectId - the object id that the file belongs to
     * @param string $objectKey - an optional key
     * @param string $subPath - an optional sub path
     * @param string $store - a preferred store for the file
     * @return bool|\SidraBlue\Lote\Entity\File
     */
    public function saveByContent(
        $content,
        $filename,
        $isPublic,
        $isSingle,
        $object,
        $objectId,
        $objectKey = '',
        $subPath = '',
        $store = ''
    ) {
        $tempPath = LOTE_TEMP_PATH . 'file_model_temp_' . uniqid();
        Dir::make(LOTE_TEMP_PATH);
        file_put_contents($tempPath, $content);
        return $this->save($tempPath, $filename, $isPublic, $isSingle, $object, $objectId, $objectKey, $subPath,
            $store);
    }

    /**
     * Get the store type for this system
     * @access private
     * @param bool $isPublic
     * @return string
     * */
    private function getStore($isPublic = false)
    {
        $defaultAdapter = $this->getState()->getSettings()->get('media.adapter', 'local');
        if ($isPublic) {
            $result = $this->getState()->getSettings()->get('media.adapter.public', $defaultAdapter);
        } else {
            $result = $this->getState()->getSettings()->get('media.adapter.private', $defaultAdapter);
        }
        return $result;
    }

    /**
     * Make a reference for the file, to use in saving the file.
     * @access private
     * @param string $object - the reference of the object that this file is for
     * @param int $objectId - the ID of the object that the file is for
     * @param string $objectKey - the key of the object ID if one is required, such as a field name
     * @param string $subPath - sub path of the file
     * @param string $name - the name of the file if it is known
     * @param bool $useTime - true to use the current time in the reference
     * @return string
     * @todo - add in validation for the object and object ID
     * */
    private function makeReference($object, $objectId, $objectKey = '', $subPath = '', $name = '', $useTime = false)
    {
        $subPath = $this->cleanSubPath($subPath);
        $object = str_replace('.', '/', $object);
        $reference = "{$object}/{$objectId}";
        if ($objectKey) {
            $reference .= '/' . $objectKey;
        }
        if ($subPath != "" && $subPath != "/" && $subPath != "\\") {
            $reference .= '/' . ltrim($subPath, "/");
        }
        if ($useTime) {
            $reference .= '/' . Time::getUtcNow("ymdhis") . substr((string)microtime(), 2, 3);
        }
        if ($name) {
            $reference .= '/' . $name;
        }
        return strtolower($reference);
    }

    /**
     * Make a new reference from an entity and a name
     * @access private
     * @param FileEntity $e
     * @param string $name
     * @return string
     * */
    private function makeReferenceByEntity(FileEntity $e, $name)
    {
        return $this->makeReference($e->object_ref, $e->object_id, $e->object_key, $e->sub_path, $name, true);
    }

    /**
     * Save a file to the filesystem in the store specified or in the default store as defined in the settings
     * @param string $reference - the unique reference of the file
     * @param string $tempPath - the current temporary path of the file to save
     * @param bool $isPublic - true if this is to be saved to the public filesystem
     * @param string|bool $store - the name of the store if one is being specified
     * @param bool $saveByStream - True is file should be saved by file stream, false if file should be saved by content string
     * @access private
     * @return int;
     * */
    private function saveToFileSystem($reference, $tempPath, $isPublic = false, $store = false, $saveByStream = false)
    {
        $result = 0;
        $store ? null : $store = $this->getStore($isPublic);
        $fs = $this->getFileSystem($this->getStore($isPublic), $isPublic);
        if (file_exists($tempPath)) {
            if ($saveByStream) {
                $fileStream = fopen($tempPath, 'r');
                if ($fs->writeStream($reference, $fileStream)) {
                    $result = 1;
                    $fileStats = fstat($fileStream);
                    if($fileStats && isset($fileStats['size'])) {
                        $result = max($result, fstat($fileStream)['size']);
                    }
                }
            } else {
                $fileData = file_get_contents($tempPath);
                if ($fs->write($reference, $fileData) > 0) {
                    $result = max(1, strlen($fileData));
                }
            }
        }
        return $result;
    }

    /**
     * Get the file location to store local files in
     * @access private
     * @param string $store
     * @param bool $isPublic
     * @return Filesystem
     * @todo - create a store factory
     * */
    private function getFileSystem($store = '', $isPublic = false)
    {
        if ($store == '') {
            $store = $this->getStore($isPublic);
        }
        if (!$result = $this->getCachedFilesystem('local', $isPublic)) {
            if ($store == 'aws') {
                $result = new Filesystem(new AwsS3Adapter($this->getS3Client($isPublic),
                    $this->getBucketName($isPublic), $this->getBucketPrefix($isPublic)));
            } else {
                $result = new Filesystem(new Local($this->getLocalPath($isPublic)));
            }
            $this->filesystems[$this->makeCacheFilesystemKey($store, $isPublic)] = $result;
        }
        return $result;
    }

    /**
     * Get a filesystem already created by this instance of the object, if it exists
     * @access private
     * @param string $store - the name of hte store
     * @param bool $isPublic - the privacy status of the store
     * @return false|Filesystem
     * */
    private function getCachedFilesystem($store, $isPublic)
    {
        $result = false;
        if (isset($this->filesystems[$this->makeCacheFilesystemKey($store, $isPublic)])) {
            $result = "";
        }
        return $result;
    }

    /**
     * Get a filesystem cache key name
     * @access private
     * @param string $store - the name of hte store
     * @param bool $isPublic - the privacy status of the store
     * @return string
     * */
    private function makeCacheFilesystemKey($store, $isPublic)
    {
        return "{$store}_{$isPublic}";
    }

    /**
     * Get the name of the bucket to use
     * @access private
     * @param bool $isPublic
     * @return string
     * */
    private function getBucketName($isPublic = false)
    {
        $result = $this->getState()->getSettings()->get('media.adapter.aws_bucket');
        if ($isPublic) {
            $result = $this->getState()->getSettings()->get('media.adapter.public.aws_bucket', $result);
        } else {
            $result = $this->getState()->getSettings()->get('media.adapter.private.aws_bucket', $result);
        }
        return $result;
    }

    /**
     * Get the name of the bucket to use
     * @access private
     * @param bool $isPublic
     * @return S3Client
     * */
    private function getS3Client($isPublic = false)
    {
        if($isPublic) {
            $result = $this->getState()->getS3PublicClient();
        } else {
            $result = $this->getState()->getS3PrivateClient();
        }
        return $result;
    }

    /**
     * Get the prefix of the bucket to use
     * @access private
     * @param bool $isPublic
     * @return string
     * */
    private function getBucketPrefix($isPublic = false)
    {
        $result = $this->getState()->getSettings()->get('media.adapter.aws_prefix');
        if ($isPublic) {
            $result = $this->getState()->getSettings()->get('media.adapter.public.aws_prefix', $result);
        } else {
            $result = $this->getState()->getSettings()->get('media.adapter.private.aws_prefix', $result);
        }
        return $result;
    }

    /**
     * Get the file location to store local files in
     * @access private
     * @param bool $isPublic
     * @return string
     * @todo - move this to the factory when it is created
     * */
    private function getLocalPath($isPublic = false)
    {
        $isPublic ? $suffix = "public" : $suffix = "private";
        if ($this->getState()->masterOnly) {
            $result = LOTE_ASSET_PATH . 'media/master/' . $suffix;
        } else {
            $result = LOTE_ASSET_PATH . 'media/' . $this->getState()->accountReference . "/{$suffix}";
        }
        Dir::make($result);
        return $result;
    }

    /**
     * Get a list of files by their IDs
     * @access public
     * @param array $fileIds - the ID's of the files that we want the data for
     * @param bool $indexById - true if the result is to be indexed by the file ID
     * @return array
     *  */
    public function getFilesById($fileIds, $indexById = false)
    {
        $result = [];
        if (is_array($fileIds) && count($fileIds) > 0) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('f.*')
                ->from('sb__file', 'f')
                ->where('f.id in (:file_ids)')
                ->setParameter('file_ids', $fileIds, Connection::PARAM_INT_ARRAY);
            $files = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
            foreach ($files as $f) {
                if ($indexById) {
                    $result[$f['id']] = $f;
                } else {
                    $result[] = $f;
                }
            }
        }
        return $result;
    }

    /**
     * Add information to an array of data about a file that it points to via a file_id field
     * @access public
     * @param array $data
     * @param string $dataFieldName - the field name that the file information should be appended to
     * @return array
     * */
    public function addFileInfo($data, $dataFieldName = '_file', $fileFieldName = 'file_id')
    {
        if (is_array($data) && count($data) > 0) {
            $ids = Arrays::getFieldValuesFrom2dArray($data, $fileFieldName, true);
            if (is_array($ids) && count($ids) > 0) {
                $fileList = $this->getFilesById($ids, true);
                $cnt = count($data);
                for ($i = 0; $i < $cnt; $i++) {
                    if (isset($fileList[$data[$i][$fileFieldName]])) {
                        $data[$i][$dataFieldName] = $fileList[$data[$i][$fileFieldName]];
                    }
                }
            }
        }
        return $data;
    }

    /**
     * @param $tag
     * @param array $paging
     * @return array
     */
    public function getTagItems($tag, $paging = [])
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();

        $q->select('f.*')
            ->from($this->tableName, 'f')
            ->leftJoin('f', 'sb__tag_usage', 'u', 'u.object_id = f.id')
            ->leftJoin('u', 'sb__tag', 't', 't.id = u.tag_id')
            ->andWhere('t.value = :tag')
            ->andWhere('u.object_ref = :reference')
            ->setParameter('tag', $tag)
            ->setParameter('reference', 'media.file');

        if (!empty($paging)) {
            $q->setFirstResult($paging['start'] - 1 >= 0 ? $paging['start'] - 1 : $paging['start']);
            $q->setMaxResults($paging['limit']);
        }
        $q->addOrderBy('lote_created', 'desc');

        $query = $q->execute();
        while ($row = $query->fetchAll(\PDO::FETCH_ASSOC)) {
            $result = $row;
        }
        return $result;
    }

    /**
     * @param $id
     * @param int $folderId
     */
    public function cloneFile($id, $folderId = 0)
    {
        $fe = null;

        // Get parent's CmsMediaFile Entry
        $pmfe = new MediaFileEntity($this->getState());
        $pmfe->setDb($this->getState()->getParentDb());
        if ($pmfe->load($id)) {
            // Get parent's File Entry
            $mediaData = $pmfe->getViewData();
            $pfm = new File($this->getState());
            $pfm->setDb($this->getState()->getParentDb());
            $fileData = $pfm->get($mediaData['file_id']);

            // Create new CmsMediaFile entry in own db
            unset($mediaData['id']);
            unset($mediaData['file_id']);
            $mfe = new MediaFileEntity($this->getState());
            $mediaData['parent_id'] = $folderId;
            $mfe->setData($mediaData);
            $mfe->save();

            // Create new File entry in own db
            if ($mfe->id > 0) {
                // Copy image from parent host to own temp folder
                $a = new \Lote\System\Client\Model\Master\Account($this->getState());
                $parent = $a->getParentAccount($this->getState()->getSettings()->get('system.reference'));
                $origImgUrl = 'http://' . $parent['domain'] . '/_image/' . $fileData['reference'];
                $clonePath = LOTE_TEMP_PATH . uniqid('clone_') . '_' . $fileData['filename'];
                copy($origImgUrl, $clonePath);

                // Save from temp folder to configured file system
                $fe = $this->save(
                    $clonePath,
                    $fileData['filename'],
                    $fileData['is_public'],
                    false, //@todo - make this work off of the file single status
                    $fileData['object_ref'],
                    $mfe->id,
                    $fileData['object_key'],
                    $fileData['sub_path']
                );

                $mfe->file_id = $fe->id;
                $mfe->save();
            }
        }
        $this->getState()->getView()->success = (!is_null($fe) && $fe->id > 0) ? true : false;
    }

    /**
     * Prepare the file for saving, by checking if its an image and then rotating the image if required
     * @access private
     * @param string $path - the path of the image
     * @return void
     * */
    private function prepareFile($path)
    {
        if (@exif_imagetype($path)) { //Check if file is an image
            $exif = @exif_read_data($path);
            if ($exif && isset($exif['Orientation']) && $exif['Orientation'] != '1') {
                $this->rotateImage($path, $exif['Orientation']);
            }
        }
    }

    /**
     * Strip any exif data from an image, primarily because we would have applied any orientation changes already
     *
     * @access private
     * @param string $path - the current path of the image
     * @return void
     * */
    private function stripExifData($path)
    {
        try {
            $img = new \Imagick($path);
            $img->removeImageProfile('exif');
            $img->writeImage($path);
            $img->clear();
            $img->destroy();
        } catch(\Exception $e) {
            //do nothing
        }
    }

    /**
     * Rotate an image based off of its exif data rotation value
     *
     * @access private
     * @param string $path - the current path of the image
     * @param int|string $rotationValue
     * @return void
     * */
    private function rotateImage($path, $rotationValue)
    {
        $img = imagecreatefromjpeg($path);

        switch ($rotationValue) {
            case 3:
                $img = imagerotate($img, 180, 0);
                break;
            case 6:
                $img = imagerotate($img, -90, 0);
                break;
            case 8:
                $img = imagerotate($img, 90, 0);
                break;
        }

        switch (exif_imagetype($path)) {
            case 1:
                imagegif($img, $path);
                break;
            case 2:
                imagejpeg($img, $path);
                break;
            case 3:
                imagepng($img, $path);
                break;
            case 6:
                if(function_exists('imagebmp')) {
                    imagebmp($img, $path);
                } else {
                    imagejpeg($img, $path);
                }
                break;
            default:
                imagejpeg($img, $path);
                break;
        }

        imagedestroy($img);
    }




}
