<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;

/**
 * Model class for a Firewall
 *
 * @package SidraBlue\Lote\Model
 */
class Firewall extends Base
{

    /**
     * @var string $tableName
     */
    protected $tableName = 'sb__firewall';

    /**
     * Check if a specified IP address fits within an allowed set of IP addresses or IP subnets
     * @param string $ipAddress
     * @param string $objectRef - the object in question
     * @param int $objectId - an object ID
     * @return boolean
     * */
    public function isAllowed($ipAddress, $objectRef, $objectId = 0)
    {
        $result = false;
        $rules = $this->getAllowRules($objectRef, $objectId);
        foreach($rules as $r) {
            if($this->matchesSubnet($ipAddress, $r['ip_address'], $r['ip_subnet'])) {
                $result = true;
                break;
            }
        }
        return $result;
    }

    /**
     * Check if the specified IP address fits within the subnet and mask specified
     * @param string $ipAddress
     * @param string $subnetAddress
     * @param string $subnetMask
     * @return boolean
     * */
    private function matchesSubnet($ipAddress, $subnetAddress, $subnetMask)
    {
        if(!$subnetMask) {
            $subnetMask = 32;
        }
        return ( ip2long( $ipAddress ) & ~ ( ( 1 << ( 32 - $subnetMask ) ) - 1 ) ) == ( ip2long( $subnetAddress ) >> ( 32 - $subnetMask ) ) << ( 32 - $subnetMask );
    }

    /**
     * Check if a specified IP address fits within an allowed set of IP addresses or IP subnets
     * @param string $objectRef - the object in question
     * @param int $objectId - an object ID
     * @return array|false
     * */
    public function getAllowRules($objectRef, $objectId = 0)
    {
        return $this->getRules('allow', $objectRef, $objectId);
    }

    /**
     * Check if a specified IP address fits within an allowed set of IP addresses or IP subnets
     * @param string $objectRef - the object in question
     * @param int $objectId - an object ID
     * @return array|false
     * */
    public function getDenyRules($objectRef, $objectId = 0)
    {
        return $this->getRules('deny', $objectRef, $objectId);
    }

    /**
     * Check if a specified IP address fits within an allowed set of IP addresses or IP subnets
     * @param string $type - 'allow' or 'deny'
     * @param string $objectRef - the object in question
     * @param int $objectId - an object ID
     * @return array|false
     * */
    public function getRules($type, $objectRef, $objectId = 0)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("f.*")
            ->from("sb__firewall", "f")
            ->where("object_ref = :object_ref")
            ->setParameter("object_ref", $objectRef)
            ->andWhere("type = :type")
            ->setParameter("type", $type);
        if($objectId) {
            $q->andWhere("object_id = :object_id")
                ->setParameter("object_id", $objectId);
        }
        $q->andWhere("lote_deleted is null");
        return $q->execute()->fetchAll();
    }

}
