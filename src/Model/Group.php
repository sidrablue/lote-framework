<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model;

use Doctrine\DBAL\Connection;
use Lote\System\Admin\Entity\Query as QueryEntity;
use Lote\System\Admin\Model\QueryUser as QueryUserModel;
use SidraBlue\Lote\Entity\Group as GroupEntity;
use SidraBlue\Lote\Model\User as UserModel;
use SidraBlue\Lote\Object\Model\Base;
use SidraBlue\Util\Arrays;

/**
 * Model class for Users
 */
class Group extends Base
{

    /**
     * @var String $tableName - the name of the table for this model
     * */
    protected $tableName = 'sb__group';

    /**
     * Get selected groups
     * @param array $selectedGroupsArr
     * @param string $kind
     * @return array
     */
    public function getUserSelectedGroups($selectedGroupsArr, $kind = '')
    {

        $ids = join(',', $selectedGroupsArr);

        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('g.*')
            ->from('sb__group', 'g')
            ->where('g.id in (:ids)')
            ->setParameter('ids', $ids);

        if(!empty($kind)) {
            $q->andWhere('g.kind = :kind')
                ->setParameter('kind', $kind);
        }

        $query = $q->execute()->fetchAll(
            \PDO::FETCH_ASSOC
        );

        if (!empty($query)) {
            $result = $query;
        }
        return $result;
    }

    /**
     * Get all of the groups, and optionally of a particular tag
     * @param array|string $tag
     * @todo - Make these tags work
     * @param string $kind
     * @param boolean $active
     * @return array
     * */
    public function getGroups($tag = '', $kind = '', $active = false)
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*')->from($this->getTableName(),'t');
        if(is_string($tag) && $tag) {
            $q->leftJoin("t", "sb__tag_usage", "tu", "t.id = tu.object_id")
                ->leftJoin("tu", "sb__tag", "tag", "tag.id = tu.tag_id")
                ->andWhere("tag.lote_deleted is null")
                ->andWhere("tu.lote_deleted is null")
                ->andWhere("tag.value = :tagVal")
                ->setParameter("tagVal", $tag);
        }
        elseif(is_array($tag) && count($tag) > 0) {

        }
        $q->andWhere("t.lote_deleted is null");

        if(!empty($kind)) {
            $q->andWhere('t.kind = :kind')
                ->setParameter('kind', $kind);
        }
        if($active){
            $q->andWhere("t.active = 1");
        }

        $q->addOrderBy('t.sort_order', 'asc');
        $q->addOrderBy('t.name', 'asc');
        $q->addOrderBy('t.id', 'asc');

        $m = new Group($this->getState());
        $m->addLoteAccessViewClauses($q, 't');

        $s = $q->execute();
        while($row = $s->fetch(\PDO::FETCH_ASSOC)) {
            $result[] = $row;
        }
        return $result;
    }

    /**
     * Get all of the groups that a user is in
     * @param int $userId
     * @param string $kind
     * @return array|false
     * */
    public function getUserGroups($userId, $kind = '')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('g.*')->from('sb__group', 'g')
            ->leftJoin('g', 'sb__user_group', 'ug', 'ug.group_id = g.id')
            ->where('ug.user_id = :user_id')->setParameter('user_id', $userId);

        if(!empty($kind)) {
            $q->andWhere('g.kind = :kind')
                ->setParameter('kind', $kind);
        }
        $q->andWhere("g.lote_deleted is null")->andWhere("ug.lote_deleted is null");
        $s = $q->execute();
        return $s->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Get the information for a set of groups as specified by an array of IDS
     * @access public
     * @param array $groupIds
     * @param boolean $indexByIds
     * @param boolean $activeOnly
     * @return array
     * */
    public function getGroupList($groupIds, $indexByIds = false, $activeOnly = false)
    {
        $result = [];
        if(is_array($groupIds)) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('g.*')
                ->from('sb__group', 'g')
                ->where('g.id in (:group_ids)')
                ->setParameter('group_ids', $groupIds, Connection::PARAM_INT_ARRAY)
                ->andWhere("g.lote_deleted is null");
            if ($activeOnly) {
                $q->andWhere('t.active = 1');
            }
            $rows = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
            if(is_array($rows)) {
                foreach ($rows as $v) {
                    if ($indexByIds) {
                        $result[$v['id']] = $v;
                    } else {
                        $result[] = $v;
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Ensure that a specific user belongs to a set of provided groups
     * @param int $userId
     * @param array $groupIds
     * */
    public function addUserGroups($userId, $groupIds)
    {
        if(is_array($groupIds) && count($groupIds)>0) {
            $userGroups = Arrays::indexArrayByField($this->getUserGroups($userId), 'id');
            foreach ($groupIds as $gid) {
                if (!isset($userGroups[$gid])) {
                    //Assign new user group data
                    $data = [
                        'user_id' => $userId,
                        'group_id' => $gid,
                    ];
                    $this->getState()->getWriteDb()->insert('sb__user_group', $data);
                }
            }
        }
    }

    /**
     * Get the public groups
     * @param string $kind
     * @param boolean $activeOnly
     * @return array
     */
    public function getPublicGroupsList($kind = 'user', $activeOnly = false)
    {
        $result = [];

        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('g.*')
            ->from($this->getTableName(), 'g')
            ->where('g.is_public = 1')
            ->andWhere('g.lote_deleted is null')
            ->andWhere('g.kind = :kind')
            ->setParameter('kind', $kind)
            ->orderBy('weighting', 'desc')
            ->addOrderBy('name', 'asc');
        if ($activeOnly) {
            $q->andWhere('g.active = 1');
        }
        $query = $data->execute();
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $result[] = $row;
        }
        return $result;
    }


    /**
     * Get groups by tag
     * @param array $tag
     * @param array $paging
     * @param string $kind
     * @param array/string $orderBy
     * @param $groupType $kind
     * @param boolean $activeOnly
     * @return array
     */
    public function getByTag($tag, $paging = [], $kind = '', $orderBy = [], $groupType = false, $activeOnly = false)
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('distinct g.*')
            ->from($this->tableName, 'g')
            ->leftJoin('g', 'sb__tag_usage', 'u', 'u.object_id = g.id')
            ->leftJoin('u', 'sb__tag', 't', 't.id = u.tag_id')
            ->andWhere('u.object_ref = :reference')
            ->setParameter('reference', 'user.group');

        if(is_string($tag)) {
            $q->andWhere('t.value = :tag')
                ->setParameter('tag', $tag);
        }
        elseif(is_array($tag)) {
            $q->andWhere('t.value in (:tags)')
                ->setParameter('tags', $tag, Connection::PARAM_STR_ARRAY);
        }

        if(!empty($kind)) {
            $q->andWhere('g.kind = :kind')
                ->setParameter('kind', $kind);
        }
        if($groupType) {
            $q->andWhere('g.group_type = :group_type')
                ->setParameter('group_type', $groupType);
        }
        if ($activeOnly) {
            $q->andWhere('g.active = 1');
        }

        $q->andWhere('g.lote_deleted is null');
        $q->andWhere("t.lote_deleted is null");
        if(!empty($paging)){
            $q->setFirstResult($paging['start'] - 1 >= 0 ? $paging['start'] - 1 : $paging['start']);
            $q->setMaxResults($paging['limit']);
        }
        if(!empty($orderBy)){
            if(is_array($orderBy)){
                foreach($orderBy as $sort=>$order){
                    if(is_string($sort) && is_string($order) && !empty($sort) && !empty($order)){
                        $q->addOrderBy($sort, $order);
                    }
                }
            }
            else{
                if(is_string($orderBy) && !empty($orderBy)){
                    $q->addOrderBy($orderBy, "asc");
                }
            }
        }
        else{
            $q->addOrderBy('weighting', 'desc');
            $q->addOrderBy('name', 'asc');
        }


        $query = $q->execute();
        while ($row = $query->fetchAll(\PDO::FETCH_ASSOC)) {
            $result = $row;
        }
        return $result;
    }

    public function getIdsFromLdapNames($ldapNames)
    {
        $result = [];
        if (count($ldapNames) > 0) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select("id")
                ->from("sb__group", "g")
                ->where("g.ldap_path in (:paths)")
                ->setParameter("paths", array_values($ldapNames), Connection::PARAM_STR_ARRAY);
            foreach($q->execute()->fetchAll(\PDO::FETCH_ASSOC) as $v) {
                $result[] = $v['id'];
            }
        }
        return $result;
    }

    public function updateGroups($userId, $userGroups)
    {
        $currentGroups = $this->getUserGroups($userId);
        $currentGroups = Arrays::getFieldValuesFrom2dArray($currentGroups, 'id');

        $groupsToRemove = array_diff($currentGroups, $userGroups);
        $groupsToAdd = array_diff($userGroups, $currentGroups);

        if (!empty($groupsToRemove)) {
            $this->removeUserGroups($userId, $groupsToRemove);
        }
        if (!empty($groupsToAdd)) {
            $this->insertUserGroups($userId, $groupsToAdd);
        }
    }

    /**
     * @todo - Michael centralise this - find other instances and remove
     * */
    public function insertUserGroups($userId, $userGroups)
    {
        if (is_numeric($userId) && count($userGroups) > 0) {
            foreach ($userGroups as $v) {
                $this->getWriteDb()->insert('sb__user_group', ['user_id' => $userId, 'group_id' => $v]);
            }
        }
    }

    /**
     * @param $userId
     * @param $userGroups
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function removeUserGroups($userId, $userGroups)
    {
        if (is_numeric($userId) && count($userGroups) > 0) {
            foreach($userGroups as $groupId) {
                $params = ["user_id" => $userId, "group_id" => $groupId];
                $types = ["user_id" => \PDO::PARAM_INT, "group_id" => \PDO::PARAM_INT];
                $this->getWriteDb()->delete("sb__user_group", $params, $types);
            }
        }
    }

    /**
     * Code exists
     * @access public
     * @param $code
     * @param int $id
     * @return bool
     */
    public function codeExists($code, $id = 0)
    {
        return $this->valueExists('code', $code, $id);
    }


    /**
     * Retrieve all children groups to a selected group
     *
     * @access public
     * @param int $groupId - Parent group ID
     * @return array
     */
    public function getChildGroups($groupId)
    {
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('g.*')
            ->from($this->getTableName(), 'g')
            ->where('g.parent_id = :parentId')
            ->setParameter('parentId', $groupId);
        $groups = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return $groups;
    }


    /**
     * Retrieve all users linked to a selected group
     *
     * @access public
     * @param int $groupId - Group ID
     * @param string $select
     * @return array
     */
    public function getUsersInGroup($groupId, $select = 'u.*')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select($select)
            ->from('sb__user_group', 'ug')
            ->leftJoin('ug', 'sb__user', 'u', 'ug.user_id = u.id')
            ->leftJoin('ug', 'sb__group', 'g', 'ug.group_id = g.id')
            ->where('ug.group_id = :groupId')
            ->setParameter('groupId', $groupId)
            ->andWhere("ug.lote_deleted is null")
            ->andWhere("u.lote_deleted is null")
            ->andWhere("g.lote_deleted is null")
            ->andWhere("u.id > 0")
            ->addOrderBy('u.last_name')
            ->addOrderBy('u.first_name');
        $users = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return $users;
    }

    public function isDynamic($groupId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.group_type')
            ->from($this->getTableName(), 't')
            ->andWhere('t.id = :id')
            ->setParameter('id', $groupId)
            ->andWhere('t.group_type = "dynamic"')
            ->andWhere('t.lote_deleted is null')
            ->setMaxResults(1);
        return $q->execute()->fetch(\PDO::FETCH_COLUMN) !== false;
    }
    public function getUsersDynamicGroups($user_id){
        $return = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*')
            ->from($this->getTableName(), 't')
            ->andWhere('t.group_type = "dynamic"')
            ->andWhere('t.lote_deleted is null');
        $data = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        foreach($data as $d){
            $members = $this->getDynamicGroupMembersForUser($d['id'], $user_id);
            foreach($members as $member){
                if(isset($member['id']) && $member['id'] == $user_id){
                    $return[] = $d;
                }
            }
        }
        return $return;
    }
    public function getDynamicGroupMembers($groupId, $select = "t.*")
    {
        $e = new GroupEntity($this->getState());
        $e->load($groupId);

        // Retrieve query clauses
        $qum = new QueryUserModel($this->getState());
        $data = $qum->getQuery($e->query_id);
        $clauses = $qum->buildCustomFieldClauses($data['clauses']);

        // Retrieve query logic
        $qe = new QueryEntity($this->getState());
        $qe->load($e->query_id);
        $logic = $qe->logic;

        $m = new UserModel($this->getState());
        return $m->getListData(1, $clauses, [], $logic, 999999, $select);
    }

    public function getDynamicGroupMembersForUser($groupId, $userId ,$select = "t.*")
    {
        $e = new GroupEntity($this->getState());
        $e->load($groupId);

        // Retrieve query clauses
        $qum = new QueryUserModel($this->getState());
        $data = $qum->getQuery($e->query_id);



        $clauses = $qum->buildCustomFieldClauses($data['clauses']);

        $c = [];
        $c['field'] = 'id';
        $c['operator'] = 'equals';
        $c['value'] = $userId;
        $c['data_type'] = \PDO::PARAM_INT;
        $clauses[] = $c;

        // Retrieve query logic
        $qe = new QueryEntity($this->getState());
        $qe->load($e->query_id);
        $logic = $qe->logic;
        

        $m = new UserModel($this->getState());
        return $m->getListData(1, $clauses, [], $logic, 999999, $select);
    }

    public function getAllUsersInAdminGroups()
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("u.*")
            ->from('sb__user_group', 'ug')
            ->leftJoin('ug', 'sb__user', 'u', 'ug.user_id = u.id')
            ->leftJoin('ug', 'sb__group', 'g', 'ug.group_id = g.id')
            ->where('g.kind = :kind')
            ->setParameter('kind', 'admin')
            ->andWhere("ug.lote_deleted is null")
            ->andWhere("u.lote_deleted is null")
            ->andWhere("g.lote_deleted is null")
            ->andWhere("u.id > 0");
        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Check if a user is in an admin group
     * @param int $userId
     * @access public
     * @return array
     * */
    public function isInAdminGroup($userId)
    {
        $result = false;
        $userGroups = $this->getUserGroups($userId);
        if($userGroups && is_array($userGroups)) {
            foreach ($userGroups as $g) {
                if ($g['kind'] == 'admin') {
                    $result = true;
                    break;
                }
            }
        }
        return $result;
    }


    /**
     * Check if a user is in a protected group
     * @param int $userId
     * @access public
     * @return array
     * */
    public function isInProtectedGroup($userId)
    {
        $result = false;
        $userGroups = $this->getUserGroups($userId);
        if($userGroups && is_array($userGroups)) {
            foreach ($userGroups as $g) {
                if ($g['is_protected'] == '1') {
                    $result = true;
                    break;
                }
            }
        }
        return $result;
    }

    /**
     * Retrieve all groups with a broadcast reference
     *
     * @access public
     * @param boolean $activeOnly
     * @return array
     */
    public function getBroadcastGroups($activeOnly = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('g.*')
            ->from($this->getTableName(), 'g')
            ->where('g.broadcast_ref IS NOT NULL')
            ->andWhere('g.lote_deleted IS NULL');
        if ($activeOnly) {
            $q->andWhere('g.active = 1');
        }
        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Get all the groups
     *
     * @access public
     * @param int $parentId
     * @param string $kind
     * @param boolean $includeDynamic
     * @param boolean $isParentAccountAccessible
     * @param boolean $includeInactive
     * @return array
     */
    public function getAllGroups($parentId = 0, $kind = '', $includeDynamic = true, $isParentAccountAccessible = null, $includeInactive = true)
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('*')
            ->from($this->getTableName(), 'sbc')
            ->andWhere('parent_id = :parent_id')
            ->setParameter('parent_id', $parentId);
        if (!$includeDynamic) {
            $q->andWhere('group_type != "dynamic"');
        }
        if ($isParentAccountAccessible) {
            $q->andWhere('is_parent_account_accessible = 1');
        }
        if ($includeInactive) {
            $q->andWhere('active = 1');
        }
        $q->andWhere('lote_deleted is null')
            ->orderBy('sort_order')
            ->addOrderBy('id');

        if (!empty($kind)) {
            $q->andWhere('kind = :kind')
                ->setParameter('kind', $kind);
        }
        $this->addLoteAccessViewClauses($q, 'sbc', 'sb__group');

        $query = $data->execute();
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $row['children'] = $this->getAllGroups($row['id'], $kind);
            $result[] = $row;
        }
        return $result;
    }


    /**
     * @return GroupEntity[]
     * */
    public function getAllContactLists($parentId = false)
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('*')
            ->from($this->getTableName(), 'sbc')
            ->andWhere("group_type = :group_type")
            ->setParameter("group_type", GroupEntity::GROUP_TYPE_LIST);
        if ($parentId) {
            if ($parentId == false) {
                $q->andWhere("parent_id is null or parent_id = '' or parent_id = '0'");
            } else {
                $q->andWhere('parent_id = :parent_id')->setParameter('parent_id', $parentId);
            }
        }
        $q->andWhere('lote_deleted is null')->orderBy('sort_order')->addOrderBy('id');

        $this->addLoteAccessViewClauses($q, 'sbc', 'sb__group');

        $query = $data->execute();
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $e = new GroupEntity($this->getState());
            $e->setData($row);
            $result[] = $e;
        }
        return $result;
    }


    /**
     * Retrieve all groups with external_id set
     *
     * @access public
     * @return array
     */
    public function getAllGroupsWithExternalId()
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('g.*')
            ->from($this->getTableName(), 'g')
            ->andWhere('g.external_id IS NOT NULL')
            ->andWhere('g.external_id != ""')
            ->andWhere('g.lote_deleted IS NULL');
        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Retrieve all groups with tag and user_id
     *
     * @access public
     * @param $tag
     * @param $user_id
     * @return array
     */
    public function loadGroupsByUserTag($tag, $user_id, $kind = 'admin')
    {
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('distinct g.*')
            ->from('sb__group', 'g')
            ->leftJoin('g', 'sb__tag_usage', 'tu', 'tu.object_id = g.id')
            ->leftJoin('tu', 'sb__tag', 't', 't.id = tu.tag_id')
            ->leftJoin('g', 'sb__user_group', 'ug', 'g.id = ug.group_id')
            ->andWhere('tu.object_ref = :reference')
            ->setParameter('reference', 'user.group')
            ->andWhere('t.value = :tag_value')
            ->setParameter('tag_value', $tag)
            ->andWhere('ug.user_id = :user_id')
            ->setParameter('user_id', $user_id)
            ->andWhere('g.lote_deleted IS NULL')
            ->andWhere('tu.lote_deleted IS NULL')
            ->andWhere('t.lote_deleted IS NULL')
            ->andWhere('ug.lote_deleted IS NULL');

            if($kind) {
                $q->andWhere('g.kind = :kind')
                    ->setParameter('kind', $kind);
            }

        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }
}
