<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */


namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;

/**
 * Class GroupOwner
 * @package SidraBlue\Lote\Model
 */
class GroupOwner extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__group_owners';

    /**
     * Remove owners
     *
     * @access public
     * @param $id
     * @return void
     */
    public function removeOwners($id)
    {
        $this->getWriteDb()->delete('sb__group_owners', ['id' => $id]);
    }

    /**
     * Get owners
     *
     * @access public
     * @param $groupId
     * @param string $phrase
     * @return array
     */
    public function getOwners($groupId, $phrase = '')
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*,g.id as owner_id')
            ->from('sb__user', 'u')
            ->leftJoin('u', 'sb__group_owners', 'g', 'u.id = g.user_id')
            ->andWhere('g.group_id = :group_id')
            ->setParameter('group_id', $groupId);
        if (!empty($phrase)) {
            $q->andWhere('u.first_name like :phrase or u.last_name like :phrase or u.email like :phrase or u.username like :phrase')
                ->setParameter('phrase', "%$phrase%");
        }
        $query = $q->execute();
        $row = $query->fetchAll(\PDO::FETCH_ASSOC);

        if (!empty($row)) {
            foreach ($row as $value) {
                array_push($result, $value);
            }
        }
        return $result;
    }

    /**
     * Get group owners
     *
     * @access public
     * @param $groupId
     * @return array
     */
    public function getGroupOwners($groupId)
    {
        $groupId = intval($groupId);
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->tableName, 'u')
            ->where('u.group_id = :group_id')
            ->setParameter('group_id', $groupId);
        $query = $q->execute();
        $result = [];
        while ($row = $query->fetchAll(\PDO::FETCH_ASSOC)) {
            foreach ($row as $value) {
                $result[] = $value;
            }
        }
        return $result;
    }


    /**
     * Retrieve all groups owned by a selected user
     *
     * @access public
     * @param int $userId - User ID
     * @return array
     */
    public function getOwnedGroups($userId)
    {
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('g.*')
            ->from($this->getTableName(), 'o')
            ->leftJoin('o', 'sb__group', 'g', 'g.id = o.group_id')
            ->where('o.user_id = :userId')
            ->setParameter('userId', $userId);
        $groups = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return $groups;

    }
}