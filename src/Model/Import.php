<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Entity\Queue as QueueEntity;
use SidraBlue\Lote\Entity\Import as ImportEntity;
use SidraBlue\Lote\Object\Model\Base;

/**
 * Model class for Importing
 */
class Import extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__import';

    /**
     * Check if an import has already been completed
     * */
    public function isIncomplete($id)
    {
        $importEntity = new ImportEntity($this->getState());
        $importEntity->load($id);
        $result = $importEntity->id && $importEntity->isIncomplete();
        $importEntity = null;
        return $result;
    }

    /**
     * Check if an import is stopped
     * @param int $id - the import ID
     * @return boolean
     * */
    public function isStopped($id)
    {
        $result = false;
        $queueEntity = new QueueEntity($this->getState());
        if ($queueEntity->loadByObjectRefAndId('sb__import', $id)) {
            $result = $queueEntity->status == QueueEntity::STATUS_CANCELLED || $queueEntity->status == QueueEntity::STATUS_FAILED || $queueEntity->status == QueueEntity::STATUS_SUSPENDED;
        }
        $queueEntity = null;
        return $result;
    }

    public function updateQueueStatus($id)
    {
        $queueEntity = new QueueEntity($this->getState());
        if ($queueEntity->loadByObjectRefAndId('sb__import', $id)) {
            $importEntity = new ImportEntity($this->getState());
            if ($importEntity->load($id)) {
                if($importEntity->total_lines==0) {
                    $queueEntity->status = QueueEntity::STATUS_EMPTY;
                }
                else {
                    $queueEntity->percent_completed = min(floor(($importEntity->total_processed / $importEntity->total_lines)*100), 100);
                    $queueEntity->status_message = "Completed {$importEntity->total_processed} out of {$importEntity->total_lines}";
                }
                $queueEntity->save();
            }
            $importEntity = null;
        }
        $queueEntity = null;
    }

    public function createQueueEntry($importId)
    {
        $queueEntity = new QueueEntity($this->getState());
        $queueEntity->object_ref = 'sb__import';
        $queueEntity->object_id = $importId;
        $queueEntity->queued = true;
        $queueEntity->status = QueueEntity::STATUS_PENDING;
        $queueEntity->save();
        return $queueEntity;
    }

    /**
     * Synchronise status of import entity with its queue entity counterpart
     * @param $id - Import entity ID
     * @return bool|int - success
     */
    public function pullStatusFromQueueObject($id)
    {
        $success = false;
        $queueEntity = new QueueEntity($this->getState());
        // Search for queue entity and ensure it has a valid status
        if ($queueEntity->loadByObjectRefAndId('sb__import', $id) && !empty($queueEntity->status)) {
            $importEntity = new ImportEntity($this->getState());
            if ($importEntity->load($id)) {
                $importEntity->status = $queueEntity->status;
                $success = $importEntity->save();
            }
        }
        return $success;
    }

    public function queueJob($importId, $mode)
    {
        // Get or Create queue entity
        $qe = new QueueEntity($this->getState());
        $qe->loadByFields([
            'object_ref' => $this->getTableName(),
            'object_id' => $importId
        ]);
        if (!$qe->id) {
            $qe = $this->createQueueEntry($importId);
        }

        // Setup data
        $queueData = [];
        $queueData['reference'] = $this->getState()->getSettings()->get("system.reference");
        $queueData['data']['id'] = $importId;
        $queueData['data']['mode'] = $mode;
        $queueData['queue_entity_id'] = $qe->id;
        $notifData = [
            'user_id' => $this->getState()->getUser()->id,
            'subject' => 'User Import ',
            'view_url' => '_admin/import/'.$importId
        ];

        // Set mode-specific data and Enqueue the job
        if ($mode == 'remove') {
            $qe->title = 'User Removal Import';
            $notifData['subject'] .= 'Removal ';
            $notifData['view_url'] = str_replace('import/', 'import/removal/', $notifData['view_url']);
            $queueData['notification_data'] = $notifData;
            $this->getState()->getQueue()->add('user-update', '\Lote\System\Admin\Queue\Processor\User\Import\Removal', $queueData);
        } else {
            $qe->title = 'User Import';
            $queueData['notification_data'] = $notifData;
            /** @see \Lote\System\Admin\Queue\Processor\User\Import\Addition */
            $this->getState()->getQueue()->add('user-import', '\Lote\System\Admin\Queue\Processor\User\Import\Addition', $queueData);
        }
    }

    public function queueEventJob($importId)
    {
        // Get or Create queue entity
        $qe = new QueueEntity($this->getState());
        $qe->loadByFields([
            'object_ref' => $this->getTableName(),
            'object_id' => $importId
        ]);
        if (!$qe->id) {
            $qe = $this->createQueueEntry($importId);
        }

        // Setup data
        $queueData = [];
        $queueData['reference'] = $this->getState()->getSettings()->get("system.reference");
        $queueData['data']['id'] = $importId;
        $queueData['queue_entity_id'] = $qe->id;
        $notifData = [
            'user_id' => $this->getState()->getUser()->id,
            'subject' => 'Event Import ',
            'view_url' => '_admin/cms/event/import/' . $importId
        ];

        $qe->title = 'Event Import';
        $queueData['notification_data'] = $notifData;
        /** @see \Lote\Module\Event\Queue\Processor\Import */
        $this->getState()->getQueue()->add('event-import', '\Lote\Module\Event\Queue\Processor\Import', $queueData);
    }

    public function searchHistory($page = 1, $mode = null, $phrase = '', $sortField = 'id', $sortDirection = 'ASC')
    {
        $q = $this->getReadDb()->createQueryBuilder();

        $q->select('t.*, u.first_name as _user_first_name, u.last_name as _user_last_name, f.filename')
            ->from($this->getTableName(), 't')
            ->leftJoin('t', 'sb__user', 'u', 'u.id = t.lote_author_id')
            ->leftJoin('t', 'sb__file', 'f', 'f.id = t.file_id')
            ->andWhere('t.status not like "new"')
            ->andWhere('t.lote_deleted is null');

        // Mode: import|remove or null for both
        if ($mode == 'import') {
            $q->andWhere('t.mode not like "remove"');
        } elseif ($mode == 'remove') {
            $q->andWhere('t.mode like "remove"');
        }

        if (!empty(trim($sortField))) {
            // Default sort direction to ASC if $sortDirection is empty or not equal to 'DESC'
            if (empty($sortDirection) || strtoupper($sortDirection) != 'DESC') {
                $sortDirection = 'ASC';
            }
            $q->orderBy($sortField, $sortDirection);
        }

        return $this->getListByQuery($q, $page, 20);
    }

}
