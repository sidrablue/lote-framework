<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;
use SidraBlue\Lote\Entity\ImportRow as ImportRowEntity;

/**
 * Model class for Importing
 */
class ImportRow extends Base
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__import_row';

    /**
     * Get the latest import list
     * @param int $importId
     * @param String $note
     * @param array $data
     * @return array
     */
    public function saveError($importId, $note, $data)
    {
        return $this->saveRow($importId, $note, $data, 'error');
    }

    public function saveNew($importId, $note, $data)
    {
        return $this->saveRow($importId, $note, $data, 'new');
    }

    public function saveUpdate($importId, $note, $data)
    {
        return $this->saveRow($importId, $note, $data, 'update');
    }

    public function saveRemove($importId, $note, $data)
    {
        return $this->saveRow($importId, $note, $data, 'remove');
    }

    public function saveIgnore($importId, $note, $data)
    {
        return $this->saveRow($importId, $note, $data, 'ignored');
    }

    /**
     * Get the latest import list
     * @param int $importId
     * @param String $note
     * @param array $data
     * @param String $status
     * @return array
     */
    private function saveRow($importId, $note, $data, $status) {
        $m = new ImportRowEntity($this->getState());
        $m->import_id = $importId;
        $m->note = $note;
        $m->data = json_encode(array_map('utf8_encode', $data));
        $m->status = $status;
        $result = $m->save();
        $m = null;
        return $result;
    }



}
