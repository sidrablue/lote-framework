<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */


namespace SidraBlue\Lote\Model\Master;


use SidraBlue\Lote\Object\Model\Base;
use SidraBlue\Lote\Service\Aws as AwsService;
use SparkPost\SparkPost;
use SparkPost\SparkPostException;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use GuzzleHttp\Client;


/**
 * Class AccountDomain
 * @package SidraBlue\Lote\Model\Master
 */
class AccountDomain extends Base
{
    /**
     * @var string $tableName
     */
    protected $tableName = 'sb_master_account_domain';

    /**
     * @var SparkPost $sparky - SparkPost instance
     */
    private $sparky;

    /**
     * Retrieve SparkPost instance
     *
     * @access private
     * @return SparkPost
     */
    private function getSparky() {
        if($this->sparky == null) {
            $httpClient = new GuzzleAdapter(new Client);
            $options = [
                'key' => $this->getState()->getSettings()->getSettingOrConfig('sparkpost.key'),
                'async' => false
            ];
            $this->sparky = new SparkPost($httpClient, $options);
        }
        return $this->sparky;
    }

    /**
     * Retrieve all domains linked to the specified account
     *
     * @access public
     * @param string $accountRef - Account reference
     * @return array
     */
    public function getAccountDomains($accountRef)
    {
        $accountDomains = [];
        if($accountRef) {
            $this->setDb($this->getState()->getMasterDb());
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('ad.*')
                ->from($this->getTableName(), 'ad')
                ->where('ad.is_active = 1')
                ->andWhere('ad.lote_deleted IS NULL')
                ->andWhere('ad.account_ref = :account OR ad.account_ref = "*"')
                ->setParameter('account', $accountRef);
            $accountDomains = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        }

        return $accountDomains;
    }

    /**
     * Add an account domain as a SparkPost record
     *
     * @access public
     * @param int $id - Account domain entity
     * @return void
     */
    public function addAccountDomainToSparkpost($domain)
    {
        $target = $this->getState()->getSettings()->getSettingOrConfig('sparkpost.reply_webhook');

        $response = [];
        $response['inbound'] = $this->addSparkPostInboundDomain($domain);
        $response['relay'] = $this->addSparkPostRelayWebhook($domain, $target, 'Reply Relay Webhook');
        $response['sending'] = $this->addSparkPostSendingDomain($domain);
        if($response['sending']['success']) {
            $response['sending'] = $this->addAwsDkimRecord($domain, $response['sending']);
            if($response['sending']['success']) {
                $response['sending'] = $this->verifySparkPostSendingDomain($domain);
            }
        }
        ddd($response);
    }

    /**
     * Add inbound domain to SparkPost
     *
     * @access private
     * @param string $domain - Inbound domain
     * @return array
     */
    private function addSparkPostInboundDomain($domain)
    {
        $result = [
            'success' => false
        ];

        try {
            $this->getSparky()->request('POST', 'inbound-domains', [
                'domain' => $domain
            ]);
            $result['success'] = true;
        } catch(SparkPostException $ex) {
            if($ex->getCode() == 409) {
                $result['success'] = true;
            } else {
                $result['error'] = $ex->getMessage();
            }
        }
        return $result;
    }

    /**
     * Add relay webhook to SparkPost
     *
     * @access private
     * @param string $domain - Inbound domain
     * @param string $target - Webhook target
     * @param string $name - Webhook name
     * @return array
     */
    private function addSparkPostRelayWebhook($domain, $target, $name)
    {
        $result = [
            'success' => false
        ];
        try {
            $this->getSparky()->request('POST', 'relay-webhooks', [
                'name' => $name,
                'target' => $target,
                'auth_token' => '',
                'match' => [
                    'protocol' => 'SMTP',
                    'domain' => $domain,
                ]
            ]);
            $result['success'] = true;
        } catch(SparkPostException $ex) {
            if($ex->getCode() == 409) {
                $result['success'] = true;
            } else {
                $result['error'] = $ex->getMessage();
            }
        }
        return $result;
    }

    /**
     * Add sending domain to SparkPost
     *
     * @access private
     * @param string $domain - Sending domain
     * @return array
     */
    private function addSparkPostSendingDomain($domain)
    {
        $result = [
            'success' => false
        ];
        try {
            $response = $this->getSparky()->request('POST', 'sending-domains', [
                'domain' => $domain,
                'generate_dkim' => true,
            ]);
            $result['success'] = true;
            $result['dkim'] = $response->getBody()['results']['dkim']['public'];
            $result['selector'] = $response->getBody()['results']['dkim']['selector'];
        } catch(SparkPostException $ex) {
            if($ex->getCode() == 409) {
                $result = $this->getSparkPostSendingDomainDkim($domain);
            } else {
                $result['error'] = $ex->getMessage();
            }
        }
        return $result;
    }

    /**
     * Retrieve sending domain dkim from existing sending domain
     *
     * @access private
     * @param string $domain - Sending domain
     * @return array
     */
    private function getSparkPostSendingDomainDkim($domain)
    {
        $result = [
            'success' => false
        ];
        try {
            $response = $this->getSparky()->request('GET', "sending-domains/{$domain}");
            $result['success'] = true;
            $result['dkim'] = $response->getBody()['results']['dkim']['public'];
            $result['selector'] = $response->getBody()['results']['dkim']['selector'];
        } catch(SparkPostException $ex) {
            $result['error'] = $ex->getMessage();
        }
        return $result;
    }

    /**
     * Verify SparkPost sending domain
     *
     * @access private
     * @param string $domain - Sending domain
     * @return array
     */
    private function verifySparkPostSendingDomain($domain)
    {
        $result = [
            'success' => false
        ];
        try {
            $ii = 0;
            $verified = false;
            $limit = 5;
            while(!$verified && $ii != $limit) {
                if($ii > 0) {
                    sleep(2);
                }
                $response = $this->getSparky()->request('POST', "sending-domains/{$domain}/verify", [
                    'dkim_verify' => true
                ]);
                $verified = $response->getBody()['results']['ownership_verified'];
                $ii++;
            }
            $result['success'] = $verified;
            if(!$verified) {
                $result['error'] = "Unable to verify sending domain";
            }
        } catch(SparkPostException $ex) {
            $result['error'] = $ex->getMessage();
        }
        return $result;
    }

    /**
     * Add AWS DKIM TXT record for a given domain
     *
     * @access private
     * @param string $domain - Sending domain
     * @param string $publicDkim  - Sending domain DKIM public key
     * @return array
     */
    private function addAwsDkimRecord($domain, $dkimDetails)
    {
        $hostname = "{$dkimDetails['selector']}._domainkey.{$domain}";
        $recordString = "\"v=DKIM1; k=rsa; p={$dkimDetails['dkim']};\"";
        $parentZoneId = $this->getState()->getSettings()->getSettingOrConfig('aws.route53.parent_zone_id');

        $as = new AwsService($this->getState());
        $result = $as->addRoute53Record($parentZoneId, $hostname, 'TXT', $recordString);
        return $result;
    }
}
