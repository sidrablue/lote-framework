<?php

namespace SidraBlue\Lote\Model\Master\Beacon;

use SidraBlue\Lote\Object\Model\BaseMaster;

/**
 * Class for Theme
 */
class Site extends BaseMaster
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb_beacon_site';

    public function getParentOptions($id = ''){
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->tableName, 't');
        if($id){
            $q->andWhere('t.id != :id')
                ->setParameter('id', $id)
                ->andWhere('t.parent_id != :parent_id or parent_id is null')
                ->setParameter('parent_id', $id)
                ->andWhere('t.lote_path not like :lote_path')
                ->setParameter('lote_path', "%".$id."%");
        }
        $q->andWhere('t.lote_deleted is null');
        $q->addOrderBy('t.name')
            ->addOrderBy('t.id');
        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }


}