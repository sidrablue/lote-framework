<?php

namespace SidraBlue\Lote\Model\Master\Beacon;

use SidraBlue\Lote\Object\Model\BaseMaster;

/**
 * Class for Theme
 */
class SiteCategory extends BaseMaster
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb_beacon_site_category';

    /**
     * Retrieve category search query using
     *
     * @param $params
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    public function getSearchQuery($params)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('c.*')
            ->from($this->getTableName(), 'c')
            ->where('c.lote_deleted is null');

        //Add phrase search query
        if(isset($params['phrase']) && !empty($params['phrase'])) {
            $q->andWhere('c.name like :phrase')
                ->setParameter('phrase', '%' . $params['phrase'] . '%');
        }

        //Add account_ref search query
        if(isset($params['account_ref']) && !empty($params['account_ref'])) {
            $q->andWhere('c.account_ref = :account_ref')
                ->setParameter('account_ref', $params['account_ref']);
        }

        //Add parent_id search query
        if(isset($params['parent_id']) && $params['parent_id'] > 0) {
            $q->andWhere('c.parent_id = :parent_id')
                ->setParameter('parent_id', $params['parent_id']);
        }else{
            $q->andWhere('c.parent_id = :parent_id or c.parent_id is null')
                ->setParameter('parent_id', 0);
        }

        return $q;
    }


}