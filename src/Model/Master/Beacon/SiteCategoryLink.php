<?php

namespace SidraBlue\Lote\Model\Master\Beacon;

use SidraBlue\Lote\Object\Model\BaseMaster;

/**
 * Class for Theme
 */
class SiteCategoryLink extends BaseMaster
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb_beacon_site_category_link';

    /**
     * Delete the categories for a site
     * @access public
     * @param int $siteId
     * @param int $categoryId
     * @return boolean
     */

    public function deleteSiteCategory($siteId, $categoryId)
    {
        return $this->getWriteDb()->delete($this->getTableName(), ['site_id' => $siteId, 'category_id' => $categoryId]);
    }

    /**
     * Get the categories for a site
     * @access public
     * @param int $siteId
     * @return array
     */
    public function getSiteCategories($siteId) {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("c.*")
            ->from($this->tableName, 'l')
            ->leftJoin('l', 'sb_beacon_site_category', 'c', 'c.id = l.category_id')
            ->where("l.site_id = :site_id")
            ->setParameter("site_id", $siteId)
            ->addOrderBy("l.is_primary", 'desc')
            ->addOrderBy("l.sort_order", 'asc');
        $q->andWhere("c.lote_deleted is null");
        $q->andWhere("l.lote_deleted is null");
        $s = $q->execute();
        if($data = $s->fetchAll(\PDO::FETCH_ASSOC)) {
            $result = $data;
        }
        return $result;
    }


}