<?php

namespace SidraBlue\Lote\Model\Master\Beacon;

use SidraBlue\Lote\Object\Model\BaseMaster;

/**
 * Class for Theme
 */
class SiteImage extends BaseMaster
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb_beacon_site_image';

    /**
     * Get the images for a site
     * @access public
     * @param int $siteId
     * @return array
     */
    public function getSiteImages($siteId) {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("i.*")
            ->from($this->tableName, 'i')
            ->where("i.site_id = :site_id")
            ->setParameter("site_id", $siteId)
            ->addOrderBy("i.sort_order", 'asc');
        $q->andWhere("i.lote_deleted is null");
        $s = $q->execute();
        if($data = $s->fetchAll(\PDO::FETCH_ASSOC)) {
            $result = $data;
        }
        return $result;
    }

    /**
     * Set primary image
     * @access public
     * @param int $id
     * @param int $siteId
     * @return array
     */
    public function setPrimaryImage($id , $siteId)
    {

        $data = ['is_primary' => 0];
        $this->getWriteDb()->update($this->getTableName(), $data, ['site_id' => $siteId]);

        $data = ['is_primary' => 1];
        $this->getWriteDb()->update($this->getTableName(), $data, ['id' => $id]);

        return true;

    }


}