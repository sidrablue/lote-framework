<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model\Master;

use SidraBlue\Lote\Object\Model\BaseMaster;

/**
 * Model class for Importing
 */
class BusinessType extends BaseMaster
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__business_type';

}
