<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model\Master;


use SidraBlue\Lote\Model\Queue;
/**
 * Model class for Importing
 */
class QueueMaster extends Queue
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb_master_queue';
    
    /**
     * Define the read DB function so that it can be overwritten
     * @access protected
     * @return \Doctrine\DBAL\Connection
     * */
    protected function getReadDb()
    {
        return $this->getState()->getMasterDb();
    }

    /**
     * Define the write DB function so that it can be overwritten
     * @access protected
     * @return \Doctrine\DBAL\Connection
     * */
    protected function getWriteDb()
    {
        return $this->getState()->getMasterDb();
    }

    /**
     * Set a job as queued
     * @param int $jobId - the ID of the job
     * @param string|bool $queueId - the ID of the job in the queue
     * @return boolean
     * */
    public function setQueued($jobId, $queueId = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->update($this->getTableName())
            ->set('queued', ':queued')
            ->setParameter('queued', '1')
            ->set("status", ":status")
            ->setParameter('status', 'queued')
            ->andWhere('`status` = "pending"')
            ->andWhere('id = :id')
            ->setParameter('id', $jobId);
        if($queueId) {
            $q->set("queue_id", ":queue_id")
                ->setParameter("queue_id", $queueId);
        }
        return $q->execute();
    }

}
