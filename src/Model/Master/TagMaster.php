<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model\Master;


use SidraBlue\Lote\Model\Tag;
/**
 * Model class for Importing
 */
class TagMaster extends Tag
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__tag';
    
    /**
     * Define the read DB function so that it can be overwritten
     * @access protected
     * @return \Doctrine\DBAL\Connection
     * */
    protected function getReadDb()
    {
        return $this->getState()->getMasterDb();
    }

    /**
     * Define the write DB function so that it can be overwritten
     * @access protected
     * @return \Doctrine\DBAL\Connection
     * */
    protected function getWriteDb()
    {
        return $this->getState()->getMasterDb();
    }

    public function masterTagExists($accountReference, $value, $id = 0)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("*")
            ->from('sb__tag', 'a')
            ->where("lower(a.value) = :value")
            ->setParameter("value" , strtolower($value))
            ->andWhere("lower(a.account_ref) = :account_ref")
            ->setParameter("account_ref" , strtolower($accountReference))
            ->andWhere('a.lote_deleted is null');

        if($id) {
            $q->andWhere("id != :id")
                ->setParameter("id", $id);
        }
        return $q->execute()->fetchColumn() > 0;
    }

}
