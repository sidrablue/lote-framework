<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model\Master;


use SidraBlue\Lote\Model\TagUsage;
use SidraBlue\Lote\Entity\Master\TagUsageMaster as TagUsageMasterEntity;
/**
 * Model class for Importing
 */
class TagUsageMaster extends TagUsage
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__tag_usage';
    
    /**
     * Define the read DB function so that it can be overwritten
     * @access protected
     * @return \Doctrine\DBAL\Connection
     * */
    protected function getReadDb()
    {
        return $this->getState()->getMasterDb();
    }

    /**
     * Define the write DB function so that it can be overwritten
     * @access protected
     * @return \Doctrine\DBAL\Connection
     * */
    protected function getWriteDb()
    {
        return $this->getState()->getMasterDb();
    }

    public function createTagUsageIfNonExistent($tagId, $objectRef, $objectId, $fieldRef = '')
    {
        $e = new TagUsageMasterEntity($this->getState()->getMasterState());
        if(!$e->loadByFields(['tag_id' => $tagId, 'object_ref' => $objectRef, 'object_id' => $objectId, 'field_ref' => $fieldRef])) {
            $e->tag_id = $tagId;
            $e->object_ref = $objectRef;
            $e->object_id = $objectId;
            $e->field_ref = $fieldRef;

            $e->save();
        }
    }

    public function deleteTagUsageIfExistent($tagId, $objectRef, $objectId, $fieldRef = '')
    {
        $e = new TagUsageMasterEntity($this->getState()->getMasterState());
        if($e->loadByFields(['tag_id' => $tagId, 'object_ref' => $objectRef, 'object_id' => $objectId, 'field_ref' => $fieldRef])) {
            $e->delete();
        }
    }

    public function deleteTagUsageByObjectRef($accountRef, $objectId, $objectRef)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.id')
            ->from($this->tableName, 't')
            ->where('t.lote_deleted is null')
            ->andWhere('t.object_id = :themeId')
            ->setParameter('themeId', $objectId)
            ->andWhere('t.object_ref = :objectRef')
            ->setParameter('objectRef', $objectRef)
            ->leftJoin('t', 'sb__tag', 'u', 't.tag_id = u.id')
            ->andWhere('u.lote_deleted is null')
            ->andWhere('u.account_ref = :accountRef')
            ->setParameter('accountRef', $accountRef);
        $results = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);

        $e = new TagUsageMasterEntity($this->getState()->getMasterState());
        foreach ($results as $result) {
            $e->load($result['id']);
            $e->delete();
        }
    }

    public function getSharedTagsByAccountRef($accountRef, $themeId, $objectRef)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.display_value')
            ->from('sb__tag', 't')
            ->where('t.lote_deleted is null')
            ->andWhere('t.account_ref = :accountRef')
            ->leftJoin('t', 'sb__tag_usage', 'u', 't.id = u.tag_id')
            ->andWhere('u.lote_deleted is null')
            ->andWhere('u.object_ref = :objRef')
            ->andWhere('u.object_id = :themeId')
            ->setParameter('objRef', $objectRef)
            ->setParameter('accountRef', $accountRef)
            ->setParameter('themeId', $themeId);

        $results = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        $resultsR = [];

        foreach($results as $result) {
            $resultsR[] = $result['display_value'];
        }

        return $resultsR;
    }

}
