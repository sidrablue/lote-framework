<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use Doctrine\DBAL\Connection;
use SidraBlue\Lote\Entity\Message as MessageEntity;
use Doctrine\DBAL\Query\QueryBuilder;
use SidraBlue\Lote\Object\Model\Base;

/**
 * Model class for a Message
 *
 * @package SidraBlue\Lote\Model
 */
class Message extends Base
{
    /**
     * @var string $tableName
     */
    protected $tableName = 'sb_message';

    /**
     * Retrieve message search query
     *
     * @access public
     * @param array $params - Search parameters
     * @param boolean $parentDb - to use parent db connection
     * @return QueryBuilder|false
     */
    public function getSearchQuery($params, $parentDb = false)
    {
        $result = false;
        if ($parentDb) {
            $db = $this->getState()->getSuperParentDb();
        } else {
            $db = $this->getReadDb();
        }
        if($db) {
            $sq = new QueryBuilder($db);

            $sq->select('m.*')
                ->from($this->getTableName(), 'm')
                ->andWhere('m.lote_deleted is null');

            if (!empty($params['id'])) {
                $sq->andWhere('m.id = :id')
                    ->setParameter('id', $params['id']);
            }

            if (!empty($params['type'])) {
                $sq->andWhere('m.type = :type')
                    ->setParameter('type', $params['type']);
            }

            if (!empty($params['reference'])) {
                if (is_array($params['reference'])) {
                    $sq->andWhere('m.reference in (:reference)')
                        ->setParameter('reference', $params['reference'], Connection::PARAM_STR_ARRAY);
                } else {
                    $sq->andWhere('m.reference = :reference')
                        ->setParameter('reference', $params['reference']);
                }
            }

            if (!empty($params['phrase'])) {
                $phraseSearchFields = [
                    'like' => ['m.name', 'm.description', 'm.content'],
                    'exact' => ['m.id']
                ];
                $wherePhrase = '';
                foreach ($phraseSearchFields as $type => $fields) {
                    if ($type == 'like') {
                        foreach ($fields as $field) {
                            $wherePhrase .= "or {$field} like :phrase_like ";
                        }
                    }
                }
                foreach ($phraseSearchFields as $type => $fields) {
                    if ($type == 'exact') {
                        foreach ($fields as $field) {
                            $wherePhrase .= "or {$field} = :phrase_exact ";
                        }
                    }
                }
                $wherePhrase = substr($wherePhrase, 2);

                $sq->andWhere($wherePhrase)
                    ->setParameter('phrase_exact', $params['phrase'])
                    ->setParameter('phrase_like', '%' . $params['phrase'] . '%');
            }

            if (!empty($params['tags'])) {
                $sq->leftJoin("m", "sb__tag_usage", "tu", "tu.object_id = m.id and tu.object_ref = :object_ref")
                    ->setParameter("object_ref", "sb_message")
                    ->leftJoin('tu', 'sb__tag', 't', 't.id = tu.tag_id')
                    ->andWhere('tu.lote_deleted is null')
                    ->andWhere('t.value in (:tags)')
                    ->setParameter('tags', $params['tags'], Connection::PARAM_STR_ARRAY);
            }
            $result = $sq;
        }
        return $result;
    }

    /**
     * Get list by search
     *
     * @access public
     * @param array $searchParams - Search parameters
     * @return array
     */
    public function getListBySearch($searchParams)
    {
        $status = isset($searchParams['status']) ? $searchParams['status'] : '';
        $messages = [];
        if (!$status || $status == 'custom') {
            if($q = $this->getSearchQuery($searchParams)) {
                $results = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);

                foreach ($results as $k => $v) {
//                if ($this->getState()->getSuperParentState()->accountReference == $this->getState()->accountReference) {
//                    $v['status'] = 'default';
//                } else {
                    $v['status'] = 'custom';
//                }
                    $tu = new TagUsage($this->getState());
                    $v['_tags'] = $tu->getObjectTagArray('sb_message', $v['id']);
                    $messages[$v['reference']][] = $v;
                }
            }
        }

        if ($this->getState()->getSuperParentState()->accountReference != $this->getState()->accountReference) {
            if (!$status || $status == 'default') {
                if($pq = $this->getSearchQuery($searchParams, true)) {
                    $parentResults = $pq->execute()->fetchAll(\PDO::FETCH_ASSOC);

                    foreach ($parentResults as $k => $v) {
                        if (!isset($messages[$v['reference']])) {
                            $v['status'] = 'default';
                            $tu = new TagUsage($this->getState()->getSuperParentState());
                            $v['_tags'] = $tu->getObjectTagArray('sb_message', $v['id']);
                            $messages[$v['reference']][] = $v;
                        } elseif (isset($messages[$v['reference']])) {
                            $messages[$v['reference']][0]['has_default'] = true;
                        } else {
                            $messages['noreference'][] = $v;
                        }
                    }
                }
            }
        }

        $results = [];
        foreach ($messages as $k => $v) {
            $results[] = $v[0];
        }

        return $results;
    }

    /**
     * Function to get a message by field. if exists as custom get the message else get default if exists
     *
     * @param string $value
     * @param string $field
     * @return array
     */
    public function getMessageByField($value, $field)
    {
        if ($field == 'wildcard') {
            $value = str_replace('%%__', '', $value);
            $value = str_replace('__%%', '', $value);
            $value = str_replace('%%_', '', $value);
            $value = str_replace('_%%', '', $value);
            $value = str_replace('%%', '', $value);
            $value = str_replace('%%', '', $value);
        }

        $result = [];
        $me = new MessageEntity($this->getState());
        if ($me->loadByFields([$field => $value])) {
            $result = $me->getData();
        } else {
            $dme = new MessageEntity($this->getState()->getSuperParentState());
            if ($dme->loadByFields([$field => $value])) {
                $result = $dme->getData();
            }
        }

        return $result;
    }

    public function getMessageByReference($searchParams)
    {
        $status = isset($searchParams['status']) ? $searchParams['status'] : '';
        $message = [];
        if (!$status || $status == 'custom') {
            if ($q = $this->getSearchQuery($searchParams)) {
                $message = $q->execute()->fetch(\PDO::FETCH_ASSOC);
            }
            if (!$message && $this->getState()->getSuperParentState()->accountReference != $this->getState()->accountReference) {
                if (!$status || $status == 'default') {
                    if ($pq = $this->getSearchQuery($searchParams, true)) {
                        $message = $pq->execute()->fetch(\PDO::FETCH_ASSOC);
                    }
                }
            }
            return $message;
        }
    }

}
