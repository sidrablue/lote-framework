<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use Doctrine\DBAL\Query\QueryBuilder;
use SidraBlue\Lote\Object\Model\Base;
use SidraBlue\Lote\Entity\MessageEmail as MessageEmailEntity;
use SidraBlue\Util\Time;

/**
 * Class MessageEmail
 * @package SidraBlue\Lote\Model
 */
class MessageEmail extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb_message_email';

    /**
     * Retrieve message emails search query
     *
     * @access public
     * @param array $params - Search parameters
     * @return QueryBuilder
     */
    public function getSearchQuery($params)
    {
        $sq = new QueryBuilder($this->getReadDb());
        $sq->select('me.*')
            ->from($this->getTableName(), 'me')
            ->andWhere('me.lote_deleted is null');

        if(isset($params['id'])) {
            $sq->andWhere('me.id = :id')
                ->setParameter('id', $params['id']);
        }

        if(isset($params['email_type']) && $params['email_type']) {
            $sq->andWhere('me.email_type = :email_type')
                ->setParameter('email_type', $params['email_type']);
        }

        if(isset($params['email_type_like']) && $params['email_type_like']) {
            $sq->andWhere('me.email_type like :email_type')
                ->setParameter('email_type', "'%{$params['email_type']}%'");
        }

        if(isset($params['object_ref']) && $params['object_ref']) {
            $sq->andWhere('me.object_ref = :object_ref')
                ->setParameter('object_ref', $params['object_ref']);
        }

        if(isset($params['object_id']) && $params['object_id']) {
            $sq->andWhere('me.object_id = :object_id')
                ->setParameter('object_id', $params['object_id']);
        }

        if(isset($params['processed_is_null'])) {
            if ((bool)$params['processed_is_null']) {
                $sq->andWhere('me.processed is null');
            } else {
                $sq->andWhere('me.processed is not null');
            }
        }

        if(isset($params['scheduled_before']) && $params['scheduled_before']) {
            $sq->andWhere('me.scheduled <= :scheduled_before')
                ->setParameter('scheduled_before', $params['scheduled_before']);
        }
        if(isset($params['scheduled_after']) && $params['scheduled_after']) {
            $sq->andWhere('me.scheduled >= :scheduled_after')
                ->setParameter('scheduled_after', $params['scheduled_after']);
        }

        if(isset($params['phrase']) && $params['phrase']) {
            $phraseSearchFields = [
                'like' => ['data'],
                'exact' => ['id']
            ];
            //$sq->addPhraseFilter('me', $phraseSearchFields, $params['phrase']);
        }

        return $sq;
    }

    /**
     * Get list by search
     *
     * @access public
     * @param array $searchParams - Search parameters
     * @param int $page - Page
     * @param int $perPage - Results per page
     * @return array
     */
    public function getListBySearch($searchParams, $page = 1, $perPage = 20)
    {
        $sq = $this->getSearchQuery($searchParams);
        return $this->getListByQuery($sq, $page, $perPage);
    }

    /**
     * Check if there are emails to process
     *
     * @access public
     * @param string $emailType - Email Type
     * @return boolean
     */
    public function hasEmailsToProcess($emailType)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('count(*) as count')
            ->from($this->tableName, 'e')
            ->where('e.email_type = :email_type')
            ->setParameter('email_type', $emailType)
            ->andWhere('(e.scheduled < now() or e.scheduled is null)')
            ->andWhere('e.processed is null');
        $q->andWhere("e.lote_deleted is null");

        $s = $q->execute();
        $result = $s->fetch(\PDO::FETCH_ASSOC);
        return $result['count'] > 0;

    }

    /**
     * Get Emails to process
     *
     * @access public
     * @param string $emailType - Email Type
     * @return array|false
     */
    public function getEmailsToProcess($emailType)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('e.*')
            ->from($this->tableName, 'e')
            ->where('e.email_type = :email_type')
            ->setParameter('email_type', $emailType)
            ->andWhere('(e.scheduled < now() or e.scheduled is null)')
            ->andWhere('e.processed is null');
        $q->andWhere("e.lote_deleted is null");

        $s = $q->execute();
        $result = $s->fetchAll(\PDO::FETCH_ASSOC);
        return $result;

    }

    /**
     * Adding new message email entry for Parent and Staff
     * Business logic applies on setting the email type based on the occasion
     *
     * @access public
     * @param array $input - Array of Message email input data
     * @return void
     */
    public function addNewEntry($input)
    {

        $data = [
            'email_type' => $input['email_type'],
            'object_id' => $input['object_id'],
            'object_ref' => $input['object_ref'],
            'data' => $input['data'],
            'processed' => $input['processed'],
            'scheduled' => Time::getUtcNow(),
            'note' => isset($input['note']) ? $input['note'] : ''
        ];

        $this->processAddNewEntry($data);
    }

    /**
     * Process for adding new Message email entry into database
     *
     * @access private
     * @param array $data - Array of Message email input data
     * @return void
     */
    protected function processAddNewEntry($data)
    {
        if($data['processed'] == '') {
            unset($data['processed']);
        }

        $mee = new MessageEmailEntity($this->getState());
        $mee->setData($data);
        $mee->save();
    }

}
