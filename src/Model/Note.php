<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;
use SidraBlue\Lote\Entity\Note as NoteEntity;
use SidraBlue\Util\Arrays;

/**
 * Model class for a Note
 *
 * @package SidraBlue\Lote\Model
 */
class Note extends Base
{
    /**
     * @var string $tableName
     */
    protected $tableName = 'sb__note';

    /**
     * Get the content of a note
     *
     * @param int $noteId - the ID of the note to get
     * @access public
     * @return string
     */
    public function getNote($noteId)
    {
        $content = '';
        $e = new NoteEntity($this->getState());
        $e->load($noteId);

        return $content;
    }

    /**
     * Get the content of a list of notes
     *
     * @access public
     * @param $noteIds
     * @return array
     */
    public function getNotes(Array $noteIds)
    {
        $notes = [];

        foreach ($noteIds as $noteId) {
            $notes[$noteId] = $this->getNote($noteId);
        }

        return $notes;
    }

    /**
     * Get an array list of the data for one or more notes by object id
     *
     * @param int $id - the ID of the object
     * @return array|false
     * */
    public function getNotesByObjectId($id)
    {
        $result = false;
        if ($id > 0) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('o.*')->from($this->getTableName(), 'o')->where('o.object_id = :id')->setParameter('id', $id);
            $s = $q->execute();
            $result = $s->fetchAll(\PDO::FETCH_ASSOC);
        }
        return $result;
    }

    /**
     * Add data to a note
     *
     * @access public
     * @param array $data Data to be added
     * @return array
     */
    public function addNoteData($data)
    {
        $values = Arrays::getFieldValuesFrom2dArray($data, 'id', true);
        $notes = $this->getNotes($values);
        $cnt = count($data);
        for ($i = 0; $i < $cnt; $i++) {
            if (isset($notes[$data[$i]['id']])) {
                $data[$i]['_note'] = $notes[$data[$i]['id']];
            }
        }
        return $data;
    }

    /**
     * Delete multiple notes using their ID's
     *
     * @access public
     * @param array $noteIds - An array of note ID's to delete
     */
    public function bulkDelete($noteIds)
    {
        foreach ($noteIds as $noteId) {
            $this->delete($noteId);
        }
    }

    /**
     * Retrieves all notes associated with a selected object
     *
     * @param string $objectRef - Object name
     * @param string $objectId - Object id name
     * @param boolean $isConfidential - True if result contains confidential notes
     * @return array
     */
    public function loadByObject($objectId, $objectRef, $isConfidential = false)
    {
        $clause = [];
        $c = [];
        $c['field'] = 'object_ref';
        $c['operator'] = 'equals';
        $c['value'] = $objectRef;
        $c['data_type'] = \PDO::PARAM_STR;
        $clause[] = $c;

        $c = [];
        $c['field'] = 'object_id';
        $c['operator'] = 'equals';
        $c['value'] = $objectId;
        $c['data_type'] = \PDO::PARAM_INT;
        $clause[] = $c;

        $c = [];
        $c['field'] = 'confidential';
        $c['operator'] = 'equals';
        $c['value'] = $isConfidential;
        $c['data_type'] = \PDO::PARAM_BOOL;
        $clause[] = $c;

        return $this->getList(1, $clause);

    }

}