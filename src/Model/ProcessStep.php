<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use Doctrine\DBAL\Connection;
use SidraBlue\Lote\Object\Model\Base;

/**
 * Model class for a file
 *
 * @package Lote\System\Crm\Model
 */
class ProcessStep extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__process_step';

    public function getProcessSteps($processId){
       /* $processId = intval($processId);
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->tableName, 'u')
            ->where('u.process_id = :process_id')
            ->setParameter('process_id',$processId);
        $query = $q->execute();
        $result = [];
        while ($row = $query->fetchAll(\PDO::FETCH_ASSOC)) {
            foreach($row as $value){
                $result[] = $value;
            }
        }
        return $result;*/
        $processId = intval($processId);

        $result = [];
        $clause = [];
        $options = [];

        $clause['field'] = 'process_id';
        $clause['operator'] = 'equals';
        $clause['value'] = $processId;
        $clause['data_type'] = \PDO::PARAM_INT;
        $clauses[] = $clause;

        $page = 1;
        $listArr = $this->getList($page, $clauses, $options, '', 9999);
        if (isset($listArr['rows'])) {
            $result = $listArr['rows'];
        }
        return $result;

    }

    public function getProcessMaxGroup($processId){
        $processId = intval($processId);
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('max(group_id) as max_value')
            ->from($this->tableName, 'u')
            ->where('u.process_id = :process_id')
            ->setParameter('process_id',$processId);
        $query = $q->execute();
        $result = 0;
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            if(!empty($row['max_value'])){
                $result = $row['max_value'];
            }

        }
        return $result;
    }

} 