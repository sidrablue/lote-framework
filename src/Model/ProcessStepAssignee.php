<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;

/**
 * Model class for a process step assignee
 *
 * @package SidraBlue\Lote\Model
 */
class ProcessStepAssignee extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__process_step_assignee';


    /**
     * Retrieves all assignees associated with a step
     *
     * @access public
     * @param int $stepId - Step ID
     * @return array
     */
    public function getStepAssignees($stepId){
        /*$stepId = intval($stepId);
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->tableName, 'u')
            ->where('u.step_id = :step_id')
            ->setParameter('step_id',$stepId);
        $query = $q->execute();
        $result = [];
        while ($row = $query->fetchAll(\PDO::FETCH_ASSOC)) {
            foreach($row as $value){
                $result[] = $value;
            }
        }
        return $result;*/
        $result = [];
        $stepId = intval($stepId);
        $clauses = $this->searchClauses($stepId);
        $options = $this->searchOptions();
        $page = 1;

        $listArr = $this->getList($page, $clauses, $options, '', 9999);
        if (isset($listArr['rows'])) {
            $result = $listArr['rows'];
        }
        return $result;
    }

    private function searchClauses($stepId)
    {
        $clauses = [];

        $clause = [];
        $clause['field'] = 'step_id';
        $clause['operator'] = 'equals';
        $clause['value'] = $stepId;
        $clause['data_type'] = \PDO::PARAM_INT;
        $clauses[] = $clause;

        return $clauses;
    }

    private function searchOptions()
    {
        $options = [];
        //$options['name'] = 'asc';
        return $options;
    }


}