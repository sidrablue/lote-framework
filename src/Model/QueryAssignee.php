<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
 

namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;


class QueryAssignee extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__query_assignee';

    public function getQueryAssignees($queryId){
        $clauses = [];

        $c = [];
        $c['field'] = 'query_id';
        $c['operator'] = 'equals';
        $c['value'] = intval($queryId);
        $c['data_type'] = \PDO::PARAM_INT;
        $clauses[] = $c;

        $result = $this->getList(1, $clauses);

        return $result;
    }




}