<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use Doctrine\DBAL\Query\QueryBuilder;
use SidraBlue\Lote\Entity\RecurrenceRule as RecurrenceRuleEntity;
use SidraBlue\Lote\Object\Model\Base;

class RecurrenceRule extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__recurrence_rule';

    public function deleteByObjectRefAndId($objectRef, $objectId)
    {
        $data = $this->getRulesByObject($objectRef, $objectId);
        foreach($data as $r) {
            $eve = new RecurrenceRuleEntity($this->getState());
            $eve->load($r['id']);
            $eve->delete(true);
        }
    }


    public function getSearchQuery($params = [])
    {
        $q = $this->getReadDb()->createQueryBuilder();

        $q->select('t.*')
            ->from($this->getTableName(), 't')
            ->andWhere('t.lote_deleted is null');

        // Add filter clauses if parameter is present
        if (isset($params['filters'])) {
            $this->addFilterClauses($q, $params['filters']);
        }

        // Add order by clause if parameters are present
        if (isset($params['sort_field'])) {
            if (isset($params['sort_direction'])) {
                $this->addOrderByClause($q, $params['sort_field'], $params['sort_direction']);
            } else {
                $this->addOrderByClause($q, $params['sort_field']);
            }
        }

        return $q;
    }

    /**
     * @param QueryBuilder $q
     * @param array $filters
     * @return QueryBuilder - $q for chaining
     */
    protected function addFilterClauses($q, $filters)
    {
        if (is_array($filters)) {
            foreach ($filters as $column => $value) {
                // Validate/Sanitise column name
                if (preg_match('/^[a-zA-Z_][a-zA-Z0-9_]*$/', $column)) {
                    $q->andWhere("t.$column = :f_val_$column")
                        ->setParameter("f_val_$column", $value);
                }
            }
        }
    }

    /**
     * @param QueryBuilder $q
     * @param string $sortField
     * @param string $sortDirection
     * @return QueryBuilder - $q for chaining
     */
    protected function addOrderByClause($q, $sortField, $sortDirection = 'ASC')
    {
        if (!empty(trim($sortField))) {
            // Default sort direction to ASC if $sortDirection is empty or not equal to 'DESC'
            if (empty($sortDirection) || strtoupper($sortDirection) != 'DESC') {
                $sortDirection = 'ASC';
            }
            $q->orderBy($sortField, $sortDirection);
        }

        return $q;
    }

    public function getRulesByObject($objectRef = '', $objectID, $sortField = '', $sortDirection = 'ASC')
    {
        $query = $this->getSearchQuery([
            'sort_field' => $sortField,
            'sort_direction' => $sortDirection,
            'filters' => [
                'object_ref' => $objectRef,
                'object_id' => $objectID
            ]
        ]);
        $data = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

        $rre = new RecurrenceRuleEntity($this->getState());
        foreach ($data as $k => $rule) {
            $rre->load($rule['id']);
            $data[$k]['r_string'] = $rre->getTimezoneAdjustedString();
            $data[$k]['_expiry'] = $rre->getExpiryString();
            $data[$k]['_duration'] = $rre->calculateEventDuration();
            $broadcastPeriod = $rre->calculateBroadcastPeriodParameters($rule['start_date'], $rule['end_date']);
            $data[$k]['release_date'] = $broadcastPeriod['release_date'];
            $data[$k]['start_time'] = $broadcastPeriod['start_time'];
            $data[$k]['end_time'] = $broadcastPeriod['end_time'];
            $data[$k]['end_time_add_days'] = $broadcastPeriod['end_time_add_days'];
            
        }

        return $data;
    }

    public function getRulesByTable($objectRef = '')
    {
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->tableName, 'rr')
            ->andWhere('rr.lote_deleted IS NULL')
            ->andWhere('rr.object_ref = :obj_ref')
            ->setParameter('obj_ref', $objectRef);

        $query = $q->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);

        return $result;
    }
}