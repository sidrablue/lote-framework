<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */


namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;

/**
 * Class RecurrenceRuleEvent
 * @package SidraBlue\Lote\Model
 */
class RecurrenceRuleEvent extends Base
{
    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__recurrence_rule_event';

    /**
     * Retrieve all recurrences for a particular rule
     *
     * @param $ruleId
     * @return Array
     */
    public function getRuleRecurrences($ruleId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("r.*")
            ->from($this->getTableName(), 'r')
            ->where("r.rule_id = :ruleId")
            ->andWhere("r.lote_deleted is null")
            ->setParameter("ruleId", $ruleId);
        $recurrences = $this->getListByQuery($q);

        return $recurrences;
    }

    public function getNextRecurrence($object_id, $startDate, $object_ref = "sb_event"){
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("r.*")
            ->from($this->getTableName(), 'r')
            ->leftJoin("r", "sb__recurrence_rule", 'rr', 'rr.id = r.rule_id')
            ->where("rr.object_ref = :object_ref")
            ->setParameter("object_ref", $object_ref)
            ->andWhere("rr.object_id = :object_id")
            ->setParameter("object_id", $object_id)
            ->andWhere("r.lote_deleted is null and rr.lote_deleted is null")
            ->andWhere("r.start_date >= :startDate")
            ->setParameter("startDate", $startDate)
            ->orderBy("r.start_date", "asc");
        $query = $q->execute();
        $result = $query->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }

}