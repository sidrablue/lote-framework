<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */


namespace SidraBlue\Lote\Model\Region;


use SidraBlue\Lote\Object\Model\Base;

/**
 * Class Country
 * @package SidraBlue\Lote\Model\Region
 */
class Country extends Base
{
    protected $tableName = 'sb__region_country';
}