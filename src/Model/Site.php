<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\State\Web;
use SidraBlue\Lote\Object\Model\Base;
use SidraBlue\Util\Arrays;

/**
 * Model class for Files
 */
class Site extends Base
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__site';

    /**
     * @param array $sites - An array of sites
     * */
    protected $sites = [];


    /**
     * @var bool $sitesLoaded - true if the sites were loaded, to track the result of none being found
     * */
    protected $sitesLoaded = false;


    /**
     * Get the theme for a specified URL, and returning a default if a default is specified an a theme URL is not found
     * @access public
     * @param string $url
     * @param string $default
     * @return string
     * */
    public function getTheme($url = '', $default = '')
    {
        return $this->getDomainParam($url, 'theme', $default);
    }

    /**
     * Get the skin for a specified URL, and returning a default if a default is specified an a theme URL is not found
     * @access public
     * @param string $url
     * @param string $default
     * @return string
     * */
    public function getSkin($url = '', $default = '')
    {
        return $this->getDomainParam($url, 'skin', $default);
    }

    /**
     * Get the URL for a specific site reference
     * @access public
     * @param string $reference
     * @return string
     * */
    public function getUrl($reference)
    {
        $result = $_SERVER['HTTP_HOST'];
        $sites = $this->getAllSites();
        foreach($sites as $s) {
            if($s['reference']==$reference) {
                $result = 'http://'.$s['_domains'][0]['url'];
                break;
            }
        }
        return $result;
    }

    /**
     * Get the skin for a specified URL, and returning a default if a default is specified an a theme URL is not found
     * @access public
     * @return string
     * */
    public function getCurrentReference()
    {
        return $this->getDomainParam($this->getHost(), 'reference', 'default');
    }

    /**
     * Get the current site ID
     * @access public
     * @return string
     * */
    public function getCurrentSiteId()
    {
        return $this->getDomainParam($this->getHost(), 'id', '1');
    }

    /**
     * Get the current host
     * @access private
     * @return string
     * */
    private function getHost()
    {
        $url = '';
        if(php_sapi_name()=='cli') {
            $sites = array_reverse($this->getAllSites());
            $firstSite = array_pop($sites);
            if ($firstSite && isset($firstSite['_domains']) && count($firstSite['_domains']) > 0) {
                $domain = $firstSite['_domains'][0];
                if (isset($domain['url'])) {
                    $url = $domain['url'];
                }
            }
        }
        else {
            $url = $_SERVER['HTTP_HOST'];
        }
        return $url;
    }

    /**
     * Get the current site URL
     * @access public
     * @return string
     * */
    public function getCurrentSiteUrl()
    {
        return $this->getDomainParam($this->getHost(), 'url', '1');
    }

    /**
     * Get the current site URL
     * @access public
     * @return string
     * */
    public function getCurrentSiteScheme($default = 'http://')
    {
        $result = $default;
        $domainParam = $this->getDomainParam($this->getHost(), 'ssl_enabled', '0');
        if($domainParam =='1') {
            $result = "https://";
        }
        elseif($domainParam==='0') {
            $result = "http://";
        }
        return $result;
    }

    /**
     * Get a parameter from a domain if it is found for a site.
     * This is a helper function to centralise code for the getSkin and getTheme functions
     * @see getSkin
     * @see getTheme
     * @access private
     * @param string $url - if empty, it will use the URL of the request
     * @param string $paramName - the name of the parameter to return
     * @param boolean|string $default
     * @return string
     * */
    private function getDomainParam($url = '', $paramName, $default = '') {
        if(empty($url)){
            if($this->getState() instanceof Web) {
                $url = $this->getState()->getRequest()->getHost();
            }
            else {
                $url = $this->getHost();
            }
        }
        if(empty($default) && $default !== '0') {
            $default = $this->getState()->getSettings()->get('website.'.$paramName, 'default');
        }
        $result = $default;
        $this->loadSites();
        foreach($this->sites as $v){
            foreach($v['_domains'] as $d) {
                if($d['url'] == $url) {
                    if(isset($d[$paramName])) {
                        $result = $d[$paramName];
                    }
                    elseif(isset($v[$paramName])) {
                        $result = $v[$paramName];
                    }
                    break 2;
                }
            }
        }
        return $result;
    }

    /**
     *
     * */
    protected function loadSites($force = false)
    {
        if($force) {
            $this->sitesLoaded = false;
            $this->sites = [];
        }
        if(!$this->sitesLoaded) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('s.*, d.url as domain_url, d.enabled as domain_enabled, d.ssl_force as domain_force_ssl')
                ->from('sb__site', 's')
                ->rightJoin('s', 'sb__site_domain' , 'd', 'd.site_id = s.id')
                ->where('s.enabled = "1"')
                ->andWhere('d.enabled = "1"');
            $s = $q->execute();
            foreach($s->fetchAll(\PDO::FETCH_ASSOC) as $row) {
                $domainData = Arrays::getValuesByPrefix($row, 'domain_');
                if(!isset($this->sites[$row['id']])) {
                    $this->sites[$row['id']] = Arrays::removeValuesByKeyPrefix($row, 'domain_');
                }
                $this->sites[$row['id']]['_domains'][] = Arrays::removeKeyPrefix($domainData, 'domain_');
            }
            $this->sitesLoaded = true;
        }
    }

    /**
     * Check if the current install has more than one site
     * @access public
     * @return boolean
     */
    public function isMultiSite()
    {
        $this->loadSites();
        return count($this->sites) > 1;
    }

    /**
     * Get the current sites defined for this install
     * @access public
     * @return array
     */
    public function getAllSites()
    {
        $this->loadSites();
        return $this->sites;
    }

    /**
     * Get the reference for the default site, which is defined as the only valid site
     * @access public
     * @return string
     */
    public function getDefaultReference()
    {
        $result = 'default';
        $this->loadSites();
        if(count($this->sites) > 0) {
            $site = array_values($this->sites)[0];
            $result = $site['reference'];
        }
        return $result;
    }

    /**
     * Get the id for the default site, which is defined as the only valid site
     * @access public
     * @return int
     */
    public function getDefaultId()
    {
        $result = 1;
        $this->loadSites();
        if(count($this->sites) > 0) {
            $site = array_values($this->sites)[0];
            $result = $site['id'];
        }
        return $result;
    }
}
