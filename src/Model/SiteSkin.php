<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;

/**
 * Model class for Files
 */
class SiteSkin extends Base
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__site_skin';

    public function getActiveSkins()
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("s.*")
            ->from("sb__site_skin", "s")
            ->where("lote_deleted is null")
            ->addOrderBy("id", "asc");
        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }
    public function getDefaultSkin(){
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("s.*")
            ->from("sb__site_skin", "s")
            ->where("lote_deleted is null")
            ->andWhere("is_default = 1")
            ->addOrderBy("sort_order", "asc")
            ->orderBy("name");
        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }
    public function getSkinsIndexed()
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("s.*")
            ->from("sb__site_skin", "s")
            ->where("lote_deleted is null")
            ->orderBy("is_default","desc")
            ->addOrderBy("sort_order", "asc")
            ->addOrderBy("id", "asc");

        $result = $q->execute();
        $skins = [];
        while($row = $result->fetch(\PDO::FETCH_ASSOC)) {
            $skins[$row['id']] = $row;
        }

        return $skins;
    }
}
