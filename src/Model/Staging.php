<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model;

use Doctrine\DBAL\Connection;
use SidraBlue\Lote\Object\Model\Base;
use SidraBlue\Util\Arrays;

/**
 * Model class for Staging
 */
class Staging extends Base
{

    /**
     * @var String $tableName - the name of the table for this model
     * */
    protected $tableName = 'sb__staging';


    public function checkStagingExist($objectId, $objectRef)
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('p.*')
            ->from($this->tableName, 'p')
            ->andWhere('p.object_id = :object_id')
            ->setParameter('object_id', $objectId)
            ->andWhere('p.object_ref = :object_ref')
            ->setParameter('object_ref', $objectRef)
            ->andWhere('p.status = :status')
            ->setParameter('status', 'preview');
        $q->andWhere("p.lote_deleted is null");
        $query = $q->execute();
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $result = $row;
        }
        return $result;

    }

}
