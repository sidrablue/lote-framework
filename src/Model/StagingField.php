<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model;

use Doctrine\DBAL\Connection;
use SidraBlue\Lote\Object\Model\Base;
use SidraBlue\Util\Arrays;

/**
 * Model class for Staging Field
 */
class StagingField extends Base
{

    /**
     * @var String $tableName - the name of the table for this model
     * */
    protected $tableName = 'sb__staging_field';

}
