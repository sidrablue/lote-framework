<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Entity\Stat as StatEntity;
use SidraBlue\Lote\Entity\StatValue as StatValueEntity;
use SidraBlue\Lote\Object\Model\Base;

class Stat extends Base
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__stat';

    /**
     * @param array $cache
     * */
    protected $cache = [];

    /**
     * Get or create a stat entry. This will not create a value.
     * @access public
     * @param string $accountRef
     * @param string|null $objectRef
     * @param string|null $appRef
     * @param string|null $objectId
     * @param string|null $reference
     * @param string $name
     * @return StatEntity
     */
    public function addOrCreateStat(
        $accountRef,
        $appRef = null,
        $objectRef = null,
        $objectId = null,
        $reference = null,
        $name = ''
    ) {
        $e = new StatEntity($this->getState());
        if (!$e->loadByFields([
            'account_ref' => $accountRef,
            'object_ref' => $objectRef,
            'app_ref' => $appRef,
            'object_id' => $objectId,
            'reference' => $reference
        ])
        ) {
            $e->account_ref = $accountRef;
            $e->app_ref = $appRef;
            $e->object_ref = $objectRef;
            $e->object_id = $objectId;
            $e->reference = $reference;
            $e->name = $name;
            $e->save();
        }
        return $e;
    }

    /**
     * Add a stat value
     * @access public
     * @param $statId
     * @param $statValue
     * @param $rangeType
     * @param $dateTime
     * @return int
     */
    public function addStatValue($statId, $statValue, $rangeType, $dateTime)
    {
        $e = new StatValueEntity($this->getState());
        if (!$e->loadByFields(['stat_id' => $statId, 'stat_range_type' => $rangeType, 'datetime' => $dateTime])) {
            $e->stat_id = $statId;
            $e->stat_value = $statValue;
            $e->stat_range_type = $rangeType;
            $e->datetime = $dateTime;
        } elseif ($e->stat_value != $statValue) {
            $e->stat_value = $statValue;
        }
        return $e->save();
    }

    public function getLatestValue($accountRef,
        $appRef = null,
        $objectRef = null,
        $objectId = null,
        $reference = null,
        $default = null) {

    }

    private function makeKey($accountRef, $appRef = null, $objectRef = null, $objectId = null, $reference = null)
    {
        return "key_[account='{$accountRef}']_[app_ref='{$appRef}']_[object_ref='{$objectRef}']_[object_id='{$objectId}'][reference='{$reference}']";
    }

    private function isCached($accountRef, $appRef = null, $objectRef = null, $objectId = null, $reference = null) {
        return array_key_exists($this->makeKey($accountRef, $appRef, $objectRef, $objectId, $reference), $this->cache);
    }

    /**
     * load the latest stat value for a combination of value parameters
     * @access public
     * @param $accountRef
     * @param string|null $appRef
     * @param string|null $objectRef
     * @param int|null $objectId
     * @param string|null $reference
     * @param mixed|null $default
     * @return bool|null|string
     */
    public function loadLatestStatValue(
        $accountRef,
        $appRef = null,
        $objectRef = null,
        $objectId = null,
        $reference = null,
        $default = null
    ) {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("v.stat_value")
            ->from("sb__stat", "s")
            ->leftJoin("s", "sb__stat_value", "v", "v.stat_id = s.id")
            ->where("s.account_ref = :account_ref")
            ->setParameter("account_ref", $accountRef);
        if ($appRef) {
            $q->andWhere("s.app_ref = :app_ref")
                ->setParameter("app_ref", $appRef);
        } else {
            $q->andWhere("s.app_ref is null");
        }

        if ($objectRef) {
            $q->andWhere("s.object_ref = :object_ref")
                ->setParameter("object_ref", $objectRef);
        } else {
            $q->andWhere("s.object_ref is null");
        }

        if ($objectId) {
            $q->andWhere("s.object_id = :object_id")
                ->setParameter("object_id", $objectId);
        } else {
            $q->andWhere("s.object_id is null");
        }

        if ($reference) {
            $q->andWhere("s.reference = :reference")
                ->setParameter("reference", $reference);
        } else {
            $q->andWhere("s.reference is null");
        }

        $q->andWhere("s.lote_deleted is null")
            ->andWhere("v.lote_deleted is null")
            ->orderBy("v.datetime", "desc")
            ->setMaxResults(1);
        $result = $q->execute()->fetchColumn();
        if(!$result && $default) {
            $result = $default;
        }
        $this->cache[$this->makeKey($accountRef, $appRef, $objectRef, $objectId, $reference)] = $result;
        return $result;
    }

    /**
     * Get the latest stat value for a combination of value parameters
     * @access public
     * @param $accountRef
     * @param string|null $appRef
     * @param string|null $objectRef
     * @param int|null $objectId
     * @param string|null $reference
     * @param mixed|null $default
     * @return bool|null|string
     */
    public function getLatestStatValue(
        $accountRef,
        $appRef = null,
        $objectRef = null,
        $objectId = null,
        $reference = null,
        $default = null
    ) {
        if($this->isCached($accountRef, $appRef, $objectRef, $objectId, $reference)) {
            $result = $this->cache[$this->makeKey($accountRef, $appRef, $objectRef, $objectId, $reference)];
        }
        else {
            $result = $this->loadLatestStatValue($accountRef, $appRef, $objectRef, $objectId, $reference, $default);
        }
        return $result;
    }

    public function preloadLatestObjectValues($accountRef = null, $appRef = null, $objectRef = null, $reference = null) {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("s.app_ref, s.account_ref, s.object_ref, s.object_id, s.reference, s.latest_value")
            ->from("sb__stat", "s");
        if ($accountRef) {
            $q->andWhere("s.account_ref = :account_ref")
                ->setParameter("account_ref", $accountRef);
        }
        if ($appRef) {
            $q->andWhere("s.app_ref = :app_ref")
                ->setParameter("app_ref", $appRef);
        }
        if ($objectRef) {
            $q->andWhere("s.object_ref = :object_ref")
                ->setParameter("object_ref", $objectRef);
        }
        if ($reference) {
            $q->andWhere("s.reference = :reference")
                ->setParameter("reference", $reference);
        }
        $s = $q->execute();
        while($row = $s->fetch(\PDO::FETCH_ASSOC)) {
            $this->cache[$this->makeKey($row['account_ref'], $row['app_ref'], $row['object_ref'], $row['object_id'], $row['reference'])] = $row['latest_value'];
        }
    }

}
