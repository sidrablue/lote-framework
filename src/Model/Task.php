<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use Doctrine\DBAL\Connection;
use SidraBlue\Lote\Entity\TaskOwner as TaskOwnerEntity;
use SidraBlue\Lote\Entity\User as UserEntity;
use SidraBlue\Lote\Object\Model\Base;
use SidraBlue\Util\Paging;

/**
 * Model class for a file
 *
 * @package Lote\System\Crm\Model
 */
class Task extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__task';

    /**
     * Retrieves the tasks owned by the user or assigned to the user split into each of it's time groups
     *
     * @param array $paging - Paging information for past, future and current tasks
     * @return array
     */
    public function getMyTasks($paging)
    {
        $userId = $this->getState()->getUser()->id;

        //Finds which groups the owner is a member of
        $gm = new Group($this->getState());
        $userGroups = $gm->getUserGroups($userId);
        $groupIds = [];
        foreach($userGroups as $group) {
            $groupIds[] = $group['id'];
        }

        //Query to find each of the tasks that the user owns or is assigned to
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q  ->select('distinct t.*')
            ->from('sb__task', 't')
            ->leftJoin('t','sb__task_assignee', 'a', 'a.task_id = t.id')
            ->leftJoin('t', 'sb__task_owner', 'o', 'o.task_id = t.id')
            ->where('o.user_id = (:userId)')
            ->orWhere('a.type_ref = "user" AND (a.type_id = (:userId))')
            ->orWhere('(a.type_ref = "group") AND (a.type_id IN (:groupIds))')
            ->setParameter('userId', $userId)
            ->setParameter('groupIds', $groupIds, Connection::PARAM_INT_ARRAY)
            ->andWhere('t.lote_deleted IS NULL')
            ->andWhere('a.lote_deleted IS NULL')
            ->andWhere('o.lote_deleted IS NULL');

        $tasks = $this->getListByQuery($q);


        $currentTaskList = [];
        $currentTaskList['total'] = 0;
        $currentLimit = $paging['current']['page'] * $paging['current']['perPage'];
        $futureTaskList = [];
        $futureTaskList['total'] = 0;
        $futureLimit = $paging['future']['page'] * $paging['future']['perPage'];
        $pastTaskList = [];
        $pastTaskList['total'] = 0;
        $pastLimit = $paging['past']['page'] * $paging['past']['perPage'];

        $today = new \DateTime('now');
        $zero = new \DateTime('0000-00-00');

        if(!is_null($tasks) && isset($tasks['rows'])) {
            foreach($tasks['rows'] as $task) {
                $completed = new \DateTime($task['date_completed'], new \DateTimeZone('Australia/Perth'));
                $enabled = new \DateTime($task['date_enabled'], new \DateTimeZone('Australia/Perth'));
                $task['owner']['none'] = true;

                //Find the owner and add a field in the task
                $toe = new TaskOwnerEntity($this->getState());
                if($toe->loadByField('task_id', $task['id'])) {
                    $ue = new UserEntity($this->getState());
                    if($ue->load($toe->user_id)) {
                        $task['owner'] = $ue->getViewData();
                        $task['owner']['none'] = false;
                    }
                }

                //Assign the task to either past, present or future task lists
                if ($completed > $zero) {
                    if($pastTaskList['total'] < $pastLimit) {
                        $pastTaskList['rows'][] = $task;
                    }
                    $pastTaskList['total']++;
                } elseif ($enabled < $today) {
                    if($currentTaskList['total'] < $currentLimit) {
                        $currentTaskList['rows'][] = $task;
                    }
                    $currentTaskList['total']++;
                } else {
                    if($futureTaskList['total'] < $futureLimit) {
                        $futureTaskList['rows'][] = $task;
                    }
                    $futureTaskList['total']++;
                }
            }
        }

        //Find the paging information individually for each task group
        $pastTaskList['paging'] = Paging::getPaging(
            $pastTaskList['total'],
            $this->getOffset($paging['past']['page'],$paging['past']['perPage']),
            $paging['past']['perPage']);

        $futureTaskList['paging'] = Paging::getPaging(
            $futureTaskList['total'],
            $this->getOffset($paging['future']['page'],$paging['future']['perPage']),
            $paging['future']['perPage']);

        $currentTaskList['paging'] = Paging::getPaging(
            $currentTaskList['total'],
            $this->getOffset($paging['current']['page'],$paging['current']['perPage']),
            $paging['current']['perPage']);

        //Returns each list as a seperate element in a single array
        $allTasks['past'] = $pastTaskList;
        $allTasks['future'] = $futureTaskList;
        $allTasks['current'] = $currentTaskList;

        return $allTasks;

    }

    /**
     * Sorts the rows in an array by the due date
     *
     * @access private
     * @param array $a - First array of Task Entity values
     * @param array $b - First array of Task Entity values
     * @return int
     */
    function sortByDueDate($a, $b)
    {
        $result = 0;
        if($a['date_required'] > $b['date_required']) {
            $result = 1;
        }
        elseif($a['date_required'] < $b['date_required']) {
            $result = -1;
        }
        return $result;
    }

    public function checkTaskCompleted($referenceId, $userId, $reference)
    {
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->tableName, 'u')
            ->leftJoin('u', "sb__task_complete", 'l', 'u.id = l.task_id')
            ->andWhere('u.reference_id = :reference_id')
            ->andWhere('u.reference = :reference')
            ->andWhere('l.user_id = :user_id');

        $q->setParameter('reference_id', $referenceId)
            ->setParameter('reference', $reference)
            ->setParameter('user_id', $userId);

        $query = $q->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);

        $return = false;
        if(count($result) > 0){
            $return = true;
        }

        return $return;
    }

    public function checkTaskCreated($referenceId, $userId, $reference)
    {
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->tableName, 'u')
            ->leftJoin('u', "sb__task_link", 'l', 'u.id = l.task_id')
            ->andWhere('u.reference_id = :reference_id')
            ->andWhere('u.reference = :reference')
            ->andWhere('l.link_object_id = :user_id');

        $q->setParameter('reference_id', $referenceId)
            ->setParameter('reference', $reference)
            ->setParameter('user_id', $userId);

        $query = $q->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);

        return count($result);
    }

    public function getTaskByReference($referenceId, $userId, $reference)
    {
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->tableName, 'u')
            ->leftJoin('u', "sb__task_link", 'l', 'u.id = l.task_id')
            ->andWhere('u.reference_id = :reference_id')
            ->andWhere('u.reference = :reference')
            ->andWhere('l.link_object_id = :user_id');

        $q->setParameter('reference_id', $referenceId)
            ->setParameter('reference', $reference)
            ->setParameter('user_id', $userId);

        $query = $q->execute();
        $result = $query->fetch(\PDO::FETCH_ASSOC);

        return $result;
    }

    public function getUserTasks($userId, $userGroups)
    {
        $result = [];
        $query = $this->getState()->getReadDb()->query(
            "select task_id from sb__task_assignee where (assignee_id = $userId and assignee = 'user') or (assignee_id in (".implode($userGroups,',').") and assignee = 'group')"
        )->fetchAll(
                \PDO::FETCH_ASSOC
            );

        foreach ($query as $value) {
            $result[] = $value['task_id'];
        }
        return array_unique($result);
    }


    public function getMyTasksList($myTaskIds)
    {
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->tableName, 'u')
            ->leftJoin('u', "sb__task_link", 'l', 'u.id = l.task_id')
            ->andWhere('u.id in (:my_tasks)')
            ->andWhere('u.completed = :completed')
            ->setParameter('my_tasks', $myTaskIds, Connection::PARAM_INT_ARRAY)
            ->setParameter('completed', 0);

        $query = $q->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);

        return $result;
    }


    public function getOwners()
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('distinct t.lote_author_id as id,u.first_name,u.last_name')
            ->from($this->tableName, 't')
            ->leftJoin('t', "sb__user", 'u', 't.lote_author_id = u.id')
            ->andWhere('t.reference = :reference')
            ->setParameter('reference', 'sb_projectmanager_project')
            ->andWhere('t.parent_id = :parent_id')
            ->setParameter('parent_id', 0);
        $q->andWhere('t.lote_deleted is null');
        $q->andWhere('u.lote_deleted is null');
        $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function getProjectTaskProjectedHrs($projectId, $fromDate='', $toDate='')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*')
            ->from($this->tableName, 't')
            ->andWhere('t.reference = :reference')
            ->setParameter('reference', 'sb_projectmanager_project')
            /*->andWhere('t.parent_id = :parent_id')
            ->setParameter('parent_id', 0)*/
            ->andWhere('t.reference_id = :reference_id')
            ->setParameter('reference_id', $projectId);
        if (!empty($fromDate)) {
            $q->andWhere('t.lote_created  >= :date_from')
                ->setParameter('date_from', $fromDate);
        }

        if (!empty($toDate)) {
            $q->andWhere('t.lote_created  <= :date_to')
                ->setParameter('date_to', $toDate);
        }
        $q->andWhere('t.lote_deleted is null');
        $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        $ids = [];
        if (!empty($result)) {
            foreach ($result as $value) {
                $ids[] = $value['hours_projected'];
            }
        }
        return $ids;
    }


    public function getProjectTaskIds($projectId, $fromDate='', $toDate='')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*')
            ->from($this->tableName, 't')
            ->andWhere('t.reference = :reference')
            ->setParameter('reference', 'sb_projectmanager_project')
            /*->andWhere('t.parent_id = :parent_id')
            ->setParameter('parent_id', 0)*/
            ->andWhere('t.reference_id = :reference_id')
            ->setParameter('reference_id', $projectId);
        if (!empty($fromDate)) {
            $q->andWhere('t.lote_created  >= :date_from')
                ->setParameter('date_from', $fromDate);
        }

        if (!empty($toDate)) {
            $q->andWhere('t.lote_created  <= :date_to')
                ->setParameter('date_to', $toDate);
        }
        $q->andWhere('t.lote_deleted is null');
        $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        $ids = [];
        if (!empty($result)) {
            foreach ($result as $value) {
                $ids[] = $value['id'];
            }
        }
        return $ids;
    }


    public function getTaskCountByStatus($projectId,$statusId,$taskIds=[],$fromDate='',$toDate='')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('count(*) as count')
            ->from($this->tableName, 't')
            ->andWhere('t.reference = :reference')
            ->setParameter('reference', 'sb_projectmanager_project')
            ->andWhere('t.reference_id = :reference_id')
            ->setParameter('reference_id', $projectId)
            ->andWhere('t.status_id = :status_id')
            ->setParameter('status_id', $statusId);
        if (!empty($fromDate)) {
            $q->andWhere('t.lote_created  >= :date_from')
                ->setParameter('date_from', $fromDate);
        }
        if (!empty($toDate)) {
            $q->andWhere('t.lote_created  <= :date_to')
                ->setParameter('date_to', $toDate);
        }

        $q->andWhere('t.lote_deleted is null');
        if(!empty($taskIds)){
            $q->andWhere('t.id in (:task_ids)');
            $q->setParameter('task_ids', $taskIds, Connection::PARAM_STR_ARRAY);
        }
        $result = $q->execute()->fetch(\PDO::FETCH_ASSOC);
        $count = false;
        if(isset($result['count'])){
            $count = $result['count'];
        }
        return $count;
    }

    /**
     * Get project task query
     *
     * @access public
     * @param $projectId
     * @param string $fromDate
     * @param string $toDate
     * @return mixed
     */
    public function getProjectTaskQuery($projectId, $fromDate = '', $toDate = '')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("t.*, s.name as task_status")
            ->from('sb__task', 't')
            ->leftJoin('t', 'sb__task_status', 's', 't.status_id = s.id')
            /*->andWhere('t.parent_id = :parent_id')
            ->setParameter('parent_id', 0)*/
            ->andWhere('t.reference_id = :reference_id')
            ->setParameter('reference_id', $projectId)
            ->andWhere("t.lote_deleted is null")
            ->andWhere("s.lote_deleted is null");

        if (!empty($fromDate)) {
            $q->andWhere('t.lote_created  >= :date_from')
                ->setParameter('date_from', $fromDate);
        }

        if (!empty($toDate)) {
            $q->andWhere('t.lote_created  <= :date_to')
                ->setParameter('date_to', $toDate);
        }

        return $q;
    }
    public function checkTaskStatus($objectId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('count(t.id)')
            ->from($this->tableName, 't')
            ->where('t.status_id = 2')
            ->andWhere('id = :objectId')
            ->setParameter('objectId', $objectId)
            ->andWhere('t.lote_deleted is null');
        $s = $q->execute();
        return $s->fetchColumn() > 0;
    }

    public function getTaskLinkData($objectRef ,$objectId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*')
            ->from($objectRef, 't')
            ->andWhere('t.id = :id')
            ->setParameter('id', $objectId)
            ->andWhere('t.lote_deleted is null');
        $result = $q->execute()->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }


    public function getSearchQuery($params = [])
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("t.*,s.name as task_status,ty.name as task_type,tp.name as task_priority,p.name as project_name,p.acronym")
            ->from('sb__task', 't')
            ->leftJoin('t', 'sb__task_status', 's', 't.status_id = s.id')
            ->leftJoin('t', 'sb__task_type', 'ty', 't.type_id = ty.id')
            ->leftJoin('t', 'sb__task_priority', 'tp', 't.priority_id = tp.id')
            ->leftJoin('t', 'sb_projectmanager_project', 'p', 'p.id = t.reference_id and t.reference="sb_projectmanager_project"')
            ->andWhere('t.lote_deleted is null')
            ->andWhere('p.lote_deleted is null');
        if (isset($params['projects']) && !empty($params['projects'])) {
            $q->andWhere('t.reference = :reference')
                ->setParameter('reference', 'sb_projectmanager_project')
                ->andWhere('t.reference_id in (:reference_ids)')
                ->setParameter('reference_ids', $params['projects'], Connection::PARAM_STR_ARRAY);
        }
        if (isset($params['type']) && !empty($params['type']) && is_array($params['type'])) {
            $q->andWhere('t.type_id in (:type_ids)')
                ->setParameter('type_ids', $params['type'], Connection::PARAM_STR_ARRAY);
        } elseif (isset($params['type']) && !empty($params['type'])) {
            $q->andWhere('t.type_id = :type_id')
                ->setParameter('type_id', $params['type']);
        }
        if (isset($params['status']) && !empty($params['status'])) {
            $q->andWhere('t.status_id = :status_id')
                ->setParameter('status_id', $params['status']);
        }

        if(isset($params['date_enabled']) && !empty($params['date_enabled'])){
            $q->andWhere('t.date_enabled >=:date_enabled')
                ->setParameter('date_enabled',$params['date_enabled']);
        }
        if(isset($params['date_required']) && !empty($params['date_required'])){
            $q->andWhere('t.date_enabled <=:date_required')
                ->setParameter('date_required',$params['date_required']);
        }

        if (isset($params['priority']) && !empty($params['priority'])) {
            $q->andWhere('t.priority_id = :priority_id')
                ->setParameter('priority_id', $params['priority']);
        }

        if (isset($params['owner']) && !empty($params['owner'])) {
            $q->andWhere('t.lote_author_id = :owner_id')
                ->setParameter('owner_id', $params['owner']);
        }
        if(isset($params['phrase']) && $params['phrase']) {
            $q = $this->getPhraseSplitQuery($q, $params['phrase'], ['t.name','t.description']);
        }
        if (isset($params['task_ids']) && $params['task_ids']) {
            $q->andWhere('t.id in (:task_ids)')
                ->setParameter('task_ids', $params['task_ids'], Connection::PARAM_STR_ARRAY);
        }
        if (isset($params['sort_field']) && !empty(trim($params['sort_field']))) {
            if (empty($params['sort_direction']) || strtoupper($params['sort_direction']) != 'DESC') {
                $params['sort_direction'] = 'ASC';
            }
            $q->orderBy($params['sort_field'], $params['sort_direction']);
        }else{
            if (isset($params['sort_task_ids']) && $params['sort_task_ids']) {
                $sortTaskIds = implode(',', $params['sort_task_ids']);
                $q->orderBy('field(t.id,' . $sortTaskIds . ')', 'asc');
            }
        }
        return $q;
    }
} 