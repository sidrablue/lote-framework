<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;
use SidraBlue\Lote\Model\TaskLogFile as TaskLogFileModel;

/**
 * Model class for a file
 *
 * @package Lote\System\Crm\Model
 */
class TaskActivityLog extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__task_activity_log';

    public function getSearchQuery($taskId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('ta.*,u.first_name,u.last_name,u.middle_name')
            ->from($this->tableName, 'ta')
            ->leftJoin('ta', 'sb__user', 'u', 'u.id = ta.user_id')
            ->andWhere('ta.lote_deleted is null')
            ->andWhere('ta.task_id =:task_id')
            ->setParameter('task_id', $taskId)
            ->andWhere('u.lote_deleted is null');
        return $q;
    }

    public function getTaskActivityListQuery()
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('ta.*,t.name,u.first_name,u.last_name')
            ->from($this->tableName, 'ta')
            ->leftJoin('ta', 'sb__task', 't', 't.id = ta.task_id')
            ->leftJoin('ta', 'sb__user', 'u', 'u.id = ta.user_id')
            ->andWhere('ta.lote_deleted is null')
            ->andWhere('t.lote_deleted is null')
            ->andWhere('u.lote_deleted is null')
            ->orderBy("ta.id", "DESC");
        return $q;
    }

} 