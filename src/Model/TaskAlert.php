<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use Doctrine\DBAL\Connection;
use Lote\System\Crm\Entity\Notification as NotificationEntity;
use Lote\System\Crm\Entity\NotificationLink as NotificationLinkEntity;
use Lote\System\Crm\Model\Notification as NotificationModel;
use Lote\System\Crm\Model\NotificationLink as NotificationLinkModel;
use SidraBlue\Lote\Model\TaskAssignee as TaskAssigneeModel;
use SidraBlue\Lote\Object\Model\Base;

/**
 * Model class for a file
 *
 * @package Lote\System\Crm\Model
 */
class TaskAlert extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__task_alert';

    public function getTaskAlerts($taskId){
        $taskId = intval($taskId);
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->tableName, 'u')
            ->where('u.task_id = :task_id')
            ->andWhere('u.lote_deleted is null')
            ->setParameter('task_id',$taskId);
        $query = $q->execute();
        $result = [];
        while ($row = $query->fetchAll(\PDO::FETCH_ASSOC)) {
            foreach($row as $value){
                $result[] = $value;
            }
        }
        return $result;
    }

    /**
     * Creates a notification from a task alert
     *
     * @param array $alertValues - Alert form values
     * @return array
     */
    public function setNotification($alertValues, $alertId)
    {
        $ids = [];

        //Check whether the notification exists already
        //Note: a single alert can only have one notification
        $nm = new NotificationModel($this->getState());
        $notificationId = $nm->checkExists('alert', $alertId);

        //Create a notification from the alert
        $notificationValues = [];
        $newNotification = true;

        if($notificationId > 0){
            $notificationValues['id'] = $notificationId;
            $newNotification = false;
        }
        $notificationValues['object_ref'] = 'alert';
        $notificationValues['object_id'] = $alertId;
        $notificationValues['description'] = $alertValues['name'];
        $notificationValues['expiry'] = $alertValues['date_required'];
        $notificationValues['require_all'] = false;
        $notificationValues['completed'] = false;
        $ne = new NotificationEntity($this->getState());
        $ne->setData($notificationValues);
        $id = $ne->save();

        //Only regard assignees if it is a new notification, otherwise it is handled elsewhere.
        if($newNotification) {
            $tam = new TaskAssigneeModel($this->getState());
            $ids[$id] = $tam->setNotificationAssignees($alertValues['task_id'], $id);
        }
        return $ids;
    }

    public function getTaskAlertsQuery($taskIds)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*, "alert" as notification_type')
            ->from($this->tableName, 'u')
            ->where('u.task_id in (:task_id)')
            ->setParameter('task_id',$taskIds, Connection::PARAM_INT_ARRAY);
        return $q;
    }
} 