<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use Doctrine\DBAL\Connection;
use Lote\System\Crm\Model\Notification as NotificationModel;
use SidraBlue\Lote\Object\Model\Base;
use SidraBlue\Lote\Model\TaskAlert as TaskAlertModel;
use Lote\System\Crm\Entity\NotificationLink as NotificationLinkEntity;

/**
 * Model class for a file
 *
 * @package Lote\System\Crm\Model
 */
class TaskAssignee extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__task_assignee';

    public function getTaskAssignees($taskId){
        $clauses = [];

        $c = [];
        $c['field'] = 'task_id';
        $c['operator'] = 'equals';
        $c['value'] = intval($taskId);
        $c['data_type'] = \PDO::PARAM_INT;
        $clauses[] = $c;

        $result = $this->getList(1, $clauses);

        return $result;
    }

    /**
     * Creates notification links from task assignees
     *
     * @param int $taskId - Task ID
     * @param int $notificationId - Notification ID
     * @return array
     */
    public function setNotificationAssignees($taskId, $notificationId)
    {
        $ids = [];

        //Find the assignees linked to the alert
        $clauses = [];
        $c = [];
        $c['field'] = 'task_id';
        $c['operator'] = 'equals';
        $c['value'] = $taskId;
        $c['data_type'] = \PDO::PARAM_INT;
        $clauses[] = $c;
        $assignees = $this->getList(1, $clauses, [], 'and', 99999);

        //Create notification links from these assignees
        if($assignees['total'] > 0) {
            foreach($assignees['rows'] as $assignee) {
                $notificationLinkValues = [];
                $notificationLinkValues['notification_id'] = $notificationId;
                $notificationLinkValues['link_ref'] = $assignee['type_ref'];
                $notificationLinkValues['link_id'] = $assignee['type_id'];
                $notificationLinkValues['completed'] = false;

                $nle = new NotificationLinkEntity($this->getState());
                $nle->setData($notificationLinkValues);
                $ids[] = $nle->save();
            }
        }
        return $ids;
    }

    /**
     * Creates new notification links for alert notifications when a new assignee is attached to a task
     *
     * @param int $taskId - Task ID
     * @param string $linkRef - Assignee link type reference
     * @param int $linkId - Assignees link ID
     */
    public function addNotificationAssignee($taskId, $linkRef, $linkId)
    {
        $nm = new NotificationModel($this->getState());
        $talm = new TaskAlertModel($this->getState());

        //Find a list of alerts attached to the task
        $clauses = [];
        $c = [];
        $c['field'] = 'task_id';
        $c['operator'] = 'equals';
        $c['value'] = $taskId;
        $c['data_type'] = \PDO::PARAM_INT;
        $clauses[] = $c;

        $alerts = $talm->getList(1, $clauses, [], 'and', 99999);


        /**
         * Attach the new assignee to each notification connected to the task
         * Each alert should have exactly one notification attached to it.
         */
        if($alerts['total'] > 0) {
            foreach($alerts['rows'] as $alert) {

                $notificationId = $nm->checkExists('alert', $alert['id']);

                //Assignees have already been filtered by whether or not they exist
                $notificationLinkValues = [];
                $notificationLinkValues['notification_id'] = $notificationId;
                $notificationLinkValues['link_ref'] = $linkRef;
                $notificationLinkValues['link_id'] = $linkId;
                $notificationLinkValues['completed'] = false;

                $nle = new NotificationLinkEntity($this->getState());
                $nle->setData($notificationLinkValues);
                $nle->save();
            }
        }
    }

    /**
     * function used to check the user already linked in
     * @param  $taskId - Task ID
     * @param  $userId - user Id
     * @return boolean
     */
    public function checkLinkedUser($taskId = '', $userId = '')
    {
        $count = 0;
        $return = false;
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('count(*) as count')
            ->from($this->tableName, 't')
            ->andWhere('t.task_id = :task_id')
            ->setParameter('task_id', $taskId)
            ->andWhere('t.type_id = :type_id')
            ->setParameter('type_id', $userId)
            ->andWhere('t.type_ref = :type_ref')
            ->setParameter('type_ref', "sb__user")
            ->andWhere('t.lote_deleted is null');

        $query = $data->execute();
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $count = $row['count'];
        }

        if ($count == 0) {
            $return = true;
        }

        return $return;

    }

    /**
     * function used to fetch linked users
     * @param  $taskId - Task ID
     * @return array
     */
    public function getLinkedUsers($taskId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select("distinct ta.*,ta.type_id as user_id,u.first_name,u.last_name")
            ->from($this->tableName, 'ta')
            ->leftJoin('ta', 'sb__user', 'u', 'ta.type_id = u.id')
            ->andWhere('ta.task_id = :task_id')
            ->setParameter('task_id', $taskId)
            ->andWhere('ta.type_ref = :type_ref')
            ->setParameter('type_ref', "sb__user")
            ->andWhere('ta.lote_deleted is null')
            ->andWhere('u.lote_deleted is null');
        $query = $data->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        return $result;

    }


    /**
     * function used to fetch task details query
     * @param  $userId - User ID
     * @param  $recent - Order By
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    public function getTaskDetailsQuery($userId = '', $recent = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("t.*,p.name as project_name,ts.name as task_status")
            ->from($this->tableName, 'ta')
            ->leftJoin('ta', 'sb__task', 't', 't.id = ta.task_id')
            ->leftJoin('t', 'sb_projectmanager_project', 'p', 'p.id = t.reference_id')
            ->leftJoin('t', 'sb__task_status', 'ts', 'ts.id = t.status_id')
            ->where("ta.lote_deleted is null")
            ->andWhere('t.lote_deleted is null')
            ->andWhere('p.lote_deleted is null');
        if ($userId) {
            $q->andWhere('ta.type_id = :type_id')
                ->setParameter('type_id', $userId)
                ->andWhere('ta.type_ref = :type_ref')
                ->setParameter('type_ref', "sb__user");
        }
        if ($recent) {
            $q->addOrderBy("ta.lote_created", "desc");
        }
        return $q;
    }

    /**
     * function used to fetch user task ids
     * @param  $userId - User ID
     * @param  $distinct - Group by
     * @param  $projectId - Project ID
     * @return array
     */
    public function getUserTaskIds($userId = '', $distinct = false, $projectId = '')
    {

        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("ta.task_id,ta.type_id as user_id,u.first_name,u.last_name")
            ->from($this->tableName, 'ta')
            ->leftJoin('ta', 'sb__user', 'u', 'ta.type_id = u.id')
            ->leftJoin('ta', 'sb__task', 't', 't.id = ta.task_id')
            ->andWhere('u.lote_deleted is null')
            ->andWhere('t.lote_deleted is null')
            ->andWhere('ta.lote_deleted is null');
        if ($userId) {
            $q->andWhere('ta.type_id = :type_id')
                ->setParameter('type_id', $userId)
                ->andWhere('ta.type_ref = :type_ref')
                ->setParameter('type_ref', "sb__user");
        }
        if ($projectId) {
            $q->andWhere('t.reference_id = :reference_id')
                ->setParameter('reference_id', $projectId);
        }
        if ($distinct) {
            $q->groupBy('ta.type_id');
        }
        $query = $q->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function getTaskFromUsers($userIds)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("ta.*")
            ->from($this->tableName, 'ta')
            ->andWhere('ta.lote_deleted is null')
            ->andWhere('ta.type_ref = :type_ref')
            ->setParameter('type_ref', "sb__user")
            ->andWhere('ta.type_id in (:type_ids)')
            ->setParameter('type_ids', $userIds, Connection::PARAM_STR_ARRAY);
        $query = $q->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }


}