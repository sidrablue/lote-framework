<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;

/**
 * Model class for a Task Complete
 *
 * @package Lote\System\Crm\Model
 */
class TaskComplete extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__task_complete';

    /**
     * Check if task is completed by user
     *
     * @param int $taskId
     * @param int $userId
     * @access public
     * @return boolean $result
     */

    public function getTaskCompleteStatus($taskId, $userId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('count(*) as count')
            ->from($this->tableName, 'o')
            ->where('o.task_id = :task_id')
            ->setParameter('task_id', $taskId)
            ->andWhere('o.user_id = :user_id')
            ->setParameter('user_id', $userId)
            ->andWhere('o.lote_deleted is null');

        $query = $q->execute();
        $result = false;
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            if(!empty($row['count']) && $row['count'] > 0){
                $result = true;
            }
        }
        return $result;

    }
    public function getStepCompleteStatus($taskId, $stepId, $userId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('count(*) as count')
            ->from($this->tableName, 'o')
            ->where('o.task_id = :task_id')
            ->setParameter('task_id', $taskId)
            ->andWhere('o.user_id = :user_id')
            ->setParameter('user_id', $userId)
            ->andWhere('o.step_id = :step_id')
            ->setParameter('step_id', $stepId)
            ->andWhere('o.lote_deleted is null');

        $query = $q->execute();
        $result = false;
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            if(!empty($row['count']) && $row['count'] > 0){
                $result = true;
            }
        }
        return $result;

    }

}