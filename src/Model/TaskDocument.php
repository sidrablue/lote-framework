<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;

/**
 * Class TaskDocument
 * @package SidraBlue\Lote\Model
 */
class TaskDocument extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__task_document';

    /**
     * Get documents
     *
     * @param $taskId
     * @return array
     */
    public function getDocuments($taskId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('f.*,d.id as document_id')
            ->from($this->tableName, 'd')
            ->leftJoin('d', 'sb__file', 'f', 'd.file_id = f.id')
            ->andWhere('d.task_id = :task_id')
            ->setParameter('task_id', $taskId)
            ->andWhere("d.lote_deleted is null")
            ->orderBy("d.sort_order", "asc");
        $query = $data->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

} 