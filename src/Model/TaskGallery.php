<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;
/**
 * Class TaskGallery
 * @package SidraBlue\Lote\Model
 */
class TaskGallery extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__task_gallery';

    /**
     * Get task images
     *
     * @param $taskId
     * @return array
     */
    public function getTaskImages($taskId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('f.*,g.id as task_gallery_id')
            ->from($this->tableName, 'g')
            ->leftJoin('g', 'sb__file', 'f', 'g.file_id = f.id')
            ->andWhere('g.task_id = :task_id')
            ->setParameter('task_id', $taskId)
            ->andWhere("g.lote_deleted is null")
            ->orderBy("g.sort_order", "asc");
        $query = $data->
            execute();
        $result =
            $query->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

} 