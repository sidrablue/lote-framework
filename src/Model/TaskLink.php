<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use Lote\System\Crm\Entity\Notification as NotificationEntity;
use Lote\System\Crm\Entity\NotificationLink as NotificationLinkEntity;
use Lote\System\Crm\Model\Notification as NotificationModel;
use Lote\System\Crm\Model\NotificationLink as NotificationLinkModel;
use SidraBlue\Lote\Object\Model\Base;

/**
 * Model class for a file
 *
 * @package Lote\System\Crm\Model
 */
class TaskLink extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__task_link';
} 