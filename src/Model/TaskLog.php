<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;
use SidraBlue\Lote\Model\TaskLogFile as TaskLogFileModel;

/**
 * Model class for a file
 *
 * @package Lote\System\Crm\Model
 */
class TaskLog extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__task_log';

    public function getTaskLogs($taskId){

        $clauses = [];

        $c = [];
        $c['field'] = 'task_id';
        $c['operator'] = 'equals';
        $c['value'] = $taskId;
        $c['data_type'] = \PDO::PARAM_INT;
        $clauses[] = $c;

        $result = $this->getList(1, $clauses, [], 'and', 9999);

        return $result;
    }

    /**
     * Get all log files
     *
     * @param int $logId - The log ID
     * @return array
     */
    public function getLogFiles($logId)
    {
        $tlfm = new TaskLogFileModel($this->getState());
        $clauses = [];

        $c = [];
        $c['field'] = 'log_id';
        $c['operator'] = 'equals';
        $c['value'] = $logId;
        $c['data_type'] = \PDO::PARAM_INT;
        $clauses[] = $c;

        $result = $tlfm->getList(1, $clauses, [], 'and', 9999);

        return $result;
    }

} 