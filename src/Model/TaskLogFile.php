<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model;

use Doctrine\DBAL\Connection;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use SidraBlue\Lote\Object\Model\Base;
use SidraBlue\Lote\Entity\File as FileEntity;
use SidraBlue\Util\Arrays;

class TaskLogFile extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__task_log_file';

    /**
     * Delete a file
     *
     * @access public
     * @param string $path - the path of the file to delete
     * */
    public function deleteFile($path)
    {
        //TODO: Do we need to pass a $store here?
        $fs = $this->getFileSystem('');

        $fs->delete($path);
    }

    /**
     * Delete multiple files using their ID's
     *
     * @access public
     * @param array $fileIds - An array of file ID's to delete
     */
    public function bulkDelete($fileIds)
    {
        foreach ($fileIds as $fileId) {
            $this->delete($fileId);
        }
    }

    /**
     * Get the count of the number of lines is a specific file.
     *
     * Typical usage would be for CSV files
     *
     * @access public
     * @param int $fileId - the ID of the file
     * @return int
     *
     * */
    public function getLineCount($fileId)
    {
        $result = 0;
        if ($content = $this->getContent($fileId)) {
            $file = new \SplTempFileObject();
            $file->fwrite($content);
            foreach($file as $lineNumber => $content) {
                $result = ($lineNumber + 1);
            }
        }
        return $result;
    }

    /**
     * Download a file
     *
     * @access public
     * @param int $id - the ID of the file to download
     * */
    public function download($id)
    {
        $e = new FileEntity($this->getState());
        $e->load($id);
        if($e->id > 0) {
            $fs = $this->getFileSystem();
            $contents = $contents = $fs->read($e->reference);
            header('Pragma: public'); 	// required
            header('Expires: 0');		// no cache
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Cache-Control: private',false);
            header('Content-Type: ' . $fs->getMimetype($e->reference));
            header('Content-Disposition: attachment; filename="'.$e->filename.'"');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: '.$fs->getSize($e->reference));
            header('Connection: close');
            echo($contents);
            die;
        }
    }

    /**
     * Get the content of a file
     *
     * @param int $fileId - the ID of the file to get
     * @access public
     * @return string
     */
    public function getContent($fileId)
    {
        $content = '';
        $e = new FileEntity($this->getState());
        $e->load($fileId);
        $fs = $this->getFileSystem($fileId);
        if($fs->has($e->reference)) {
            $content = $fs->read($e->reference);
        }
        return $content;
    }

    /**
     * Get an array list of the data for one or more files by object id
     *
     * @access public
     * @param int $id - the ID of the object
     * @return array|false
     * */
    public function getListByObjectId($id)
    {
        $result = false;
        if($id > 0) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('o.*')->from($this->getTableName(), 'o')->where('o.object_id = :id')->setParameter('id', $id);
            $s = $q->execute();
            $result = $s->fetchAll(\PDO::FETCH_ASSOC);
        }
        return $result;
    }

    /**
     * Get the content of a file
     *
     * @param int $fileId - the ID of the file to get
     * @access public
     * @return string
     */
    public function getFileContent($fileId)
    {
        $content = '';
        $e = new FileEntity($this->getState());
        $e->load($fileId);
        $fs = $this->getFileSystem($fileId);
        if($fs->has($e->reference)) {
            $content = $fs->read($e->reference);
        }
        return $content;
    }

    /**
     * Save a file, and if it exists delete the previous file and replace it with the current one
     *
     * @access public
     * @param $tempPath - the temporary path of the file
     * @param $filename - the filename
     * @param $object - the object that the file belongs to
     * @param $objectId - the object id that the file belongs to
     * @param string $objectKey - an optional key
     * @param string $store - a preferred store for the file
     * @return bool|\SidraBlue\Lote\Entity\File
     */
    public function saveReplace($tempPath, $filename, $object, $objectId, $objectKey = '', $store = '')
    {
        if($item = $this->getFileByObject($object, $objectId, $objectKey)) {
            $fs = $this->getFileSystem();
            if($fs->has($item->reference)) {
                $fs->delete($item->reference);
            }
            $item->delete();
        }
        if($item) {
            $result = $this->update($item, $tempPath, $filename,  $store);
        }
        else {
            $result = $this->save($tempPath, $filename, $object, $objectId, $objectKey, $store);
        }
        return $result;
    }

    /**
     * Make a reference for the file, to use in saving the file.
     *
     * @param string $object - the reference of the object that this file is for
     * @param int $objectId - the ID of the object that the file is for
     * @param string $objectKey - the key of the object ID if one is required, such as a field name
     * @param string $name - the name of the file if it is known
     * @access public
     * @return FileEntity|false;
     * */
    public function getFile($object, $objectId, $objectKey = '', $name = '')
    {
        $reference = $this->makeReference($object, $objectId, $objectKey, $name);
        return $this->getEntityByReference($reference);
    }

    /**
     * Make a reference for the file, to use in saving the file.
     *
     * @param string $object - the reference of the object that this file is for
     * @param int $objectId - the ID of the object that the file is for
     * @param string $objectKey - the key of the object ID if one is required, such as a field name
     * @access public
     * @return FileEntity|false;
     * */
    public function getFileByObject($object, $objectId, $objectKey = '')
    {
        $result = false;
        $e = new FileEntity($this->getState());
        $e->loadByObjectParams($object, $objectId, $objectKey);
        if($e->id) {
            $result = $e;
        }
        return $result;
    }

    /**
     * Get a file entity object by its reference
     * @access public
     * @param string $reference - the reference of the entity
     * @return FileEntity|false
     * */
    public function getEntityByReference($reference)
    {
        $result = false;
        $e = new FileEntity($this->getState());
        $e->loadByReference($reference);
        if($e->id) {
            $result = $e;
        }
        return $result;
    }

    /**
     * Save a file to a store
     *
     * @param string $path - the temporary path of the file to save
     * @param string $name - the name of the file
     * @param string $object - the reference of the object that the file is for
     * @param int $objectId - the ID of the object in question
     * @param string $objectKey - the key for the object, if its for a specific field or part of an array
     * @param string $store - the reference of the store to save the file in, if one is explicitly required
     * @access protected
     * @return boolean|FileEntity
     * @todo - implement a proper store
     * */
    protected function save($path, $name, $object, $objectId, $objectKey, $store)
    {
        $result = false;
        $reference = $this->makeReference($object, $objectId, $objectKey, $name);
        $size = $this->saveToFileSystem($reference, $path, $store);
        if ($size > 0) {
            $e = new FileEntity($this->getState());
            $e->store = 'local';//$store;
            $e->object = $object;
            $e->object_id = $objectId;
            $e->object_key = $objectKey;
            $e->filename = $name;
            $e->extension = pathinfo($name, PATHINFO_EXTENSION);
            $e->mime_type = \SidraBlue\Util\File::getMimeType($path);
            $e->reference = $reference;
            $e->size = $size;
            if($data = $e->save()) {
                $result = $e;
            }
        }
        return $result;
    }

    /**
     * Save a file to a store
     *
     * @param FileEntity $fileEntity - the temporary path of the file to save
     * @param string $path - the temp path of the file
     * @param string $name - the name of the file
     * @param string $store - the reference of the store to save the file in, if one is explicitly required
     * @access protected
     * @return boolean|FileEntity
     * */
    protected function update($fileEntity, $path, $name, $store)
    {
        $result = false;
        $size = $this->saveToFileSystem($fileEntity->reference, $path, $store);
        if ($size > 0) {
            $fileEntity->store = 'local';//$store;
            $fileEntity->filename = $name;
            $fileEntity->extension = pathinfo($name, PATHINFO_EXTENSION);
            $fileEntity->mime_type = \SidraBlue\Util\File::getMimeType($path);
            $fileEntity->size = $size;
            if($fileEntity->save()) {
                $result = $fileEntity;
            }
        }
        return $result;
    }

    /**
     * Make a reference for the file, to use in saving the file.
     *
     * @param string $object - the reference of the object that this file is for
     * @param int $objectId - the ID of the object that the file is for
     * @param string $objectKey - the key of the object ID if one is required, such as a field name
     * @param string $name - the name of the file if it is known
     * @access private
     * @return string;
     * @todo - add in validation for the object and object ID
     * */
    private function makeReference($object, $objectId, $objectKey = '', $name = '')
    {
        $object = str_replace('.', '/', $object);
        $reference = "{$object}/{$objectId}";
        if($objectKey) {
            $reference .= '/'.$objectKey;
        }
        if($name) {
            $reference .= '/'.$name;
        }
        return $reference;
    }

    /**
     * Save a file to the filesystem in the store specified or in the default store as defined in the settings
     *
     * @param string $reference - the unique reference of the file
     * @param string $tempPath - the current temporary path of the file to save
     * @param string $store - the reference of the store to use, if an explicit one is to be used
     * @access private
     * @return int;
     * */
    private function saveToFileSystem($reference, $tempPath, $store = '')
    {
        $result = 0;
        $fs = $this->getFileSystem($store);
        if (file_exists($tempPath)) {
            $fileData = file_get_contents($tempPath);
            if($fs->write($reference, $fileData) > 0){
                $result = max(1, strlen($fileData));
            }
        }
        return $result;
    }

    /**
     * Get the file location to store local files in
     *
     * @access private
     * @param string $store
     * @return Filesystem
     * @todo - create a store factory
     * */
    private function getFileSystem($store = '')
    {
        if ($store) {
            //$store = 'Local';
        }
        $adapter = new Local($this->getLocalPath());
        $fs = new Filesystem($adapter);
        return $fs;
    }

    /**
     * Get the file location to store local files in
     *
     * @access private
     * @return string
     * @todo - move this to the factory when it is created
     * */
    private function getLocalPath()
    {
        return LOTE_ASSET_PATH . 'task_log';
    }

    /**
     * Get a list of files by their IDs
     *
     * @access public
     * @param array $fileIds - the ID's of the files that we want the data for
     * @param bool $indexById - true if the result is to be indexed by the file ID
     * @return array
     *  */
    public function getFilesById($fileIds, $indexById = false)
    {
        $result = [];
        if(is_array($fileIds) && count($fileIds) > 0) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('f.*')
                ->from('sb__task_log_file', 'f')
                ->where('f.id in (:file_ids)')
                ->setParameter('file_ids', $fileIds, Connection::PARAM_INT_ARRAY);
            $files = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
            foreach($files as $f) {
                if($indexById) {
                    $result[$f['id']] = $f;
                }
                else {
                    $result[] = $f;
                }
            }
        }
        return $result;
    }

    /**
     * Add information to an array of data about a file that it points to via a file_id field
     *
     * @access public
     * @param array $data
     * @param string $dataFieldName - the field name that the file information should be appended to
     * @return array
     * */
    public function addFileInfo($data, $dataFieldName = '_file')
    {
        if(is_array($data) && count($data) > 0) {
            $ids = Arrays::getFieldValuesFrom2dArray($data, 'file_id', true);
            if(is_array($ids) && count($ids) > 0) {
                $fileList = $this->getFilesById($ids, true);
                $cnt = count($data);
                for($i=0; $i<$cnt; $i++) {
                    if(isset($fileList[$data[$i]['file_id']])) {
                        $data[$i][$dataFieldName] = $fileList[$data[$i]['file_id']];
                    }
                }
            }
        }
        return $data;
    }

    /**
     * Returns all matches for a specified tag within the specified page
     *
     * @access public
     * @param string $tag - The tab being searched
     * @param array $paging - The page reference to return
     * @return array
     */
    public function getTagItems($tag, $paging = [])
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();

        $q->select('f.*')
            ->from($this->tableName, 'f')
            ->leftJoin('f', 'sb__tag_usage', 'u', 'u.object_id = f.id')
            ->leftJoin('u', 'sb__tag', 't', 't.id = u.tag_id')
            ->andWhere('t.value = :tag')
            ->andWhere('u.object_ref = :reference')
            ->setParameter('tag', $tag)
            ->setParameter('reference', 'media.file');

        if(!empty($paging)){
            $q->setFirstResult($paging['start'] - 1 >= 0 ? $paging['start'] - 1 : $paging['start']);
            $q->setMaxResults($paging['limit']);
        }
        $q->addOrderBy('lote_created', 'desc');

        $query = $q->execute();
        while ($row = $query->fetchAll(\PDO::FETCH_ASSOC)) {
            $result = $row;
        }
        return $result;
    }

    /**
     * Delete entry in file table and crm table
     *
     * @access public
     * @param int $id file ID
     * @param bool $strongDelete - Whether or not to delete the table object completely
     */
    public function delete($id, $strongDelete = false)
    {
        parent::delete($id, $strongDelete);
        $this->getFileByObject('task_log', $id)->delete($strongDelete);
    }
}