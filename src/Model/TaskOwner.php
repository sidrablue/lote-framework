<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;

/**
 * Model class for a file
 *
 * @package Lote\System\Crm\Model
 */
class TaskOwner extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__task_owner';

    /**
     * Check if task is participated by user
     *
     * @param int $taskId
     * @param int $userId
     * @access public
     * @return boolean $result
     */

    public function getSelfOwnershipStatus($taskId, $userId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('count(*) as count')
            ->from($this->tableName, 'o')
            ->where('o.task_id = :task_id')
            ->setParameter('task_id', $taskId)
            ->andWhere('o.user_id = :user_id')
            ->setParameter('user_id', $userId)
            ->andWhere('o.lote_deleted is null');

        $query = $q->execute();
        $result = false;
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            if(!empty($row['count']) && $row['count'] > 0){
                $result = true;
            }
        }
        return $result;

    }

    /**
     * Check if task is participated by other user
     *
     * @param int $taskId
     * @param int $userId
     * @access public
     * @return boolean $result
     */

    public function getOtherOwnershipStatus($taskId, $userId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('count(*) as count')
            ->from($this->tableName, 'o')
            ->where('o.task_id = :task_id')
            ->setParameter('task_id', $taskId)
            ->andWhere('o.lote_deleted is null')
            ->andWhere('o.user_id != :user_id')
            ->setParameter('user_id', $userId);

        $query = $q->execute();
        $result = false;
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            if(!empty($row['count']) && $row['count'] > 0){
                $result = true;
            }
        }
        return $result;

    }

    /**
     * Get list of tasks participated by user
     *
     * @param int $userId
     * @access public
     * @return array $result
     */

    public function getUserTasks($userId){

        $c = [];
        $c['field'] = 'user_id';
        $c['operator'] = 'equals';
        $c['value'] = $userId;
        $c['data_type'] = \PDO::PARAM_STR;
        $clauses[] = $c;

        $clauseResult = $this->getList(1, $clauses, ['lote_created' => 'asc'], 'and', 1000);

        $result = null;
        if($clauseResult['total'] > 0) {
            $result = $clauseResult['rows'];
        }

        return $result;
    }

    /**
     * Get owner associated with a task
     *
     * @access public
     * @param $taskId
     * @return array
     */
    public function getTaskOwner($taskId)
    {
        //Create clause
        $c = [];
        $c['field'] = 'task_id';
        $c['operator'] = 'equals';
        $c['value'] = $taskId;
        $c['data_type'] = \PDO::PARAM_STR;
        $clauses[] = $c;

        $c = [];
        $c['field'] = 'is_primary';
        $c['operator'] = 'equals';
        $c['value'] = 1;
        $c['data_type'] = \PDO::PARAM_INT;
        $clauses[] = $c;

        //execute clause
        $clauseResult = $this->getList(1, $clauses, ['id' => 'desc'], 'and');

        if(isset($clauseResult)) {
            if(isset($clauseResult['rows'])){
                $clauseResult = $clauseResult['rows'][0];
            }
            else{
                $clauseResult = null;
            }
        }
        else {
            $clauseResult = null;
        }

        return $clauseResult;
    }

    /**
     * Get list of tasks participated by user
     *
     * @param int $userId
     * @access public
     * @return array $result
     */

    public function getUserTasksQuery($userId){

        $q = $this->getReadDb()->createQueryBuilder();
       $q->select('*, "task" as notification_type')
            ->from($this->tableName, 'o')
            ->where('o.user_id = :user_id')
            ->setParameter('user_id', $userId);

        return $q;
    }
}