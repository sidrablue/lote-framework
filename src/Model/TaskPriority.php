<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;

class TaskPriority extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__task_priority';
}