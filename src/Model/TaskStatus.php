<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;

class TaskStatus extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__task_status';

    public function getTaskStatusList()
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*,t.id as task_status_id')
            ->from($this->getTableName(), 't')
            ->andWhere("t.lote_deleted is null");
        $query = $q->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
}