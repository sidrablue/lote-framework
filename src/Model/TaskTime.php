<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;

/**
 * Model class for a Task Time
 *
 * @package Lote\System\Pm\Model
 */
class TaskTime extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__task_time';

    public function getTaskTimeData($taskId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select("s.task_id,sum(s.week) as weekSum,sum(s.day) as daySum, sum(s.hour) as hourSum, sum(s.minute) as minuteSum" )
            ->from($this->tableName, 's')
            ->andWhere('s.task_id = :task_id')
            ->setParameter('task_id', $taskId)
            ->andWhere('s.lote_deleted is null');
        $query = $data->execute();
        $result= $query->fetch(\PDO::FETCH_ASSOC);
        return $result;

    }

    /**
     * @param string $phrase
     * @param int $taskId
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    public function getSearchQuery($taskId, $phrase = '')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("tt.*,u.first_name,u.last_name")
            ->from($this->getTableName(), 'tt')
            ->leftJoin('tt', 'sb__user', 'u', 'u.id = tt.user_id')
            ->andWhere("tt.lote_deleted is null")
            ->andWhere("u.lote_deleted is null")
            ->andWhere('tt.task_id = :task_id')
            ->setParameter('task_id', $taskId);
        if ($phrase) {
            $q->andWhere('tt.description like :phrase')
                ->setParameter('phrase', "%" . $phrase . "%");
        }
        return $q;
    }
}