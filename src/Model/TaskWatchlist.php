<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;

/**
 * Model class for a Task Watchlist
 *
 * @package Lote\System\Crm\Model
 */
class TaskWatchlist extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__task_watchlist';


    public function checkLinkedUser($taskId = '', $userId = '')
    {
        $count = 0;
        $return = false;
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('count(*) as count')
            ->from($this->tableName, 't')
            ->andWhere('t.task_id = :task_id')
            ->setParameter('task_id', $taskId)
            ->andWhere('t.user_id = :user_id')
            ->setParameter('user_id', $userId)
            ->andWhere('t.lote_deleted is null');

        $query = $data->execute();
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $count = $row['count'];
        }

        if ($count == 0) {
            $return = true;
        }

        return $return;

    }

    public function getLinkedUsers($taskId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select("distinct s.*,u.first_name,u.last_name")
            ->from($this->tableName, 's')
            ->leftJoin('s', 'sb__user', 'u', 's.user_id = u.id')
            ->andWhere('s.task_id = :task_id')
            ->setParameter('task_id', $taskId)
            ->andWhere('s.lote_deleted is null')
            ->andWhere('u.lote_deleted is null');
        $query = $data->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        return $result;

    }

    public function getUserTaskIds($userId='',$distinct=false,$projectId='')
    {

        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("tw.task_id,tw.user_id,u.first_name,u.last_name")
            ->from($this->tableName, 'tw')
            ->leftJoin('tw', 'sb__user', 'u', 'tw.user_id = u.id')
            ->leftJoin('tw', 'sb__task', 't', 't.id = tw.task_id')
            ->Where("tw.lote_deleted is null")
            ->andWhere('u.lote_deleted is null')
            ->andWhere('t.lote_deleted is null')
            ->andWhere('tw.lote_deleted is null');
        if($userId){
            $q->andWhere('tw.user_id = :id')
                ->setParameter('id', $userId);
        }
        if($projectId){
            $q->andWhere('t.reference_id = :reference_id')
                ->setParameter('reference_id', $projectId);
        }
        if($distinct){
            $q->groupBy('tw.user_id');
        }
        $query = $q->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function getTaskDetailsQuery($userId='', $recent = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("t.*,p.name as project_name,ts.name as task_status")
            ->from($this->tableName, 'tw')
            ->leftJoin('tw', 'sb__task', 't', 't.id = tw.task_id')
            ->leftJoin('t', 'sb_projectmanager_project', 'p', 'p.id = t.reference_id')
            ->leftJoin('t', 'sb__task_status', 'ts', 'ts.id = t.status_id')
            ->Where("tw.lote_deleted is null")
            ->andWhere('t.lote_deleted is null')
            ->andWhere('p.lote_deleted is null');
        if($userId){
            $q->andWhere('tw.user_id = :id')
                ->setParameter('id', $userId);
        }
        if($recent){
            $q->addOrderBy("tw.lote_created", "desc");
        }
        return $q;
    }

}