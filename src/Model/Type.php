<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model;

use Doctrine\DBAL\Connection;
use SidraBlue\Lote\Object\Model\Base;
use SidraBlue\Util\Arrays;

/**
 * Model class for Type
 */
class Type extends Base
{

    /**
     * @var String $tableName - the name of the table for this model
     * */
    protected $tableName = 'sb__type';

    /**
     * Get all of the types that a object ID is in
     * @param int $objectId
     * @param string $objectRef
     * @param string $indexBy
     * @return array|false
     * */
    public function getObjectTypes($objectId, $objectRef, $indexBy = '')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*')->from('sb__type', 't')
            ->leftJoin('t', $objectRef.'_type', 'ot', 'ot.type_id = t.id')
            ->where('ot.object_id = :object_id AND t.object_ref = :object_ref')
            ->setParameter('object_id', $objectId)
            ->setParameter('object_ref', $objectRef)
            ->andWhere("t.lote_deleted is null")
            ->andWhere("ot.lote_deleted is null");
        $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        if(is_array($result) && $indexBy) {
            $tmp = [];
            foreach ($result as $v) {
                $tmp[$v[$indexBy]] = $v;
            }
            $result = $tmp;
        }
        return $result;
    }

    /**
     * Get the set of types by IDs
     * @access public
     * @param array $typeIds
     * @param string $indexBy
     * @return array
     * */
    public function getTypesListById($typeIds, $indexBy = '')
    {
        $result = [];
        if(is_array($typeIds)) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('t.*')
                ->from('sb__type', 't')
                ->where('t.id in (:type_ids)')
                ->setParameter('type_ids', $typeIds, Connection::PARAM_INT_ARRAY)
                ->andWhere("t.lote_deleted is null");
            $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
            if(is_array($result) && $indexBy) {
                $tmp = [];
                foreach ($result as $v) {
                    $tmp[$v[$indexBy]] = $v;
                }
                $result = $tmp;
            }
        }

        return $result;
    }

    /**
     * Get the types by object ref
     * @param string $objectRef
     * @return array
     */
    public function getTypesList($objectRef, $indexBy = '')
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('t.*')
            ->from($this->getTableName(), 't')
            ->where('t.object_ref = :object_ref')
            ->setParameter('object_ref', $objectRef)
            ->andWhere('t.lote_deleted is null')
            ->addOrderBy('name', 'asc');

        $result = $data->execute()->fetchAll(\PDO::FETCH_ASSOC);
        if(is_array($result) && $indexBy) {
            $tmp = [];
            foreach ($result as $v) {
                $tmp[$v[$indexBy]] = $v;
            }
            $result = $tmp;
        }

        return $result;
    }

    /**
     * Ensure that a specific object belongs to a set of provided types
     * @param int $objectId
     * @param string $objectRef
     * @param array $typeIds
     * */
    public function insertObjectTypes($objectId, $objectRef, $typeIds)
    {
        if(is_numeric($objectId) && is_array($typeIds) && count($typeIds) > 0) {
            $objectTypes = Arrays::indexArrayByField($this->getObjectTypes($objectId, $objectRef), 'id');
            foreach ($typeIds as $tid) {
                if (!isset($objectTypes[$tid])) {
                    $data = [
                        'object_id' => $objectId,
                        'type_id' => $tid,
                    ];
                    $this->getWriteDb()->insert($objectRef . '_type', $data);
                }
            }
        }
    }

    /**
     * Remove Types from object
     * @param int $objectId
     * @param string $objectRef
     * @param array $typeIds
     */
    public function removeObjectTypes($objectId, $objectRef, $typeIds)
    {
        if (is_numeric($objectId) && is_array($typeIds) && count($typeIds) > 0) {
            foreach($typeIds as $typeId) {
                $params = [
                    "object_id" => $objectId,
                    "type_id" => $typeId
                ];
                $types = [
                    "object_id" => \PDO::PARAM_INT,
                    "type_id" => \PDO::PARAM_INT
                ];
                $this->getWriteDb()->delete($objectRef . "_type", $params, $types);
            }
        }
    }

    /**
     * Retrieve all object linked to a selected type
     *
     * @access public
     * @param int $typeId - Type ID
     * @param string $objectRef - object ref
     * @return array
     */
    public function getObjectsInTypeByRef($typeId, $objectRef)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("o.*")
            ->from($objectRef . '_type', 'ot')
            ->leftJoin('ot', $objectRef, 'o', 'ot.object_id = o.id')
            ->leftJoin('ot', 'sb__type', 't', 'ot.type_id = t.id')
            ->where('ot.type_id = :type_id')
            ->setParameter('type_id', $typeId)
            ->andWhere("ot.lote_deleted is null")
            ->andWhere("o.lote_deleted is null")
            ->andWhere("t.lote_deleted is null")
            ->andWhere("o.id > 0");
        $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);

        return $result;
    }

    /**
     * Get type id by reference
     *
     * @param $reference
     * @return int|null
     */
    public function getTypeIdByReference($reference)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*')
            ->from('sb__type', 't')
            ->where('t.reference = :reference')
            ->setParameter('reference', $reference)
            ->andWhere('t.lote_deleted is null');

        $result = $q->execute()->fetch(\PDO::FETCH_ASSOC);
        return !empty($result['id']) ? $result['id'] : null;
    }
}
