<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use Doctrine\DBAL\Connection;
use Lote\Module\Page\Entity\Item;
use SidraBlue\Lote\Object\Model\Base;
use SidraBlue\Util\Arrays;
use SidraBlue\Lote\Entity\Url as UrlEntity;

/**
 * Class for management of URLs in the system
 * */
class Url extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__url';

    /**
     * Attempt to match a URL in the database
     * @access public
     * @param string $url
     * @param int $siteId - defaults to 0 to imply using the current site ID.
     * @return false|array
     * */
    public function match($url, $siteId = 0)
    {
        $url = ltrim($url, '/');
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('*')
            ->from($this->getTableName(), 'u')
            ->where('(u.uri = :uri or u.uri = :uri2)')
            ->andWhere('u.lote_deleted is null')
            ->setParameter('uri', $url)
            ->setParameter('uri2', '/'.$url);
        if ($siteId == 0) {
            $siteId = $this->getState()->getSites()->getCurrentSiteId();
        }
        if ($siteId) {
            $q->andWhere('(u.site_id = :site_id or u.site_id is null or u.site_id = 0)')
                ->setParameter('site_id', $this->getState()->getSites()->getCurrentSiteId())
                ->addOrderBy("u.site_id", "desc");
        }
        $query = $data->execute();
        $result = $query->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }

    /**
     * Check if a URL is taken, or if it is not assigned to the specified object ID and reference
     * @access public
     * @param $url
     * @param int $siteId
     * @param int $objectId
     * @param string $objectReference
     * @param bool $checkStatic
     * @return boolean
     */
    public function urlIsTaken($url, $siteId, $objectId, $objectReference, $includeDeleted=false, $checkStatic = true)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('count(*)')
            ->from('sb__url', 'u')
            ->where('u.uri = :uri')
            ->setParameter('uri', $url)
            ->andWhere('(object_ref <> :object_ref or object_id <> :object_id)')
            ->setParameter('object_ref', $objectReference)
            ->setParameter('object_id', $objectId);
        if ($siteId) {
            $data->andWhere('site_id = :site_id')->setParameter('site_id', $siteId);
        }
        if (!$includeDeleted) {
            $q->andWhere('lote_deleted is null');
        }
        $result = $q->execute()->fetchColumn();
        $hasStatic = $checkStatic && $this->getState()->hasStaticMatch($url);

        $taken = ($hasStatic || $result > 0);
        return $taken;
    }

    /**
     * Get the urls for a list of results based on an object reference
     * @param array $data
     * @param string $objectRef
     * @return array
     * */
    public function addUrlsToData($data, $objectRef)
    {
        $ids = Arrays::getFieldValuesFrom2dArray($data, 'id', true);
        if(count($ids) > 0) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select("u.uri, u.object_id")
                ->from("sb__url", 'u')
                ->where("object_id in (:object_id)")
                ->setParameter("object_id", $ids, Connection::PARAM_INT_ARRAY)
                ->andWhere("object_ref = :object_ref")
                ->setParameter("object_ref", $objectRef)
                ->andWhere("lote_deleted is null");
            $matches = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
            $matches = Arrays::indexArrayByField($matches, 'object_id');
            foreach($data as $k=>$v) {
                if(isset($matches[$v['id']])) {
                    $data[$k]['_url'] = $matches[$v['id']]['uri'];
                }
            }
        }
        return $data;
    }

    /**
     * Get the URL for an object
     * @param int $objectId
     * @param string $objectRef
     * @return string|false
     * */
    public function getUrl($objectId, $objectRef)
    {
        $result = false;
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("u.uri")
            ->from("sb__url", 'u')
            ->where("object_id = :object_id")
            ->setParameter("object_id", $objectId)
            ->andWhere("object_ref = :object_ref")
            ->setParameter("object_ref", $objectRef)
            ->andWhere("lote_deleted is null");
        $data = $q->execute()->fetch(\PDO::FETCH_ASSOC);
        if($data) {
            $result = $data['uri'];
        }
        return $result;
    }

    /**
     * Create or update a URL entity by object id and ref
     * Delete if updating with empty URI.
     * @param $uri
     * @param $object_id
     * @param $object_ref
     * @param $siteId
     */
    public function updateUriByObject($uri, $object_id, $object_ref, $siteId = 0)
    {

        $result = false;
        $e = new UrlEntity($this->getState());
        $e->loadByObject($object_id, $object_ref);
        // If URI not empty, Create or update; else don't add to DB - delete existing entry
        if (!empty(trim($uri))) {
            //Don't validate unique URL if object is page.item and webpage has type = link
            $bypassStaticMatch = false;
            if($object_ref == "page.item"){
                $p = new Item($this->getState());
                if($p->load($object_id)) {
                    if ($p->lote_page_type == "cms_page_link" ) {
                        if($p->lote_link_type == "cms_link_external" || $p->lote_link_type == "cms_link_page") {
                            $bypassStaticMatch = true;
                        }
                    }
                }
            }
            //Don't create url if it exists statically already
            if($bypassStaticMatch || !$this->getState()->hasStaticMatch($uri) || $uri == $this->getState()->getSettings()->get("event.url.calendar_prefix", "/calendar") ) {
                $e->object_id = $object_id;
                $e->object_ref = $object_ref;
                $e->uri = $uri;
                $e->site_id = $siteId;
                $result = $e->save();
            }
        } elseif ($e->id) {
            $e->delete();
        }

        return $result;
    }

}
