<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;
use SidraBlue\Util\Arrays;
use SidraBlue\Util\Time;

/**
 * Class for management of URLs in the system
 * */
class UrlPassword extends Base
{

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName = 'sb__url_password';

    /**
     * Get password
     *
     * @access public
     * @param $urlId
     * @param bool $checkActive
     * @return array
     */
    public function getPassword($urlId, $checkActive = true)
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select("s.*")
            ->from('sb__url_password', 's')
            ->leftJoin('s', 'sb__url', 'o', 's.url_id = o.id')
            ->andWhere('s.url_id=:url_id')
            ->setParameter('url_id', $urlId)
            ->andWhere('s.lote_deleted is null');
        if($checkActive) {
            $q->setParameter('active', '1')->andWhere('s.lote_deleted is null');
        }
        $query = $data->execute();
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $result[] = $row;
        }
        return $result;
    }

    /**
     * Get passwords for an object
     *
     * @access public
     * @param string $objectRef
     * @param int $objectId
     * @param bool $checkDates
     * @param bool $checkActive
     * @return array
     */
    public function getPasswordsByObject($objectRef, $objectId, $checkDates = false, $checkActive = true)
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select("s.*")
            ->from('sb__url_password', 's')
            ->where('s.object_ref = :object_ref')
            ->setParameter('object_ref', $objectRef)
            ->andWhere('s.object_id = :object_id')
            ->setParameter('object_id', $objectId)
            ->andWhere('s.lote_deleted is null');
        if($checkActive) {
            $q->andWhere('s.active = :active')->setParameter('active', '1');
        }
        if($checkDates) {
            $q->andWhere("(start_date is null or start_date = '0000-00-00' or start_date <= :current_date)")
                ->andWhere("(end_date is null or end_Date='0000-00-00' or end_date >= current_date)")
                ->setParameter("current_date", Time::getUtcNow("Y-m-d"));
        }
        $query = $data->execute();
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $result[] = $row;
        }
        return $result;
    }

    public function getCurrentPasswords($urlId){
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select("s.password")
            ->from('sb__url_password', 's')
            ->andWhere('s.url_id=:url_id')
            ->setParameter('url_id', $urlId)
            ->andWhere('s.lote_deleted is null')
            ->andWhere("(start_date is null or start_date = '0000-00-00' or start_date <= :current_date)")
            ->andWhere("(end_date is null or end_Date='0000-00-00' or end_date >= current_date)")
            ->setParameter("current_date", Time::getUtcNow("Y-m-d"))
            ->andWhere("s.active = '1'");
        $query = $data->execute();
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $result[] = $row["password"];
        }
        return $result;
    }

    /**
     * Get password
     *
     * @access public
     * @param $uri
     * @return array
     */
    public function getPasswordsByUri($uri)
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("s.*")
            ->from('sb__url_password', 's')
            ->leftJoin('s', 'sb__url', 'o', 's.url_id = o.id')
            ->andWhere('o.uri = :uri')
            ->setParameter('uri', $uri)
            ->andWhere('s.lote_deleted is null')
            ->andWhere('o.lote_deleted is null');
        if($matches = $q->execute()->fetchAll(\PDO::FETCH_ASSOC)) {
            foreach($matches as $m) {
                $result[] = $m['password'];
            }
        }
        return $result;
    }

    /**
     * Get password
     *
     * @access public
     * @param $uri
     * @return array
     */
    public function getCurrentPasswordsByUri($uri)
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("s.*")
            ->from('sb__url_password', 's')
            ->leftJoin('s', 'sb__url', 'o', 's.url_id = o.id')
            ->andWhere('o.uri = :uri')
            ->setParameter('uri', $uri)
            ->andWhere('s.start_date <= :start_date')
            ->setParameter('start_date', date('Y-m-d'))
            ->andWhere('s.end_date >= :end_date')
            ->setParameter('end_date', date('Y-m-d'))
            ->andWhere('s.active = :active')
            ->setParameter('active', 1)
            ->andWhere('s.lote_deleted is null')
            ->andWhere('o.lote_deleted is null');
        if($matches = $q->execute()->fetchAll(\PDO::FETCH_ASSOC)) {
            foreach($matches as $m) {
                $result[] = $m['password'];
            }
        }
        return $result;
    }

    /**
     * Check if a given password is valid for a specific URI
     *
     * @access public
     * @param $uri
     * @param $password
     * @return array
     */
    public function isValidPassword($uri, $password)
    {
        $result = false;
        if ($passwords = $this->getPasswordsByUri($uri)) {
            $result = in_array($password, $passwords);
        }
        return $result;
    }

    /**
     * Check if a given password is valid for a specific URI by its object parameters
     *
     * @access public
     * @param $objectRef
     * @param $objectId
     * @param $password
     * @return array
     */
    public function isValidObjectPassword($objectRef, $objectId, $password)
    {
        $result = false;
        if ($passwords = $this->getPasswordsByObject($objectRef, $objectId, true)) {
            $passwords = Arrays::getFieldValuesFrom2dArray($passwords, 'password');
            $result = in_array($password, $passwords);
        }
        return $result;
    }

    /**
     * Check if a given password is already defined for a given combination of object Id and object Ref
     *
     * @access public
     * @param $password
     * @param $objectRef
     * @param $objectId
     * @param $objectId
     * @param $urlId
     * @return boolean
     */
    public function passwordExistsByObjectParams($password, $objectRef, $objectId, $urlId = 0)
    {
        $result = false;
        $p = new \SidraBlue\Lote\Entity\UrlPassword($this->getState());
        if($p->loadByFields(['password' => $password, 'object_id' => $objectId, 'object_ref' => $objectRef])) {
            if(!$urlId || $p->id != $urlId) {
                $result = true;
            }
        }
        return $result;
    }

}
