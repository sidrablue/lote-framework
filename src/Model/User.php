<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\PDOConnection;
use Doctrine\DBAL\Query\QueryBuilder;
use Lote\System\Admin\Model\Group;
use SidraBlue\Lote\Entity\Audit\ObjectLog;
use SidraBlue\Lote\Entity\User as UserEntity;
use SidraBlue\Lote\Model\User\Subscription;
use SidraBlue\Lote\Object\Model\CustomField;
use SidraBlue\Lote\Service\Email;
use SidraBlue\Lote\View\Transform\Html\Service;
use SidraBlue\Util\Arrays;
use SidraBlue\Util\Number\Mobile;
use SidraBlue\Util\Strings;
use SidraBlue\Lote\Event\Data;

/**
 * Model class for Users
 */
class User extends CustomField
{
    protected $tableName = 'sb__user';
    /**
     * @var string $objectReference
     * The reference of the object
     * */
    protected $objectReference = '_user';


    /**
     * Get all of a users groups
     * @param int $userId
     * @param string $tag - the "tag" of the groups that we are looking at
     * @todo - Make these tags work
     * @param string $kind
     * @return array of groups
     */
    public function getUserGroups($userId, $tag = '', $kind = '', $active = false)
    {
        $result = false;
        if ($userId) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('g.*')
                ->from('sb__group', 'g')
                ->leftJoin('g', 'sb__user_group', 'l', 'l.group_id = g.id')
                ->where('l.user_id = :user_id')
                ->setParameter('user_id', $userId)
                ->andWhere("g.lote_deleted is null")
                ->andWhere("l.lote_deleted is null")
                ->groupBy('g.id');
            if($active) {
                $q->andWhere('g.active = 1');
            }
            if (!empty($kind)) {
                $q->andWhere('g.kind = :kind')
                    ->setParameter('kind', $kind);
            }

            $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        }
        return $result;
    }
    public function getAppUsers($groupId, $page = 1){
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('u.*, d.device_id, d.type as device_type')
            ->from('sb__user', 'u')
            ->leftJoin('u', 'sb_edm_notification_device', 'd', 'd.id = u.notification_device_id')
            ->leftJoin('d', 'sb_edm_notification_device_group', 'g' ,'d.device_id =  g.device_id')
            ->where("g.group_id = :group_id")
            ->setParameter("group_id", $groupId);
        return $this->getListByQuery($q, $page, 10);
    }

    /**
     * Email exists
     *
     * @access public
     * @param $email
     * @param int $id
     * @return bool
     */
    public function emailExists($email, $id = 0)
    {
        return $this->valueExists('email', $email, $id) || $this->valueExists('email_secondary', $email, $id);
    }

    /**
     * Get all of a users group ID's
     * @param int $userId
     * @param string $tag - the "tag" of the groups that we are looking at
     * @param string $kind - the group kind
     * @return array of ID's
     */
    public function getUserGroupIds($userId, $tag = '', $kind = '')
    {
        $groups = $this->getUserGroups($userId, $tag, $kind);
        return Arrays::getFieldValuesFrom2dArray($groups, 'id');
    }

    /**
     * Insert a user into a specified set of groups
     * @param int $userId - the user id for which to insert the groups
     * @param array $userGroups - the user groups IDs to insert
     * @return void
     * */
    public function insertUserGroups($userId, $userGroups)
    {
        if ($userId && is_array($userGroups) && count($userGroups) > 0) {
            foreach ($userGroups as $v) {
                $this->insertGroup($userId, $v);
            }
        }
    }

    /**
     * Insert a user into a specified set of groups
     * @param int $userId - the user id for which to insert the groups
     * @param int $groupId - the user groups ID
     * @param ObjectLog $o
     */
    public function insertGroup($userId, $groupId, ObjectLog $o = null)
    {
        if (is_numeric($userId) && is_numeric($groupId)) {
            if (!$this->isInGroup($userId, $groupId)) {
                $this->getWriteDb()->insert('sb__user_group', ['user_id' => $userId, 'group_id' => $groupId]);
                $this->getState()->getAudit()->addLinkLog('sb__user', $userId, 'sb__group', $groupId, $o);
            }
        }
    }

    /**
     * Insert a user into a specified set of groups
     * @param int $userId - the user id for which to insert the groups
     * @param int $groupId - the user groups ID
     * @return int
     * */
    public function isInGroup($userId, $groupId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('count(*)')
            ->from("sb__user_group", 'g')
            ->where('g.user_id = :user_id')
            ->setParameter("user_id", $userId)
            ->andWhere("g.group_id = :group_id")
            ->setParameter("group_id", $groupId)
            ->andWhere("g.lote_deleted is null");
        return $q->execute()->fetchColumn() > 0;
    }

    /**
     * Delete a users membership of specific groups
     * @param int $userId - the user id for which to delete the groups
     * @param array $userGroups - the user groups IDs to delete
     * @return void
     * */
    public function removeUserGroups($userId, $userGroups)
    {
        if (is_numeric($userId) && is_array($userGroups) && count($userGroups) > 0) {
            foreach ($userGroups as $g) {
                $this->removeGroup($userId, $g);
            }
        }
    }

    /**
     * Delete a users membership of specific groups
     * @param int $userId - the user id for which to delete the groups
     * @param int $groupId - the user groups ID
     * @param ObjectLog|null $o
     * @return void
     * */
    public function removeGroup($userId, $groupId, ObjectLog $o = null)
    {
        if (is_numeric($groupId)) {
            if ($this->isInGroup($userId, $groupId)) {
                $this->getWriteDb()->delete('sb__user_group', ['user_id' => $userId, 'group_id' => $groupId]);
                $this->getState()->getAudit()->addUnlinkLog('sb__user', $userId, 'sb__group', $groupId, $o);
            }
        }
    }

    /**
     * Delete a user
     *
     * @param int $id - the user to delete
     * @param bool $strongDelete - Whether or not to delete the table object completely
     * @return void
     * @todo - implement a trigger on delete for other things to take note of...
     */
    public function delete($id, $strongDelete = false)
    {
        $this->getWriteDb()->update('sb__user_group', ['lote_deleted' => date('c')], ['user_id' => $id]);
        parent::delete($id, $strongDelete);
        $this->getState()->getEventManager()->dispatch('user.deleted', new Data(['user_id' => $id]));
    }

    /**
     * Get a users timezone
     * @param int $timezoneId
     * @return array of timezone details
     */
    public function getUserTimezone($timezoneId)
    {
        $result = false;
        if ($timezoneId) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('t.*')
                ->from('sb__timezone', 't')
                ->where('t.id = :id')
                ->setParameter('id', $timezoneId);
            $result = $q->execute()->fetch(\PDO::FETCH_ASSOC);
        }
        return $result;
    }

    /**
     * Get the data for a set of users defined by an Array of ID's
     * @param array $userIds - an array of user ids
     * @param bool|Array $fields - the fields to return
     * @return array
     * */
    public function getUsers(Array $userIds, $fields = false)
    {
        $result = [];
        if (count($userIds) > 0) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('u.*')->from('sb__user', 'u')->where('u.id in (:ids)')->setParameter(
                'ids',
                $userIds,
                Connection::PARAM_INT_ARRAY
            );
            $s = $q->execute();
            $rows = $s->fetchAll(\PDO::FETCH_ASSOC);
            if (is_array($rows)) {
                foreach ($rows as $u) {
                    if (!($fields)) {
                        $result[$u['id']] = $u;
                    } else {
                        $result[$u['id']] = Arrays::cleanArray($u, $fields, true);
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Get the user to use for a parent auto login
     * @todo - currently this is just the first admin, but we may want to change it
     * @return array
     * */
    public function getParentLoginUser()
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('u.*')->from('sb__user',
            'u')->where('u.is_admin = true')->andWhere("u.lote_deleted is null")->setMaxResults(1)->orderBy('id');
        $s = $q->execute();
        return $s->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * Get the user to use for a parent auto login
     * @todo - currently this is just the first admin, but we may want to change it
     * @return array
     * */
    public function getAdminUsers()
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('u.*')->from('sb__user', 'u')->where('u.is_admin = true');
        $s = $q->execute();
        return $s->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Add user data to an existing array that contains entries with a user_id field in each row
     * @param array $data - the data that contains the user_id fields
     * @return array
     * */
    public function addUserData($data)
    {
        $values = Arrays::getFieldValuesFrom2dArray($data, 'user_id', true);
        $users = $this->getUsers($values);
        $cnt = count($data);
        for ($i = 0; $i < $cnt; $i++) {
            $userId = isset($data[$i]['user_id']);
            if (!empty($userId) && isset($users[$userId])) {
                $data[$i]['_user'] = $users[$data[$i]['user_id']];
            }
        }
        return $data;
    }

    /**
     * Function to check if username exists
     * @access public
     * @param $username
     * @param int $id
     * @return bool
     */
    public function usernameExists($username, $id = 0, $source = "local")
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("count(*) as cnt")
            ->from("sb__user", "u")
            ->where("u.username = :username")
            ->andWhere('u.lote_deleted is null')
            ->setParameter("username", $username);
        if ($id) {
            $q->andWhere("id != :id")
                ->setParameter("id", $id);
        }
        if ($source) {
            $q->andWhere("source = :source")
                ->setParameter("source", $source);
        }
        return $q->execute()->fetchColumn() > 0;
    }

    /**
     * Check if this user has a valid username and password, and if not then create them
     * @param UserEntity $user
     * @return void
     * */
    public function setupLoginCredentials(UserEntity $user)
    {
        if ($user->id) {
            if (!$user->username || !$user->password || !$user->active) {
                if (!$user->username) {
                    $user->username = $this->generateUsername($user);
                }
                if (!$user->password) {
                    $user->password = password_hash(substr(uniqid(), 0, 8), PASSWORD_DEFAULT);
                }
                $user->active = true;
                $user->save();
            }
        }
    }

    /**
     * Generate a username for the current user
     * @access public
     * @param UserEntity $user
     * @return string
     * */
    public function generateUsername(UserEntity $user)
    {
        $result = '';
        if ($user->id) {
            $userNames = [];
            $userNames[] = strtolower(substr($user->id, 0, 8));
            $userNames[] = strtolower(substr($user->first_name, 0, 8));
            $userNames[] = strtolower(substr($user->last_name, 0, 8));
            $userNames[] = strtolower(substr($user->last_name . $user->first_name, 0, 8));
            $userNames[] = strtolower(substr($user->first_name . $user->last_name, 0, 8));
            $userNames[] = $user->email;
            $userNames[] = $user->email_secondary;
            foreach ($userNames as $v) {
                if ($v && !$this->usernameExists($v, $user->id)) {
                    $result = $v;
                    break;
                }
            }
            while (!$result) {
                $result = substr(uniqid(strtolower(substr($user->first_name, 0, 3))), 0, 7);
                if (!$this->usernameExists($result, $user->id)) {
                    break;
                }
            }
        }
        return $result;
    }

    /**
     * Get the unique sources for users within this system
     * @access public
     * @return array
     * */
    public function getSources()
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('distinct(u.source) as src')->from('sb__user', 'u')->execute();
        $s = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($s as $row) {
            $s = [];
            $s['id'] = $row['src'];
            if ($row['src'] == 'ldap') {
                $s['name'] = 'LDAP';
            } else {
                $s['name'] = ucfirst($row['src']);
            }
            $result[] = $s;
        }
        return $result;
    }

    /**
     * Send resubscribe email
     *
     * @access protected
     * @param int $userId
     * @return bool
     */
    public function sendResubscribeEmail($userId)
    {
        $userData = $this->get($userId);

        $u = new UserAutoLogin($this->getState());
        $redirect = $this->getState()->getRequest()->getSchemeAndHttpHost() . '/user/resubscribe?subscribe=1';

        $result = false;
        if (isset($userData['email'])) {
            $content = Service::renderView(
                $this->getState(),
                'user/resubscribe_email_content',
                [
                    'user_data' => $userData,
                    'login_link' => $u->createLoginLink($userId, $redirect)->getLinkUrl()
                ]
            );

            $e = new Email($this->getState());
            $senderEmail = $this->getState()->getSettings()->get('system.email.address');
            $senderName = $this->getState()->getSettings()->get('system.email.from') . ' Website';
            $subject = $this->getState()->getSettings()->get('system.name') . ' Resubscribe';
            $result = $e->sendEmail($subject, $content, $userData['email'], $senderEmail, $senderName);

            $userSubscriptionModel = new Subscription($this->getState());
            $userSubscriptionModel->setSubscription($userData['id'], 'email', $userData['email'], $userData['subscribed'], 'resubscribe_email', 'User resubscription email sent');
        }

        return $result;
    }

    /**
     * Send subscription confirmation email
     *
     * @param UserEntity $userEntity
     * @return void
     * */
    public function sendActivationEmail($userEntity)
    {
        $content = Service::renderView(
            $this->getState(),
            'user/email/activate',
            [
                'user' => $userEntity,
                'url' => $this->getState()->getRequest()->getSchemeAndHttpHost(),
                'state' => $this->getState()
            ]
        );
        $subject = 'Subscription Confirmation : ' . $this->getState()->getSettings()->get('system.name');
        $e = new Email($this->getState());
        $senderEmail = $this->getState()->getSettings()->get('email.smtp.username');
        $senderName = $this->getState()->getSettings()->get('system.name') . ' Website';
        $e->sendEmail($subject, $content, $userEntity->email, $senderEmail, $senderName);
    }

    /**
     * @param array $splitArr
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    public function getUserBuildQuery($splitArr = [])
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("concat(o.first_name,' ', o.last_name) as name,o.title as title,o.id as id,'1' as type, 'Contact' as object_name")
            ->from($this->tableName, 'o');
        if (!empty($splitArr)) {
            foreach ($splitArr as $key => $value) {
                $q->orWhere('o.username like :phrase_' . $key)
                    ->orWhere('o.first_name like :phrase_' . $key)
                    ->orWhere('o.middle_name like :phrase_' . $key)
                    ->orWhere('o.last_name like :phrase_' . $key)
                    ->orWhere('o.email like :phrase_' . $key);
            }
        }
        return $q;
    }

    public function execute($sql, $params)
    {
        return $result = $this->getState()->getReadDb()->executeQuery($sql, $params)->fetchAll(\PDO::FETCH_ASSOC);

    }

    public function checkExistence($email, $mobile)
    {
        $result = [];
        $where = "";
        if (empty($mobile) && !empty($email)) {
            $where = "u.email = :email or u.email_secondary = :email";
        } elseif (empty($email) && !empty($mobile)) {
            $where = "u.mobile = :mobile";
        } elseif (!empty($email) && !empty($mobile)) {
            $where = "u.email = :email or u.email_secondary = :email or u.mobile = :mobile";
        }

        if (!empty($where)) {
            $q = $this->getReadDb()->createQueryBuilder();
            $data = $q->select('*')
                ->from($this->tableName, 'u')
                ->where($where)
                ->setParameter('email', $email)
                ->setParameter('mobile', $mobile)
                ->andWhere('lote_deleted is null');
            $query = $data->execute();
            $result = $query->fetch(\PDO::FETCH_ASSOC);
        }

        return $result;
    }

    public function checkForDuplicateEmails($email)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('*')
            ->from($this->tableName, 'u')
            ->where("u.email = :email")
            ->setParameter('email', $email)
            ->andWhere('lote_deleted is null');

        $query = $data->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);

        return $result;
    }

    /**
     * Adds is_protected column to a user data array
     * @param $userData
     * @return mixed
     */
    public function addIsProtectedField($userData)
    {
        if (is_array($userData) && !empty($userData)) {

            $userIds = array_column($userData, 'id');

            if (is_array($userIds) && !empty($userIds)) {

                $q = $this->getReadDb()->createQueryBuilder();
                $q->select('t.id, max(g.is_protected) as _is_protected')
                    ->from('sb__user', 't')
                    ->leftJoin('t', 'sb__user_group', 'ug', 'ug.user_id = t.id')
                    ->leftJoin('ug', 'sb__group', 'g', 'g.id = ug.group_id')
                    ->where('t.id in (:user_ids)')
                    ->setParameter('user_ids', $userIds, Connection::PARAM_INT_ARRAY)
                    ->andWhere('t.lote_deleted is null')
                    ->andWhere('ug.lote_deleted is null')
                    ->andWhere('g.lote_deleted is null')
                    ->groupBy('t.id');

                $results = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
                $results = array_combine(array_column($results, 'id'), $results);

                foreach ($userData as $k => $user) {
                    if (isset($results[$user['id']])) {
                        $is_protected = $results[$user['id']]['_is_protected'];
                        unset($results[$user['id']]);
                        $userData[$k]['_is_protected'] = $is_protected;
                    }
                }
            }
        }

        return $userData;
    }

    public function getAllProtected()
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q = $this->getBaseProtectedUsersQuery($q);
        $q->addSelect('distinct t.*');

        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function countAllProtected()
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q = $this->getBaseProtectedUsersQuery($q);
        return $this->getQueryCount($q, 'count(distinct t.id) as cnt');
    }

    /**
     * @param QueryBuilder $q
     */
    protected function getBaseProtectedUsersQuery($q)
    {
        $q->from($this->tableName, 't')
            ->innerJoin('t', 'sb__user_group', 'ug', 'ug.user_id = t.id')
            ->innerJoin('ug', 'sb__group', 'g', 'g.id = ug.group_id')
            ->andWhere('g.is_protected = 1')
            ->andWhere('t.lote_deleted is null')
            ->andWhere('ug.lote_deleted is null')
            ->andWhere('g.lote_deleted is null');

        return $q;
    }

    /**
     * Delete all users in the system
     * @return void
     * */
    public function truncateUsers()
    {
        $this->getWriteDb()->exec("delete from sb__user where id > 0");
        $this->getWriteDb()->exec("delete from sb__user_group where user_id > 0");
    }

    /**
     * Get a user by their mobile number
     * @param string $mobileNumber
     * @return array|false
     * */
    public function getUserByMobile($mobileNumber)
    {
        $number = Mobile::standardise($mobileNumber, $this->getState()->getSettings()->getSettingOrConfig("system.locale"));
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("*")
            ->from("sb__user", "u")
            ->where("u.mobile = :mobile")
            ->setParameter("mobile", $number)
            ->andWhere("u.lote_deleted is null");
        return $q->execute()->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * Get a user by their email
     * @param string $email
     * @return array|false
     * */
    public function getUserByEmail($email)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("*")
            ->from("sb__user", "u")
            ->where("u.email = :email")
            ->setParameter("email", $email)
            ->andWhere("u.lote_deleted is null");
        return $q->execute()->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * Unsubscribe a users mobile communication
     * @param int $userId
     * @return void
     * */
    public function unsubscribeMobile($userId)
    {
        $this->getWriteDb()->update("sb__user", ['subscribed_mobile' => -1], ['id' => $userId]);
    }

    /**
     * Get users with status subscribed
     * @access public
     * @param $subscribed
     * @return array
     */
    public function getUsersWithSubscribedStatus($subscribed)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('id');
        $q->from('sb__user', 'u');
        $q->where('subscribed = :subscribed')
            ->setParameter('subscribed', $subscribed)
            ->andWhere("lote_deleted is null");
        return $q->execute()->fetchAll(\PDO::FETCH_COLUMN);
    }

    /**
     * Create a new admin user
     * @param string $firstName
     * @param string $lastName
     * @param string $username
     * @param string $password
     * @param string $email
     * @param string $mobile
     * @param string $phone
     * @return int
     * */
    public function createAdmin($firstName, $lastName, $username, $password, $email, $mobile, $phone)
    {
        $u = new UserEntity($this->getState());
        $u->first_name = $firstName;
        $u->last_name = $lastName;
        $u->username = $username;
        $u->password = $password;
        $u->email = $email;
        $u->mobile = $mobile;
        $u->phone = $phone;
        $u->is_admin = 1;
        $u->active = 1;
        $u->first_name = $firstName;
        if($u->save()) {
            $this->getWriteDb()->update("sb__user", ['is_admin' => 1], ['id' => $u->id]);
        }
        return $u->id;
    }

    public function getAdminUsersByQuery($phrase = "", $page = 1, $perPage = 20)
    {
        $q = $this->getState()->getReadDb()->createQueryBuilder();

//        $q->select('id, first_name, last_name, email')
        $q->select('*')
            ->from($this->getTableName())
            ->where('active = :active')
            ->setParameter('active', 1)
            ->andWhere('is_admin = :admin')
            ->setParameter('admin', 1);

        if (!is_null($phrase) && $phrase !== "") {
            $q->andWhere('(first_name like :phrase or last_name like :phrase or email like :phrase)')
                ->setParameter('phrase', "%$phrase%");
        }
        $q->andWhere("lote_deleted is null");

        return $this->getListByQuery($q, $page, $perPage);
    }

    /**
     * Clean a list of dynamic query clauses
     * @access public
     * @param array $clauses
     * @return array
     * */
    public function cleanDynamicQueryClauses($clauses) {
        $result = [];
        if(is_array($clauses)) {
            foreach ($clauses as $key => $val) {
                if (isset($val['field'])) {
                    if (isset($val['data_type']) && $val['data_type'] == 'boolean') {
                        if (isset($val['operator']) && $val['operator'] == 'is_false') {
                            $val['data_type'] = 'string';
                            $val['operator'] = 'not_equals';
                            $val['value'] = '1';
                        } elseif (isset($val['operator']) && $val['operator'] == 'is_true') {
                            $val['data_type'] = 'string';
                            $val['operator'] = 'equals';
                            $val['value'] = '1';
                        }
                    }
                    $result[$key] = $val;
                }

                if(isset($val['field_type']) && $val['field_type'] != 'core' && isset($val['join_data']) && ((Strings::isJson($val['join_data']) && !is_null(json_decode($val['join_data'], true))) || is_array($val['join_data']))) {
                    if (!is_array($val['join_data'])) {
                        $val['join'] = json_decode($val['join_data'], true);
                    } else {
                        $val['join'] = $val['join_data'];
                    }

                    $aliasOne = uniqid();
                    $aliasTwo = uniqid();

                    $val['join']['joins'][0]['value'] = $val['value'];
                    $val['join']['custom_alias'] .= "_".$aliasOne;
                    $val['join']['joins'][0]['from_alias'] .= "_".$aliasOne;
                    $val['join']['joins'][0]['custom_alias'] .= "_".$aliasTwo;
                    if($val['field_type']!='dynamic_list') {
                        $val['join']['joins'][0]['operator'] = $val['operator'];
                        $val['operator'] = 'equals';
                        unset($val['value']);
                        $val['join']['clause'][0]['value'] = $val['custom_field_id'];
                    }
                    unset($val['join_data']);
                    unset($val['custom_field_id']);

                    $result[$key] = $val;
                }
            }
        }
        return $result;
    }


    public function userHasContextAccess($userId, $context){
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $e = new UserEntity($this->getState());
        if($e->load($userId) && $e->is_admin == 1){
            return true;
        }
        else{
            $q->select('u.*')
                ->from($this->getTableName(), "u")
                ->leftJoin("u", "sb__user_group", "ug", "u.id = ug.user_id")
                ->leftJoin("ug", "sb__group", "g", "g.id = ug.group_id")
                ->leftJoin("g", "sb__group_rights", "r", "r.group_id = g.id")
                ->andWhere("u.lote_deleted is null")
                ->andWhere("ug.lote_deleted is null")
                ->andWhere("g.lote_deleted is null")
                ->andWhere("r.lote_deleted is null")
                ->andWhere("u.id = :user_id")
                ->andWhere("r.context = :context")
                ->andWhere("g.kind = 'admin'")
                ->setParameter("user_id", $userId)
                ->setParameter("context", $context)
            ->andWhere("r.mode != 'none'");
            $result =  $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
            if(count($result) > 0){
                return true;
            }
            else{
                return false;
            }
        }
    }

    /**
     * Retrieve users search query using
     * @param string $phrase
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    public function getSearchQuery($phrase = '')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('u.*')
            ->from($this->tableName, 'u')
            ->andWhere("u.lote_deleted is null");
        if($phrase){
            $q->andWhere('u.first_name like :phrase OR u.last_name like :phrase')
                ->setParameter('phrase', "%".$phrase."%");
        }
        return $q;
    }

    public function getLiaisonSyncList(){
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("distinct u.id")
            ->from("sb__user", "u")
            ->leftJoin("u", "sb_schoolzine_school_user", "su", "u.id = su.user_id")
            ->leftJoin("su", "sb_schoolzine_school", "sc", "sc.id = su.school_id")
            ->where("u.lote_deleted is null")
            ->andWhere("sc.lote_deleted is null")
            ->andWhere("su.lote_deleted is null")
            ->andWhere("sc.status = 'active'")
            ->andWhere("(sc.newsletter_type = 'school' OR sc.newsletter_type = 'corporate' OR sc.newsletter_type = 'childcare')");
        $result =  $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }



    /**
     * Search function for users
     *
     * @access public
     * @param string $phrase
     * @param array|string $groupId
     * @param int $status
     * @param int $subscribed
     * @param int $adminStatus
     * @param string $source
     * @param array $sortOptions
     * @param int $maxResults
     * @return object
     */
    public function getUsersSearchQuery($phrase, $groupId, $status, $subscribed, $adminStatus, $source, $gdprConsent, $sortOptions = [], $maxResults = null, $liaisonUsers = false, $isSzUser = false)
    {

        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('u.*, CONCAT_WS(" ", u.first_name, u.last_name) as name')
            ->from($this->getTableName(), 'u')
            ->where('u.lote_deleted is null');

        if (!is_null($phrase) && $phrase != '') {
            $q->andWhere('u.email like :phrase or u.first_name like :phrase or u.last_name like :phrase')
                ->setParameter('phrase', "%$phrase%");
        }

        if (!is_null($groupId) && !empty($groupId)) {
            $q->leftJoin('u', 'sb__user_group', 'ug', 'u.id = ug.user_id')
                ->andWhere('ug.lote_deleted is null');
            if (is_array($groupId) && count($groupId) > 0) {
                $q->andWhere("ug.group_id IN (:group_id)")
                    ->setParameter('group_id', $groupId, \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);
            } else {
                $q->andWhere('ug.group_id = :group_id')
                    ->setParameter('group_id', $groupId);
            }
        }

        if($liaisonUsers){
            $ug = new Group($this->getState());
            $adminGroups = $ug->getUsersInAdminGroups();
            $q->andWhere("(u.id IN (:group_id_admin) or u.is_admin = 1)")
                ->setParameter('group_id_admin', $adminGroups, \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);
            if($isSzUser){
                $q->andWhere("u.email not like '%schoolzine.co%'");
            }
        }else{
            $q->andWhere("u.is_admin != 1");
        }



        if (!is_null($status) && $status !== '') {
            $q->andWhere('u.active = :active')
                ->setParameter('active', $status);
        }

        if (!is_null($subscribed) && !$subscribed !== '') {
            if (is_numeric($subscribed)) {
                $q->andWhere('u.subscribed = :subscribed')
                    ->setParameter('subscribed', $subscribed);
            } elseif ($subscribed === 'is_suppressed') {
                $q->andWhere('u.is_suppressed = "1"');
            } elseif ($subscribed === 'app') {
                $q->andWhere('u.notification_device_id is not null');
            }
        }

        if (!is_null($adminStatus) && $adminStatus !== '') {
            $q->andWhere('u.is_admin = :admin_status')
                ->setParameter('admin_status', $adminStatus);
        }

        if (!is_null($source) && $source !== '') {
            $q->andWhere('u.source = :source')
                ->setParameter('source', $source);
        }

        if (!is_null($gdprConsent) && isset($gdprConsent)) {
            $q->andWhere('u.gdpr_consent = :gdpr_consent')
                ->setParameter('gdpr_consent', $gdprConsent);
        }

        if (!empty($sortOptions)) {
            $sortField = $sortOptions['sort_field'];
            $sortDirection = $sortOptions['sort_direction'];
            $customSortField = $sortOptions['custom_sort'];
            if ($customSortField) {
                $sortFieldId = $sortOptions['custom_field_id'];
                $q->leftJoin('u', 'sb__user__cf_value', 'cfv', 'u.id = cfv.object_id')
                    ->andWhere('cfv.field_id = :field_id')
                    ->setParameter('field_id', $sortFieldId)
                    ->andWhere('cfv.lote_deleted is null')
                    ->groupBy('u.id')
                    ->orderBy('cfv.value_string', $sortDirection)
                    ->addOrderBy('cfv.value_number', $sortDirection)
                    ->addOrderBy('cfv.value_date', $sortDirection)
                    ->addOrderBy('cfv.value_text', $sortDirection)
                    ->addOrderBy('cfv.value_boolean', $sortDirection);
            } else {
                $q->orderBy('u.' . $sortField, $sortDirection);
            }
        }

        if (!empty($maxResults)) {
            $q->setMaxResults($maxResults);
        }
        return $q;
    }

    public function getUserPasswordHistory($userId){
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("o.*")
            ->from("sb__audit_object_log", "o")
            ->leftJoin("o", "sb__audit_field_log", "f", "o.id = f.object_id")
            ->where("o.object_id = :user_id")
            ->andWhere("o.object_ref = 'sb__user'")
            ->andWhere("f.field_name = 'password'")
            ->andWhere("o.lote_deleted is null and f.lote_deleted is null")
            ->setParameter("user_id", $userId)
            ->orderBy("o.id", "desc");
        $result =  $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function getUsersThrottleHistory($userId){
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("*")
            ->from("sb__user_login_throttle", "t")
            ->where("t.user_id = :user_id")
            ->setParameter("user_id", $userId)
            ->orderBy("t.id", "desc");
        $result =  $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
}
