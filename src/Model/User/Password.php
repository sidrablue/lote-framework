<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model\User;

use SidraBlue\Lote\Entity\User\Password as PasswordEntity;
use SidraBlue\Lote\Entity\User;
use SidraBlue\Lote\Object\Model\CustomField;
use SidraBlue\Util\Time;
use SidraBlue\Lote\Entity\Setting;


/**
 * Model class for Users
 */
class Password extends CustomField
{

    protected $tableName = 'sb__user_password';

    /**
     * Get all of a users groups
     * @param string $password - the password to validate
     * @param int $userId
     * @return int
     */
    public function isValidPassword($password, $userId)
    {
        $result = 0;
        if ($passwords = $this->getValidPasswords($userId)) {
            foreach ($passwords as $p) {
                if (password_verify($password, $p['password'])) {
                    $result = $p['id'];
                    break;
                }
            }
        }
        return $result;
    }

    public function checkRecentPassword($userId, $newPassword){
        $se = new Setting($this->getState());
        $se->loadByField('reference', 'security.enabled.password.history');
        $recentPassLimit = $se->value_custom;
        $forbiddenPasswords = $this->getRecentPasswords($userId, $recentPassLimit);
        $result = true;

        //prevent the current password
        $u = new User($this->getState());
        $u->load($userId);
        if (password_verify($newPassword,$u->password)){
            $result = false;
        }

        foreach($forbiddenPasswords as $oldPassword){
            if (password_verify($newPassword,$oldPassword['password'])){
                $result = false;
            }
        }
        return $result;
    }

    public function getRecentPasswords($userId, $limit){
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('p.*')
            ->from('sb__user_password', 'p')
            ->where("p.user_id = :user_id")
            ->setParameter("user_id", $userId)
            ->orderBy('lote_created', 'desc')
            ->setMaxResults($limit);
        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getValidPasswords($userId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('p.*')
            ->from('sb__user_password', 'p')
            ->where("p.user_id = :user_id")
            ->setParameter("user_id", $userId)
            ->andWhere("p.lote_deleted is null")
            ->andWhere("(usage_type = :primary and is_primary = true) or (usage_type=:range and :now between range_from and range_to) or (usage_type = :limit and limit_remaining > 0)")
            ->setParameter("primary", PasswordEntity::USAGE_TYPE_PRIMARY)
            ->setParameter("range", PasswordEntity::USAGE_TYPE_RANGE)
            ->setParameter("limit", PasswordEntity::USAGE_TYPE_LIMIT)
            ->setParameter("now", Time::getUtcNow());
        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function recordUsage($passwordId)
    {
        $pe = new PasswordEntity($this->getState());
        if ($pe->load($passwordId) && $pe->usage_type == PasswordEntity::USAGE_TYPE_LIMIT) {
            $pe->limit_remaining = max(0, $pe->limit_remaining - 1);
            $pe->save();
            if ($pe->limit_remaining <= 0) {
                $pe->delete();
            }
        }
    }

    public function getUserPasswords($userId, $joinAuthorData = false)
    {
        $result = [];
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('p.*')
            ->from('sb__user_password', 'p')
            ->where("p.user_id = :user_id")
            ->setParameter("user_id", $userId)
            ->andWhere("p.lote_deleted is null");
        if ($joinAuthorData) {
            $q->addSelect("u.id as user_id, u.first_name as user_first_name, u.last_name as user_last_name");
            $q->leftJoin("p", "sb__user", "u", "u.id = p.lote_author_id");
        }
        if ($data = $q->execute()->fetchAll(\PDO::FETCH_ASSOC)) {
            $result = $data;
        }
        return $result;
    }

}
