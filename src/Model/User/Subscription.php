<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model\User;

use SidraBlue\Lote\Entity\User;
use SidraBlue\Lote\Entity\User\Subscription as SubscriptionEntity;
use SidraBlue\Lote\Object\Model\Base;

/**
 * Model class for user subscriptions
 */
class Subscription extends Base
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__user_subscription';

    /**
     * Get all of a users subscriptions
     * @param $userId
     * @return array
     */
    public function getSubscriptions($userId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('s.*')
            ->from($this->tableName, 's')
            ->where('s.user_id = :user_id')
            ->setParameter('user_id', $userId)
            ->andWhere("s.lote_deleted is null");
        $query = $data->execute();
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Get a specific subscription for a users channel
     * @param $userId
     * @param $channel
     * @return mixed
     */
    public function getSubscription($userId, $channel, $currentOnly = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('s.*')
            ->from($this->tableName, 's')
            ->where('s.user_id = :user_id')
            ->setParameter('user_id', $userId)
            ->andWhere('s.channel = :channel')
            ->setParameter('channel', $channel)
            ->andWhere("s.lote_deleted is null");
        if($currentOnly) {
            $q->andWhere("is_current = '1'")->addOrderBy("id", 'desc')->setMaxResults(1);
        }
        $s = $q->execute();
        return $s->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * Save a new subscription status for a user
     * @param $userId
     * @param $channel
     * @param $channelValue
     * @param $subscribed
     * @param $subscribedReason
     * @param $subscribedDesc
     * @param $sourceRef
     * @return mixed
     */
    public function setSubscription($userId, $channel, $channelValue, $subscribed, $subscribedReason='', $subscribedDesc='', $sourceRef = '')
    {
        $result = false;
        $s = new SubscriptionEntity($this->getState());
        if($s->loadByFields(['user_id' => $userId, 'channel' => $channel, 'is_current' => '1']) && $s->subscribed != $subscribed) {
            $s->is_current = false;
            $s->save();
        }
        $sNew = new SubscriptionEntity($this->getState());
        if(!$s->id || $s->subscribed != $subscribed) {
            $sNew->user_id = $userId;
            $sNew->is_current = true;
            $sNew->channel = $channel;
            $sNew->channel_value = $channelValue;
            $sNew->subscribed = $subscribed;
            $sNew->subscribed_reason = $subscribedReason;
            $sNew->subscribed_description = $subscribedDesc;
            $sNew->source_ref = $sourceRef;
            $result = $sNew->save();
        }
        return $result;
    }

    /**
     *
     * */
    public function wasSuppressedBySparkpost(User $user)
    {
        $userSubscription = $this->getSubscription($user->id, 'email', true);
        return stripos($userSubscription['subscribed_description'], 'sparkpost') !== false || stripos($userSubscription['subscribed_reason'], 'sparkpost') !== false;
    }

}
