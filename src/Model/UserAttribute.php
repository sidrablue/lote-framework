<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;
use SidraBlue\Lote\Entity\UserAttribute as UserAttributeEntity;

/**
 * Model class for Users attributes
 */
class UserAttribute extends Base
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__user_attribute';

    public function getAllByUserId($userId)
    {
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from($this->tableName, 'ua')
            ->where('ua.lote_deleted is null')
            ->andWhere('ua.user_id = :userId')
            ->setParameter('userId', $userId);

//        if (!is_null($phrase) && $phrase !== '') {
//            $q->andWhere('ua.attribute_value like :phrase')
//                ->setParameter('phrase', "%$phrase%");
//        }
//
//        if (!is_null($type) && $type !== '') {
//            $q->andWhere('ua.attribute_type = :type')
//                ->setParameter('type', "$type");
//        }
//
//        if (!is_null($reference) && $reference !== '') {
//            $q->andWhere('ua.attribute_reference = :reference')
//                ->setParameter('reference', "$reference");
//        }

        return $this->getListByQuery($q);
    }

}
