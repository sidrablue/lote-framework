<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Entity\UserAutoLogin as UserAutoLoginEntity;
use SidraBlue\Lote\Object\Model\Base;

/**
 * Model class for Users auto logins
 */
class UserAutoLogin extends Base
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__user_auto_login';

    /**
     * Create a login link
     * @access public
     * @param int $userId
     * @param string $redirectUrl - the URL to redirect the user to after the login completes
     * @param string $expires - the string to use in the DateTime modify in order to create the expiry
     * @param bool $twoFactorCompleted - true if two factor login has already been completed
     * @return UserAutoLoginEntity
     * */
    public function createLoginLink($userId, $redirectUrl = '', $expires = '+7 days', $twoFactorCompleted = false)
    {
        $l = new UserAutoLoginEntity($this->getState());
        $l->setDb($this->getWriteDb());
        $l->user_id = $userId;
        $l->redirect = $redirectUrl;
        $l->two_factor_completed = $twoFactorCompleted;
        $d = new \DateTime();
        $d->modify($expires);
        $l->expires = $d->format('c');
        $l->save();
        return $l;
    }

    /**
     * Create a login link
     * @access public
     * @param string $loginHash - the login hash
     * @param string $userHash - the user hash
     * @return UserAutoLoginEntity
     * */
    public function getAutoLogin($loginHash, $userHash)
    {
        $l = false;
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('*')
            ->from('sb__user_auto_login', 'l')
            ->where('login_hash = binary :login_hash')
            ->setParameter('login_hash', $loginHash)
            ->andWhere('user_hash = binary :user_hash')
            ->setParameter('user_hash', $userHash)
            ->andWhere('expires >= now()');
        $r = $q->execute()->fetch(\PDO::FETCH_ASSOC);
        if($r) {
            $l = new UserAutoLoginEntity($this->getState());
            $l->setData($r);
        }
        return $l;
    }

}
