<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;

/**
 * Model class for user consent log
 */
class UserGdprConsentLog extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__user_gdpr_consent_log';

    /**
     * Check user has consents pending
     *
     * @access public
     * @param int $userId - user ID
     * @return boolean
     *  */
    public function hasConsentPending($userId)
    {
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('count(id) as count')
            ->from($this->tableName, 'l')
            ->andWhere("l.lote_deleted is null")
            ->andWhere("l.processed is null")
            ->andWhere('l.user_id = :user_id')
            ->setParameter('user_id', $userId);
        $result = $q->execute()->fetch(\PDO::FETCH_ASSOC);

        return $result['count'] > 0;
    }

    /**
     * Get all GDPR consents which are still pending
     *
     * @access private
     * @param int $userId
     * @return array
     */
    public function getGdprConsentPendingLogByUserId($userId)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $data = $q->select('cl.*')
            ->from($this->tableName, 'cl')
            ->andWhere("cl.lote_deleted is null")
            ->andWhere("cl.user_id = :uid")
            ->setParameter("uid", $userId)
            ->andWhere("cl.processed is null")
            ->orderBy('cl.lote_created', 'asc');
        $query = $data->execute();
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }
}
