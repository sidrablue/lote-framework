<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;
use SidraBlue\Util\Arrays;
use Doctrine\DBAL\Connection;
use SidraBlue\Util\Time;

/**
 * Model class for user subscriptions source
 */
class UserGdprConsentType extends Base
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__user_gdpr_consent_type';

    public function getSearchQuery($params, $showDeleted = false)
    {
        $q = $this->getState()->getReadDb()->createQueryBuilder();

        $q->select('c.*, cl.date_confirmed, cl.description_add, cl.description_delete, cl.consent_source_id, cl.consent_method, s.name as source_name')
            ->from($this->tableName, 'c')
            ->leftJoin('c', 'sb__user_gdpr_consent_type_link', 'cl', 'cl.consent_type_id = c.id')
            ->leftJoin('cl', 'sb__user_subscription_consent_source', 's', 'cl.consent_source_id = s.id');

        if (!$showDeleted) {
            $q->andWhere('c.lote_deleted is null');
        }

        // Only include the Consent Type link which has associated with the user i.e. lote_deleted is null
        if (!empty($params['excludeDeletedConsent']) && $params['excludeDeletedConsent']) {
            $q->andWhere('cl.lote_deleted is null');
        }

        if (!empty($params['phrase'])) {
            $phraseSearchFields = [
                'like' => ['c.name', 'c.description'],
                'exact' => ['c.id']
            ];
            $wherePhrase = '';
            foreach ($phraseSearchFields as $type => $fields) {
                if ($type == 'like') {
                    foreach ($fields as $field) {
                        $wherePhrase .= "OR {$field} LIKE :phrase_like ";
                    }
                }
            }
            foreach ($phraseSearchFields as $type => $fields) {
                if ($type == 'exact') {
                    foreach ($fields as $field) {
                        $wherePhrase .= "OR {$field} = :phrase_exact ";
                    }
                }
            }
            $wherePhrase = substr($wherePhrase, 2);

            $q->andWhere($wherePhrase)
                ->setParameter('phrase_exact', $params['phrase'])
                ->setParameter('phrase_like', '%' . $params['phrase'] . '%');
        }

        if (!empty($params['reference'])) {
            $q->andWhere('s.reference = :reference')
                ->setParameter('reference', $params['reference']);
        }

        if (!empty($params['user_id'])) {
            $q->andWhere('cl.user_id = :user_id')
                ->setParameter('user_id', $params['user_id']);
        }

        if (!empty($params['id'])) {
            if (is_array($params['id'])) {
                $q->andWhere('c.id IN (:id)')
                    ->setParameter('id', $params['id'], Connection::PARAM_INT_ARRAY);
            } else {
                $q->andWhere('c.id = :id')
                    ->setParameter('id', $params['id']);
            }
        }

        if (!empty($params['sortorder'])) {
            $q->orderBy($params['sortorder']['field'], $params['sortorder']['direction']);
        }

        return $q;
    }

    /**
     * Get the types that match a set of ids passed in
     *
     * @access public
     * @param array $id - the type IDs
     * @return array
     *  */
    public function getTypesById($id)
    {
        $q = $this->getSearchQuery(['id' => $id]);
        if (is_array($id)) {
            $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
        } else {
            $result = $q->execute()->fetch(\PDO::FETCH_ASSOC);
        }

        return $result;
    }

    /**
     * Get all consent types
     *
     * @param array $searchParams
     * @param string $indexBy
     * @return array
     */
    public function getGdprConsentTypes($searchParams = [], $indexBy = '')
    {
        $q = $this->getSearchQuery($searchParams);
        $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);

        if (!empty($result) && $indexBy) {
            $types = Arrays::indexArrayByField($result, $indexBy);
        } else {
            $types = $result;
        }

        return $types;
    }

    /**
     * Get a consent type by id
     * @param $id
     * @return mixed
     */
    public function getGdprConsentType($id)
    {
        $searchParams = [];
        $searchParams['id'] = $id;

        $q = $this->getSearchQuery($searchParams);
        return $q->execute()->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * Get a consent types by user id
     *
     * @access public
     * @param int $userId - the user id
     * @param string $indexBy - the user id
     * @param boolean $excludeDeletedConsent
     * @return mixed
     */
    public function getGdprConsentTypesByUserId($userId, $indexBy = '', $excludeDeletedConsent = false)
    {
        $searchParams = [];
        $searchParams['user_id'] = $userId;
        $searchParams['sortorder'] = ['field' => 'c.id', 'direction' => 'asc'];
        $searchParams['excludeDeletedConsent'] = $excludeDeletedConsent;

        $q = $this->getSearchQuery($searchParams);
        $result = $q->execute()->fetchAll(\PDO::FETCH_ASSOC);

        if (!empty($result) && $indexBy) {
            $types = Arrays::indexArrayByField($result, $indexBy);
        } else {
            $types = $result;
        }

        return $types;
    }

    /**
     * Get a consent log changes
     * @param $userId - the user id
     * @return mixed
     */
    public function getUserConsentLog($userId, $page = 1, $limit = 20)
    {
        $searchParams = [];
        $searchParams['user_id'] = $userId;
        $searchParams['sortorder'] = ['field' => "1", 'direction' => 'desc'];

        $q = $this->getSearchQuery($searchParams, true);
        $q->addSelect('case when cl.lote_deleted then cl.lote_deleted else cl.lote_created end as date_actioned')
            ->addSelect('case when cl.lote_deleted then "0" else "1" end as is_active');

        return $this->getListByQuery($q, $page, $limit);
    }

    /**
     * Set the consent types for a specific user
     *
     * @access public
     * @param int $userId - the user ID in question
     * @param array $typeIds - an array of integers representing current consent types
     * @param string|int $source - the consent source
     * @param string $reason - the reason for the change
     * @param int $logId
     * @param string $method
     * @return void
     * */
    public function setUserConsentTypes($userId, $typeIds, $source, $reason, $logId = null, $method)
    {
        $currentConsentIds = array_keys($this->getGdprConsentTypesByUserId($userId, 'id', true));
        $addedIds = array_diff($typeIds, $currentConsentIds);
        $deletedIds = array_diff($currentConsentIds, $typeIds);

        $sourceId = 0;
        if (is_numeric($source)) {
            $sourceId = $source;
        } elseif (is_string($source)) {
            $usse = new \SidraBlue\Lote\Entity\UserSubscriptionConsentSource($this->getState());
            if ($usse->loadByField('reference', $source)) {
                $sourceId = $usse->id;
            }
        }

        foreach ($addedIds as $id) {
            $ugctl = new \SidraBlue\Lote\Entity\UserGdprConsentTypeLink($this->getState());
            $ugctl->loadByFields(['user_id' => $userId, 'consent_type_id' => $id], true);
            $ugctl->user_id = $userId;
            $ugctl->consent_type_id = $id;
            $ugctl->consent_source_id = $sourceId;
            $ugctl->consent_log_id = $logId;
            $ugctl->description_add = $reason;
            $ugctl->consent_method = $method;
            $ugctl->date_confirmed = Time::getUtcNow();
            $ugctl->description_delete = null;
            $ugctl->lote_deleted = null;
            $ugctl->lote_deleted_by = null;
            $ugctl->save();
        }

        foreach ($deletedIds as $id) {
            $ugctl = new \SidraBlue\Lote\Entity\UserGdprConsentTypeLink($this->getState());
            $ugctl->loadByFields(['user_id' => $userId, 'consent_type_id' => $id]);
            $ugctl->description_delete = $reason;
            $ugctl->consent_log_id = $logId;
            $ugctl->consent_method = $method;
            $ugctl->date_confirmed = Time::getUtcNow();
            $ugctl->save();
            $ugctl->delete();
        }
    }

}
