<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;

/**
 * Model class for Users attributes
 */
class UserGroup extends Base
{

    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__user_group';

    public function getAllGroupUserIds($groupId)
    {
        $result = [];
        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('ug.user_id')
            ->from($this->tableName, 'ug')
            ->andWhere('ug.lote_deleted is null')
            ->andWhere('ug.group_id = :group_id')
            ->setParameter('group_id', $groupId);
        $s = $q->execute();
        while ($row = $s->fetch()) {
            $result[] = $row['user_id'];
        }
        return $result;
    }

}
