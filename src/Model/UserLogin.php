<?php

/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;

class UserLogin extends Base
{

    /**
     * The table name related to this model
     * @var string $tableName
     * */
    protected $tableName = 'sb__user_login';

    /**
     *   */
    public function getGroups()
    {
        $sql = "select id,name from sb__group";
        return $this->getReadDb()->fetchAll($sql);
    }
	
	/**
     *   */
    public function getCouncils()
    {
        $sql = "select id,name from sb_mycare_council`";
        return $this->getReadDb()->fetchAll($sql);
    }
	
	/**
     *   */
    public function insertUser($detailsArr)
    {		
		$insert =$this->getReadDb()->insert($this->tableName, $detailsArr);
		
		$inserted_id = 0;
		
		if($insert){
			$inserted_id = $this->getReadDb()->lastInsertId();
		}
		
		return $inserted_id;
    }
	
	/**
     *   */
    public function insertGroups($userId, $userGroups)
    {
        $sql = array(); 
        foreach( $userGroups as $key=>$value ) {
            $sql[] = '("'.$userId.'", '.$value.')';
        }
        $this->getReadDb()->executeQuery('insert into sb__user_group (user_id, group_id) VALUES '.implode(',', $sql));
    }
	
	/**
     *   */
    public function insertCouncils($userId, $userCouncils)
    {
        $sql = array(); 
        foreach( $userCouncils as $key=>$value ) {
            $sql[] = '("'.$userId.'", '.$value.')';
        }
        $this->getReadDb()->executeQuery('insert into sb_mycare_user_council (user_id, council_id) VALUES '.implode(',', $sql));
    }

}
