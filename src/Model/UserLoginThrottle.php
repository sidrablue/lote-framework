<?php

/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Object\Model\Base;

/**
 * Class UserLoginThrottle
 *
 * @package SidraBlue\Lote\Model
 */
class UserLoginThrottle extends Base
{

    /**
     * The table name related to this model
     *
     * @var string $tableName
     * */
    protected $tableName = 'sb__user_login_throttle';

    /**
     * Find whether user is suspended
     *
     * @access public
     * @param int $userId - User ID
     * @return bool
     */
    public function isUserSuspended(int $userId): bool
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('count(ult.id)')
            ->from($this->getTableName(), 'ult')
            ->andWhere('ult.user_id = :userId')
            ->setParameter('userId', $userId)
            ->andWhere('ult.suspended = true')
            ->andWhere('ult.lote_deleted is null');
        return $q->execute()->fetchColumn() > 0;
    }

    /**
     * Find whether user is banned
     *
     * @access public
     * @param int $userId - User ID
     * @return bool
     */
    public function isUserBanned(int $userId): bool
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('count(ult.id)')
            ->from($this->getTableName(), 'ult')
            ->andWhere('ult.user_id = :userId')
            ->setParameter('userId', $userId)
            ->andWhere('ult.banned = true')
            ->andWhere('ult.lote_deleted is null');
        return $q->execute()->fetchColumn() > 0;
    }

    /**
     * Find whether user is suspended or banned
     *
     * @access public
     * @param int $userId - User ID
     * @return bool
     */
    public function isUserSuspendedOrBanned(int $userId): bool
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('count(ult.id)')
            ->from($this->getTableName(), 'ult')
            ->andWhere('ult.user_id = :userId')
            ->setParameter('userId', $userId)
            ->andWhere('ult.banned = true or ult.suspended = true')
            ->andWhere('ult.lote_deleted is null');
        return $q->execute()->fetchColumn() > 0;
    }

}
