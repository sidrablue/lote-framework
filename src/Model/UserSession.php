<?php

/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model;

use SidraBlue\Lote\Entity\UserSession as UserSessionEntity;
use SidraBlue\Lote\Object\Model\Base;

/**
 * @package use SidraBlue\Lote\Object\Model
 */
class UserSession extends Base
{

    protected $tableName = 'sb__user_session';

    /**
     * Get a login session from a username, ip, site ID and optional session Id.
     * This function updates the session login entry if the IP does not match but the IP is within an allowed subnet
     * @param int $userId
     * @param string $ip
     * @param int $siteId
     * @param string|boolean $sessionId
     * @return array|false
     */
    public function getLogin($userId, $ip, $siteId, $sessionId = false)
    {
        $loginFirewall = false;
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select("*")
            ->from('sb__user_session', 's')
            ->where('s.user_id = :user_id')
            ->setParameter('user_id', $userId)
            ->andWhere('s.site_id = :site_id')
            ->setParameter('site_id', $siteId);
        $q->andWhere('(s.session_id = :session_id)')
            ->setParameter('session_id', $sessionId);
        $q->orderBy('id', 'desc');
        $result = $q->execute()->fetch(\PDO::FETCH_ASSOC);
        if($loginFirewall && $result['ip_address'] != $ip) {
            $f = new Firewall($this->getState());
            if($f->isAllowed($result['ip_address'], '_login')) {
                $this->getWriteDb()->update('sb__user_session', ['ip_address' => $ip], ['id' => $result['id']]);
                $result['ip_address'] = $ip;
            }
            else {
                $result = false;
            }
        }
        return $result;
    }

    /**
     * Delete a login session by its ID
     * @param int $id
     * @return void
     * */
    public function removeLoginById($id)
    {
        $this->getWriteDb()->delete('sb__user_session', ['id' => $id]);
    }

    /**
     * Remove a login session from a username, ip and site ID
     * @param string|boolean $sessionId
     * @param string $username
     * @param int $siteId
     * @return array|false
     */
    public function removeLogin($sessionId, $username, $siteId)
    {
        $params = ['username' => $username, 'site_id' => $siteId, 'session_id' => $sessionId];
        return $this->getWriteDb()->delete('sb__user_session', $params);
    }

    /**
     * Save a login to the user session table
     * @param UserSessionEntity $userSessionEntity
     * @param string $username
     * @param int $userId
     * @param string $ip
     * @param int $siteId
     * @param string $sessionId
     * @param string $source
     * @param int $masqueradeUserId
     * @param bool $twoFactorCompleted
     * @access public
     * @return boolean
     */
    public function saveLogin(UserSessionEntity $userSessionEntity, $userId, $username, $ip, $siteId, $sessionId = '', $source = 'local', $masqueradeUserId = 0, $twoFactorCompleted = false)
    {
        if($data = $this->getLogin($userId, $ip, $siteId, $sessionId)) {
            $userSessionEntity->setData($data);
            $userSessionEntity->save();
        }
        else {
            $userSessionEntity->user_id = $userId;
            $userSessionEntity->username = $username;
            $userSessionEntity->site_id = $siteId;
            $userSessionEntity->ip_address = $ip;
            $userSessionEntity->source = $source;
            $userSessionEntity->session_id = $sessionId;
            $userSessionEntity->masquerade_user_id = $masqueradeUserId;
            $userSessionEntity->hash = password_hash($data['token'] . $data['username'], PASSWORD_DEFAULT);
            if($twoFactorCompleted) {
                $userSessionEntity->is_two_factor_verified = true;
            }
            $userSessionEntity->save();
        }
        return $userSessionEntity->id;
    }

    /**
     * Utility function to deleting expired login's periodically.
     * This function can be called but will only execute once every thousand times
     * */
    public function cleanup()
    {
        if (rand(1, 1000) == 19) {
            $this->deleteExpiredSessions();
        }
    }

    /**
     * Remove all expired login's from the session table
     * @access public
     * @return void
     * */
    public function deleteExpiredSessions()
    {
        $this->getWriteDb()->executeQuery('delete from sb__user_session where expires > utc_timestamp()');
    }

    /**
     * Remove the current session ID
     * @access public
     * @return int
     * */
    public function getSessionId()
    {
        $result = 0;
        if(isset($this->data) && isset($this->data['id'])) {
            $result = $this->data['id'];
        }
        return $result;
    }

}
