<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model;


use SidraBlue\Lote\Object\Model\Base;

class UserSessionData extends Base
{

    protected $values;

    protected $sessionId;

    /**
     * Get session details
     * @param int $key
     * @param int $default
     * @access public
     * @return array
     */
    public function get($key, $default = '')
    {
        $result = $default;
        $this->load($key);
        if ($this->data[$key] != null) {
            $result = $this->data[$key];
        }
        return $result;
    }

    /**
     * Set session details
     * @param string $key
     * @param string $value
     * @return array
     */
    public function set($key, $value)
    {
        $sessionId = $this->getState()->getUserSession()->session_id;
        if ($sessionId) {
            $this->getWriteDb()->delete('sb__user_session_data', ['session_id' => $sessionId, 'data_key' => $key]);
            $this->getWriteDb()->insert(
                'sb__user_session_data',
                ['session_id' => $sessionId, 'data_key' => $key, 'data_value' => $value]
            );
            $this->{$key} = $value;
        }
    }

    private function load($key, $force = false)
    {
        $sessionId = $this->getState()->getUserSession()->session_id;
        if ($sessionId && ($force || !$this->dataIsLoaded($key))) {
            $sql = 'select * from sb__user_session_data d where d.data_key = :key and d.session_id = :sid';
            $query = $this->getReadDb()->executeQuery($sql, ['key' => $key, 'sid' => $sessionId])->fetch(\PDO::FETCH_ASSOC);
            if (!empty($query)) {
                $this->{$key} = $query['data_value'];
            } else {
                $this->{$key} = null;
            }
        }
    }

    private function dataIsLoaded($key)
    {
        return isset($this->data[$key]);
    }

    private function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }

}
