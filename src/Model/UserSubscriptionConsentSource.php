<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Model;

use Doctrine\DBAL\Connection;
use SidraBlue\Lote\Object\Model\Base;

/**
 * Model class for user subscriptions source
 */
class UserSubscriptionConsentSource extends Base
{
    /**
     * @param string $tableName
     * */
    protected $tableName = 'sb__user_subscription_consent_source';

    public function getSearchQuery($params)
    {
        $q = $this->getState()->getReadDb()->createQueryBuilder();

        $q->select('s.*')
            ->from($this->tableName, 's')
            ->leftJoin('s', 'sb__user_gdpr_consent_type_link', 'cl', 'cl.consent_source_id = s.id')
            ->leftJoin('cl', 'sb__user_gdpr_consent_type', 'c', 'cl.consent_type_id = c.id')
            ->andWhere('s.lote_deleted is null');

        if (!empty($params['phrase'])) {
            $phraseSearchFields = [
                'like' => ['s.name', 's.description'],
                'exact' => ['s.id']
            ];
            $wherePhrase = '';
            foreach ($phraseSearchFields as $type => $fields) {
                if ($type == 'like') {
                    foreach ($fields as $field) {
                        $wherePhrase .= "OR {$field} LIKE :phrase_like ";
                    }
                }
            }
            foreach ($phraseSearchFields as $type => $fields) {
                if ($type == 'exact') {
                    foreach ($fields as $field) {
                        $wherePhrase .= "OR {$field} = :phrase_exact ";
                    }
                }
            }
            $wherePhrase = substr($wherePhrase, 2);

            $q->andWhere($wherePhrase)
                ->setParameter('phrase_exact', $params['phrase'])
                ->setParameter('phrase_like', '%' . $params['phrase'] . '%');
        }

        if (!empty($params['reference'])) {
            $q->andWhere('s.reference = :reference')
                ->setParameter('reference', $params['reference']);
        }

        if (!empty($params['user_id'])) {
            $q->andWhere('cl.user_id = :user_id')
                ->setParameter('user_id', $params['user_id']);
        }

        if (!empty($params['id'])) {
            if (is_array($params['id'])) {
                $q->andWhere('s.id IN (:id)')
                    ->setParameter('id', $params['id'], Connection::PARAM_INT_ARRAY);
            } else {
                $q->andWhere('s.id = :id')
                    ->setParameter('id', $params['id']);
            }
        }

        if (!empty($params['sortorder'])) {
            $q->orderBy($params['sortorder']['field'], $params['sortorder']['direction']);
        }

        return $q;
    }

    /**
     * Get all subscription sources
     *
     * @param array $searchParams
     * @return array
     */
    public function getConsentSources($searchParams = [])
    {
        $q = $this->getSearchQuery($searchParams);
        return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Get a specific subscription source
     * @param string|int $source
     * @return mixed
     */
    public function getConsentSource($source)
    {
        $searchParams = [];

        if (is_numeric($source)) {
            $searchParams['id'] = $source;
        } elseif (is_string($source)) {
            $searchParams['reference'] = $source;
        }

        $q = $this->getSearchQuery($searchParams);
        return $q->execute()->fetch(\PDO::FETCH_ASSOC);
    }
}
