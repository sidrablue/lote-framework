<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Object\Data\Field;

use SidraBlue\Lote\Object\Data\Field\Type\Base;

class Factory
{

    private static $map = [
        'checkbox' => 'SidraBlue\Lote\Object\Data\Field\Type\CheckBox',
        'publicgroup' => 'SidraBlue\Lote\Object\Data\Field\Type\PublicGroup',
        'gdpr_consent' => 'SidraBlue\Lote\Object\Data\Field\Type\GdprConsent'
    ];


    /**
     * Create an instance of field data
     * @param $state \SidraBlue\Lote\State\Web
     * @param string $reference
     * @return Base
     * */
    public static function createInstance($state, $reference = "text")
    {
        $className = __NAMESPACE__ . '\Type\Base';
        $reference = str_replace(' ', '', ucwords(str_replace('_', ' ', $reference)));
        if ($reference) {
            if (isset(self::$map[strtolower($reference)])) {
                $referenceClass = self::$map[strtolower($reference)];
            } else {
                $referenceClass = __NAMESPACE__ . '\Type\\' . $reference;
            }
            if (class_exists($referenceClass)) {
                $className = $referenceClass;
            }
        }
        return new $className($state);
    }

    /**
     * Register a field type
     * @param string $reference
     * @param string $namespace
     * @access public static
     * @return void
     * */
    public static function registerField($reference, $namespace)
    {
        self::$map[$reference] = $namespace;
    }

}
