<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Object\Data\Field\Type;


class Boolean extends Base
{

    public function validate()
    {
        $result = parent::validate();
        return $result;
    }

}
