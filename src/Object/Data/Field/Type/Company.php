<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Object\Data\Field\Type;

use Lote\App\CRM\Entity\Link as LinkEntity;
use Lote\App\CRM\Model\Link as LinkModel;
use Lote\System\Schoolzine\Model\Metadata\Region;
use Sabre\VObject\Parser\Json;
use SidraBlue\Lote\Entity\CfValue;
use Lote\App\CRM\Entity\Company as CompanyEntity;
use Lote\App\CRM\Model\Company as CompanyModel;
use Lote\App\CRM\Model\CompanyGroup as CompanyGroupModel;
use SidraBlue\Lote\Entity\User as UserEntity;
use SidraBlue\Util\Arrays;

class Company extends BaseOption
{

    public function getEditData($isPreview = false)
    {
        $cm = new CompanyModel($this->getState());
        $allCompanies = $cm->getAllAlphabetically();
        if(is_array($this->getValue())) {
            $selected = $this->getValue();
            $selected = Arrays::getFieldValuesFrom2dArray($selected, "id");
            $count = count($allCompanies);
            for ($i = 0; $i < $count; $i++) {
                if (in_array($allCompanies[$i]['id'], $selected) !== false) {
                    $allCompanies[$i]['_selected'] = true;
                }
            }
        }
        return $allCompanies;
    }

    /**
     * Get the view data of this field
     * @access public
     * @param int $objectId
     * @return mixed
     * */
    public function getViewData($objectId = 0)
    {
        $results = [];
        if(is_array($this->getValue())) {
            $results = $this->getValue();
            foreach($results as $index => $result) {
                $companyId = $result['id'];

                $ce = new CompanyEntity($this->getState());
                if($ce->load($companyId)) {
                    $groupModel = new CompanyGroupModel($this->getState());
                    $this->getState()->getView()->company_groups = $groupModel->getCompanyGroups([$companyId]);
                    if($regionData = $ce->getCustomFieldValue("region_6232")) {
                        $results[$index]['value'] .= " - DET Region: " . $regionData[0]['value'];
                    }
                }
            }
        }
        return $results;
    }

    protected function afterCfValueSave(CfValue $cfValue, $setByText = true, $fieldDeletedInfo)
    {
        return true;
    }

    /**
     * Perform actions after user save
     *
     * @access public
     * @param UserEntity $userEntity - User entity
     * @param string value - Field value
     * @return void
     */
    public function afterUserSave(UserEntity $userEntity, $value)
    {
        $companies = json_decode($value, true);
        if(is_array($companies)) {
            foreach($companies as $company) {
                $le = new LinkEntity($this->getState());
                if(!$le->loadByFields([
                    'link_name' => 'Link',
                    'from_object_ref' => 'user',
                    'from_object_id' => $userEntity->id,
                    'to_object_ref' => 'company',
                    'to_object_id' => $company['id'],
                ])) {
                    $le->loadByFields([
                        'link_name' => 'Link',
                        'to_object_ref' => 'user',
                        'to_object_id' => $userEntity->id,
                        'from_object_ref' => 'company',
                        'from_object_id' => $company['id'],
                    ]);
                }

                $le->setData([
                    'link_name' => 'Link',
                    'from_object_ref' => 'user',
                    'from_object_id' => $userEntity->id,
                    'to_object_ref' => 'company',
                    'to_object_id' => $company['id'],
                ]);
                $le->save();
            }
        }
    }

    public function validate()
    {
        return true;
    }

    /**
     * Validate the option data of this field
     * @access public
     * @param array|mixed $optionData - the config data to validate
     * @param array $errors - the error messages array
     * @return boolean
     * */
    public function validateOptionData($optionData, &$errors)
    {
        $cm = new CompanyModel($this->getState());
        $companies = $cm->getAll();
        return is_array($companies) && count($companies) > 0;
    }

    /**
     * Set the value of this field
     * @access public
     * @param CfValue $value
     * */
    public function setValueFromObject(CfValue $value)
    {
        $this->value = json_decode($value->value_text, true);
    }

}
