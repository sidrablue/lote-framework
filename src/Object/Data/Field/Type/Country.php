<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Object\Data\Field\Type;

class Country extends Base
{

    public function validate()
    {
        $result = parent::validate();
        if($result && !is_numeric($this->getValue())) {
            $result = false;
            $this->error = 'Invalid numeric valid';
        }
        return $result;
    }

}
