<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Object\Data\Field\Type;

use Lote\Module\Form\Entity\Form as FormEntity;
use Lote\Module\Form\Entity\Record as RecordEntity;
use SidraBlue\Lote\Entity\CfField;
use SidraBlue\Lote\Entity\CfValue;
use SidraBlue\Lote\Entity\File as FileEntity;
use SidraBlue\Lote\Event\Data;
use SidraBlue\Lote\Model\File as FileModel;
use SidraBlue\Lote\Service\Timezone;

/**
 * Class File
 * @package SidraBlue\Lote\Object\Data\Field\Type
 */
class File extends Base
{
    const FIELD_ERROR_FILE_INVALID_TYPE = 300;

    /**
     * Set current value from existing value object if current value not already set
     *
     * @access public
     * @param CfValue $value - Existing cf value
     * @return void
     */
    public function setValueFromObject(CfValue $value)
    {
        if(!$this->value) {
            $this->value = $value->value_string;
        }
    }

    /**
     * Validate field
     *
     * @access public
     * @return boolean
     */
    public function validate()
    {
        $result = parent::validate();
        return $result;
    }

    /**
     * Validate the input for the file input
     *
     * @access protected
     * @return boolean
     **/
    protected function validateFieldInputData()
    {
        $result = true;
        if($fileData = $this->getFile()) {
            $fileExtension = $fileData['extension'];
            $allowedExtensions = $this->getAllowedExtensions();
            if($allowedExtensions){
                if(array_search($fileExtension, $allowedExtensions) === false) {
                    $this->error = self::FIELD_ERROR_FILE_INVALID_TYPE;
                    $result = false;
                }
            }
        }

        return $result;
    }

    /**
     * Retrieve list of accepted extensions
     *
     * @access public
     * @return array
     */
    public function getAllowedExtensions()
    {
        $allowedExtensions = [];
        $configData = json_decode($this->definition->config_data, true);
        if(isset($configData['allowed_extensions']) && $configData['allowed_extensions']) {
            $allowedExtensions = explode(',', $configData['allowed_extensions']);
        }
        return $allowedExtensions;
    }

    /**
     * Retrieve file entity associated with this field
     *
     * @access public
     * @return array
     */
    public function getFile()
    {
        $fileId = $this->getValue();
        $m = new FileModel($this->getState());
        return $m->get($fileId);
    }

    /**
     * Get the error code of the current field, if one is specified
     *
     * @access public
     * @return string
     * */
    public function getErrorText()
    {
        if ($this->error == self::FIELD_ERROR_FILE_INVALID_TYPE) {
            $allowedExtensions = $this->getAllowedExtensions();
            $allowedExtensionsString = implode(', ', $allowedExtensions);
            $text = "File type not accepted. Valid extensions: {$allowedExtensionsString}";
        } else {
            $text = parent::getErrorText();
        }
        return $text;
    }

    /**
     * Action functionality after field save
     *
     * @access protected
     * @param CfValue $cfValue - Custom field value
     * @param bool $setByText
     * @param array $fieldDeletedInfo
     */
    protected function afterCfValueSave(CfValue $cfValue, $setByText, $fieldDeletedInfo)
    {
        $fileId = $this->getViewData();
        $fe = new FileEntity($this->getState());
        if ($fe->load($fileId)) {
            $this->dispatchFieldFileSaveEvent($cfValue->object_id, $cfValue->field_id, $fe);
        }
    }

    /**
     * Dispatch field file save event
     *
     * @access protected
     * @param int $recordId - the ID of the record
     * @param int $fieldId - the ID of the field
     * @param FileEntity $fe - File entity
     * @return void
     */
    protected function dispatchFieldFileSaveEvent($recordId, $fieldId, FileEntity $fe)
    {
        $re = new RecordEntity($this->getState());
        $re->load($recordId);
        $foe = new FormEntity($this->getState());
        $foe->load($re->form_id);
        $cffe = new CfField($this->getState());
        $cffe->load($fieldId);

        $configData = json_decode($cffe->config_data, 1);
        if (isset($configData['is_sharepoint']) && $configData['is_sharepoint'] && isset($configData['sharepoint_folder']) && $configData['sharepoint_folder']) {
            $date = new Timezone($this->getState());
            $created = $date->convertToDisplay($re->lote_created, "Y-m-d_H-i-s");

            $directoryPath = "{$configData['sharepoint_folder']}/form/{$foe->name}/{$created}__{$recordId}/{$cffe->name}";
            $directoryPath = preg_replace('/[^A-Za-z0-9_\- \/]/', '_', $directoryPath);

            $data = new Data();
            $data->setData([
                'file_id' => $fe->id,
                'destination_path' => $directoryPath,
            ]);
            $this->getState()->getEventManager()->dispatch('form.field.file.save', $data);
        }
    }
}
