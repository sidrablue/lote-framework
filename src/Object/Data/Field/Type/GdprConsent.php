<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Object\Data\Field\Type;

use SidraBlue\Lote\Entity\CfValue;
use SidraBlue\Lote\Model\UserGdprConsentType;

class GdprConsent extends Base
{

    /**
     * Get the view data of this field
     * @access public
     * @param int $objectId
     * @return mixed
     * */
    public function getViewData($objectId = 0)
    {
        $value = $this->getValue();
        if (!is_array($value)) {
            $value = json_decode($value, true);
        }
        $tmp = [];
        if (!empty($value)) {
            foreach ($value as $type_id => $v) {
                if ($v) {
                    $tmp[] = $type_id;
                }
            }
        }
        $m = new UserGdprConsentType($this->getState());
        return $m->getTypesById($tmp);
    }

    public function getEditData($isPreview = false)
    {
        $gdprTypeModel = new UserGdprConsentType($this->getState());
        $allTypes = $gdprTypeModel->getGdprConsentTypes();
        if(is_array($this->getValue())) {
            $cnt = count($allTypes);
            for ($i = 0; $i < $cnt; $i++) {
                if (array_search($allTypes[$i]['id'], $this->getValue()) !== false) {
                    $allTypes[$i]['_selected'] = true;
                }
            }
        }
        if($this->getState()->getUser()->id) {
            $ugm = new UserGdprConsentType($this->getState());
            $userConsentTypeIds = array_keys($ugm->getGdprConsentTypesByUserId($this->getState()->getUser()->id, 'id'));
            $cnt = count($allTypes);
            for ($i = 0; $i < $cnt; $i++) {
                if (array_search($allTypes[$i]['id'], $userConsentTypeIds) !== false) {
                    $allTypes[$i]['_selected'] = true;
                }
            }
        }
        return $allTypes;
    }

    public function validate()
    {
        $result = true;
        $value = json_decode($this->getValue(),true);
        if (is_array($value) && array_search(1, $value) === false) {
            $result = false;
        }

        return $result;
    }

    /**
     * Get the error text
     * @access public
     * @return string
     * */
    public function getErrorText()
    {
        $text = "At least 1 communication consent must be selected";
        return $text;
    }

    /**
     * Get the value to be saved
     * @access protected
     * @param CfValue $v
     * @return string
     * */
    protected function setValueData(CfValue $v)
    {
        return $v->value_text = json_encode($this->getValue());
    }

    /**
     * Set the value of this field
     * @access public
     * @param CfValue $value
     * */
    public function setValueFromObject(CfValue $value)
    {
        $this->value = json_decode($value->value_text, true);
    }

}
