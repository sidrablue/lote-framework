<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Object\Data\Field\Type;

use Lote\Module\Form\Entity\RecordValue;
use SidraBlue\Lote\Entity\CfField;
use SidraBlue\Lote\Entity\CfValue;
use SidraBlue\Lote\Model\CfFieldOption as CfFieldOptionModel;
use SidraBlue\Util\Arrays;
use SidraBlue\Util\Strings;

class Matrix extends BaseOption
{

    /**
     * @var boolean $supportsExporting
     * True if this object can be used in exports
     * */
    protected $supportsExporting = true;

    /**
     * @var bool $isMatrix - true if this field is a matrix and we expect 2D values
     * */
    protected $isMatrix = true;

    const FIELD_ERROR_MATRIX_ALL_ROWS_NOT_SELECTED = 209;
    const FIELD_ERROR_MATRIX_ALL_ROWS_NOT_INPUTTED = 210;

    const FIELD_MATRIX_TYPE_TEXT = 'text';
    const FIELD_MATRIX_TYPE_RADIO = 'radio';
    const FIELD_MATRIX_TYPE_CHECKBOX = 'checkbox';

    /**
     * Validate the input for this multi select field
     * @access protected
     * @return boolean
     * */
    protected function validateFieldInputData()
    {
        $result = true;
        if (is_string($this->definition->config_data)) {
            $data = json_decode($this->definition->config_data, true);
        }
        $options = $this->getOptions();
        $optionRows = $options['row'];
        $optionCols = $options['column'];

        if(is_string($this->getValue())) {
            $submitData = json_decode($this->getValue(), true);
        } else {
            $submitData = $this->getValue();
        }
        if ($this->isRadioMatrix() || $this->isCheckboxMatrix()) {
            $result = true;
            if (isset($options['row'])) {
                if ($this->definition->mandatory) {
                    foreach ($optionRows as $r) {
                        if (!isset($submitData[$r['id']]) || !count($submitData[$r['id']]) > 0) {
                            $result = false;
                            $this->error = self::FIELD_ERROR_MATRIX_ALL_ROWS_NOT_SELECTED;
                            break;
                        }
                    }
                }
            }
        } else if ($this->isTextMatrix()) {
            $result = true;
            if (isset($options['row'])) {
                if ($this->definition->mandatory) {
                    foreach ($optionRows as $r) {
                        foreach ($optionCols as $c) {
                            if (!isset($submitData[$r['id']]) || !isset($submitData[$r['id']][$c['id']])) {
                                $result = false;
                                $this->error = self::FIELD_ERROR_MATRIX_ALL_ROWS_NOT_INPUTTED;
                                break;
                            }
                        }
                    }
                }
            }
        }
        if ($this->isRadioMatrix() || $this->isCheckboxMatrix() || $this->isTextMatrix()) {
            if ($result) {
                $optionRows = Arrays::indexArrayByField($optionRows, 'id');
                $optionCols = Arrays::indexArrayByField($optionCols, 'id');
                foreach ($submitData as $row => $columns) {
                    if (!isset($optionRows[$row])) {
                        $result = false;
                        $this->error = self::FIELD_ERROR_OPTION_DOES_NOT_EXIST;
                        break;
                    }
                    foreach ($columns as $colId => $colSelected) {
                        if (!isset($optionCols[$colId])) {
                            $result = false;
                            $this->error = self::FIELD_ERROR_OPTION_DOES_NOT_EXIST;
                            break 2;
                        }
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Check if this matrix is of type checkbox
     * @access public
     * @return bool
     */
    public function isCheckboxMatrix()
    {
        return $this->isMatrixInputType(self::FIELD_MATRIX_TYPE_CHECKBOX);
    }

    /**
     * Check if this matrix is of type radio
     * @access public
     * @return bool
     */
    public function isRadioMatrix()
    {
        return $this->isMatrixInputType(self::FIELD_MATRIX_TYPE_RADIO);
    }

    /**
     * Check if this matrix is of type text
     * @access public
     * @return bool
     */
    public function isTextMatrix()
    {
        return $this->isMatrixInputType(self::FIELD_MATRIX_TYPE_TEXT);
    }

    /**
     * Check if this matrix matches a given input type
     * @access public
     * @param string $inputType
     * @return bool
     */
    private function isMatrixInputType($inputType)
    {
        $data = $this->definition->config_data;
        if (is_string($data)) {
            $data = json_decode($data, true);
        }
        return isset($data['matrix_field_type']) && $data['matrix_field_type'] == $inputType;
    }

    /**
     * Get the error code of the current field, if one is specified
     * @access public
     * @return int
     * */
    public function getErrorText()
    {
        if ($this->error == self::FIELD_ERROR_MATRIX_ALL_ROWS_NOT_SELECTED) {
            $text = "Please specify selections in all rows";
        } elseif ($this->error == self::FIELD_ERROR_MATRIX_ALL_ROWS_NOT_INPUTTED) {
            $text = "Please specify values for each input";
        } else {
            $text = parent::getErrorText();
        }
        return $text;
    }

    /**
     * Get the options available for this field
     * @access public
     * @param int|boolean $fieldId - a field ID if one is not already specified as part of the field definition
     * @return array
     * */
    public function getOptions($fieldId = false)
    {
        $result = ['row' => [], 'column' => []];
        $m = new CfFieldOptionModel($this->getState());
        $fieldId ? $id = $fieldId : $id = $this->definition->id;
        $options = $m->getFieldOptions($id);
        foreach ($options as $v) {
            if ($v['direction'] == 'row') {
                $result['row'][] = $v;
            } elseif ($v['direction'] == 'column') {
                $result['column'][] = $v;
            }
        }
        return $result;
    }

    /**
     * Get the export columns for this cfField
     * @param CfField $field
     * @param boolean $includeGroupName
     * @return array
     * */
    public function getExportColumns(CfField $field, $includeGroupName = false)
    {
        $result = [];
        $options = $this->getOptions();
        if (count($options['row']) > 0 && count($options['column']) > 0) {
            foreach ($options['row'] as $i) {
                foreach ($options['column'] as $j) {
                    $result[] = "{$i['value']} - {$j['value']}";
                }
            }
        }
        return $result;
    }

    /**
     * Get the export columns for this cfField
     * @param CfField $field
     * @param array|false $exportData
     * @return array
     * */
    public function getExportCells(CfField $field, $exportData)
    {
        $viewData = $this->getViewData();
        $options = $this->getOptions();
        $result = [];
        if (count($options['row']) > 0 && count($options['column']) > 0) {
            foreach ($options['row'] as $row) {
                foreach ($options['column'] as $col) {
                    $cellValue = '';
                    foreach ($viewData as $v) {
                        if ($v['option_id'] == $row['id'] && $v['option_id_col'] == $col['id']) {
                            if ($this->isCheckboxMatrix() || $this->isRadioMatrix()) {
                                if ($v['value_text'] == '1') {
                                    $cellValue = 'Yes';
                                } else {
                                    $cellValue = 'No';
                                }
                            } else {
                                $cellValue = $v['value_text'];
                            }
                        }
                    }
                    $result[] = $cellValue;
                }
            }
        }
        return $result;
    }

    /**
     * Get the value to be saved
     * @access protected
     * @param CfValue $v
     * @return string
     * */
    protected function setValueData(CfValue $v)
    {
        if (!Strings::isJson($this->getValue())) {
            $str = $this->getValue();
            if (!$str) {
                $v->value_text = "";
            } else {
                $v->value_text = json_encode($this->getValue());
            }
        } elseif (is_scalar($this->getValue())) {
            $v->value_text = $this->getValue();
        }
        return $v->value_text;
    }

    /**
     * Set the value of this field
     * @access public
     * @param CfValue $value
     * */
    public function setValueFromObject(CfValue $value)
    {
        $this->value = json_decode($value->value_text, true);
    }

    /**
     * Get a string representation of the display value
     * @param RecordValue|array $recordValue
     * @return string
     * */
    public function getDisplayValue($recordValue)
    {
        $result = "";
        if ($recordValue instanceof RecordValue) {
            $data = json_decode($recordValue->value_text, true);
        } elseif (is_array($recordValue) && isset($recordValue['value_text'])) {
            $data = json_decode($recordValue['value_text'], true);
        }
        if (isset($data) && is_array($data)) {
            $result = nl2br(implode(",", $data));
        }
        return $result;
    }

    public function getEditData($isPreview = false)
    {
        $this->getValue();
        $result = parent::getEditData();
        if ($this->configData && isset($this->configData['matrix_field_type'])) {
            if (isset($this->configData['matrix_field_type'])) {
                $result['matrix_field_type'] = $this->configData['matrix_field_type'];
            }
        }
        return $result;
    }

    public function getPdfExportData($data)
    {
        $config = json_decode($data['config'], true);
        //$rows = $config['rows'];
        //$columns = $config['columns'];
        $type = isset($config['matrix_field_type']) ? $config['matrix_field_type'] : "";

        $textBased = false;
        if ($type == 'text') {
            $textBased = true;
        }

        $options = $this->getOptions($data['field_id']);

        $answers = [];
        $totalAnswersPerRow = [];
        foreach ($options['row'] as $row) {
            $totalAnswersPerRow[$row['value']] = 0;
            foreach ($options['column'] as $column) {
                if (!$textBased) {
                    $answers[$row['value']][$column['value']] = ['count' => 0, 'weighting' => 0.0];
                } else {
                    $answers[$row['value']][$column['value']] = [];
                }
            }
        }

//        if ($type == 'checkbox') {
//            ddd($answers);
//        }

//        if ($type == 'text') {
//            ddd($answers);
//        }

//        $rows = [];
//        foreach ($options['row'] as $row) {
//            $rows[] = $row['value'];
//        }
//        $columns = [];
//        foreach ($options['column'] as $column) {
//            $columns[] = $column['value'];
//
//        }

//        $answers = [];
//        $totalAnswersPerRow = [];
//        for ($i = 0; $i < count($rows); $i++) {
//            $totalAnswersPerRow[$i] = 0;
//            for ($j = 0; $j < count($columns); $j++) {
//                if (!$textBased) {
//                    $answers[$i][$j] = ['count' => 0, 'weighting' => 0.0];
//                } else {
//                    $answers[$i][$j] = [];
//                }
//            }
//        }
//        $rowsPerRowInitialised = false;
        foreach ($data['answers'] as $answer) {
//            $answer = json_decode($answer, true);
            foreach ($answer as $rowKey => $column) {
//                $rowIndex = array_search(strtolower($key), array_map(function ($row) {
//                    return str_replace(' ', '_', strtolower($row));
//                }, $rows));
//                if (!$rowsPerRowInitialised) {
//                    $totalAnswersPerRow[] = 0;
//                }
//                foreach ($valueArr as $value => $text) {
//                    $columnIndex = array_search(strtolower($value), array_map(function ($column) {
//                        return str_replace(' ', '_', strtolower($column));
//                    }, $columns));
//                $rowIndex = array_search($rowKey, $rows);
//                $columnIndex = array_search($value, $columns);
                    if (!$textBased) {
                        $answers[$rowKey][$column['key']]['count']++;

                        $totalAnswersPerRow[$rowKey]++;
                    } else {
                        $answers[$rowKey][$column['key']][] = $column['value'];
                    }
//                }
            }

//            $rowsPerRowInitialised = true;
        }

//        if ($type == 'checkbox') {
//            ddd($answers);
//        }
//
//        if ($type == 'text') {
//            ddd($answers);
//        }

        // fill remaining fields if not full
        if (!$textBased) {
            foreach ($answers as $rowKey => $columnArray) {
                foreach ($columnArray as $columnKey => $columnValue) {
                    $answers[$rowKey][$columnKey]['weighting'] = $totalAnswersPerRow > 0 ? ((float) $columnValue['count'] / (float) $totalAnswersPerRow[$rowKey]) : 0.0;
                }
            }
//            for ($i = 0; $i < count($rows); $i++) {
//                for ($j = 0; $j < count($columns); $j++) {
//                    if (!isset($answers[$rows[$i]][$columns[$j]]['count'])) {
//                        $answers[$rows[$i]][$columns[$j]]['count'] = 0;
//                    }
//
//                    $answers[$rows[$i]][$columns[$j]]['weighting'] = (float)$answers[$rows[$i]][$columns[$j]]['count'] / (float)$totalAnswersPerRow[$i];
//                }
//            }
        }


        $results = [];
        $results['results'] = $answers;
        $results['options'] = [];
        $results['options']['textBased'] = $textBased;
//        $results['options']['rows'] = $rows;
//        $results['options']['columns'] = $columns;

        return $results;
    }

}
