<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Object\Data\Field\Type;

use Lote\Module\Form\Entity\Form as FormEntity;
use Lote\Module\Form\Entity\Record as RecordEntity;
use SidraBlue\Lote\Entity\CfField as CfFieldEntity;
use SidraBlue\Lote\Entity\CfValue;
use SidraBlue\Lote\Entity\File as FileEntity;
use SidraBlue\Lote\Event\Data;
use SidraBlue\Lote\Service\Timezone;

/**
 * Class Media
 * @package SidraBlue\Lote\Object\Data\Field\Type
 */
class Media extends Base
{
    const FIELD_ERROR_FILE_IN_PROGRESS = 300;
    const FIELD_ERROR_FILE_DOES_NOT_EXIST = 301;
    const FIELD_ERROR_FILE_DOES_NOT_MATCH = 302;

    /**
     * Set current value from existing value object if current value not already set
     *
     * @access public
     * @param CfValue $value - Existing cf value
     * @return void
     */
    public function setValueFromObject(CfValue $value)
    {
        if (!$this->value) {
            $this->value = $value->value_string;
        }
    }

    /**
     * Validate field
     *
     * @access public
     * @return boolean
     */
    public function validate()
    {
        return parent::validate();
    }

    /**
     * Validate the input for the file input
     *
     * @access protected
     * @return boolean
     **/
    protected function validateFieldInputData()
    {
        $result = true;
        if ($this->value == 'in-progress') {
            $result = false;
            $this->error = self::FIELD_ERROR_FILE_IN_PROGRESS;
        } else {
            $value = json_decode($this->value, 1);
            foreach ($value as $uuid => $fileId) {
                $fe = new FileEntity($this->getState());
                if ($fe->load($fileId)) {
                    if (preg_match("/record\/[a-zA-Z0-9]+\/field\/[0-9]+\/{$uuid}/", $fe->object_key) != 1) {
                        $result = false;
                        $this->error = self::FIELD_ERROR_FILE_DOES_NOT_MATCH;
                    }
                } else {
                    $result = false;
                    $this->error = self::FIELD_ERROR_FILE_DOES_NOT_EXIST;
                }
            }
        }

        return $result;
    }

    /**
     * Retrieve media view data
     *
     * @access public
     * @param int $objectId - Object ID
     * @return mixed
     */
    public function getViewData($objectId = 0)
    {
        $viewData = json_decode(parent::getViewData($objectId), 1);
        foreach ($viewData as $index => $fileId) {
            $fe = new \SidraBlue\Lote\Entity\File($this->getState());
            $fe->load($fileId);
            $viewData[$index] = $fe->getViewData();
        }
        return $viewData;
    }

    /**
     * Get the error code text of the current field, if one is specified
     *
     * @access public
     * @return string
     * */
    public function getErrorText()
    {
        if ($this->error == self::FIELD_ERROR_FILE_IN_PROGRESS) {
            $text = "File upload in progress";
        } elseif ($this->error == self::FIELD_ERROR_FILE_DOES_NOT_EXIST) {
            $text = "Selected file does not exist";
        } elseif ($this->error == self::FIELD_ERROR_FILE_DOES_NOT_MATCH) {
            $text = "Selected file does not match the given UUID";
        } else {
            $text = parent::getErrorText();
        }
        return $text;
    }

    /**
     * Action functionality after field save
     *
     * @access protected
     * @param CfValue $cfValue - Custom field value
     * @param bool $setByText
     * @param array $fieldDeletedInfo
     */
    protected function afterCfValueSave(CfValue $cfValue, $setByText, $fieldDeletedInfo)
    {
        $files = $this->getViewData();
        foreach ($files as $file) {
            $fe = new FileEntity($this->getState());
            if ($fe->load($file['id'])) {
                $this->dispatchFieldFileSaveEvent($cfValue->object_id, $cfValue->field_id, $fe);
            }
        }
    }

    /**
     * Dispatch field file save event
     *
     * @access protected
     * @param int $recordId - the ID of the record
     * @param int $fieldId - the ID of the field
     * @param FileEntity $fe - File entity
     * @return void
     */
    protected function dispatchFieldFileSaveEvent($recordId, $fieldId, FileEntity $fe)
    {
        $re = new RecordEntity($this->getState());
        $re->load($recordId);
        $foe = new FormEntity($this->getState());
        $foe->load($re->form_id);
        $cffe = new CfFieldEntity($this->getState());
        $cffe->load($fieldId);

        $configData = json_decode($cffe->config_data, 1);
        if (isset($configData['is_sharepoint']) && $configData['is_sharepoint'] && isset($configData['sharepoint_folder']) && $configData['sharepoint_folder']) {
            $date = new Timezone($this->getState());
            $created = $date->convertToDisplay($re->lote_created, "Y-m-d_H-i-s");

            $directoryPath = "{$configData['sharepoint_folder']}/form/{$foe->name}/{$created}__{$recordId}/{$cffe->name}/";
            $directoryPath = preg_replace('/[^A-Za-z0-9_\- \/]/', '_', $directoryPath);

            $data = new Data();
            $data->setData([
                'file_id' => $fe->id,
                'destination_path' => $directoryPath,
            ]);
            $this->getState()->getEventManager()->dispatch('form.field.file.save', $data);
        }
    }
}
