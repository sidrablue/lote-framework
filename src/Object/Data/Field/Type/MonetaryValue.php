<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Object\Data\Field\Type;

class MonetaryValue extends Number
{

    public function validate()
    {
        $result = parent::validate();
        if($result && $this->minValue() !== false && $this->getValue() < $this->minValue()) {
            $result = false;
            $this->error = self::FIELD_ERROR_VALUE_TOO_SMALL;
        }
        else if($result && $this->maxValue() !== false && $this->getValue() > $this->maxValue()) {
            $result = false;
            $this->error = self::FIELD_ERROR_VALUE_TOO_LARGE;
        }
        return $result;
    }

    public function decimalPlaces()
    {
        return 2;
    }

    public function getValue()
    {
        return floatval(str_replace([' ',',', '$','£'], ['','', '',''], $this->value));
    }

}
