<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Object\Data\Field\Type;

class Number extends Base
{

    /**
     * @var boolean $supportsImporting
     * True if this object can be used in imports
     * */
    protected $supportsImporting = true;

    /**
     * @var boolean $supportsQueryBuilder
     * True if this object can be used in the query builder
     * */
    protected $supportsQueryBuilder = true;

    const FIELD_ERROR_VALUE_BAD_FOORMAT = 100;
    const FIELD_ERROR_VALUE_TOO_SMALL = 101;
    const FIELD_ERROR_VALUE_TOO_LARGE = 102;
    const FIELD_ERROR_VALUE_TOO_MANY_DP = 103;

    public function getValue()
    {
        return floatval(str_replace(',', '', str_replace(' ', '', parent::getValue())));
    }

    public function validate()
    {
        $result = parent::validate();
        if($result && ( (strlen($this->value)>0 && $this->value !== '0' && $this->getValue()==0) || (preg_match('/[a-z]/i',$this->value)) ) ) {
            $result = false;
            $this->error = self::FIELD_ERROR_VALUE_BAD_FOORMAT;
        }
        if($result && $this->minValue() !== false && $this->getValue() < $this->minValue()) {
            $result = false;
            $this->error = self::FIELD_ERROR_VALUE_TOO_SMALL;
        }
        else if($result && $this->maxValue() !== false && $this->getValue() > $this->maxValue()) {
            $result = false;
            $this->error = self::FIELD_ERROR_VALUE_TOO_LARGE;
        }
        else if($result && $this->decimalPlaces() !== false && strpos($this->getValue(), '.') !== false) {
            $val = explode(".", $this->getValue());
            if(isset($val[1]) && strlen($val[1]) > $this->decimalPlaces()) {
                $result = false;
                $this->error = self::FIELD_ERROR_VALUE_TOO_MANY_DP;
            }
        }
        return $result;
    }

    public function decimalPlaces()
    {
        $result = false;
        if (isset($this->configData['decimal_places']) && (!empty($this->configData['decimal_places']) || $this->configData['decimal_places']==='0')) {
            $result = $this->configData['decimal_places'];
        }
        return $result;
    }

    public function minValue()
    {
        $result = false;
        if (isset($this->configData['minimum_value']) && !empty($this->configData['minimum_value'])) {
            $result = $this->configData['minimum_value'];
        }
        return $result;
    }

    public function maxValue()
    {
        $result = false;
        if (isset($this->configData['maximum_value']) && !empty($this->configData['maximum_value'])) {
            $result = $this->configData['maximum_value'];
        }
        return $result;
    }

    /**
     * Get the error text
     * @access public
     * @return string
     * */
    public function getErrorText()
    {
        if($this->error == self::FIELD_ERROR_VALUE_BAD_FOORMAT) {
            $text = "Invalid number format";
        }
        elseif($this->error == self::FIELD_ERROR_VALUE_TOO_SMALL) {
            $text = "This value is too small, the minimum allowed value is ".$this->minValue();
        }
        elseif($this->error == self::FIELD_ERROR_VALUE_TOO_LARGE) {
            $text = "This value is too large, the minimum allowed value is ".$this->maxValue();
        }
        elseif($this->error == self::FIELD_ERROR_VALUE_TOO_MANY_DP) {
            $text = "Too many decimal places, only up to ".$this->decimalPlaces()." decimal places are allowed";
        }
        else {
            $text = parent::getErrorText();
        }
        return $text;
    }

    /**
     * Get the query builder clause for this field type
     * @access public
     * @param string $joinTable
     * @param string $value
     * @param string $labelPrefix
     * @return array
     * */
    public function getQueryBuilderClause($joinTable, $value, $labelPrefix)
    {
        return [
            'field' => 'value_string',
            'field_type' => 'custom_field',
            'data_type' => 'string',
            'label' => $labelPrefix . ' : ' . $this->definition->name,
            'clause_type' => 'join',
            'join_table' => $joinTable,
            'join_field_from' => 'object_id',
            'join_field_to' => 'id',
            'join_clause_field_from' => 'field_id',
            'join_clause_operator' => 'equals',
            'join_clause_value' => $value,
        ];
    }

}
