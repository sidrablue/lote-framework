<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Object\Entity;

use Doctrine\DBAL\Query\QueryBuilder;
use SidraBlue\Lote\Entity\Audit\ObjectLog;
use SidraBlue\Lote\Entity\User;
use SidraBlue\Lote\Model\Group;
use SidraBlue\Lote\State\Web;
use SidraBlue\Util\Time;
use SidraBlue\Lote\Object\StateDb as StateDbBase;

/**
 * Base class for all entities
 */
abstract class Base extends StateDbBase
{

    /**
     * @var int $id
     * The id of the table row
     * @dbColumn id
     * @dbType integer
     * @dbOptions autoincrement = true
     * @dbKey id
     * */
    public $id;

    /**
     * @var int $id
     * The id of the table row
     * */
    public $entityData;

    /**
     * @var ObjectLog $auditObjectLog
     * The id of the table row
     * */
    public $auditObjectLog;

    /**
     * @var string $tableName
     * The name of the table to override the class name if required
     * */
    protected $tableName;

    /**
     * @var boolean $hasCustomFields
     * True if this entity has custom fields
     * */
    protected $hasCustomFields = false;

    /**
     * @var boolean $isAudited
     * True if this entity is audited
     * */
    protected $isAudited = false;

    /**
     * @var boolean $ignoreModifications
     * True if any modifications to this entity are to be ignored, and therefore will not result in "dirty" fields
     * */
    protected $ignoreModifications = false;

    /**
     * @var array $_privateProperties
     * Properties associated to the class and not to the data
     * */
    protected $privateProperties = [
        'privateProperties',
        'entityProperties',
        'auditObjectLog',
        'modifiedProperties',
        'ignoreModifications',
        'isAudited',
        'tableName',
        'requestState',
        'hasCustomFields',
        'entityData',
        'customData',
        'customDataReference',
        'childFields',
        'fields',
        '_custom',
        'readDb',
        'writeDb'
    ];

    /**
     * The properties of this entity which are defined for programming purposes, but are unset at the point of
     * construction in order to track changes
     * */
    protected $entityProperties;

    /**
     * Properties that have been modified for this entity
     * */
    protected $modifiedProperties = [];

    /**
     * @var string $lote_created - the time that this row was created. This is a \DateTime compatible 'c' format value
     * @dbColumn lote_created
     * @dbType datetime
     * @dbOptions notnull = false
     */
    public $lote_created;

    /**
     * @var string $lote_updated - the last time that this row was updated. This is a \DateTime compatible 'c' format value
     * @dbColumn lote_updated
     * @dbType datetime
     * @dbOptions notnull = false
     */
    public $lote_updated;

    /**
     * @var string $lote_deleted - the time that this row was deleted. This is a \DateTime compatible 'c' format value
     * @dbColumn lote_deleted
     * @dbType datetime
     * @dbOptions notnull = false
     * @dbIndex true
     */
    public $lote_deleted;

    /**
     * @var int $lote_deleted_by - the user that deleted this object
     * @dbColumn lote_deleted_by
     * @dbType integer
     * @dbOptions notnull = false
     */
    public $lote_deleted_by;

    /**
     * @var string $lote_author_id - the ID of the user who created this object
     * @dbColumn lote_author_id
     * @dbType integer
     * @dbOptions notnull = false
     */
    public $lote_author_id;

    /**
     * @var int $lote_access - the access for this entity
     * @dbColumn lote_access
     * @dbType integer
     * @dbOptions notnull = false, default = 0, length = 4
     */
    public $lote_access;


    /**
     * @param Web $state
     * @param int|array $data
     * */
    public function __construct($state = null, $data = null)
    {
        parent::__construct($state);
        $this->beforeCreate();
        $this->unsetEntityProperties();
        if (is_numeric($data) && $data > 0) {
            $this->load($data);
        } elseif (is_array($data)) {
            $this->setData($data);
        }
        $this->afterCreate();
    }

    /**
     * Unset the entity properties for the purpose of tracking changes via magic methods, but still having them
     * available for programming purposes
     * @access protected
     * @return void
     * */
    protected function unsetEntityProperties()
    {
        $reflectionClass = new \ReflectionClass(get_called_class());
        foreach ($reflectionClass->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
            if (array_search($property->name, $this->privateProperties) === false && $property->name != 'id') {
                $this->entityProperties[] = $property->name;
                unset($this->{$property->name});
            }
        }
    }

    /**
     * Before create event handler to be implemented in the child
     * @access protected
     * @return void
     * */
    protected function beforeCreate()
    {

    }

    /**
     * After create event handler to be implemented in the child
     * @access protected
     * @return void
     * */
    protected function afterCreate()
    {

    }

    /**
     * Set the data for this object
     * @param array $data
     * @return void
     * */
    public function setData($data)
    {
        if (is_array($data) && count($data) > 0) {
            foreach ($data as $k => $v) {
                $this->entityData[$k] = $v;
                if ($k == 'id') {
                    $this->$k = $v;
                }
                if (!$this->ignoreModifications) {
                    $this->modifiedProperties[$k] = $v;
                }
            }
        }
    }

    /**
     * Get the data for this object as an array
     * @return array
     * */
    public function getData()
    {
        $fields = $this->getFields();
        $result = [];
        foreach ($fields as $v) {
            if (is_string($v)) {
                if (isset($this->entityData[$v])) {
                    $result[$v] = $this->entityData[$v];
                } else {
                    $result[$v] = null;
                }
            }
        }
        return $result;
    }

    /**
     * Get the edit for this object as an array
     * @return array
     * */
    public function getEditData()
    {
        $data = $this->getData();
        if (property_exists($this, 'lote_access') && $data['lote_access'] == -1) {
            $data['_lote_access_groups'] = $this->getEntityAccess();
        }
        return $data;
    }

    /**
     * Get the edit for this object as an array
     * @return array
     * */
    public function getViewData()
    {
        $data = $this->getData();
        if (property_exists($this, 'lote_access') && $data['lote_access'] == -1) {
            $data['_lote_access_groups'] = $this->getEntityAccess();
        }
        return $data;
    }

    /**
     * @param mixed $tableName
     */
    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return !empty($this->tableName) ? $this->tableName : get_called_class();
    }

    /**
     * Get all class properties which should map to database fields
     * @return array
     */
    protected function getFields()
    {
        $fields = [];
        $reflectionClass = new \ReflectionClass(get_called_class());
        foreach ($reflectionClass->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
            if (array_search($property->name, $this->privateProperties) === false) {
                $fields[] = $property->name;
            }
        }
        return $fields;
    }

    /**
     * Check if a default value exists on a field that is not 'null'
     * @param string $fieldName - the name of the field in question
     * @return bool
     */
    protected function hasDefaultFieldValue($fieldName)
    {
        return $this->getDefaultFieldValue($fieldName) !== null;
    }

    /**
     * Get all class properties which should map to database fields
     * @param string $fieldName - the name of the field in question
     * @return mixed
     */
    protected function getDefaultFieldValue($fieldName)
    {
        $result = null;
        $reflectionClass = new \ReflectionClass(get_called_class());
        $properties = $reflectionClass->getDefaultProperties();
        if(array_key_exists($fieldName, $properties)) {
            $result = $properties[$fieldName];
        }
        return $result;
    }

    /**
     * Get all class properties which should map to database fields
     * @return array
     */
    protected function getSaveFields()
    {
        $result = [];
        foreach ($this->getFields() as $v) {
            if (in_array($v, $this->entityProperties)) {
                $result[] = $v;
            }
        }
        return $result;
    }

    /**
     * Get the value of a specific field for this model
     * @param string $fieldName
     * @param mixed $default
     * @access public
     * @return string
     * */
    public function getField($fieldName, $default = '')
    {
        $result = $default;
        if (isset($this->{$fieldName})) {
            $result = $this->{$fieldName};
        }
        return $result;
    }

    /**
     * Get the base select query builder statement
     * @return QueryBuilder
     */
    protected function getSelect()
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select(implode(', ', self::getFields()))
            ->from($this->getTableName(), 't');
        return $q;
    }

    /**
     * Check if a field has been modified in this entity
     * @access private
     * @param string $fieldName
     * @return boolean
     * @todo - change the implementation of this...
     * */
    private function isDirtyField($fieldName)
    {
        return count($this->modifiedProperties) > 0 && array_key_exists($fieldName, $this->modifiedProperties);
    }

    /**
     * Save this object
     * @access public
     * @return int
     */
    public function save()
    {
        $fields = $this->getSaveFields();
        $data = [];

        foreach ($fields as $v) {
            if (!$this->id || $this->isDirtyField($v)) {
                if($this->isDirtyField($v)) {
                    $data[$v] = $this->$v;
                }
                elseif(!$this->id && $this->hasDefaultFieldValue($v)) {
                    $data[$v] = $this->getDefaultFieldValue($v);
                }
                else {
                    $data[$v] = $this->$v;// @todo - not sure if this part is really needed
                }
            }
        }

        if (isset($data['lote_access']) && is_array($data['lote_access'])) {
            $data['lote_access'] = '-1';
        }
        unset($data['id']);
        if ($this->id > 0) {
            $result = $this->id;
            $this->lote_updated = $data['lote_updated'] = Time::getUtcNow();
            $data = $this->beforeUpdate($data);
            $this->clearCachedData();
            if($data) {
                $this->logAudit(true);
                $this->getWriteDb()->update($this->getTableName(), $data, ['id' => $this->id]);
            } else {
                $result = false;
            }
        } else {
            if (!isset($data['lote_author_id']) && $this->getState()->getUser() && $this->getState()->getUser()->id > 0) {
                $data['lote_author_id'] = $this->getState()->getUser()->id;
            }
            $this->lote_created = $this->lote_updated = $data['lote_created'] = $data['lote_updated'] = Time::getUtcNow();
            $data = $this->beforeInsert($data);
            $this->getWriteDb()->insert($this->getTableName(), $data);
            $result = $this->id = $this->entityData['id'] = $data['id'] = $this->getWriteDb()->lastInsertId();
            $this->clearCachedData();
            $this->logAudit();
            $this->afterInsertData($data);
        }
        return $result;
    }

    /**
     * Clear any references to this data if it exists
     * @access private
     * @return void
     */
    private function clearCachedData()
    {
        $this->clearRedisCache();
        $this->clearDataCache();
        $this->onClearCachedData();
    }

    /**
     * Clear any references to this data if it exists
     * @access private
     * @return void
     */
    protected function updateCachedData()
    {
        $this->updateDataCache();
    }

    /**
     * Update the local data cache with the content of this entity
     * @access private
     * @return void
     * */
    protected function updateDataCache() {
        $this->getState()->getData()->setValue("entity:" . $this->getTableName(), $this->id, $this->getData());
        if ($this->location_reference) {
            $this->getState()->getData()->setValue("entity:" . $this->getTableName(), $this->location_reference,
                $this->getData());
        }
    }

    /**
     * Clear the local data cache for this entity
     * @access private
     * @return void
     * */
    private function clearDataCache() {
        $this->getState()->getData()->deleteValue("entity:" . $this->getTableName(), $this->id);
        if ($this->reference) {
            $this->getState()->getData()->deleteValue("entity:" . $this->getTableName(), $this->reference);
        }
        if (isset($this->location_reference)) {
            $this->getState()->getData()->deleteValue("entity:" . $this->getTableName(), $this->location_reference);
        }
    }

    /**
     * Clear the redis cache for this entity
     * @access private
     * @return void
     * */
    private function clearRedisCache() {
        $redisClient = $this->getState()->getRedisDbClient();
        if ($this->id && $redisClient->exists('entity:' . $this->getTableName() . ':' . $this->id)) {
            $redisClient->delete('entity:' . $this->getTableName() . ':' . $this->id);
        }
        if (isset($this->reference) && $redisClient->exists('entity:' . $this->getTableName() . ':' . $this->reference)) {
            $redisClient->delete('entity:' . $this->getTableName() . ':' . $this->reference);
        }
        if (isset($this->location_reference) && $redisClient->exists('entity:' . $this->getTableName() . ':' . $this->location_reference)) {
            $redisClient->delete('entity:' . $this->getTableName() . ':' . $this->location_reference);
        }
    }

    /**
     * Function to call in the child after clearing cached data
     * @access protected
     * @return array
     * */
    protected function onClearCachedData()
    {

    }

    /**
     * Function to call before inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeInsert($data)
    {
        return $data;
    }

    /**
     * Function to call after inserting data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function afterInsertData($data)
    {
        return $data;
    }

    /**
     * Function to call before updating data so that data can be manipulated if required
     * @param array $data
     * @return array
     * */
    protected function beforeUpdate($data)
    {
        return $data;
    }

    /**
     * Get a single object by id
     *
     * @param integer $id
     * @param bool $includeDeleted - Whether or not to include objects that have been deleted
     * @return int|bool
     */
    public function load($id, $includeDeleted = false)
    {
        $this->ignoreModifications = true;
        if ($id) {
            if($this->getState()->getData()->hasValue("entity:sb__file", $id)) {
                $this->setData($this->getState()->getData()->getValue("entity:" . $this->getTableName(), $id));
            }
            else {
                $q = $this->getReadDb()->createQueryBuilder();
                $q->select('t.*')
                    ->from($this->getTableName(), 't')
                    ->where('id = :id')
                    ->setParameter('id', $id);
                if (!$includeDeleted) {
                    $q->andWhere('lote_deleted is null');
                }

                $s = $q->execute();
                $this->setData($s->fetch(\PDO::FETCH_ASSOC));
                $this->updateDataCache();

            }
            $this->afterLoad();
        }
        $this->ignoreModifications = false;
        return $this->id;
    }

    /**
     * Reload the details of this entity
     * @access public
     * @return boolean
     * */
    public function reload()
    {
        $result = false;
        $id = $this->id;
        if ($id) {
            $this->reset();
            $result = $this->load($id) > 0;
            $this->getState()->getData()->setValue("entity:" . $this->getTableName(), $this->id, $this->getData());
        }
        return $result;
    }

    /**
     * Event handler to call after the load of an entity
     * */
    protected function afterLoad()
    {
        //implement in child
    }

    /**
     * Get a single object by a specific field
     *
     * @param string $fieldName
     * @param string $fieldValue
     * @param bool $includeDeleted - Whether or not to include objects that have been deleted
     * @return boolean
     */
    public function loadByField($fieldName, $fieldValue, $includeDeleted = false)
    {
        return $this->loadByFieldBase($fieldName, $fieldValue, false, $includeDeleted);
    }

    /**
     * Get a single object by a specific field using a binary check
     *
     * @param string $fieldName
     * @param string $fieldValue
     * @param bool $includeDeleted - Whether or not to include objects that have been deleted
     * @return boolean
     */
    public function loadByBinaryField($fieldName, $fieldValue, $includeDeleted = false)
    {
        return $this->loadByFieldBase($fieldName, $fieldValue, true, $includeDeleted);
    }

    /**
     * Get a single object by a specific field with an optional binary match flag
     *
     * @param string $fieldName
     * @param string $fieldValue
     * @param bool $binaryMatch - Whether or not to match the binary value
     * @param bool $includeDeleted - Whether or not to include objects that have been deleted
     * @return boolean
     * */
    private function loadByFieldBase($fieldName, $fieldValue, $binaryMatch = false, $includeDeleted = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*')
            ->from($this->getTableName(), 't');
        if ($binaryMatch) {
            $q->where('binary ' . $fieldName . ' = :' . $fieldName);
        } else {
            $q->where($fieldName . ' = :' . $fieldName);
        }

        $q->setParameter($fieldName, $fieldValue);
        if (!$includeDeleted) {
            $q->andWhere('lote_deleted is null');
        }
        $s = $q->execute();
        $data = $s->fetch(\PDO::FETCH_ASSOC);
        $this->setData($data);
        $this->getState()->getData()->setValue("entity:" . $this->getTableName(), $this->id, $this->getData());

        return $data;
    }

    /**
     * Get a single object by specific fields
     *
     * @param array $fieldNameValues
     * @param bool $includeDeleted - Whether or not to include objects that have been deleted
     * @return boolean
     */
    public function loadByFields($fieldNameValues, $includeDeleted = false)
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*')
            ->from($this->getTableName(), 't');
        foreach ($fieldNameValues as $fn => $fv) {
            if ($fv === null) {
                $q->andWhere($fn . ' is null');
            } else {
                $q->andWhere($fn . ' = :' . $fn)
                    ->setParameter($fn, $fv);
            }
        }

        if (!$includeDeleted) {
            $q->andWhere('lote_deleted is null');
        }
        $s = $q->execute();
        $data = $s->fetch(\PDO::FETCH_ASSOC);
        $this->setData($data);
        return $data;
    }

    /**
     * Update properties for this object
     * @param array $properties
     * @return boolean
     */
    public function update(Array $properties)
    {
        $result = false;
        if ($this->id > 0) {
            $params = [];
            foreach ($properties as $key => $value) {
                if (property_exists($this, $key)) {
                    $this->{$key} = $value;
                    $params[$key] = $value;
                }
            }
            $this->getWriteDb()->update($this->getTableName(), $params, ['id' => $this->id]);
            $result = true;
        }
        return $result;
    }


    /**
     * Update a single property
     *
     * @param string $key Property name
     * @param mixed $value Property value
     *
     * @return boolean
     */
    public function updateProperty($key, $value)
    {
        $result = false;
        if ($this->id > 0 && property_exists($this, $key)) {
            $this->{$key} = $value;
            $this->getWriteDb()->update($this->getTableName(), [$key => $value], ['id' => $this->id]);
            $this->save();
            $result = true;
        }
        return $result;
    }

    /**
     * Delete this object
     * @param bool $strongDelete - Whether or not to delete the table object completely
     * @return boolean
     */
    public function delete($strongDelete = false)
    {
        if ($strongDelete) {
            $result = $this->getWriteDb()->delete($this->getTableName(), ['id' => $this->id]) > 0;
        } else {
            $data = ['lote_deleted' => Time::getUtcNow(), 'lote_deleted_by' => $this->getState()->getUser()->id];
            $result = $this->getWriteDb()->update($this->getTableName(), $data, ['id' => $this->id]) > 0;
        }
        $this->clearCachedData();
        $this->reset();
        return $result;
    }

    /**
     * Get the access for this entity
     * @access protected
     * @return array
     *  */
    protected function getEntityAccess()
    {
        $result = [];
        if ($this->id) {
            $q = $this->getReadDb()->createQueryBuilder();
            $q->select('a.*')
                ->from('sb__access_entity', 'a')
                ->andWhere('object_ref = :object_ref')
                ->setParameter('object_ref', $this->getTableName())
                ->andWhere('object_id = :object_id')
                ->setParameter('object_id', $this->id)
                ->andWhere("a.lote_deleted is null")
                ->orderBy("id" ,"asc");
            foreach ($q->execute()->fetchAll(\PDO::FETCH_ASSOC) as $row) {
                if($row['access_type']=='group') {
                    $g = new \SidraBlue\Lote\Entity\Group($this->getState());
                    $g->load($row['access_type_id']);
                    $row['_group'] = $g;
                }
                $result[] = $row;
            }
        }
        return $result;
    }

    /**
     * Reset this entity to its original state
     * @access public
     * @return void
     * @todo - reset all of the fields to their default state
     * */
    public function reset()
    {
        $this->id = 0;
        $this->modifiedProperties = [];
        $this->unsetEntityProperties();
    }

    /**
     * Get the core list of core fields for this entity
     * @return array
     * */
    public function getCoreFields()
    {
        die('base function for getCoreFields in the Object Entity has not yet been implemented');
        return [];
    }

    public function checkFolderAccessInCrm($lotePath)
    {
        $return = true;
        $pathArr = explode("/", $lotePath);
        $rootId = $this->getState()->getSettings()->get('crm.document.root.id');
        if ($rootId && is_numeric($rootId)) {
            if (!in_array($rootId, $pathArr)) {
                $return = false;
            }
        }

        return $return;
    }

    /**
     * Get a single object by a specific field
     *
     * @deprecated Use model for multi-row actions
     * @param string $fieldName
     * @param string $fieldValue
     * @param bool $includeDeleted - Whether or not to include objects that have been deleted
     * @return boolean|array
     */
    public function listByField($fieldName, $fieldValue, $includeDeleted = false, $sortField = '', $sortOrder = '')
    {
        return $this->listByFields([$fieldName=>$fieldValue], $includeDeleted, $sortField, $sortOrder);
    }

    /**
     * Get a single object by a set of fields
     *
     * @deprecated Use model for multi-row actions
     * @param array $fieldNameValues - an array of field = value pairs
     * @param bool $includeDeleted - Whether or not to include objects that have been deleted
     * @param string $sortField - The field to sort by
     * @param string $sortOrder - The sort order
     * @return boolean|array
     */
    public function listByFields($fieldNameValues, $includeDeleted = false, $sortField = '', $sortOrder = 'asc')
    {
        $q = $this->getReadDb()->createQueryBuilder();
        $q->select('t.*')
            ->from($this->getTableName(), 't');

        foreach ($fieldNameValues as $fn => $fv) {
            $q->andWhere($fn . ' = :' . $fn)
                ->setParameter($fn, $fv);
        }

        if (!$includeDeleted) {
            $q->andWhere('lote_deleted is null');
        }
        if (!empty($sortField) && !empty($sortOrder)) {
            $q->orderBy($sortField, $sortOrder);
        }

        $s = $q->execute();
        $data = $s->fetchAll(\PDO::FETCH_ASSOC);
        $this->setData($data);
        return $data;
    }

    /**
     * Log changes to this entity
     * @param bool $isUpdate
     * @return null|ObjectLog
     * */
    protected function logAudit($isUpdate = true)
    {
        $result = null;
        if ($this->isAudited) {
            if (count($this->modifiedProperties) > 0) {
                $a = $this->getState()->getAudit();
                $oldObject = null;
                if ($isUpdate && $this->id > 0) {
                    $oldObject = clone $this;
                    $oldObject->load($this->id);
                }
                $this->auditObjectLog = $a->addObjectLog($this, $oldObject);
            }
        }
        return $result;
    }

    public function __set($key, $value)
    {
        if (in_array($key, $this->entityProperties)) {
            if($value !== $this->getDefaultFieldValue($key) || ($value!==null && !isset($this->$key)) || (isset($this->$key) && $this->$key !== $value) ) {
                if (!$this->ignoreModifications) {
                    $this->modifiedProperties[$key] = $value;
                }
            }
           /* if (($value!==null && !isset($this->$key)) || (isset($this->$key) && $this->$key != $value)) {
                if (!$this->ignoreModifications) {
                    $this->modifiedProperties[$key] = $value;
                }
            }*/
            $this->entityData[$key] = $value;

        } else {
            $this->$key = $value;
        }
    }

    private function getDefaultClassValue($param)
    {
        $result = null;
        $vars = get_class_vars(get_class(new \ReflectionClass(get_called_class())));
        if (isset($vars[$param])) {
            $result = $vars[$param];
        }
        return $result;
    }

    public function __get($key)
    {
        $result = $this->getDefaultClassValue($key);
        if (isset($this->entityData[$key])) {
            $result = $this->entityData[$key];
        }
        return $result;
    }

    public function __isset($key)
    {
        return array_search($key, $this->entityProperties) !== false;
    }

    public function __unset($key)
    {
        if (isset($this->entityData[$key])) {
            if (array_search($key, $this->privateProperties) === false && !$this->ignoreModifications) {
                $this->modifiedProperties[$key] = $this->entityData[$key];
            }
        }
        unset($this->entityData[$key]);
    }

    public function getModifiedProperties()
    {
        return $this->modifiedProperties;
    }

    public function setAuditing($audit)
    {
        $this->isAudited = $audit;
    }

    /**
     * Get the audit log
     * @return ObjectLog
     * */
    public function getAuditLog()
    {
        return $this->auditObjectLog;
    }

    public function markPropertyUnmodified($propertyName)
    {
        if (array_key_exists($propertyName, $this->modifiedProperties)) {
            unset($this->modifiedProperties[$propertyName]);
        }
    }


}
