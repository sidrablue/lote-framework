<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Object\Entity;

class Factory
{

    private static $referenceMap = [];

    /**
     * Create an instance of the view
     * @param $state \SidraBlue\Lote\State\Web
     * @param string $reference
     * @return Base
     * */
    public static function createInstance($state, $reference)
    {
        return; //todo
        $className = false;
        if(!$className) {
            $className = self::$referenceMap["html"];
            if ( array_key_exists($reference, self::$referenceMap)) {
                $className = self::$referenceMap[$reference];
            }
        }
        return new $className($state);
    }

    /**
     *
     * */
    public static function registerReference($reference, $object)
    {

    }

}
