<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Object;

use SidraBlue\Lote\State\Base;
use SidraBlue\Lote\State\Web;

/**
 * Base class for all state based objects
 */
class State
{

    /**
     * @var Web $requestState
     * @access private
     * The model state
     * */
    private $requestState;

    /**
     * Base constructor to define the state object
     * @access public
     * @param Base $state
     * */
    public function __construct(Base $state)
    {
        $this->requestState = $state;
        $this->onCreate();
    }

    /**
     * Get the state object
     * @access public
     * @return Web
     * */
    public function getState()
    {
        return $this->requestState;
    }

    /**
     * On create handler to be implemented in child
     * @access public
     * @return Web
     * */
    protected function onCreate()
    {

    }

}
