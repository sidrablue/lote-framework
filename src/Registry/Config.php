<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Registry;

use Pixel418\Iniliq\IniParser;

class Config
{

    /**
     * The account reference to use for loading of INI details
     * @param string $accountRef
     * */
    private $accountRef = false;

    /**
     * The config variables
     * @param array $config
     * */
    private $config = false;

    /**
     * Default constructor with the ability to specify an account reference
     * @param bool|string $accountRef
     */
    public function __construct($accountRef = false)
    {
       $this->accountRef = $accountRef;
    }

    /**
     * Get a config value
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public function get($key = null, $default = null)
    {
        if(!$this->config){
            $this->load();
        }
        //$result = $default;//$this->config;
        if($key) {
            if(isset($this->config[$key])) {
                $result = $this->config[$key];
            }
            elseif($default) {
                $result = $default;
            }
            else {
                $result = null;
            }
        }
        else {
            $result = $this->config;
        }
        return $result;
    }

    private function load()
    {
        $p = new IniParser();
        $accountFolder = 'account';
        if(defined('LOTE_TEST_MODE') && LOTE_TEST_MODE == true) {
            $accountFolder = 'account_test';
        }
        $this->config = $p->parse(array(LOTE_ASSET_PATH . "conf/base.ini"));
        if(isset($this->config['lote.mode']) || $this->getFramework()) {
            if($this->accountRef) {
                $this->config = $p->parse(array(
                    LOTE_ASSET_PATH . 'conf/'.$accountFolder.'/' . $this->accountRef . '.ini',
                    LOTE_ASSET_PATH . "conf/base.ini"
                ));
            }
            elseif(getenv('FRAMEWORK_REF')) {
                $this->config = $p->parse(array(
                    LOTE_ASSET_PATH . "conf/base.ini",
                    LOTE_ASSET_PATH . 'conf/' . $this->getFramework() . '.ini'
                ));
            }
            elseif(defined('LOTE_ACCOUNT_REF')) {
                if(file_exists(LOTE_ASSET_PATH . 'conf/'.$accountFolder.'/' . LOTE_ACCOUNT_REF . '.ini')) {
                    $this->config = $p->parse(array(
                        LOTE_ASSET_PATH . 'conf/'.$accountFolder.'/' . LOTE_ACCOUNT_REF . '.ini',
                        LOTE_ASSET_PATH . "conf/base.ini"
                    ));
                }
                elseif(file_exists(LOTE_ASSET_PATH . 'conf/' . LOTE_ACCOUNT_REF . '.ini')) {
                    $this->config = $p->parse(array(
                        LOTE_ASSET_PATH . 'conf/' . LOTE_ACCOUNT_REF . '.ini',
                        LOTE_ASSET_PATH . "conf/base.ini"
                    ));
                }
            }
            elseif(isset($this->config['lote.mode'])) {
                $this->config = $p->parse(array(
                    LOTE_ASSET_PATH . "conf/base.ini",
                    LOTE_ASSET_PATH . 'conf/' . $this->config['lote.mode'] . '.ini'
                ));
            }
            else {
                $this->config = $p->parse([LOTE_ASSET_PATH . "conf/base.ini"]);
            }
        }
    }

    public function getFramework()
    {
        if(getenv('FRAMEWORK_REF')) {
            $result = getenv('FRAMEWORK_REF');
        }
        else {
            $result = $this->get('framework.ref', 'sidrablue');
        }
        return $result;
    }

    public function isSchoolzine()
    {
        return strtolower($this->getFramework())=='schoolzine';
    }

    public function isSidraBlue()
    {
        return strtolower($this->getFramework())=='sidrablue';
    }

}
