<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Routing;

use Symfony\Component\Routing\Route as BaseRoute;

/**
 * Redis service class
 */
class Route extends BaseRoute
{
    
}
