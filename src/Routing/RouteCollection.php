<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Routing;

use Symfony\Component\Routing\RouteCollection as BaseRouteCollection;
use Symfony\Component\Routing\Route as BaseRoute;

/**
 * RouteCollection service class
 */
class RouteCollection extends BaseRouteCollection
{

    /**
     * Adds a route and throws an exception if the route already exists.
     *
     * @param string $name  The route name
     * @param BaseRoute  $route A Route instance
     * @throws \Exception $e
     */
    public function add($name, BaseRoute $route)
    {
        if (!$this->get($name)) {
            parent::add($name, $route);
        } else {
            throw new \Exception("Route '{$name}' is already defined");
        }
    }

    /**
     * Adds a route and throws an exception if the route already exists.
     *
     * @param $method - the method name that is to be called
     * @param $uriSubPath
     * @param $routeName
     * @param $controllerPath
     * @param array $requirements
     * @throws \Exception $e
     */
    public function addApiRoutes($uriSubPath, $method, $routeName, $controllerPath, $requirements = [])
    {
        /** @see \SidraBlue\Lote\Controller\Api */
        $defaults['_method'] = 'call';
        $defaults['controller'] = '\SidraBlue\Lote\Controller\Api';
        $defaults['_api_controller'] = $controllerPath;
        $defaults['_api_method'] = $method;
        $defaults['_is_api'] = true;
        $defaults['format'] = 'json';
        $requirements['lote_api_version'] = "(\d+)";
        $route = new Route('_api/v{lote_api_version}/'.$uriSubPath, $defaults, $requirements);
        $this->add('api.versioned.'.$routeName, $route);
        $route = new Route('_api/'.$uriSubPath, $defaults, $requirements);
        $this->add('api.latest.'.$routeName, $route);
    }

    /**
     * Adds a route and overrides it if it already exists.
     *
     * @param string $name  The route name
     * @param BaseRoute  $route A Route instance
     * @throws \Exception $e
     */
    public function addOverride($name, BaseRoute $route)
    {
        parent::add($name, $route);
    }

}
