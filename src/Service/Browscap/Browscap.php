<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Service\Browscap;

use SidraBlue\Lote\Service\Browscap\Parser\BrowscapPhp as BrowscapPhpParser;
use SidraBlue\Lote\Service\Browscap\Parser\Crossjoin as CrossjoinParser;
use SidraBlue\Lote\Service\Browscap\Parser\Base as BaseParser;

class Browscap
{

    /**
     * @var string $parser
     */
    private $parser;

    /**
     * @var string $browser
     */
    public $browser;

    /**
     * @var string $version
     */
    public $browserVersion;

    /**
     * @var string $deviceType
     */
    public $deviceType;

    /**
     * @var string $platform
     */
    public $platform;

    /**
     * @var string $platformVersion
     */
    public $platformVersion;

    /**
     * @var string $platformDescription
     */
    public $platformDescription;

    /**
     * @var string $isMobileDevice
     */
    public $isMobileDevice;

    /**
     * @var string $isTablet
     */
    public $isTablet;

    /**
     * Default constructor for the Browscap class
     * @access public
     * @param $parser
     * @return Browscap
     * */
    public function __construct($parser = false)
    {
        $this->parser = $parser;
        if (is_array($_SERVER) && isset($_SERVER['HTTP_USER_AGENT'])) {
            $userAgent = $_SERVER['HTTP_USER_AGENT'];
            $this->getParser()->parse($userAgent, $this);
        }
    }

    /**
     * Run and update of the data for the crossjoin browscap
     * @access public
     * @param bool $forceUpdate
     * @param bool $useHardcodedFallback
     * @return bool
     * */
    public function runUpdate($forceUpdate = false, $useHardcodedFallback = false)
    {
        $this->getParser()->update($forceUpdate, $useHardcodedFallback);
    }

    /**
     * Get the parser to use in this instance
     * @access private
     * @return BaseParser
     * */
    private function getParser()
    {
        if($this->parser == 'browscap' || (!$this->parser && class_exists("\\phpbrowscap\\Browscap"))) {
            $parser = new BrowscapPhpParser();
        }
        elseif($this->parser=='crossjoin' || (!$this->parser && class_exists("\\Crossjoin\\Browscap\\Browscap"))) {
            $parser = new CrossjoinParser();
        }
        else {
            $parser = new BaseParser();
        }
        return $parser;
    }

}
