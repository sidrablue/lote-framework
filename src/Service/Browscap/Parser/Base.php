<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Service\Browscap\Parser;

use SidraBlue\Lote\Service\Browscap\Browscap as SbBrowscap;

class Base
{

    /**
     * Parse the details using crossjoin PHP
     * @access private
     * @param string $userAgent
     * @return void
     * */
    public function parse($userAgent, SbBrowscap $sbc) {

    }

    /**
     * Run and update of the data for the crossjoin browscap
     * @access public
     * @param bool $forceUpdate
     * @return bool
     * */
    public function update($forceUpdate = false) {

    }

}