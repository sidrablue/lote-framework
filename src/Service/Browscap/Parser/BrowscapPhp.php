<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Service\Browscap\Parser;

use phpbrowscap\Browscap as BrowscapBase;
use SidraBlue\Lote\Service\Browscap\Browscap as SbBrowscap;

class BrowscapPhp extends Base
{

    /**
     * Get the cache path
     * @access private
     * @return string
     * */
    private function getCachePath()
    {
        return LOTE_CORE_PATH . 'res/meta/browscap';
    }

    /**
     * Parse the details using crossjoin PHP
     * @access private
     * @param string $userAgent
     * @return void
     * */
    public function parse($userAgent, SbBrowscap $sbc)
    {
        $browscap = new BrowscapBase($this->getCachePath());
        $browscap->doAutoUpdate = false;
        $details = $browscap->getBrowser($userAgent);

        if (isset($details->Browser)) {
            $sbc->browser = $details->Browser;
        }
        if (isset($details->Version)) {
            $sbc->browserVersion = $details->Version;
        }
        if (isset($details->Device_Type)) {
            $sbc->deviceType = $details->Device_Type;
        }
        if (isset($details->Platform)) {
            $sbc->platform = $details->Platform;
        }
        if (isset($details->Platform_Version)) {
            $sbc->platformVersion = $details->Platform_Version;
        }
        if (isset($details->Platform_Description)) {
            $sbc->platformDescription = $details->Platform_Description;
        }
    }

    /**
     * Run and update of the data for the crossjoin browscap
     * @access public
     * @param bool $forceUpdate
     * @param bool $useHardcodedFallback
     * @return bool
     * */
    public function update($forceUpdate = false, $useHardcodedFallback = false)
    {
        ini_set('memory_limit', '2056M');
        ini_set('max_execution_time', '1200');
        \SidraBlue\Util\Dir::make($this->getCachePath());
        $browscap = new BrowscapBase($this->getCachePath());
        $browscap->updateCache();
    }

}