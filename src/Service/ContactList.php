<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Service;

use Doctrine\DBAL\ConnectionException;
use SidraBlue\Lote\Entity\Group as GroupEntity;
use SidraBlue\Lote\Model\Group as GroupModel;
use SidraBlue\Lote\Model\UserGroup;
use SidraBlue\Lote\Object\State as BaseState;
use SidraBlue\Util\Time;

/**
 * Base class for all models
 */
class ContactList extends BaseState
{

    public function regenerateAllMembers()
    {
        $gm = new GroupModel($this->getState());
        foreach ($gm->getAllContactLists() as $list) {
            /** @var GroupEntity $list */
            $this->regenerateGroupMembers($list);
        }
    }

    public function regenerateGroupMembersById($groupId)
    {
        $result = 0;
        $e = new GroupEntity($this->getState());
        if ($e->load($groupId)) {
            $result = $this->regenerateGroupMembers($e);
        }
        return $result;
    }

    public function regenerateGroupMembers(GroupEntity $group)
    {
        $gm = new UserGroup($this->getState());
        $membersByQuery = $this->getAllMemberByQuery($group->list_sql);
        $membersInDb = $gm->getAllGroupUserIds($group->id);
        $addedMembers = array_diff($membersByQuery, $membersInDb);
        $deletedMembers = array_diff($membersInDb, $membersByQuery);
        $this->addGroupMembers($group->id, $addedMembers);
        $this->deleteGroupMembers($group->id, $deletedMembers);
        $group->members_generated_at = Time::getUtcNow();
        $group->save();
    }

    private function addGroupMembers($groupId, $addedMemberUserIds)
    {
        try {
            $this->getState()->getWriteDb()->beginTransaction();
            $sql = "insert into sb__user_group (user_id, group_id, lote_created, lote_updated) values (?, ?, ?, ?)";
            $s = $this->getState()->getWriteDb()->prepare($sql);
            $added = Time::getUtcNow();
            foreach ($addedMemberUserIds as $userId) {
                $s->execute([$userId, $groupId, $added, $added]);
            }
            $this->getState()->getWriteDb()->commit();
        } catch (\Exception $e) {
            try {
                $this->getState()->getWriteDb()->rollBack();
            } catch (ConnectionException $e) {
                //do nothing
            }
        }
    }

    private function deleteGroupMembers($groupId, $deletedMemberUserIds)
    {
        try {
            $this->getState()->getWriteDb()->beginTransaction();
            $sql = "update sb__user_group set lote_deleted = ? where group_id = ? and user_id = ? and lote_deleted is null";
            $s = $this->getState()->getWriteDb()->prepare($sql);
            $deleted = Time::getUtcNow();
            foreach ($deletedMemberUserIds as $userId) {
                $s->execute([$deleted, $groupId, $userId]);
            }
            $this->getState()->getWriteDb()->commit();
        } catch (\Exception $e) {
            try {
                $this->getState()->getWriteDb()->rollBack();
            } catch (ConnectionException $e) {
                //do nothing
            }
        }
    }

    private function getAllMemberByQuery($sql)
    {
        $result = [];
        try {
            $statement = $this->getState()->getReadDb()->executeQuery($sql);
            while ($row = $statement->fetch()) {
                $result[] = $row['id'];
            }
        } catch (\Exception $e) {

        }
        return $result;
    }

}
