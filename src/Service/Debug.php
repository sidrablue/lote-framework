<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Service;

use Lote\Module\Content\Entity\Holder;
use Lote\Module\Form\Entity\Form;
use Lote\Module\Form\Entity\RecordValue;
use Lote\Module\Page\Model\Item as PageModel;
use Lote\Module\Szschool\Model\Szschool as NewsletterModel;
use Lote\System\Client\Entity\Master\Account;
use Lote\System\Edm\Entity\Campaign\CampaignEdm;
use Lote\System\Schoolzine\Model\Newsletter as News;
use Lote\System\Schoolzine\Model\Newsletter;
use SidraBlue\Lote\Entity\CfField;
use SidraBlue\Lote\Entity\File;
use SidraBlue\Lote\Entity\User;
use SidraBlue\Lote\Model\Url as UrlModel;
use SidraBlue\Lote\Object\Data\Field\Factory as FieldFactory;
use SidraBlue\Lote\Object\State as BaseState;
use SidraBlue\Lote\View\Transform\Html\Service as ContentService;
use SidraBlue\Util\Time;
use SidraBlue\Util\Url as UrlUtil;
use SidraBlue\Util\Xml;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Base class for state debug
 */
class Debug extends BaseState
{

    public function getLogArray($level)
    {
        $result = [];
        $result['type'] = $this->getState()->isCli()?"cli":"web";
        if(!$this->getState()->isCli()) {
            $result['url'] = $this->getState()->getUrl()->getHttpSchemeAndHost().ltrim($this->getState()->getRequest()->getRequestUri(), '/');
        }
        $result['reference'] = $this->getState()->accountReference;
        $result['view'] = $this->getState()->getView()->getDebugData($level);
        return $result;
    }

}
