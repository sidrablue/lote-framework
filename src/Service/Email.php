<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Service;

use Lote\System\Client\Entity\Master\Account;
use Mandrill;
use SidraBlue\Lote\State\Web;
use SparkPost\SparkPost as SparkPostBase;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use GuzzleHttp\Client;

class Email
{

    /**
     * @var Web
     * */
    private $state;

    /**
     * @param Web $state
     * */
    public function __construct($state)
    {
        $this->state = $state;
    }

    /**
     * @param string $subject
     * @param string $content
     * @param array|string $to
     * @param String $from
     * @param String $fromName
     * @param array $cc
     * @param array $bcc
     * @param array $attachments
     * @return Boolean
     */
    public function sendEmail($subject, $content, $to, $from, $fromName, $cc = [], $bcc = [], $attachments = [])
    {
        $result = false;
        if ($this->state->getSettings()->getSettingOrConfig('sparkpost.key')) {
            $result = $this->sendSparkpostEmail($subject, $content, $to, $from, $fromName, $cc, $bcc, $attachments);
        } elseif ($this->state->getSettings()->get('email.method') == 'mandrill-api') {
            $result = $this->sendMandrillEmail($subject, $content, $to, $from, $fromName, $cc, $bcc, $attachments);
        }
        return $result;
    }

    /**
     * Send an email through Sparkpost
     * @param string $subject
     * @param string $htmlContent
     * @param array|string $to
     * @param string $from
     * @param string $fromName
     * @param array $cc
     * @param array $bcc
     * @param array $attachments
     * @return Boolean
     */
    public function sendSparkpostEmail($subject, $htmlContent, $to, $from, $fromName, $cc = [], $bcc = [], $attachments = [])
    {
        $result = false;
        if ($sparkpostKey = $this->state->getSettings()->getSettingOrConfig('sparkpost.key')) {
            $sparkData = [];
            $sparkData['metadata'] = [
                'reference' => $this->state->accountReference,
                'campaign_id' => 0
            ];

            $options = [];
            $options['start_time'] = 'now';
            $options['open_tracking'] = false;
            $options['click_tracking'] = false;
            $options['sandbox'] = false;
            $options['inline_css'] = false;
            $options['transactional'] = true;
            $sparkData['options'] = $options;

            $content = [];
            $content['from']['name'] = $fromName;
            $content['from']['email'] = $from;
            $content['subject'] = $subject;

            $sps = new SparkPost($this->state);
            $content['html'] = $sps->escapeBrackets($htmlContent);;

            //Create and add attachments
            if (!empty($attachments)) {
                foreach ($attachments as $attachment) {
                    if (is_array($attachment)) {
                        if (isset($attachment['name']) && isset($attachment['type']) && isset($attachment['content'])) {
                            $content['attachments'][] = [
                                'type' => $attachment['type'],
                                'name' => $attachment['name'],
                                'data' => base64_encode($attachment['content'])
                                ];

                        }
                    }
                }
            }

            $sparkData['content'] = $content;

            $recipients = $this->formatToSparkEmailRecipient($to);
            $sparkData['recipients'] = $recipients;

            if (!empty($cc)) {
                $ccRecipients = $this->formatToSparkEmailRecipient($cc);
                $sparkData['cc'] = $ccRecipients;
            }
            if (!empty($bcc)) {
                $bccRecipients = $this->formatToSparkEmailRecipient($bcc);
                $sparkData['bcc'] = $bccRecipients;
            }

            $httpAdapter = new GuzzleAdapter(new Client(['verify' => false]));
            $sparky = new SparkPostBase($httpAdapter, ['key' => $sparkpostKey]);
            $sparky->setOptions(['async' => false]);
            try {
                $transmission = $sparky->transmissions->post($sparkData);
                $responseCode = $transmission->getStatusCode();
                $responseData = $transmission->getBody();
                if ($responseCode=='200' && isset($responseData['results']) && isset($responseData['results']['id'])) {
                    $result = true;
                }
            } catch (\Exception $e) {
                $this->state->getLoggers()->getMasterLogger("sparkpost")->warn("Message not sent", ['code' => $e->getCode(), 'message' => $e->getMessage(), 'sparkpost_data' => $sparkData]);
                $result = false;
            }
        }
        return $result;
    }

    /**
     * @param $to
     * @return array
     */
    private function formatToSparkEmailRecipient($to)
    {
        $recipients = [];
        if (is_string($to)) {
            $to = str_replace(' ', '', $to);
            if (strpos($to, ';') > 0) {
                $to = explode(';', $to);
            } elseif (strpos($to, ',') > 0) {
                $to = explode(',', $to);
            } else {
                $to = [$to];
            }
            foreach ($to as $toEmail) {
                if (filter_var($toEmail, FILTER_VALIDATE_EMAIL)) {
                    $recipient = ['address'];
                    $recipient['address'] = ['email' => $toEmail];
                    $recipients[] = $recipient;
                }
            }
        } elseif (is_array($to)) {
            foreach ($to as $k => $v) {
                $k = trim($k);
                $v = trim($v);
                $recipient = ['address'];
                if (filter_var($k, FILTER_VALIDATE_EMAIL)) {
                    $recipient['address']['email'] = $k;
                    $recipient['address']['name'] = $v;
                } elseif (filter_var($v, FILTER_VALIDATE_EMAIL)) {
                    $recipient['address']['email'] = $v;
                } else {
                    continue;
                }
                $recipients[] = $recipient;
            }
        }

        return $recipients;
    }

    /**
     * Send an email through Mandrill
     * @param string $subject
     * @param string $content
     * @param array $to
     * @param String $from
     * @param String $fromName
     * @param array $cc
     * @param array $bcc
     * @param array $attachments
     * @return Boolean
     */
    public function sendMandrillEmail($subject, $content, $to, $from, $fromName, $cc = [], $bcc = [], $attachments = [])
    {
        $result = false;
        $m = new Mandrill($this->state->getSettings()->get('email.mandrill.key'));

        $message = array(
            'html' => $content,
            'subject' => $subject,
            'from_email' => $from,
            'from_name' => $fromName,
            'headers' => array('Reply-To' => $from)
        );
        if ($this->state->getSettings()->get('email.mandrill.subaccount', false)) {
            $message['subaccount'] = $this->state->getSettings()->get('email.mandrill.subaccount');
        } elseif ($this->state->getParentDb() && $this->state->getSettings()->get('system.reference', false)) {
            $a = new Account($this->state);
            if ($a->loadByField('reference', $this->state->getSettings()->get('system.reference'))) {
                $message['subaccount'] = $a->mandrill_reference;
            }
        }
        //Create and add attachments
        if (!empty($attachments)) {
            foreach ($attachments as $attachment) {
                if (is_array($attachment)) {
                    if (isset($attachment['name']) && isset($attachment['type']) && isset($attachment['content'])) {
                        $message['attachments'][] = [
                            'type' => $attachment['type'],
                            'name' => $attachment['name'],
                            'content' => $attachment['content']
                        ];
                    }
                }
            }
        }
        if (is_string($to)) {
            $message['to'] = [];
            $to = str_replace(' ', '', $to);
            if (strpos($to, ';') > 0) {
                $to = explode(';', $to);
            } elseif (strpos($to, ',') > 0) {
                $to = explode(',', $to);
            } else {
                $to = [$to];
            }
            foreach ($to as $toEmail) {
                if (filter_var($toEmail, FILTER_VALIDATE_EMAIL)) {
                    $recipient = [];
                    $recipient['type'] = 'to';
                    $recipient['email'] = $toEmail;
                    $message['to'][] = $recipient;
                }
            }
            /*              $recipient = [];
                            $recipient['type'] = 'to';
                            $recipient['email'] = 'amel@sidrablue.com.au';
                            $message['to'][] = $recipient;*/
        } elseif (is_array($to)) {
            foreach ($to as $k => $v) {
                $k = trim($k);
                $v = trim($v);
                $recipient = [];
                $recipient['type'] = 'to';
                if (filter_var($k, FILTER_VALIDATE_EMAIL)) {
                    $recipient['email'] = $k;
                    $recipient['name'] = $v;
                } elseif (filter_var($v, FILTER_VALIDATE_EMAIL)) {
                    $recipient['email'] = $v;
                } else {
                    continue;
                }
                $message['to'][] = $recipient;
            }
        }
        curl_setopt($m->ch, CURLOPT_SSL_VERIFYPEER, false);
        if (count($message['to']) > 0) {
            $result = $m->messages->send($message);
        }
        return $result;
    }

    /**
     * Send an email through SendGrid
     * @param string $subject
     * @param string $content
     * @param array $to
     * @param String $from
     * @param String $fromName
     * @param array $cc
     * @param array $bcc
     * @param array $attachments
     * @param array $substitution
     * @return Boolean
     */
    public function sendSendGridEmail($subject, $content, $to, $from, $fromName, $cc = [], $bcc = [], $attachments = [], $substitution = [])
    {
        $result = false;
        if ($sendgridKey = $this->state->getSettings()->getSettingOrConfig('sendgrid.key')) {
            $sendgrid = new SendGrid($sendgridKey);
            $email = new SendGrid\Email();

            if (is_string($to)) {
                $to = str_replace(' ', '', $to);
                if (strpos($to, ';') > 0) {
                    $to = explode(';', $to);
                } elseif (strpos($to, ',') > 0) {
                    $to = explode(',', $to);
                } else {
                    $to = [$to];
                }
            }

            $email
                ->setTos($to)
                ->setFrom($from)
                ->setFromName($fromName)
                ->setSubject($subject)
                ->setHtml($content);
            if (!empty($cc)) {
                $email->setCcs($cc);
            }
            if (!empty($bcc)) {
                $email->setBccs($bcc);
            }

            // ---------- Example of substitutions -----
            // $substitution = ['%name%' => ["Renju", "Ignatious"], '%password%' => ["ren123", "ign123"]];
            if (!empty($substitution)) {
                $email->setSubstitutions($substitution);
            }

            // ---------- Example of attachments -----
            // $attachments = ["../path/to/file1.txt", "../path/to/file2.txt"];

            if (!empty($attachments)) {
                $email->setAttachments($attachments);
            }

            try {
                $result = $sendgrid->send($email);
            } catch (\Exception $e) {
                $result = false;
            }
        }
        return $result;
    }

}
