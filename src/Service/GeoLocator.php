<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Service;

class GeoLocator
{

    /**
     * @var Web
     * */
    private $state;

    /**
     * @param Web $state
     * @return Email
     * */
    public function __construct($state)
    {
        $this->state = $state;
    }

    /**
     * @param string $ipAddress
     * @return array
     */
    public function getLocationByIp($ipAddress)
    {
        $curl     = new \Geocoder\HttpAdapter\CurlHttpAdapter();
        $geoCoder = new \Geocoder\Provider\FreeGeoIpProvider($curl);
        return $geoCoder->getGeocodedData($ipAddress);
    }

}
