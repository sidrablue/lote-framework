<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Service;

use SidraBlue\Lote\Object\State as BaseState;
use SidraBlue\Lote\View\Transform\Html\Service;

/**
 * Base class for all models
 */
class Plugin extends BaseState
{

    private $plugins;

    public function registerPlugin($reference, $view, \Closure $function)
    {
        $this->plugins[$reference] = ['view' => $view, 'function' => $function];
    }

    public function renderPlugin($reference, $params)
    {
        $result = '';
        if(isset($this->plugins[$reference])) {
            $data = $this->plugins[$reference]['function']($params);
            $result = Service::renderView(
                $this->getState(),
                $this->plugins[$reference]['view'],
                $data
            );
        }
        return $result;
    }

}
