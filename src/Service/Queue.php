<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Service;

use Resque;
use SidraBlue\Lote\Object\State as BaseState;

/**
 * Base class for all models
 */
class Queue extends BaseState
{

    protected function onCreate()
    {
        parent::onCreate();
        Resque::setBackend($this->getState()->getConfig()->get('redis.host').':'.$this->getState()->getConfig()->get('redis.port'));
    }

    /**
     * Add a request to the queue
     * @access public
     * @param $queue
     * @param $class
     * @param array $args
     * @return string
     */
    public function add($queue, $class, $args = [])
    {
        return Resque::enqueue($queue, $class, $args);
    }

    /**
     * Add a request to the queue
     * @access public
     * @param $queue
     * @param $class
     * @param array $args
     * @return string
     */
    public function addManaged($queue, $class, $args = [], $objectId, $objectRef, $reference)
    {
        //$queueEntity = new Queue
        return Resque::enqueue($queue, $class, $args);
    }

}
