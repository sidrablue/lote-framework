<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Service;

use Predis\Client;
use Predis\Collection\Iterator;

use SidraBlue\Lote\Object\State as BaseState;

/**
 * Redis service class
 */
class Redis extends BaseState
{

    /**
     * @param Client $client
     * */
    private $client = null;

    /**
     * @param string $prefix
     * */
    private $prefix = "";

    /**
     * Get the PRedis client connection
     * @access public
     * @return Client
     * */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Check if the Redis DB cache is enabled
     * @access public
     * @return boolean
     * */
    public function isEnabled()
    {
        return $this->client instanceof Client;
    }

    /**
     * Setup the redis client connection with te provided parameters
     * @param array $config - required keys are 'host' and 'port'
     * @param string $prefix
     * @return void
     * */
    public function setup($config, $prefix = 'lote:')
    {
        if ($config) {
            try {
                $parameters = [];
                $parameters['host'] = $config['host'];
                $parameters['port'] = $config['port'];
                $parameters['scheme'] = isset($config['scheme']) ? $config['scheme'] : 'redis';
                if (isset($config['database']) && ($config['database'] >= 0 && $config['database'] <= 15)) {
                    $parameters['database'] = $config['database'];
                }
                $this->client = new Client($parameters, ['prefix' => $prefix]);

            } catch (\Exception $e) {
                $this->getState()->getLoggers()->getMasterLogger("lote")->error("Could not connect to redis instance", $config);
            }
        }
    }

    /**
     * Delete a key or set of keys
     * @param array|string $key
     * @return void
     */
    public function delete($key)
    {
        if ($this->client instanceof Client && is_string($key) || is_array($key)) {
            if (!is_array($key)) {
                $key = [$key];
            }
            $this->client->del($key);
        }
    }

    /**
     * Delete all keys in a matching pattern
     * @param string $key
     * @return string
     * */
    public function deleteNamespace($pattern)
    {
        if($items = $this->scan($pattern)) {
            $this->delete($items);
        }
    }

    /**
     * Set a value in the redis cache
     * @param string $key
     * @param string $value
     * @param int $expires
     * @return void
     */
    public function set($key, $value, $expires = 3600)
    {
        if ($this->client instanceof Client) {
            $this->client->set($key, json_encode($value));
            $this->client->expire($key, $expires);
        }
    }

    /**
     * Get a value from the redis cache
     * @param string $key
     * @param mixed $default
     * @return string
     * */
    public function get($key, $default = null)
    {
        $result = $default;
        if ($this->client instanceof Client) {
            $result = json_decode($this->client->get($key), true);
        }
        return $result;
    }

    /**
     * Check if a key exists in redis
     * @param string $key
     * @return string
     * */
    public function exists($key)
    {
        $result = false;
        if ($this->client instanceof Client) {
            $result = $this->client->exists($key);
        }
        return $result;
    }

    /**
     * Find all keys that match a specific pattern
     * @param string $key
     * @return string
     * */
    private function scan($pattern)
    {
        $result = [];
        if ($this->client instanceof Client) {
            $prefix = (string)$this->client->getOptions()->prefix;
            if(is_string($prefix) && $prefix) {
                foreach (new Iterator\Keyspace($this->client, $prefix ."". $pattern) as $key) {
                    $result[] = preg_replace("#^".$prefix."(.*)#","$1", $key);
                }
            }
        }
        return $result;
    }

}
