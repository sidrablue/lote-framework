<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Service;

use Predis\Client;
use SidraBlue\Lote\Object\State as BaseState;

/**
 * Redis service class
 */
class Sql extends BaseState
{

    /**
     * Setup the redis client connection with te provided parameters
     * @access public
     * @param $table
     * @param $field
     * @param $search
     * @param $replace
     * @return bool
     */
    public function replace($table, $field, $search, $replace)
    {
        try {
            $sql = "update {$table} set {$field} = replace({$field}, :search, :replace)";
            $this->getState()->getWriteDb()->executeQuery($sql, ['search' => $search, 'replace' => $replace]);
            $result = true;
        }
        catch(\Exception $e) {
            dump(func_get_args());
            dump($e->getMessage());
            $result = false;
        }
        return $result;
    }

}
