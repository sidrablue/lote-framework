<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Service;

use Contao\ImagineSvg\Imagine;
use SidraBlue\Lote\Entity\File as FileEntity;
use SidraBlue\Lote\Event\Data as EventData;
use SidraBlue\Lote\Model\File as FileModel;
use SidraBlue\Lote\State\Base as State;
use SidraBlue\Util\Image\Resize;
use SidraBlue\Util\Time;

/**
 * Thumbnail generation class.
 *
 *  */
class Thumb
{

    /**
     * @var State $state - the state
     * */
    protected $state;

    /**
     * @var FileEntity $file - the file entity
     * */
    protected $file;

    /**
     * @var int $age specifies the expiration of cached image(in seconds)
     * */
    protected $age;

    /**
     * @var int $width specifies the width of image to be displayed
     * */
    protected $width;

    /**
     * @var int $height specifies the height of image to be displayed
     * */
    protected $height = 0;

    /**
     * @var int $size specifies the size of image to be displayed. If size is set, width and height will be
     * equal to the size.
     * */
    protected $size;

    /**
     * @var string $algorithm specifies the algorithm to use for image resizing
     * */
    protected $algorithm;

    /**
     * @var bool $useCache - true if the cache is to be used for this image
     * */
    protected $useCache = true;

    /**
     * @var int $debug specifies the type of error display. If equal to 1, text error message will be shown
     * and otherwise a blank image
     * */
    protected $debug;

    /**
     * Construct and set the image file entity
     * @param State $s - the state
     * @param FileEntity $file - the file entity
     * */
    public function __construct(State $s, FileEntity $file = null)
    {
        $this->state = $s;
        $this->file = $file;
    }

    /**
     * Set the image width
     * @param int $width - the width of the image
     * */
    public function setWidth($width)
    {
        if (intval($width) > 0) {
            $this->width = intval($width);
        }
    }

    /**
     * Set the image height
     * @param int $height - the height of the image
     * */
    public function setHeight($height)
    {
        if (intval($height) > 0) {
            $this->height = intval($height);
        }
    }

    /**
     * Set the max image size in pixels, of either the width or the height
     * @param int $size - the size of the image
     * */
    public function setSize($size)
    {
        if (intval($size) > 0) {
            $this->size = intval($size);
        }
    }

    /**
     * Set the algorithm to use for the resizing
     * @param string|bool $algorithm - the size of the image
     * */
    public function setAlgorithm($algorithm)
    {
        $this->algorithm = $algorithm;
    }

    /**
     * Set the debug state of this item
     * @param bool $debug - the debug flag
     * */
    public function setDebug($debug)
    {
        if (is_bool($debug)) {
            $this->debug = $debug;
        }
    }

    /**
     * Set the cache state of this item
     * @param bool $caching - the caching flag
     * */
    public function setCaching($caching)
    {
        if (is_bool($caching)) {
            $this->useCache = $caching;
        }
    }

    /**
     * Check if an image is cached, based on its ID and
     * @access public
     * @return boolean
     * */
    public function isCached()
    {
        return !$this->useCache || $this->state->getFileCache()->has($this->getCacheKeys());
    }

    private function getCacheKeys()
    {
        return $this->state->getFileCache()->getKeyArray($this->file->is_public, $this->file->location_reference,
            $this->width, $this->height, $this->size);
    }

    private function getCacheBaseKey()
    {
        return $this->state->getFileCache()->getKeyArray($this->file->is_public, $this->file->location_reference);
    }

    /**
     * Render the image from the cache
     * @access private
     * @return void
     * */
    private function renderFromCache()
    {
        $this->displayImage($this->state->getFileCache()->get($this->getCacheKeys()));
    }

    /**
     * Render the image from the cache
     * @access private
     * @param string $content - the content of the file
     * @param array $metadata
     * @return boolean
     * */
    private function saveToCache($content, $metadata = [])
    {
        return $this->state->getFileCache()->put($this->getCacheKeys(), $content, $metadata);
    }

    /**
     * Get the render dimensions for the image to be displayed
     * @access private
     * @return array - with keys for height and width
     * */
    private function getRenderDimensions()
    {
        $size = $this->getImageDimensions($this->getCacheBaseKey());
        $width = $size[0];
        $height = $size[1];

        $result = ['height' => $this->height, 'width' => $this->width];

        if ($this->size) {
            if ($width >= $height) {
                $result['width'] = $this->size;
                $result['height'] = round($result['width'] / $width * $height);
            } else {
                $result['height'] = $this->size;
                $result['width'] = round($result['height'] / $height * $width);
            }
        } else {
            if (!empty($this->width)) {
                $widthDifference = $width / $this->width;
                if (!$this->height) {
                    $result['height'] = round($height / $widthDifference);
                }
            }
            if (isset($result['height']) && $result['height'] > 0) {
                $heightDifference = $height / $result['height'];
                if (empty($this->width)) {
                    $result['width'] = round($width / $heightDifference);
                }

            }
        }
        return $result;
    }

    /**
     * Get the image dimensions for an image that is specified by its base keys
     * @access private
     * @param array $baseKey
     * @return array
     * */
    private function getImageDimensions($baseKey)
    {
        $imageDataString = $this->state->getFileCache()->get($baseKey);
        $extension = pathinfo(implode("/", $baseKey), PATHINFO_EXTENSION);
        if ($extension == 'svg' && strpos($imageDataString, "<?xml version=\"1.0\" encoding=\"utf-8\"?>") === 0) {
            $imagine = new Imagine();
            $size = $imagine->load($imageDataString)->getSize();
            $result = [$size->getWidth(), $size->getHeight()];
        } else {
            $result = getimagesizefromstring($imageDataString); // Read the size
        }
        return $result;
    }

    /**
     * Determine if the original image is to be rendered
     * @access private
     * @return boolean
     *  */
    private function renderOriginal()
    {
        return !$this->size && !$this->width && !$this->height;
    }

    /**
     * Function to render the image according to the request variables
     * @access public
     * @return void
     */
    public function render()
    {
        if ($this->useCache && $this->isCached($this->file->is_public)) {
            $this->renderFromCache();
        } else {
            $eventData = new EventData(['entity' => $this->file]);
            $this->state->getEventManager()->dispatch('file.cache.get_metadata', $eventData);
            $eventData = $eventData->getData("metadata");
            if ($this->renderOriginal()) {
                $m = new FileModel($this->state);
                $this->saveToCache($m->getContent($this->file->id), $eventData);
                $this->displayImage($this->state->getFileCache()->get($this->getCacheKeys()),
                    Time::dateToTimestamp($this->file->lote_updated));
            } else {
                $m = new FileModel($this->state);
                if ($fileContent = $m->getContentByEntity($this->file)) {
                    $this->state->getFileCache()->put($this->getCacheBaseKey(), $fileContent, $eventData);
                    $newFileName = $this->state->getFileCache()->getCacheKey($this->getCacheKeys());
                    $resizeObj = new Resize('', $fileContent);
                    $resizeObj->setAlgorithm($this->algorithm);
                    $dimensions = $this->getRenderDimensions();
                    if (!($imageContent = $resizeObj->resizeImage($dimensions['width'], $dimensions['height'],
                        $newFileName))) {
                        $this->state->getFileCache()->delete($this->getCacheKeys());
                        $this->displayErrorImage('blank');
                    } else {
                        $this->saveToCache($imageContent, $eventData);
                        $this->state->getFileCache()->put($this->getCacheKeys(), $imageContent, $eventData);
                        $this->displayImage($imageContent, Time::dateToTimestamp($this->file->lote_updated));
                    }
                }
            }
        }
    }

    /**
     * Render the currently requested image by echoing the headers and image contents to the browser
     * @access private
     * @param string $content - the image content
     * @param int|bool $lastUpdated - the last updated time
     * @return void
     */
    private function displayImage($content, $lastUpdated = false)
    {
        if ($content) {
            header('Content-Type:image');
            $this->sendBaseHeaders(false, $lastUpdated);
            header('Content-Length: ' . strlen($content));
            echo($content);
        }
        die;
    }

    /**
     * Send headers to prevent caching of the image. Caching should be done on the server end.
     * @param bool $force - true if the headers are to be send regardless of the input
     * @param bool|int $timestamp - The timestamp to use for headers
     * @access private
     * @return void
     * */
    private function sendBaseHeaders($force = false, $timestamp = false)
    {
        if ($timestamp && is_numeric($timestamp)) {
            header("Cache-Control: private, max-age=604800");
            header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 604800) . ' GMT');
            header("Last-Modified: " . gmdate("D, d M Y H:i:s", $timestamp));
        } elseif ($force || !$this->useCache) {
            header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
        }
    }

    /**
     * Render an error image to the browser
     * @param string $imageName - the name of the image to render
     * @access private
     * @return void
     */
    private function displayErrorImage($imageName)
    {
        $errorFileLocation = "media/$imageName.jpg";
        if (file_exists($errorFileLocation)) {
            header('Content-Type:image/jpeg');
            $this->sendBaseHeaders();
            echo file_get_contents($errorFileLocation);
        }
        die;
    }

    /**
     * Display a blank error image
     * @access public
     * @return void
     * */
    public function displayBlankImage()
    {
        $this->displayErrorImage("blank");
    }

}
