<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\Service;

use SidraBlue\Lote\Object\State;

class Url extends State
{

    /**
     * @param string $log - the scheme of this reply
     * */
    protected $scheme;

    /**
     * @param string $baseUrl
     * */
    protected $baseUrl;

    /**
     * @param string $httpHost
     * */
    protected $httpHost;

    /**
     * @param bool $useRequest
     * */
    protected $useRequest = true;

    /**
     * @param $baseUrl string
     * @return void
     * */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * Get the base URL to use
     * @param string $scheme
     * @return string
     * */
    public function getBaseUrl($scheme = "http://")
    {
        if ($this->baseUrl) {
            $result = $this->baseUrl;
        } elseif (php_sapi_name() == 'cli') {
            $url = $this->getState()->getSites()->getCurrentSiteUrl();
            if ($url && $url === '1') {
                $result = $scheme . $this->getState()->getAccount()->domain . '/';
            } else {
                if (!preg_match('|^http(s)?://.*$|i', $url)) {
                    $url = $this->getState()->getSites()->getCurrentSiteScheme($scheme) . $url . '/';
                }
                $result = preg_replace('#/+$#', '/', $url);
            }
        } else {
            $result = $this->getState()->getSites()->getCurrentSiteUrl();
            if ($result && $result === '1') {
                if (!$this->useRequest && $this->getState()->getAccount()->domain) {
                    $result = $scheme . $this->getState()->getAccount()->domain . '/';
                } else {
                    $result = $this->getState()->getRequest()->getSchemeAndHttpHost() .
                        $this->getState()->getRequest()->getBasePath() . '/';
                }
            } elseif (!$result) {
                $result = $this->getState()->getRequest()->getSchemeAndHttpHost() .
                    $this->getState()->getRequest()->getBasePath() . '/';
            } else {
                if (!preg_match('|^http(s)?://.*$|i', $result)) {
                    $result = $this->getState()->getRequest()->getScheme() . '://'. $result . '/';
                }
            }
        }
        return $result;
    }

    /**
     * Get the HTTP Host and scheme of the current state object
     * @access public
     * @return string
     * */
    public function getHttpSchemeAndHost()
    {
        if (php_sapi_name() == 'cli') {
            $result = $this->getBaseUrl("https://");
        } else {
            $result = $this->getScheme() . '://' . $this->getHttpHost() . '/';
        }
        return $result;
    }

    /**
     * Get the HTTP Host of the current state object
     * @access public
     * @return string
     * */
    public function getHttpHost()
    {
        $result = $this->getState()->getSites()->getCurrentSiteUrl();
        if (!filter_var($result, FILTER_VALIDATE_URL)) {
            if (isset($this->getState()->getAccount()->domain)) {
                $result = $this->getState()->getAccount()->domain;
            } else {
                $result = $this->getState()->getRequest()->getHttpHost();
            }
        }
        return $result;
    }

    /**
     * Get the HTTP Host of the current state object
     * @access public
     * @param string $default
     * @return string
     * */
    public function getScheme($default = "http://")
    {
        if (php_sapi_name() == 'cli') {
            $result = $default;
        } else {
            $result = $this->getState()->getRequest()->getScheme();
        }
        return $result;
    }

    /**
     * Get the Master Base URL
     * @access public
     * @return string
     * */
    public function getMasterBaseUrl()
    {
        return $this->getState()->getSettings()->getConfigOrSetting("master.url.base");
    }

    /**
     * Get the useRequest variable, which is true if we can use the HTTP request to determine the URL
     * @access public
     * @return bool
     */
    public function getUseRequest()
    {
        return $this->useRequest;
    }

    /**
     * Set the useRequest variable, which is true if we can use the HTTP request to determine the URL
     * @access public
     * @param bool $useRequest
     */
    public function setUseRequest($useRequest)
    {
        $this->useRequest = $useRequest;
    }

}
