<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\Service;

use SidraBlue\Lote\Entity\User;
use SidraBlue\Lote\Model\Message;
use SidraBlue\Lote\Model\Tag;
use SidraBlue\Lote\Object\State as BaseState;
use Lote\Module\Content\Cms\Model\Holder;
use Lote\System\Admin\Model\Setting;
use Lote\System\Client\Entity\Master\Account;
use Lote\System\Admin\Model\App as AppModel;
use Lote\Module\Form\Entity\Form;


/**
 * Base class for all models
 */
class Wildcard extends BaseState
{
    public $formFieldsId;

    private function getAppName($appRef)
    {
        $obj = new AppModel($this->getState());
        $appDetails = $obj->getAppDetailsByReference($appRef);
        return isset($appDetails['name']) && !empty($appDetails['name']) ? $appDetails['name'] : $appRef;
    }

    public function getWildcardNames(
        $wildcardTypes = [
            'user',
            'custom_field',
            'edm',
            'date',
            'content_holder',
            'setting',
            'account'
        ]
    )
    {
        $wildcards = [];

        if(in_array('user', $wildcardTypes))
        {
            /*            $userData = $this->getCoreFields();
                        foreach($userData as $k => $v) {
                            $wildcards["%%$k%%"] = $v;
                        }*/
            $wildcards["user"] = 'User';
        }

        if(in_array('custom_field', $wildcardTypes))
        {
            /*            $customFields = $this->getCustomFieldsMap('user');
                        foreach($customFields as $k => $v) {
                            $wildcards["%%__cf_$k%%"] = $v;
                        }*/
            $wildcards["user"] = 'User';
        }
        if(in_array('edm', $wildcardTypes))
        {
            $wildcards['edm'] = 'Campaign';

        }

        if(in_array('notification', $wildcardTypes))
        {
            $wildcards["%%__notification_latest__%%"] = 'Latest Link';
        }

        if(in_array('date', $wildcardTypes))
        {
            $wildcards["date"] = 'Date';
        }

        if(in_array('content_holder', $wildcardTypes))
        {
            $wildcards["content_holder"] = 'Content Holder';
        }

        if(in_array('update_form', $wildcardTypes))
        {
            $wildcards["update_form"] = 'Update Form';
        }

        if(in_array('setting', $wildcardTypes))
        {
            $wildcards["setting"] = 'Settings';
        }

        if(in_array('account', $wildcardTypes))
        {
            $wildcards["account"] = 'Account';
        }

        if(in_array('assessment', $wildcardTypes))
        {
            $wildcards["%%__form_assessment_data__%%"] = 'All Form Assessment Results';
        }


        if(in_array('formfields', $wildcardTypes))
        {
            $wildcards["formfields"] = 'Form Fields';
        }

        if($this->getState()->getApps()->hasApp("SessionKeeper")) {
            if (in_array('sessionkeeper', $wildcardTypes)) {
                $wildcards['sessionkeeper'] = 'Session Keeper';
            }
        }

        if (in_array('message', $wildcardTypes)) {
            $wildcards['message'] = 'Message Notifications';
        }

        return $wildcards;
    }

    public function getUserWildcards(
        $wildcardTypes = [
            'user',
            'custom_field'
        ]
    )
    {
        $wildcards = [];

        $user = new User($this->getState());

        if(in_array('user', $wildcardTypes))
        {
            $userData = $user->getCoreFields();
            foreach($userData as $k => $v) {
                if(strtolower($v) != 'password')
                    $wildcards["%%$k%%"] = $v;
            }
            $wildcards["%%user_password_reset%%"] = "Password Reset";
            $wildcards["%%user_password_reset_url%%"] = "Password Reset URL";
        }

        if(in_array('custom_field', $wildcardTypes))
        {
            $customFields = $user->getCustomFieldsMap('user');
            foreach($customFields as $k => $v) {
                $wildcards["%%__cf_$k%%"] = $v;
            }
        }

        return $wildcards;
    }

    public function getDateWildcards()
    {
        $wildcards = [];

        $wildcards["%%__date_MdY__%%"] = date("F jS Y");
        $wildcards["%%__date_mdY__%%"] = date("M jS Y");
        $wildcards["%%__date_MD__%%"] = date("F j");
        $wildcards["%%__date_dMY__%%"] = date("jS F Y");
        $wildcards["%%__date_dmY__%%"] = date("jS M Y");
        $wildcards["%%__date_dmy__%%"] = date("d/m/y") . ' - (dd/mm/yy)';
        $wildcards["%%__date_dmY__%%"] = date("d/m/Y") . ' - (dd/mm/yyyy)';
        $wildcards["%%__date_mdy__%%"] = date("m/d/y") . ' - (mm/dd/yy)';
        $wildcards["%%__date_mdY__%%"] = date("m/d/Y") . ' - (mm/dd/yyyy)';
        $wildcards["%%__date_year__%%"] = date("Y") . ' - (Year - Full)';
        $wildcards["%%__date_y__%%"] = date("y") . ' - (Year - Short)';
        $wildcards["%%__date_month__%%"] = date("m") . ' - (Month - Leading 0\'s)';
        $wildcards["%%__date_m0__%%"] = date("n") . ' - (Month - No leading 0\'s)';
        $wildcards["%%__date_M__%%"] = date("F") . ' - (Month - Full name)';
        $wildcards["%%__date_m__%%"] = date("M") . ' - (Month - Short name)';
        $wildcards["%%__date_d00__%%"] = date("d") . ' - (Day - Leading 0\'s)';
        $wildcards["%%__date_d0__%%"] = date("j") . ' - (Day - No leading 0\'s)';
        $wildcards["%%__date_dth__%%"] = date("jS") . ' - (Day - Ordinal)';
        $wildcards["%%__date_day__%%"] = date("l") . ' - (Day - Full name)';
        $wildcards["%%__date_d__%%"] = date("D") . ' - (Day - Short name)';

        return $wildcards;
    }

    public function getEdmWildcards()
    {
        $wildcards = [];

        $wildcards["%%__view_in_browser__%%"] = 'View in Browser Link';
        $wildcards["%%__view_in_browser_url__%%"] = 'View in Browser Link URL';
        $wildcards["%%__latest__%%"] = 'Latest Link';
        $wildcards["%%__unsubscribe_link__%%"] = 'Unsubscribe Link';
        $wildcards["%%__unsubscribe_link_url__%%"] = 'Unsubscribe Link URL';
        $wildcards["%%__manage_profile_link__%%"] = 'Manage Profile Link';
        $wildcards["%%__manage_profile_link_url__%%"] = 'Manage Profile Link URL';

        return $wildcards;
    }

    //FIXME: potential bug as this only shows the first page of the results at 20 rows and way to page through it
    public function getContentHolderWildcards()
    {
        // get content holders
        $ch = new Holder($this->getState());
        $contentHolders = $ch->getList(1, [], [], 'and');

        return $contentHolders;
    }

    public function getContentHolderWildcardsByQuery($page, $phrase)
    {
        // get content holders
        $ch = new Holder($this->getState());

        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('id, name')
            ->from($ch->getTableName())
            ->where('lote_deleted is NULL')
            ->andWhere('active = 1')
            ->orderBy('lote_created', 'desc');
        if (!empty($phrase)) {
            $q->andWhere('name like :phrase')
                ->setParameter('phrase', "%{$phrase}%");
        }
        $contentHolders = $ch->getListByQuery($q, $page, 20);

        return $contentHolders;
    }

    //FIXME: potential bug as this only shows the first page of the results at 20 rows and way to page through it
    public function getUpdateFormWildcards()
    {
        // get content holders
        $ch = new \Lote\Module\Form\Model\Form($this->getState());
        $contentHolders = $ch->getList(1, [], [], 'and');

        return $contentHolders;
    }

    public function getUpdateFormWildcardsByQuery($formType, $page, $perPage)
    {
        // get content holders
        $ch = new \Lote\Module\Form\Model\Form($this->getState());

        $q = $this->getState()->getReadDb()->createQueryBuilder();
        $q->select('id, name')
            ->from($ch->getTableName())
            ->where('lote_deleted is NULL')
            ->andWhere('form_type = :formType')
            ->setParameter('formType', $formType)
            ->orderBy('lote_created', 'desc');

        $contentHolders = $ch->getListByQuery($q, $page, $perPage);

        return $contentHolders;
    }

    private function settingSearchClauses()
    {
        $clauses = [];

        $clause = [];
        $clause['field'] = 'content_access';
        $clause['operator'] = 'equals';
        $clause['value'] = true;
        $clause['data_type'] = \PDO::PARAM_BOOL;
        $clauses[] = $clause;

        return $clauses;
    }

    private function settingSearchOptions()
    {
        $options = [];
        $options['app_ref'] = 'asc';
        $options['title'] = 'asc';
        $options['reference'] = 'asc';
        return $options;
    }

    public function getSettingWildcards()
    {
        $wildcards = [];

        // get settings
        $s = new Setting($this->getState());
        $clauses = $this->settingSearchClauses();
        $options = $this->settingSearchOptions();
        $settings = $s->getList(1, $clauses, $options, 'and', 1000);

        /*if(isset($settings['rows']))
        {
            foreach($settings['rows'] as $setting)
            {
                $wildcards["%%__setting_{$setting['reference']}__%%"] = !empty($setting['title']) ? $setting['title'] : $setting['reference'];
            }
        }*/

        if(isset($settings['rows']))
        {
            foreach($settings['rows'] as $setting)
            {
                $temp = [];
                $temp['key'] = "%%__setting_{$setting['reference']}__%%";
                $temp['value'] = !empty($setting['title']) ? $setting['title'] : $setting['reference'];
                $temp['group'] = !empty($setting['app_ref']) ? $this->getAppName($setting['app_ref']) : 'Core';
                $wildcards[] = $temp;
            }
        }

        return $wildcards;
    }

    public function getSettingsWildcardsForWYSIWYG()
    {
        $wildcards = [];

        // get settings
        $s = new Setting($this->getState());
        $clauses = $this->settingSearchClauses();
        $options = $this->settingSearchOptions();
        $settings = $s->getList(1, $clauses, $options, 'and', 1000);

        if(isset($settings['rows']))
        {
            foreach($settings['rows'] as $setting)
            {
                $wildcards["%%__setting_{$setting['reference']}__%%"] = !empty($setting['title']) ? $setting['title'] : $setting['reference'];
            }
        }

        return $wildcards;
    }

    public function getAccountWildcards()
    {
        $wildcards = [];

        $columns = [
            'domain',
            //'website', Handled below
            'name',
            'trading_name',
            'business_number',
            'business_number_type',
            'timezone',
            'phone',
            'email',
            'contact_first_name',
            'contact_last_name',
            'street',
            'city',
            'postcode',
            'state',
            'country',
            'fax',
            'mobile'
        ];

        // Only add website_secondary wildcards if the property is present in this System
        foreach (['website', 'website_secondary'] as $websiteProp) {
            $account = $this->getState()->getAccount();
            if ($account && property_exists($account, $websiteProp)) {
                $newCols = [
                    "{$websiteProp}",
                    "{$websiteProp}_html_link",
                    "{$websiteProp}_display"
                ];
                array_splice($columns, 4, 0, $newCols);
            }
        }

        foreach($columns as $property) {
            $wildcardName = str_ireplace('_', ' ', $property);
            $wildcards["%%__account_{$property}__%%"] = ucwords($wildcardName);
        }

        return $wildcards;
    }

    public function getWildcardValues(
        $wildcardTypes = [
            'user',
            'custom_field',
            'edm',
            'date',
            'content_holder',
            'setting',
            'account'
        ]
    )
    {
        $wildcards = [];

        $user = new User($this->getState());

        if(in_array('user', $wildcardTypes))
        {
            $data = $user->getData();
            foreach($data as $k=>$v) {
                $wildcards["%%$k%%"] = $v;
            }
        }

        if(in_array('custom_field', $wildcardTypes))
        {
            $data = $user->getCustomFieldValues();
            foreach($data as $k=>$v) {
                $wildcards["%%__cf_$k%%"] = $v;
            }
        }

        if(in_array('date', $wildcardTypes))
        {
            $wildcards["%%__date_MdY__%%"] = date("F jS Y");
            $wildcards["%%__date_mdY__%%"] = date("M jS Y");
            $wildcards["%%__date_dMY__%%"] = date("jS F Y");
            $wildcards["%%__date_dmY__%%"] = date("jS M Y");
            $wildcards["%%__date_dmy__%%"] = date("d/m/y");
            $wildcards["%%__date_dmY__%%"] = date("d/m/Y");
            $wildcards["%%__date_mdy__%%"] = date("m/d/y");
            $wildcards["%%__date_mdY__%%"] = date("m/d/Y");
            $wildcards["%%__date_year__%%"] = date("Y");
            $wildcards["%%__date_y__%%"] = date("y");
            $wildcards["%%__date_month__%%"] = date("m");
            $wildcards["%%__date_m0__%%"] = date("n");
            $wildcards["%%__date_M__%%"] = date("F");
            $wildcards["%%__date_m__%%"] = date("M");
            $wildcards["%%__date_d00__%%"] = date("d");
            $wildcards["%%__date_d0__%%"] = date("j");
            $wildcards["%%__date_dth__%%"] = date("js");
            $wildcards["%%__date_day__%%"] = date("l");
            $wildcards["%%__date_d__%%"] = date("D");
        }

        if(in_array('content_holder', $wildcardTypes))
        {
            // get content holders
            $ch = new Holder($this->getState());
            $contentHolders = $ch->getList(1, [], [], 'and', 1000);
            if(isset($contentHolders['rows']))
            {
                foreach($contentHolders['rows'] as $contentHolder)
                {
                    $wildcards["%%__content_snippet_{$contentHolder['id']}__%%"] = $contentHolder['content'];
                }
            }
        }

        if(in_array('edm', $wildcardTypes)){
            $wildcards["%%__view_in_browser__%%"] = 'View in Browser Link';
            $wildcards["%%__view_in_browser_url__%%"] = 'View in Browser Link URL';
            $wildcards["%%__latest__%%"] = 'Latest Link';
            $wildcards["%%__unsubscribe_link__%%"] = 'Unsubscribe Link';
            $wildcards["%%__unsubscribe_link_url__%%"] = 'Unsubscribe Link URL';
            $wildcards["%%__manage_profile_link__%%"] = 'Manage Profile Link';
            $wildcards["%%__manage_profile_link_url__%%"] = 'Manage Profile Link URL';
        }

        if(in_array('setting', $wildcardTypes))
        {
            // get settings
            $s = new Setting($this->getState());
            $settings = $s->getList(1, [], [], 'and', 1000);
            if(isset($settings['rows']))
            {
                foreach($settings['rows'] as $setting)
                {
                    // grab the user setting otherwise the default
                    if(!empty($setting['value_custom']))
                    {
                        $value = $setting['value_custom'];
                    }
                    else {
                        $value = $setting['value_default'];
                    }
                    $wildcards["%%__setting_{$setting['reference']}__%%"] = $value;
                }
            }
        }

        if(in_array('account', $wildcardTypes))
        {
            $columns = [
                'domain',
                //'website', handled below
                'name',
                'trading_name',
                'business_number',
                'business_number_type',
                'timezone',
                'phone',
                'email',
                'contact_first_name',
                'contact_last_name',
                'street',
                'city',
                'postcode',
                'state',
                'country',
                'fax',
                'mobile'
            ];

            $account = new Account($this->getState());
            $account->loadByField('reference', $this->getState()->getSettings()->get('system.reference'));

            if(!empty($account->name)) {
                foreach($columns as $property) {
                    $wildcards["%%__account_{$property}__%%"] = $account->$property;
                }

                // Create website wildcard variants
                foreach (['website', 'website_secondary'] as $websiteProp) {
                    $host = '';
                    $url = '';
                    if (!empty($account->{$websiteProp})) {
                        $urlParts = parse_url($account->{$websiteProp});
                        if (isset($urlParts['host'])) {
                            $host = $urlParts['host'];
                            $url = $urlParts['scheme'] . "://" . $host;
                        } elseif (isset($urlParts['path'])) {
                            $host = explode('/', $urlParts['path'])[0];
                            $url = "http://" . $host;
                        }
                    }
                    $wildcards["%%__account_{$websiteProp}__%%"] = $url;
                    $wildcards["%%__account_{$websiteProp}_html_link__%%"] = "<a href='$url'>$host</a>";
                    $wildcards["%%__account_{$websiteProp}_display__%%"] = $host;
                }
            }
        }

        // These are added OUTSIDE of this function
        /*        $wildcards["%%__view_in_browser__%%"] = 'View in Browser Link';
                $wildcards["%%__unsubscribe_link__%%"] = 'Unsubscribe Link';
                $wildcards["%%__manage_profile_link__%%"] = 'Manage Profile Link';*/

        return $wildcards;
    }

    public function getFormWildcards(
        $wildcardTypes = [
            'form',
            'assessment',
            'custom_field'
        ],$formFieldsId
    )
    {
        $wildcards = [];

        $form = new Form($this->getState());

        if(in_array('custom_field', $wildcardTypes))
        {
            $customFields = $form->getFormCustomFields('form', $formFieldsId);
            foreach($customFields as $k => $v) {
                // spaces in wildcard break form filling/rendering
                $wildcards["%%__form_field_".$k."_".preg_replace('/[% ]/', '', $v)."__%%"] = $v;
            }
            $wildcards["%%__form_data__%%"] = "All Form Data";
        }

        return $wildcards;
    }

    /**
     * This function is only used for the SessionKeeper app
     * It will probably show up as errors in Projects that do not have a dependency on SessionKeeper
     *
     * @todo move this function to the session keeper app but without breaking dependency
     * @fixme this class is very tightly coupled
     *
     * @return array
     */
    public function getSessionKeeperWildcards()
    {
        $wildcardNames = [
            'sessionkeeper_session' => 'Session',
            'sessionkeeper_sessionobject' => 'Session Object',
            'sessionkeeper_sessionavailability' => 'Session Availability',
            'sessionkeeper_booking' => 'Booking',
            'sessionkeeper_bookingtimeslot' => 'Booking Timeslot',
            'sessionkeeper_bookingattendant' => 'Booking Attendant'
        ];

        $wildcards = [
            'sessionkeeper_session' => \Schoolzine\Entity\SessionKeeper\Session::getWildcards(),
            'sessionkeeper_sessionobject' => \Schoolzine\Entity\SessionKeeper\SessionObject::getWildcards(),
            'sessionkeeper_sessionavailability' => \Schoolzine\Entity\SessionKeeper\SessionAvailability::getWildcards(),
            'sessionkeeper_booking' => \Schoolzine\Entity\SessionKeeper\Booking::getWildcards(),
            'sessionkeeper_bookingtimeslot' => \Schoolzine\Entity\SessionKeeper\BookingTimeslot::getWildcards(),
            'sessionkeeper_bookingattendant' => \Schoolzine\Entity\SessionKeeper\BookingAttendant::getWildcards()
        ];

        return $this->createWildcardsTree($wildcardNames, $wildcards);
    }

    public function getMessageWildcards()
    {
        // get the tags
        $tm = new Tag($this->getState());
        $tags = $tm->getTagsByReferenceDist('sb_message');

        // get the messages
        $mm = new Message($this->getState());
        $messages = $mm->getListBySearch([]);

        // index by the tags
        $wildcardNames = [];
        $wildcardNames['no_tag'] = 'No Tag';
        $wildcards = [];
        foreach ($tags as $tag) {
            $wildcardNames[$tag['value']] = !empty($tag['display_value']) ? $tag['display_value'] : $tag['value'];
        }

        $wildcards = [];
        foreach ($messages as $message) {
            if (!empty($message['_tags'])) {
                $messageTags = array_column($message['_tags'], 'tag_id');
                foreach ($tags as $tag) {
                    if (in_array($tag['id'], $messageTags)) {
                        $wildcards[$tag['value']]["%%__{$message['wildcard']}__%%"] = $message['name'];
                    }
                }
            } else {
                $wildcards['no_tag']["%%__{$message['wildcard']}__%%"] = $message['name'];
            }
        }

        return $this->createWildcardsTree($wildcardNames, $wildcards);
    }

    /**
     * @param array $wildcardsRoot this can be generated with a call to @see this->getWildcardNames
     * @param array $wildcards all required wildcards should be added in array form with the key matching the $wildcardsRoot
     * @return array wildcard tree
     */
    public function createWildcardsTree($wildcardsRoot, $wildcards)
    {
        $wildcardTree = [];
        $childKeyName = 'sb_wildcard_children';
        $i = 0;
        foreach ($wildcardsRoot as $key => $wildcard) {
            $wildcardTree[] = array('wildcard' => $key, 'name' => $wildcard);
            if (isset($wildcards[$key])) {
                $wildcardTree[$i][$childKeyName] = $wildcards[$key];
            }
            $i++;
        }

        return $wildcardTree;
    }

    public function getWildcardsCollectionTree($wildcardTypes = ['user', 'custom_field', 'edm', 'date', 'setting'],
                                               $formWildCard = null, $formWildCardId = null)
    {
        $wildcardsRoot = $this->getWildcardNames($wildcardTypes);

        $wildcards = [];
        foreach ($wildcardsRoot as $key => $value) {
            if ($key[0] != "%") {
                $wildcards[] = $key;
            }
        }

        $wildcards = $this->populateWildcardValues($wildcards);

        if (!is_null($formWildCard) && !is_null($formWildCardId)) {
            $wildcardsRoot['form'] = "Form";
            $wildcards['form'] = $this->getFormWildcards($formWildCard, $formWildCardId);

        }

        return $this->createWildcardsTree($wildcardsRoot, $wildcards);
    }

    private function populateWildcardValues($wildcardTypes)
    {
        $wildcards = [];
        foreach($wildcardTypes as $value) {
            $typeWildcards = [];
            $expandableField = false;
            if ($value == "user") {
                $typeWildcards = $this->getUserWildcards();
                $expandableField = true;
            } elseif ($value == "date") {
                $typeWildcards = $this->getDateWildcards();
                $expandableField = true;
            } elseif ($value == "edm") {
                $typeWildcards = $this->getEdmWildcards();
                $expandableField = true;
            }elseif ($value == "setting") {
                $typeWildcards = $this->getSettingsWildcardsForWYSIWYG();
                $expandableField = true;
            } elseif ($value == "account") {
                $typeWildcards = $this->getAccountWildcards();
                $expandableField = true;
            } elseif ($value == "form_fields") {
                $typeWildcards = $this->getWildcardValues(['custom_field']);
                $expandableField = true;
            } elseif ($value == "sessionkeeper") {
                $typeWildcards = $this->getSessionKeeperWildcards();
                $expandableField = true;
            } elseif ($value == "message") {
                $typeWildcards = $this->getMessageWildcards();
                $expandableField = true;
            }

            if (!$expandableField || !empty($typeWildcards)) {
                $wildcards[$value] = $typeWildcards;
            }
        }

        return $wildcards;
    }
}
