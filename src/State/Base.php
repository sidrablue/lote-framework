<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\State;

use Aura\Di;
use Aura\Signal;
use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;
use Lote\System\Client\Model\Master\Account;
use Lote\System\Client\Entity\Master\Account as AccountEntity;
use SidraBlue\Lote\Application\Composition;
use SidraBlue\Lote\Auth\Acl\Access;
use SidraBlue\Lote\Cache\File\Factory as CacheFactory;
use SidraBlue\Lote\Event\Manager;
use SidraBlue\Lote\Log\Set as LoggerSet;
use SidraBlue\Lote\Service\Closure;
use SidraBlue\Lote\Service\Debug;
use SidraBlue\Lote\Service\ImageServer;
use SidraBlue\Lote\Service\Audit as AuditLog;
use SidraBlue\Lote\Service\Data;
use SidraBlue\Lote\Service\Group as GroupService;
use SidraBlue\Lote\Model\Setting;
use SidraBlue\Lote\Entity\User;
use SidraBlue\Lote\Entity\UserSession;
use SidraBlue\Lote\Model\Site;
use SidraBlue\Lote\Service\Ldap;
use SidraBlue\Lote\Service\ParentAccount;
use SidraBlue\Lote\Service\Plugin;
use SidraBlue\Lote\Service\Queue;
use SidraBlue\Lote\Service\Redis as RedisService;
use SidraBlue\Lote\Service\Timezone;
use SidraBlue\Lote\Service\Timer;
use SidraBlue\Lote\Service\Url;
use SidraBlue\Lote\View\Factory as ViewFactory;
use SidraBlue\Lote\Auth\Acl\Context;
use Symfony\Component\Translation\MessageSelector;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\Loader\PhpFileLoader;
use SidraBlue\Lote\Registry\Config;
use Symfony\Component\HttpFoundation\Response; //@todo - move this back to the Web state
use SidraBlue\Lote\Auth\Session as LoginSession;

class Base
{

    /**
     * @var \Aura\Di\Container $di
     */
    protected $di;

    /**
     * @var boolean $isCli
     * */
    protected $isCli;

    /**
     * @var Connection\Manager
     * */
    protected $connectionManager;

    /**
     * @var array
     * */
    protected $route;

    /**
     * @var array $data ;
     * */
    public $data = [];

    /**
     * @var string $accountReference ;
     * */
    public $accountReference = "";

    /**
     * @var string $configReference ;
     * */
    public $configReference = "";

    /**
     * @var string $masterOnly ;
     * */
    public $masterOnly = false;


    /**
     * Default constructor for Base State
     *
     * @access public
     * @param null $parentReference
     * @param bool $masterOnly
     */
    public function __construct($parentReference = null, $masterOnly = false)
    {
        $this->di = new Di\Container(new Di\Forge(new Di\Config));
        $this->connectionManager = new Connection\Manager($this);
        $this->isCli = php_sapi_name() == 'cli';
        if ($masterOnly) {
            $this->accountReference = 'master';
        } elseif ($parentReference) {
            $this->accountReference = $this->configReference = $parentReference;
        } else {
            $this->accountReference = LOTE_ACCOUNT_REF;
        }
        $this->masterOnly = $masterOnly;
        $this->setup();
    }

    /**
     * Default setup function for initialising the state variables
     * @access protected
     * @return void
     * */
    protected function setup()
    {
        $this->di->set("config", function () {
            return $this->setupConfig();
        });
        $this->di->set("database.master", function () {
            return $this->setupMasterDatabase();
        });
        if (!$this->masterOnly) {
            $this->di->set("database.parent", function () {
                return $this->setupParentDatabase();
            });
            $this->di->set("database.superparent", function () {
                return $this->setupSuperParentDatabase();
            });
            $this->di->set("database.read", function () {
                return $this->setupReadDatabase();
            });
            $this->di->set("database.write", function () {
                return $this->setupWriteDatabase();
            });
        } else {
            $this->di->set("database.read", function () {
                return $this->di->get("database.master");
            });
            $this->di->set("database.write", function () {
                return $this->di->get("database.master");
            });
        }
        $this->di->set("logger.set", function () {
            return $this->setupLoggerSet();
        });
        $this->di->set("request", function () {
            return $this->setupRequest();
        });
        $this->di->set("response", function () {
            return $this->setupResponse();
        });
        $this->di->set("signal", function () {
            return $this->setupSignal();
        });
        $this->di->set("apps", function () {
            return $this->setupApps();
        });
        $this->di->set("user", function () {
            return $this->setupUser();
        });
        $this->di->set("group.service", function () {
            return $this->setupGroupService();
        });
        $this->di->set("queue", function () {
            return $this->setupQueue();
        });
        $this->di->set("user.session", function () {
            return $this->setupUserSession();
        });
        $this->di->set("view", function () {
            return $this->setupView();
        });
        $this->di->set("context", function () {
            return $this->setupContext();
        });
        $this->di->set("access", function () {
            return $this->setupAccess();
        });
        $this->di->set("translator", function () {
            return $this->setupTranslator();
        });
        $this->di->set("response", function () {
            return new Response();
        });
        $this->di->set("site", function () {
            return $this->setupSites();
        });
        $this->di->set("login.session", function () {
            return $this->setupLoginSession();
        });
        $this->di->set("settings", function () {
            return $this->setupSettings();
        });
        $this->di->set("plugins", function () {
            return $this->setupPlugins();
        });
        $this->di->set("audit", function () {
            return $this->setupAudit();
        });
        $this->di->set("ldap", function () {
            return $this->setupLdap();
        });
        $this->di->set("s3client", function () {
            return $this->setupS3Client();
        });
        $this->di->set("s3client.public", function () {
            return $this->setupS3PublicClient();
        });
        $this->di->set("s3client.private", function () {
            return $this->setupS3PrivateClient();
        });
        $this->di->set("account", function () {
            return $this->setupAccount();
        });
        $this->di->set("data", function () {
            return $this->setupSystemData();
        });
        $this->di->set("parent.account", function () {
            return $this->setupParentAccount();
        });
        $this->di->set("parent.super.account", function () {
            return $this->setupSuperParentAccount();
        });
        $this->di->set("timezone", function () {
            return $this->setupTimezone();
        });
        $this->di->set("filecache", function () {
            return $this->setupFileCache();
        });
        $this->di->set("redis.db", function () {
            return $this->setupRedisDbClient();
        });
        $this->di->set("client.account.config.file_system", function () {
            return $this->setupAccountConfigFileSystem();
        });
        $this->di->set("parent.state", function () {
            return $this->setupParentState();
        });
        $this->di->set("parent.super.state", function () {
            return $this->setupSuperParentState();
        });
        $this->di->set("master.state", function () {
            return $this->setupMasterState();
        });
        $this->di->set("url", function () {
            return $this->setupUrl();
        });
        $this->di->set("imageserver", function () {
            return $this->setupImageServer();
        });
        $this->di->set("closures", function () {
            return $this->setupClosures();
        });
        $this->di->set("timer", function () {
            return $this->setupTimer();
        });
        $this->di->set("timer", function () {
            return $this->setupTimer();
        });
        $this->di->set("event.manager", function () {
            return $this->setupEventManager();
        });
        $this->di->set("debug", function () {
            return $this->setupDebug();
        });
    }

    public function tearDown()
    {
        if (in_array("database.master", $this->di->getServices())) {
            $this->getMasterDb()->close();
        }
        if (in_array("database.parent", $this->di->getServices())) {
            $this->getParentDb()->close();
        }
        if (in_array("database.read", $this->di->getServices())) {
            $this->getWriteDb()->close();
        }
        if (in_array("database.write", $this->di->getServices())) {
            $this->getReadDb()->close();
        }
    }

    /**
     * setup the redis service for DB caching
     * */
    protected function setupRedisDbClient()
    {
        $r = new RedisService($this);
        if ($this->accountReference) {
            $redisPrefix = 'lote:db:' . $this->accountReference . ':';
        } else {
            $redisPrefix = 'lote:db:_master:';
        }
        $r->setup($this->getConfig()->get("redis.db"), $redisPrefix);
        return $r;
    }

    /**
     * Get the redis service for DB caching
     * @return RedisService
     * */
    public function getRedisDbClient()
    {
        return $this->di->get("redis.db");
    }

    /**
     * Setup the file cache object
     * @return \SidraBlue\Lote\Cache\File\Base
     */
    public function setupFileCache()
    {
        return CacheFactory::createInstance($this);
    }

    /**
     * Get the file cache
     * @return \SidraBlue\Lote\Cache\File\Base
     */
    public function getFileCache()
    {
        return $this->di->get("filecache");
    }

    /**
     * Setup the queue object
     * @return Queue
     */
    public function setupQueue()
    {
        return new Queue($this);
    }

    /**
     * Setup the queue object
     * @return Queue
     */
    public function getQueue()
    {
        return $this->di->get("queue");
    }

    /**
     * Setup the logger set object
     * @return LoggerSet
     */
    public function setupLoggerSet()
    {
        return new LoggerSet($this->getConfig(), $this);
    }

    /**
     * Get the logger set object
     * @return LoggerSet
     */
    public function getLoggers()
    {
        return $this->di->get("logger.set");
    }

    /**
     * setup the timezone object
     * @return Timezone
     */
    public function setupTimezone()
    {
        return new Timezone($this);
    }

    /**
     * Get the timezone object
     * @return Timezone
     */
    public function getTimezone()
    {
        return $this->di->get("timezone");
    }

    /**
     * Get the request object
     * @return AccountEntity
     */
    public function getAccount()
    {
        return $this->di->get("account");
    }

    /**
     * Get the request object
     * @return Data
     */
    public function getData()
    {
        return $this->di->get("data");
    }

    /**
     * Get the request object
     * @return \Symfony\Component\HttpFoundation\Response
     * @todo - move this back to the web state
     */
    public function getResponse()
    {
        return $this->di->get("response");
    }

    /**
     * Get the request object
     * @return LoginSession
     */
    public function getLogin()
    {
        return $this->di->get("login.session");
    }

    /**
     * Get the request object
     * @return Ldap
     */
    public function getLdap()
    {
        return $this->di->get("ldap");
    }

    /**
     * Get the request object
     * @return Plugin
     */
    public function getPlugins()
    {
        return $this->di->get("plugins");
    }

    /**
     * Get the settings object
     * @return Setting
     */
    public function getSettings()
    {
        return $this->di->get("settings");
    }

    /**
     * Get the settings object
     * @return S3Client
     */
    public function getS3Client()
    {
        return $this->di->get("s3client");
    }

    /**
     * Setup the view object
     * @return  \SidraBlue\Lote\View\Base
     */
    protected function setupS3Client()
    {
        return $this->getS3ClientByScope();
    }

    /**
     * Get the settings object
     * @return S3Client
     */
    public function getS3PublicClient()
    {
        return $this->di->get("s3client.public");
    }

    /**
     * Setup the view object
     * @return  \SidraBlue\Lote\View\Base
     */
    protected function setupS3PublicClient()
    {
        $scope = "";
        if($this->getSettings()->get("media.adapter.public.enabled", false)) {
            $scope = ".public";
        }
        return $this->getS3ClientByScope($scope);
    }

    /**
     * Get the settings object
     * @return S3Client
     */
    public function getS3PrivateClient()
    {
        return $this->di->get("s3client.private");
    }

    /**
     * Setup the view object
     * @return  \SidraBlue\Lote\View\Base
     */
    protected function setupS3PrivateClient()
    {
        $scope = "";
        if($this->getSettings()->get("media.adapter.private.enabled", false)) {
            $scope = ".private";
        }
        return $this->getS3ClientByScope($scope);
    }

    /**
     * Get the S3 client by a scope
     *
     * @access private
     * @param string $scope
     * @return S3Client
     * */
    private function getS3ClientByScope($scope = "")
    {
        $result = false;
        $settings = $this->getSettings();
        if ($settings->get("media.adapter{$scope}.aws_key", $settings->get("media.adapter.aws_key", false))) {
            $config = $this->getBaseS3Configuration($scope);
            $result = new S3Client($config);
        }
        return $result;
    }

    /**
     * Get the base S3 configuration for a given scope
     *
     * @access private
     * @param string $scope
     * @return array
     * */
    private function getBaseS3Configuration($scope = "")
    {
        $settings = $this->getSettings();
        return [
            'credentials' => [
                'key' => $settings->get("media.adapter{$scope}.aws_key", $settings->get("media.adapter.aws_key", false)),
                'secret' => $settings->get("media.adapter{$scope}.aws_secret", $settings->get("media.adapter.aws_secret", false)),
            ],
            'region' => $settings->getSettingOrConfig("media.adapter{$scope}.aws_region", 'ap-southeast-2'),
            'version' => 'latest',
            'scheme' => 'http'
        ];
    }

    /**
     * Setup the view object
     * @return  \SidraBlue\Lote\View\Base
     */
    protected function setupView()
    {
        return ViewFactory::createInstance($this, "html");
    }

    /**
     * Setup the view object
     * @return  \SidraBlue\Lote\View\Base
     */
    protected function setupLdap()
    {
        return new Ldap($this);
    }

    /**
     * Setup the context object
     * @return  Context
     */
    protected function setupContext()
    {
        return new Context();
    }

    /**
     * Setup the access object
     * @return  Context
     */
    protected function setupAccess()
    {
        return new Access($this);
    }

    /**
     * Setup the account object
     * @return Account|false
     */
    protected function setupAccount()
    {
        $result = false;
        if ($db = $this->getMasterDb()) {
            $e = new AccountEntity($this);
            if ($e->loadByField('reference', $this->getSettings()->get('system.reference'))) {
                $result = $e;
            }
        }
        return $result;
    }

    /**
     * Setup the data object
     * @return Data
     */
    protected function setupSystemData()
    {
        $d = new Data();
        return $d;
    }

    /**
     * Setup the sites object
     * @return  Context
     */
    protected function setupSites()
    {
        return new Site($this);
    }

    /**
     * Setup the identifier object
     * @return LoginSession
     */
    protected function setupLoginSession()
    {
        return new LoginSession($this);
    }

    /**
     * Setup the identifier object
     * @return Setting
     */
    protected function setupSettings()
    {
        return new Setting($this);
    }

    /**
     * Setup the content plugin
     * @return Setting
     */
    protected function setupPlugins()
    {
        return new Plugin($this);
    }

    /**
     * Setup the audit object
     * @return AuditLog
     */
    protected function setupAudit()
    {
        return new AuditLog($this);
    }

    /**
     * Setup the user object
     * @return User
     */
    protected function setupUser()
    {
        $u = new User($this);
        $u->setAuditing(false);
        return $u;
    }

    /**
     * Setup the groups model
     * @return GroupService
     */
    protected function setupGroupService()
    {
        return new GroupService($this);
    }

    /**
     * Setup the user login session
     * @return User
     */
    protected function setupUserSession()
    {
        return new UserSession($this);
    }

    /**
     * Setup the view object
     * @return  Array
     */
    protected function overrideRoute()
    {
        return $this->route;
    }

    /**
     * Assign
     * @param array $newRouteData
     */
    public function setRouteData($newRouteData)
    {
        $this->route = $newRouteData;
        $this->di->set("route", function () {
            return $this->overrideRoute();
        });
    }

    /**
     * Create the request object
     */
    protected function setupRequest()
    {
        //implement in child class
        return false;
    }

    /**
     * Create the response object
     */
    protected function setupResponse()
    {
        //implement in child class
        return false;
    }

    protected function setupParentAccount()
    {
        return new ParentAccount($this);
    }

    protected function setupSuperParentAccount()
    {
        $pa = new ParentAccount($this);
        $pa->loadSuperParentAccount();
        return $pa;
    }

    /**
     * Get the DI container for this state
     * @return \Aura\Di\Container
     * */
    public function getDi()
    {
        return $this->di;
    }

    /**
     * Setup the signal manager
     * @access private
     * @return \Aura\Signal\Manager
     * */
    private function setupSignal()
    {
        $signalManager = new Signal\Manager(new Signal\HandlerFactory, new Signal\ResultFactory,
            new Signal\ResultCollection);
        $this->getApps()->setupSignals($signalManager);
        return $signalManager;
    }

    /**
     * Get the signal manager
     * @access private
     * @return \Aura\Signal\Manager
     * */
    public function getSignal()
    {
        return $this->di->get('signal');
    }

    /**
     * Get the audit object
     * @return AuditLog
     */
    public function getAudit()
    {
        return $this->di->get('audit');
    }

    /**
     * Get the user
     * @access private
     * @return User
     * */
    public function getUser()
    {
        return $this->di->get('user');
    }

    /**
     * Get the group service object
     * @access private
     * @return GroupService
     * */
    public function getGroupService()
    {
        return $this->di->get('group.service');
    }

    /**
     * Get the user
     * @access private
     * @return UserSession
     * */
    public function getUserSession()
    {
        return $this->di->get('user.session');
    }

    /**
     * Get the locale for this request. This function returns 'en' as a default, with the Web and Cli child classes
     * overriding this with more specific functionality
     * @return string
     * */
    protected function getLocale()
    {
        return 'en';
    }

    /**
     * Setup the translator, loading the base translation files plus the system and module specific ones
     * @access private
     * @return \Aura\Signal\Manager
     * */
    private function setupTranslator()
    {
        $translator = new Translator($this->getLocale());
        $translator->setFallbackLocales(["en"]);
        $translator->addLoader('php', new PhpFileLoader());

        $translationFiles = $this->getTranslationFiles(LOTE_CORE_PATH . 'res/language/');
        foreach ($translationFiles as $file) {
            /**
             * @var $file \SplFileInfo
             * */
            $fileName = $file->getBasename('.php');
            $fileParts = explode('.', $fileName);
            if (count($fileParts) == 2) {
                $translator->addResource('php', $file->getPathname(), $fileParts[1]);
            }
        }
        return $translator;
    }

    /**
     * Get the translation files from a specified directory
     * @param string $directory
     * @access private
     * @return array
     * */
    protected function getTranslationFiles($directory)
    {
        $result = [];
        if (is_dir($directory)) {
            $fileIterator = new \FilesystemIterator($directory);
            $regexIterator = new \RegexIterator($fileIterator, '#.(php)$#');
            /* @var $file \SplFileInfo */
            foreach ($regexIterator as $file) {
                $result[] = $file;
            }
        }
        return $result;
    }

    /**
     * Get the translation object
     * @return  \Symfony\Component\Translation\Translator
     */
    public function getTranslator()
    {
        return $this->di->get("translator");
    }

    /**
     * Create a database connection for use in writing
     * @access private
     * @return \Doctrine\DBAL\Connection - the database connection
     */
    private function setupWriteDatabase()
    {
        return $this->setupDatabase('write');
    }

    /**
     * Create a database connection for use for reading
     * @access private
     * @return \Doctrine\DBAL\Connection - the database connection
     * @todo - implement multiple database read connection support
     */
    private function setupReadDatabase()
    {
        return $this->setupDatabase('read');
    }

    /**
     * Initialise the configuration variables
     * @access private
     * @return Config - the config object
     */
    private function setupConfig()
    {
        return new Config($this->configReference);
    }

    /**
     * Get the configuration variables
     * @access private
     * @return Config
     */
    public function getConfig()
    {
        return $this->di->get("config");
    }

    /**
     * Create a database connection to the master database
     * @access private
     * @return \Doctrine\DBAL\Connection - the database connection
     */
    private function setupMasterDatabase()
    {
        $result = false;
        $res = $this->getConfig()->get();
        if (isset($res['database.master']) && isset($res['database.master']['type'])) {
            return $this->connectionManager->createConnection($res['database.master']);
        }
        return $result;
    }

    /**
     * Create a database connection to the parent database
     * @access private
     * @return \Doctrine\DBAL\Connection - the database connection
     */
    private function setupParentDatabase()
    {
        $result = false;
        $a = new Account($this);
        if ($parent = $a->getParentAccount($this->getSettings()->get('system.reference'))) {
            $res = $this->getConfig()->get();
            if (isset($res['database.master'])) {
                $config = $res['database.master'];
                $params = [];
                $params['type'] = 'pdo_mysql';
                $params['host'] = $config['host'];
                $params['name'] = $parent['db_name'];
                $params['user'] = $config['user'];
                $params['pass'] = $config['pass'];
                $result = $this->connectionManager->createConnection($params);
            }
        }
        return $result;
    }

    /**
     * Create a database connection to the superparent database
     * @access private
     * @return \Doctrine\DBAL\Connection - the database connection
     */
    private function setupSuperParentDatabase()
    {
        $result = false;
        $a = new Account($this);
        if ($parent = $a->getSuperParentAccount($this->getSettings()->get('system.reference'))) {
            $res = $this->getConfig()->get();
            if (isset($res['database.master'])) {
                $config = $res['database.master'];
                $params = [];
                $params['type'] = 'pdo_mysql';
                $params['host'] = $config['host'];
                $params['name'] = $parent['db_name'];
                $params['user'] = $config['user'];
                $params['pass'] = $config['pass'];
                $result = $this->connectionManager->createConnection($params);
            }
        }
        return $result;
    }

    /**
     * Create a database connection based on the state (live, dev, test) and the type (read or write)
     * @access private
     * @param string $type
     * @return \Doctrine\DBAL\Connection - the database connection
     */
    private function setupDatabase($type)
    {
        $res = $this->getConfig()->get();
        if (isset($res['database.replicated']) && $res['database.replicated'] == "1") {
            $database = $this->connectionManager->createConnection($res['database'][$type]);
        } else {
            $database = $this->connectionManager->createConnection($res['database']);
        }
        return $database;
    }

    /**
     * Get the database connection used for reading
     * @access public
     * @return \Doctrine\DBAL\Connection - the database connection
     */
    public function getReadDb()
    {
        return $this->di->get("database.read");
    }

    /**
     * Get the database connection used for writing
     * @access public
     * @return \Doctrine\DBAL\Connection - the database connection
     */
    public function getWriteDb()
    {
        return $this->di->get("database.write");
    }

    /**
     * Get the master database connection
     * @access public
     * @return \Doctrine\DBAL\Connection - the database connection
     */
    public function getMasterDb()
    {
        return $this->di->get("database.master");
    }

    /**
     * Get the parent database connection
     * @access public
     * @return \Doctrine\DBAL\Connection - the database connection
     */
    public function getParentDb()
    {
        return $this->di->get("database.parent");
    }

    /**
     * Get the superparent database connection
     * @access public
     * @return \Doctrine\DBAL\Connection - the database connection
     */
    public function getSuperParentDb()
    {
        return $this->di->get("database.superparent");
    }

    /**
     * Get child database connection
     * @access public
     * @return \Doctrine\DBAL\Connection - the database connection
     */
    public function getChildren()
    {
        $a = new Account($this);
        return $a->getAllPushDownAccounts($this->getSettings()->get('system.reference'));
    }

    public function getChildDb($system = '')
    {
        $result = false;
        $childrenArr = $this->getChildren();
        if (is_array($childrenArr)) {
            foreach ($childrenArr as $child) {
                $reference = $child['reference'];
                if ($system == $reference) {
                    /* @todo Need to get database setting by reading the ini file corresponding to the selected client. This is not needed if the db settings are same for both parent and client. */
                    $result = $this->getDbConnectionByName($child['db_name']);
                }
            }
        }
        return $result;
    }

    private function getDbConnectionByName($dbName)
    {
        $result = false;
        if (!empty($dbName)) {
            $sm = $this->getMasterDb()->getSchemaManager();
            $databases = $sm->listDatabases();
            if (in_array($dbName, $databases)) {
                $res = $this->getConfig()->get();
                $config = $res['database.master'];
                $params = [];
                $params['type'] = 'pdo_mysql';
                $params['host'] = $config['host'];
                $params['name'] = $dbName;
                $params['user'] = $config['user'];
                $params['pass'] = $config['pass'];
                try {
                    $result = $this->connectionManager->createConnection($params);
                } catch (\Exception $e) {

                }
            }
        }
        return $result;
    }

    /**
     * @param string $reference
     * @return false|\Doctrine\DBAL\Connection
     * */
    public function getDescendantDb($reference)
    {
        $result = false;
        $m = new Account($this);
        if ($m->isDescendantOf($this->accountReference, $reference)) {
            if ($descendant = $m->getAccountByReference($reference)) {
                $result = $this->getDbConnectionByName($descendant['db_name']);
            }
        }
        return $result;
    }

    /**
     * Get the site Application configuration
     * @access public
     * @return Composition
     */
    public function getApps()
    {
        return $this->di->get("apps");
    }

    /**
     * Check if the system is installed in the current database
     * @access public
     * @return boolean
     * */
    public function isInstalled()
    {
        return $this->getWriteDb()->getSchemaManager()->listTableDetails('sb__app')->hasColumn('id');
    }

    /**
     * Setup the installed site systems and plugins
     * @return Composition
     * */
    private function setupApps()
    {
        $c = new Composition($this, $this->isInstalled());
        if ($this->isInstalled()) {
            $c->setupAccess($this->getContext());
            $c->setupPlugins();
            $c->setupData();
            $c->setupClosures();
        }
        return $c;
    }

    /**
     * Get the view object
     * @return  \SidraBlue\Lote\View\Base
     */
    public function getView()
    {
        return $this->di->get("view");
    }

    /**
     * Get the context object
     * @return  Context
     */
    public function getContext()
    {
        return $this->di->get("context");
    }

    /**
     * Get the access object
     * @return  Access
     */
    public function getAccess()
    {
        return $this->di->get("access");
    }

    /**
     * Get the route for this request, specifying the system, controller, action and parameters
     * @return array
     * */
    public function getRoute()
    {
        return $this->di->get("route");
    }

    /**
     * Get the site setup for this install
     * @return Site
     * */
    public function getSites()
    {
        return $this->di->get("site");
    }

    /**
     * Get parent account.
     * @return ParentAccount
     */
    public function getParentAccount()
    {
        return $this->di->get('parent.account');
    }

    /**
     * Get parent account.
     * @return ParentAccount
     */
    public function getSuperParentAccount()
    {
        return $this->di->get('parent.super.account');
    }

    /**
     * setup the account config file system
     * @return Filesystem
     * */
    public function setupAccountConfigFileSystem()
    {
        $s3Client = new S3Client([
            'credentials' => [
                'key' => $this->getConfig()->get("client.aws.config.key"),
                'secret' => $this->getConfig()->get("client.aws.config.secret")
            ],
            'region' => $this->getConfig()->get("client.aws.config.region", 'ap-southeast-2'),
            'version' => 'latest',
            'scheme' => 'http'
        ]);
        $adapter = new AwsS3Adapter($s3Client, $this->getConfig()->get("client.aws.config.bucket"),
            $this->getConfig()->get("client.aws.config.prefix"));
        return new Filesystem($adapter);
    }

    /**
     * Get the account config file system
     * @return Filesystem
     * */
    public function getAccountConfigFileSystem()
    {
        return $this->di->get("client.account.config.file_system");
    }

    /**
     * Get the parent state
     * @return Base
     * */
    public function setupParentState()
    {
        $className = get_class($this);
        $result = new $className($this->getParentAccount()->getReference());
        $result->getUrl()->setUseRequest(false);
        return $result;
    }

    /**
     * Get the superparent state
     * @return Base
     * */
    public function setupSuperParentState()
    {
        $result = null;
        $className = get_class($this);
        if($this->getAccount()->parent_reference) {
            $superParentReference = $this->getSuperParentAccount()->getReference();
            if($superParentReference) {
                $result = new $className($superParentReference);
                $result->getUrl()->setUseRequest(false);
            }
        } else {
            $result = $this;
        }
        return $result;
    }

    /**
     * Get the master state
     * @return Base
     * */
    public function setupMasterState()
    {
        $className = get_class($this);
        $result = new $className(null, true);
        $result->getUrl()->setUseRequest(false);
        return $result;
    }

    /**
     * Get the parent state
     * @return Base
     * */
    public function getParentState()
    {
        return $this->di->get("parent.state");
    }

    /**
     * Get the parent state
     * @return Base
     * */
    public function getSuperParentState()
    {
        return $this->di->get("parent.super.state");
    }

    /**
     * Get the parent state
     * @return Base
     * */
    public function getMasterState()
    {
        return $this->di->get("master.state");
    }

    /**
     * setup the URL Service
     * @return Url
     * */
    public function setupUrl()
    {
        return new Url($this);
    }

    /**
     * Get the parent state
     * @return Url
     * */
    public function getUrl()
    {
        return $this->di->get("url");
    }

    /**
     * setup the ImageServer Service
     * @return ImageServer
     * */
    public function setupImageServer()
    {
        return new ImageServer($this);
    }

    /**
     * Get the parent state
     * @return ImageServer
     * */
    public function getImageServer()
    {
        return $this->di->get("imageserver");
    }

    /**
     * Setup the Closure service
     * @return Closure
     * */
    public function setupClosures()
    {
        return new Closure($this);
    }

    /**
     * Get the Closure service
     * @return Closure
     * */
    public function getClosures()
    {
        return $this->di->get("closures");
    }

    /**
     * Setup the Timer service
     * @return Timer
     * */
    public function setupTimer()
    {
        return new Timer($this);
    }

    /**
     * Get the Closure service
     * @return Timer
     * */
    public function getTimer()
    {
        return $this->di->get("timer");
    }

    /**
     * Setup the Timer service
     * @return Manager
     * */
    public function setupEventManager()
    {
        return new Manager($this);
    }

    /**
     * Get the Closure service
     * @return Manager
     * */
    public function getEventManager()
    {
        return $this->di->get("event.manager");
    }

    /**
     * Setup the Debug Service
     * @return Debug
     * */
    public function setupDebug()
    {
        return new Debug($this);
    }

    /**
     * Get the Closure service
     * @return Debug
     * */
    public function getDebug()
    {
        return $this->di->get("debug");
    }

    /**
     * Check if the current state is CLI based
     * */
    public function isCli()
    {
        return php_sapi_name() == 'cli';
    }

}
