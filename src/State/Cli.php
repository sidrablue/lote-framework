<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\State;

use Aura\Cli\ExceptionFactory;
use Aura\Cli\Getopt;
use Aura\Cli\Option;
use Aura\Cli\OptionFactory;
use Aura\Cli\Translator;
use Aura\Di;
use Aura\Cli\Context as CliContext;
use SidraBlue\Lote\Console\Application;

class Cli extends Base
{

    /**
     *  */
    public function setup()
    {
        parent::setup();
        $this->di->set(
            "request",
            function () {
                return $this->setupRequest();
            }
        );
        //$this->di->set("response", function(){return new Response();});
        //$this->di->set("router", function(){return $this->setupRouter();});
        $this->di->set(
            "route",
            function () {
                return $this->setupRoute();
            }
        );
        $this->di->set(
            "console.application",
            function () {
                return $this->setupConsoleApplication();
            }
        );
    }

    /**
     * Create the request object
     * @return Application
     */
    protected function setupConsoleApplication()
    {
        $c = new Application();
        $c->setState($this);
        $frameworkCommandDir = realpath(str_replace("\\", "/",__DIR__).'/../Console/Command');
        $c->addCommandsFromPath($frameworkCommandDir, 'SidraBlue\Lote\Console\Command');
        $this->getApps()->setupConsole($c);
        return $c;
    }

    /**
     * Create the request object
     * @return Application
     */
    public function getConsoleApplication()
    {
        return $this->di->get("console.application");
    }

    /**
     * Create the request object
     * @return CliContext
     */
    protected function setupRequest()
    {
        $c = new CliContext($GLOBALS);
        return $c;
    }

    /**
     * Get the request object
     * @return CliContext
     */
    public function getRequest()
    {
        return $this->di->get("request");
    }

    /**
     * Setup the view object
     * @return array
     */
    protected function setupRoute()
    {
        $result = [];
        $options = [
            'controller' => [
                'long' => 'controller',
                'short' => 'c',
                'param' => Option::PARAM_REQUIRED,
                'multi' => false,
                'default' => null,
            ],
            'action' => [
                'long' => 'action',
                'short' => 'a',
                'param' => Option::PARAM_REQUIRED,
                'multi' => false,
                'default' => null,
            ],
            'module' => [
                'long' => 'module',
                'short' => 'mod',
                'param' => Option::PARAM_OPTIONAL,
                'multi' => true,
                'default' => null,
            ],
            'system' => [
                'long' => 'system',
                'short' => 'sys',
                'param' => Option::PARAM_OPTIONAL,
                'multi' => true,
                'default' => null,
            ],
            'mode' => [
                'long' => 'mode',
                'short' => 'm',
                'param' => Option::PARAM_OPTIONAL,
                'multi' => false,
                'default' => null,
            ],
            'fix' => [
                'long' => 'fix',
                'short' => 'f',
                'param' => Option::PARAM_OPTIONAL,
                'multi' => false,
                'default' => null,
            ],
            'account' => [
                'long' => 'account',
                'short' => 'a',
                'param' => Option::PARAM_OPTIONAL,
                'multi' => false,
                'default' => null,
            ],
        ];

        $cmdOptions = new Getopt(
            new OptionFactory,
            new ExceptionFactory(
                new Translator(['messages' => $this->getTranslationFiles(LOTE_CORE_PATH . 'res/language/')]
                )
            )
        );
        $cmdOptions->init($options, false);

        $cmdOptions->load($this->getRequest()->getArgv());
        $params = $cmdOptions->getOptions();
        foreach ($params as $o) {
            /**
             * @var \Aura\Cli\Option $o
             */
            $result[$o->getName()] = $o->getValue();
        }
        if($result['mode']=='check') {
            $result['controller'] = '\SidraBlue\Lote\Controller\Install\Update';
            $result['action'] = 'check';
        }
        elseif($result['mode']=='update') {
            $result['controller'] = '\SidraBlue\Lote\Controller\Install\Update';
            $result['action'] = 'update';
        }
        elseif($result['mode']=='install') {
            $result['controller'] = '\SidraBlue\Lote\Controller\Install\Update';
            $result['action'] = 'install';
        }
        elseif($result['mode']=='uninstall') {
            $result['controller'] = '\SidraBlue\Lote\Controller\Install\Update';
            $result['action'] = 'uninstall';
        }

        return $result;
    }

}
