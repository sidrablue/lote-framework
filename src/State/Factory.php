<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\State;

class Factory
{
    public static function newInstance()
    {
        if(php_sapi_name()=='cli') {
            return new Cli();
        }
        else {
            return new Web();
        }
    }
}