<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\State;

use Lote\Module\Analytics\Service\Analytics;
use Lote\Module\Page\Website\Model\Page as PageModel;
use SidraBlue\Lote\Model\Url;
use SidraBlue\Lote\Model\UrlPassword;
use SidraBlue\Lote\Service\Browscap\Browscap;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\RequestContext;
use SidraBlue\Lote\Routing\RouteCollection;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use SidraBlue\Lote\View\Factory as ViewFactory;

class Web extends Base
{

    /**
     *  */
    public function setup()
    {
        parent::setup();
        $this->di->set(
            "request",
            function () {
                return $this->setupRequest();
            }
        );
        $this->di->set(
            "router",
            function () {
                return $this->setupRouter();
            }
        );
        $this->di->set(
            "route",
            function () {
                return $this->setupRoute();
            }
        );
        $this->di->set(
            "analytics",
            function () {
                return $this->setupAnalytics();
            }
        );
        $this->di->set(
            "browscap",
            function () {
                return $this->setupBrowscap();
            }
        );
    }

    /**
     * Create the request object
     * @return Request
     */
    protected function setupRequest()
    {
        $r = Request::createFromGlobals();
        if(isset($_COOKIE['sessionid'])) {
            session_id($_COOKIE['sessionid']);
        }
        $s = new Session();
        if(isset($_COOKIE['sessionid'])) {
            session_id($_COOKIE['sessionid']);
            $s->setId($_COOKIE['sessionid']);
        }
        if(!$s->getId()) {
            $s->start();
        }
        $r->setSession($s);
        return $r;
    }

    /**
     * Setup the analytics object
     * @return Analytics
     */
    protected function setupAnalytics()
    {
        return new Analytics($this);
    }

    /**
     * Setup the browscap object
     * @return \StdClass
     */
    protected function setupBrowscap()
    {
        return new Browscap();
    }

    /**
     * Setup the view object
     * @return  \SidraBlue\Lote\View\Base
     */
    protected function setupView()
    {
        $route = $this->getRoute();
        $reference = "html";
        if (isset($route['format'])) {
            $reference = preg_replace('/^(\.)(.*)/', '\2', $route['format']);
        } else {
            $acceptType = $this->getRequest()->getAcceptableContentTypes();
            if (isset($acceptType[0]) && $acceptType[0]) {
                $reference = $acceptType[0];
            }
        }
        return ViewFactory::createInstance($this, $reference);
    }

    /**
     * @return RouteCollection
     * */
    private function setupRouter()
    {
        $router = new RouteCollection();
        return $router;
    }

    public function hasStaticMatch($url)
    {
        $result = false;
        $context = new RequestContext();
        $context->fromRequest($this->getRequest());
        $matcher = new UrlMatcher($this->getRouter(), $context);
        try {
            $result = $matcher->match($url);
        } catch (\Exception $e) {
            //do nothing
        }
        return $result;
    }

    /**
     * Get a dynamic match for a URL
     * */
    public function getDynamicMatch($url)
    {
        $controllers = [];
        $controllers['page.item'] = 'Website Page View';
        $controllers['media.file'] = 'media.website.download';
        $controllers['sb_cms_news_article'] = 'Website News Article View';
        $controllers['sb_blog_post'] = 'Website Blog Item View';
        $controllers['sb_seotools_file'] = 'seotools.website.file.view';
        $controllers['sb_newsletter_publication'] = 'newsletter.publication.website.view';
        $controllers['sb_cms_product_folder'] = 'Website Product Folder View';
        $controllers['sb_cms_product_item'] = 'Website Product Item View';
        $controllers['sz_newsletter'] = 'Website SZ Newsletter View';
        $controllers['sb_newsletter'] = 'newsletter.publication.website.view';
        $controllers['sb_sessionkeeper_session'] = 'website.sessionkeeper.view-session';
        $result = [];
        $urlModel = new Url($this);
        if($match = $urlModel->match($url)) {
            if(isset($controllers[$match['object_ref']]) && $route = $this->getRouter()->get($controllers[$match['object_ref']])) {
                if(!empty($match['data'])){
                    $result = $route->getDefaults();
                    $result['_dynamic'] = true;
                    $data = json_decode($match['data'], true);
                    foreach($data as $k=>$v){
                        $result[$k] = $v;
                    }
                    $urlPasswordModel = new UrlPassword($this);
                    $result['_passwords'] = $urlPasswordModel->getCurrentPasswords($match['id']);
                }else{
                    $result = $route->getDefaults();
                    $result['_dynamic'] = true;
                    $result['id'] = $match['object_id'];
                    $urlPasswordModel = new UrlPassword($this);
                    $result['_passwords'] = $urlPasswordModel->getCurrentPasswords($match['id']);
                }
            }
        }
        /* View publication Latest */

            if(strpos($url, '/latest') !== false){
                $url = str_replace("/latest","",$url);
                if($match = $urlModel->match($url)) {
                    $controllers['sb_newsletter_publication'] = 'newsletter.publication.website.view.latest';
                    if(isset($controllers[$match['object_ref']]) && $route = $this->getRouter()->get($controllers[$match['object_ref']])) {
                        $result = $route->getDefaults();
                        $result['_dynamic'] = true;
                        $result['id'] = $match['object_id'];
                    }
                }
            }

        /* View publication Archive */
        if(strpos($url, '/archive') !== false){
            $url = str_replace("/archive","",$url);
            if($match = $urlModel->match($url)) {
                $controllers['sb_newsletter_publication'] = 'newsletter.publication.website.view.archive';
                if(isset($controllers[$match['object_ref']]) && $route = $this->getRouter()->get($controllers[$match['object_ref']])) {
                    $result = $route->getDefaults();
                    $result['_dynamic'] = true;
                    $result['id'] = $match['object_id'];
                }
            }
        }

        /* View publication submission */
        if(strpos($url, '/submit') !== false){
            $url = str_replace("/submit","",$url);
            if($match = $urlModel->match($url)) {
                $controllers['sb_newsletter_publication'] = 'newsletter.publication.website.view.submit';
                if(isset($controllers[$match['object_ref']]) && $route = $this->getRouter()->get($controllers[$match['object_ref']])) {
                    $result = $route->getDefaults();
                    $result['_dynamic'] = true;
                    $result['id'] = $match['object_id'];
                }
            }
        }

        return $result;
    }


    public function getDefaultMatch()
    {
        $result = false;
        $m = new PageModel($this);
        if ($route = $m->getDefaultRoute()) {
            $result = $route;
        }
        return $result;
    }

    public function getLandingMatch()
    {
        $route = [];
        $route['controller'] = 'Lote\Landing';
        $route['_method'] = 'noDefaultFound';
        return $route;
    }

    public function getRequestUrl($removeParams = true)
    {
        $requestUri = $this->getRequest()->getRequestUri();
        if($removeParams) {
            $requestUri = preg_replace('/\?.*/', '', $requestUri);
        }
        $basePath = $this->getRequest()->getBasePath();
        $result = str_replace($basePath, "", $requestUri);
        if ($result != '/') {
            $result = rtrim($result, '/');
        }
        $result = preg_replace('#//(.*)#','/\1', $result);
        return $result;
    }

    protected function tryStaticMatch($url)
    {
        $context = new RequestContext();
        $context->fromRequest($this->getRequest());
        $matcher = new UrlMatcher($this->getRouter(), $context);
        try {
            $result = $matcher->match($url);
            if ($result) {
                if ($customMethod = $this->getRequest()->get("_method", false)) {
                    $result['_method'] = $customMethod;
                }
            }
        } catch (\Exception $e) {
            //do a phpcr service route check
            $result = false;
        }
        return $result;
    }

    protected function getStaticMatch() {
        if(!$result = $this->tryStaticMatch($this->getRequestUrl(false))){
            $result = $this->tryStaticMatch($this->getRequestUrl(true));
        }
        return $result;
    }

    /**
     * Setup the view object
     * @return  Array
     */
    protected function setupRoute()
    {
        $url = $this->getRequestUrl();
        if(!$result = $this->getStaticMatch()) {
            $result = $this->getDynamicMatch($url);
        }
        if(!$result) {
            if ($url == '/' && $this->getApps()->hasSystem('cms') && $this->getApps()->hasModule('page')) {
                $result = $this->getDefaultMatch();
                if(!$result) {
                    $result = $this->getLandingMatch();
                }
            }
        }
        else {
            if($url=='/') {
                $result['url'] = $this->getRequest()->getSchemeAndHttpHost().'/';
            }
            else {
                $result['url'] = $this->getRequest()->getSchemeAndHttpHost().'/'.$url;
            }
        }
        return $result;
    }

    /**
     * Get the url router
     * @return \Symfony\Component\Routing\RouteCollection
     * */
    public function getRouter()
    {
        return $this->di->get("router");
    }

    /**
     * Get the request object
     * @return \Symfony\Component\HttpFoundation\Request
     */
    public function getRequest()
    {
        return $this->di->get("request");
    }

    /**
     * Get the analytics object
     * @return Analytics
     */
    public function getAnalytics()
    {
        return $this->di->get("analytics");
    }

    /**
     * Get the analytics object
     * @return Browscap
     */
    public function getBrowscap()
    {
        return $this->di->get("browscap");
    }

    /**
     * Get the locale for this request
     * @return String
     */
    protected function getLocale()
    {
        return $this->getRequest()->getLocale();
    }

    public function getSystemByUrl($url, $default = 'website')
    {
        $result = $default;
        do {
            if($match = $this->hasStaticMatch($url)) {
                if(isset($match['_handler']['system'])) {
                    $result = $match['_handler'];
                    if(is_array($result) && isset($result['system'])) {
                        $result = $result['system'];
                    }
                    break;
                }
            }
            $url = substr($url, 0, strrpos($url, '/'));
        } while ($url);
        return $result;
    }

}
