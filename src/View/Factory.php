<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\View;

use SidraBlue\Util\Strings;

class Factory
{

    private static $referenceMap = [
        'html'       => 'SidraBlue\Lote\View\Transform\Html\Full',
        'lote/shtml' => 'SidraBlue\Lote\View\Transform\Html\Single',
        'shtml'      => 'SidraBlue\Lote\View\Transform\Html\Single',
        'embed'      => 'SidraBlue\Lote\View\Transform\Html\Embed',
        'json'       => 'SidraBlue\Lote\View\Transform\Json',
        'jsonp'       => 'SidraBlue\Lote\View\Transform\Jsonp',
        'rss'        => 'SidraBlue\Lote\View\Transform\Rss',
        'cli'        => 'SidraBlue\Lote\View\Transform\Cli',
        'wkpdf'      => 'SidraBlue\Lote\View\Transform\WkhtmlToPdf'
    ];

    private static $acceptMap = [
        'application/json'    => 'SidraBlue\Lote\View\Transform\Json',
        'application/rss+xml' => 'SidraBlue\Lote\View\Transform\Json'
    ];

    /**
     * Create an instance of the view
     * @param $state \SidraBlue\Lote\State\Web
     * @param string $reference
     * @return Base
     * */
    public static function createInstance($state, $reference = "html")
    {
        $className = false;
        if (php_sapi_name() == 'cli') {
            $className = self::$referenceMap['cli'];
        } else {
            $contentTypes = $state->getRequest()->getAcceptableContentTypes();
            foreach ($contentTypes as $type) {
                if (array_key_exists($type, self::$acceptMap)) {
                    $className = self::$acceptMap[$type];
                    break;
                }
            }
        }
        if(!$className) {
            if(Strings::startsWith("/_api/", $state->getRequest()->getRequestUri())) {
                $className = self::$referenceMap["json"];
            }
            else {
                $className = self::$referenceMap["html"];
            }
            if ( array_key_exists($reference, self::$referenceMap)) {
                $className = self::$referenceMap[$reference];
            }
        }
        return new $className($state);
    }

}
