<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\View\Transform;

use Aura\Cli\Stdio;
use Aura\Cli\StdioResource;
use Aura\Cli\Vt100;
use SidraBlue\Lote\View\Base as Base;

class Cli extends Base
{

    /**
     * @var Stdio $stdio
     * */
    private $stdio;

    protected function onCreate()
    {
        $this->stdio = new Stdio(new StdioResource('php://stdin', 'r'),
            new StdioResource('php://stdout', 'w+'),
            new StdioResource('php://stderr', 'w+'),
            new Vt100);
    }

    public function render()
    {
        if(isset($this->_renderData['success']) && $this->_renderData['success']==true) {
            $this->stdio->err('%2%k');
        }
        else {
            $this->stdio->err('%1%k');
        }
        foreach($this->_renderData as $k => $v) {
            $this->stdio->errln($k.' => '.var_export($v,true));
        }
        $this->stdio->err('%0%n');
        return;
        $this->stdio->outln('Hello World!');
        $this->stdio->out('Please enter some text: ');
        $input = $this->stdio->in();
        $this->stdio->errln('Input was ' . $input);
        return '';
        return json_encode($this->_renderData);
    }

}