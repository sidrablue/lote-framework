<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\View\Transform\Html;

use Dzango\Twig\Extension\Truncate;
use Lote\Module\Event\Model\Event;
use Lote\Module\News\Entity\Article;
use Lote\Module\Newsletter\Entity\Newsletter;
use Lote\Module\Newsletter\Model\Feed;
use Lote\Module\Newsletter\Model\NewsletterArticle;
use Lote\Module\Newsletter\Model\Publication;
use Lote\Module\Newsletter\Model\PublicationSettings;
use Lote\Module\Page\Model\ContentExtra;
use Lote\System\Client\Entity\Master\Account;
use Lote\System\Edm\Model\Campaign\CampaignNotification as CampaignNotificationModel;
use Lote\Module\Content\Cms\Model\Holder;
use Lote\Module\Page\Model\Content;
use Lote\Module\Page\Model\Item as PageItemModel;
use Psr\Log\LogLevel;
use SidraBlue\Lote\Entity\User as UserEntity;
use SidraBlue\Lote\Model\Group;
use SidraBlue\Lote\Model\Tag;
use SidraBlue\Lote\Model\Token;
use SidraBlue\Lote\Registry\Config;
use SidraBlue\Lote\Service\Content as ContentService;
use SidraBlue\Lote\Service\Timezone;
use SidraBlue\Lote\State\Web;
use SidraBlue\Lote\View\Base as ViewBase;
use SidraBlue\Lote\Controller\Base as BaseController;
use SidraBlue\Util\Country;
use SidraBlue\Util\Currency;
use SidraBlue\Util\Date;
use SidraBlue\Util\Industry;
use SidraBlue\Util\Strings;
use SidraBlue\Util\Time;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Twig_Extensions_Autoloader;
use Twig_Extensions_Extension_Text;
use Lote\Module\Association\Model\AssociationImages as ModelAssociationImages;
use Lote\Module\Norupwilson\Model\Project as ModelProject;
use Lote\Module\Norupwilson\Model\Display as ModelDisplay;
use Lote\Module\Norupwilson\Entity\Session as SessionEntity;
use Lote\Module\Norupwilson\Model\Page as PageModel;
use Lote\Module\News\Model\Article as ModelArticle;
use SidraBlue\Lote\Entity\Region\Country as CountryEntity;
use Lote\Module\Page\Entity\Item as PageEntity;
use Lote\Module\Content\Entity\Holder as HolderEntity;
use Lote\Module\Calendar\Model\CalendarEvent as CalendarEventModel;
use Lote\Module\Event\Model\CalendarEvent as CalendarEvent;
use SidraBlue\Lote\Model\SiteSkin as SkinModel;
use Lote\Module\Event\Controller\Cms\Calendar\Manage as CalendarM;
use SidraBlue\Lote\Model\Url;
use Lote\Module\Event\Model\CalendarEvent as CalMod;

class Base extends ViewBase implements \Twig_LoaderInterface
{

    /**
     * @var \Twig_Environment $_twig
     */
    protected $_twig;

    /**
     * @var array $paths
     * */
    public $paths = [];

    /**
     * @var array $customPaths
     * */
    public $customPaths = [];

    /**
     *  */
    protected function onCreate()
    {
        Twig_Extensions_Autoloader::register();

        $cache = false;
        if ($this->_state->getConfig()->get('cache.twig', '1') != '0') {
            $cacheDir = LOTE_ASSET_PATH . 'cache/'.LOTE_ACCOUNT_REF.'/twig';
            if (is_dir($cacheDir) && is_writable($cacheDir)) {
                $cache = $cacheDir;
            } elseif (!is_dir($cacheDir)) {
                if (mkdir($cacheDir, 0777, true)) {
                    $cache = $cacheDir;
                }
            }
        }

        $environmentVariables = ['cache' => $cache];


        $this->_twig = new \Twig_Environment($this, $environmentVariables);
        $this->_twig->addGlobal("state", $this->_state);
        $this->_twig->addGlobal("user", $this->_state->getUser());
        $this->_state->getApps()->setupView($this, 'twig');
        $filter = new \Twig_SimpleFilter('dump', function ($var) {
            dump($var);
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig_SimpleFilter('preg_match', function ($string, $pattern) {
            return preg_match($pattern, $string);
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig_SimpleFilter('generateHtmlId', function ($string) {
            return Strings::generateHtmlId($string);
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig_SimpleFilter('bool2str', function ($boolean) {
            $result = 'No';
            if ($boolean && $boolean != -1) {
                $result = 'Yes';
            }
            return $result;
        });
        $this->_twig->addFilter($filter);

        $function = new \Twig_SimpleFunction('isRoute', function ($route) {
            $result = false;
            $currentRoute = $this->_state->getRoute();
            if(is_array($currentRoute) && isset($currentRoute['_route'])) {
                if (is_string($route)) {
                    $result = $route == $currentRoute['_route'];
                } elseif (is_array($route)) {
                    $result = in_array($currentRoute['_route'], $route);
                }
            }
            return $result;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getPage', function ($id) {
            $result = [];
            $p = new PageEntity($this->_state);
            if ($p->load($id)) {
                $result = $p->getViewData();
            }
            return $result;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('imgUrl', function ($idOrReference, $width=0, $height=0, $size = 0, $mode='auto') {
            $c = new ContentService($this->_state);
            return $c->getImageCachedUrl($idOrReference, $width, $height, $size, $mode);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('imgUrlPreview', function ($idOrReference, $width=0, $height=0, $size = 0, $mode='auto') {
            $c = new ContentService($this->_state);
            return $c->getImageCachedUrl($idOrReference, $width, $height, $size, $mode, false, true);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('imgTsUrl', function ($idOrReference, $size = 0, $width=0, $height=0, $mode='auto') {
            $c = new ContentService($this->_state);
            $url = $c->getImageCachedUrl($idOrReference, $width, $height, $size, $mode, true);
            $url = str_replace(' ', '%20', $url);
            return $url;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('imgParentUrl', function ($idOrReference,  $size = 0, $width=0, $height=0, $mode='auto') {
            $parentReference = $this->_state->getParentAccount()->getReference();
            $s = new Web($parentReference);
            $c = new ContentService($s);
            $url = $c->getImageCachedUrl($idOrReference, $width, $height, $size, $mode);
            $url = str_replace($this->_state->getRequest()->getSchemeAndHttpHost(), $this->_state->getParentAccount()->getUrl(), $url);
            return $url;
        });
        $this->_twig->addFunction($function);


        $function = new \Twig_SimpleFunction('getLatestArticlesInPublication', function ($publication_id, $limit = false) {
            $p = new Publication($this->_state);
            $items = $p->getLatestArticlesInPublication($publication_id, $limit, false, $this->_state->getSettings()->get("szschool.website.swiper.tag", false));
            return $items;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getLatestEventsInPublication', function ($limit = '') {

            $start = date("Y-m-d H:i:s");
            $cal_id = $this->_state->getSettings()->get("szschool.website.stream.box.calendar_id");
            $modelEventObj = new CalMod($this->_state);
            $data = $modelEventObj->getCalendarEvents($cal_id, $start, '', $limit, '', false, true);
            return $data;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('imgTsParentUrl', function ($idOrReference, $size = 0, $width=0, $height=0, $mode='auto') {
            $parentReference = $this->_state->getParentAccount()->getReference();
            $s = new Web($parentReference);
            $c = new ContentService($s);
            $url = $c->getImageCachedUrl($idOrReference, $width, $height, $size, $mode, true);
            $url = str_replace($this->_state->getRequest()->getSchemeAndHttpHost(), $this->_state->getParentAccount()->getUrl(), $url);
            $url = str_replace(' ', '%20', $url);
            return $url;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('imgTsMasterUrl', function ($idOrReference, $size = 0, $width=0, $height=0, $mode='auto') {
            $c = new ContentService($this->_state->getMasterState());
            $url = $c->getImageCachedUrl($idOrReference, $width, $height, $size, $mode, true);
            $url = str_replace(' ', '%20', $url);
            return $url;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('truncateHtml', function ($html, $length = 240) {

            $truncateService = new Truncate();
            return $truncateService->truncate($html, $length);
        });
        $this->_twig->addFunction($function);


        $function = new \Twig_SimpleFunction('getContent', function ($id) {
            $result = [];
            $m = new HolderEntity($this->_state);
            if ($m->load($id)) {
                $result = $m->getViewData();
            }
            if(isset($result['featured_image']) && $result['featured_image']){
                $mFile = new \SidraBlue\Lote\Model\File($this->_state);
                $result['featured_image_file'] = $mFile->get($result['featured_image']);
            }
            return $result;
        });
        $this->_twig->addFunction($function);


        $function = new \Twig_SimpleFunction('getNewsByTag', function ($tag, $limit) {
            $options = [];
            $options['date_from'] = 'desc';
            $options['id'] = 'desc';
            $options['name'] = 'asc';
            $m = new ModelArticle($this->_state);
            $newsList = $m->getAccessibleListByTag($tag, 1, [], $options, 'and', $limit);
            if($newsList['total'] > 0) {
                $u = new Url($this->_state);
                $newsList['rows'] = $u->addUrlsToData($newsList['rows'], $m->getTableName());
                foreach($newsList['rows'] as $key => $value){
                    $mFile = new \SidraBlue\Lote\Model\File($this->_state);
                    $articleDetails = $value;
                    if ($articleDetails['featured_image']) {
                        $articleDetails['featured_image_file'] = $mFile->get($articleDetails['featured_image']);
                    }

                    if ($articleDetails['featured_document']) {
                        $articleDetails['featured_document_file'] = $mFile->get($articleDetails['featured_document']);
                    }
                    $newsList['rows'][$key] = $articleDetails;
                }
            }
            $m = new \SidraBlue\Lote\Model\Tag($this->_state);
            if(isset($newsList['rows']) && $newsList['total'] > 0){
                foreach($newsList['rows'] as $key => $value){
                    $newsList['rows'][$key]['_tags'] = $m->getTagValues($newsList['rows'][$key]['id'], 'sb_cms_news_article');
                }
            }
            return $newsList;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getPagesByTag', function ($tag) {
            $m = new PageItemModel($this->_state);
            return $m->getAccessibleListByTag($tag, 1,
                [['field' => 'published', 'value' => '1', 'data_type' => 'int', 'operator' => 'equals']],
                ['weighting' => 'desc', 'name' => 'asc']);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getChildsChildren', function ($parentId, $childId) {
            $m = new PageItemModel($this->_state);
            return $m->getChildsChildren($parentId,$childId);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getParentPagesByTag', function ($tag) {
            $m = new PageItemModel($this->_state);
            return $m->getAccessibleListByTag($tag, 1, [
                ['field' => 'published', 'value' => '1', 'data_type' => 'int', 'operator' => 'equals'],
                ['field' => 'parent_id', 'value' => '0', 'data_type' => 'int', 'operator' => 'equals']
            ], ['weighting' => 'desc', 'name' => 'asc']);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getContentByTag', function ($tag) {
            $m = new Holder($this->_state);
            return $m->getTagItems($tag, [], ['weighting' => 'desc', 'name' => 'asc']);
        });
        $this->_twig->addFunction($function);



        $function = new \Twig_SimpleFunction('masterContentByTag', function ($tag) {
            $m = new Holder($this->_state->getSuperParentState());
            return $m->getHolderWithTag($tag, true);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('superParentContentById', function ($id) {
            $m = new Holder($this->_state->getSuperParentState());
            return $m->getHolderById($id, false);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('masterArticlesByTag', function ($tag) {
            $m = new NewsletterArticle($this->_state->getSuperParentState());
            return $m->getAccessibleListByTag($tag, 1, [], ["publish_date"=>"desc"]);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getLatestArticle', function ($tag) {
            $m = new NewsletterArticle($this->_state);
            return $m->getArticlesWithTag($tag);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getSideItems', function ($pageID, $location) {
            $m = new ContentExtra($this->_state);
            return $m->getContentByPageLocationData($pageID, $location);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getWebsiteFeature', function () {
            $m = new NewsletterArticle($this->_state);
            return $m->getWebsiteFeature();
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getWebsiteArticleSlider', function () {
            $m = new NewsletterArticle($this->_state);
            return $m->getWebsiteArticleSlider();
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getParentAndChildren', function ($child) {
            $m = new PageItemModel($this->_state);
            return $m->getParentAndChildren($child);
        });
        $this->_twig->addFunction($function);



        $function = new \Twig_SimpleFunction('getPagesContentByTag', function ($tag) {
            $m = new PageItemModel($this->_state);
            $pages = $m->getAccessibleListByTag($tag, 1,
                [['field' => 'published', 'value' => '1', 'data_type' => 'int', 'operator' => 'equals']],
                ['weighting' => 'desc', 'name' => 'asc']);

            $c = new Content($this->_state);
            $h = new Holder($this->_state);

            if (isset($pages['rows'])) {

                foreach ($pages['rows'] as &$page) {
                    $content = $c->getContentsByPage($page['id']);

                    foreach ($content as $cont) {

                        if ($cont['type'] == "snippet") {
                            $holder = $h->get($cont['reference_id']);

                            if ($holder['active'] == 1) {
                                $page['holders'][] = $holder;
                            }

                        }

                    }

                }
            }

            return $pages;
        });
        $this->_twig->addFunction($function);


        $function = new \Twig_SimpleFunction('getPageItems', function ($tag) {

            //Get web pages and their content
            $p = new PageItemModel($this->_state);
            $pages = $p->getAccessibleListByTag($tag, 1,
                [['field' => 'published', 'value' => '1', 'data_type' => 'int', 'operator' => 'equals']],
                ['weighting' => 'desc', 'name' => 'asc']);
            $c = new Content($this->_state);
            $h = new Holder($this->_state);
            if (isset($pages['rows'])) {
                foreach ($pages['rows'] as &$page) {
                    $page['is_page'] = true;
                    $content = $c->getContentsByPage($page['id']);
                    foreach ($content as $cont) {
                        if ($cont['type'] == "snippet") {
                            $holder = $h->get($cont['reference_id']);
                            if ($holder['active'] == 1) {
                                $page['holders'][] = $holder;
                            }
                        }
                    }
                }
            }

            //Get content holders
            $holders = $h->getAccessibleListByTag($tag, 1,
                [[ 'data_type' => 'int', 'operator' => 'equals']],
                ['weighting' => 'desc', 'name' => 'asc']);

            //Merge and sort pages and holders
            $pageItems = [];
            $holderItems = [];
            if(isset($pages['rows'])){
                $pageItems = $pages['rows'];
            }
            if(isset($holders['rows'])){
                $holderItems = $holders['rows'];
            }

            //Merge & sort Pages & Holders
            $items = array_merge($holderItems, $pageItems);
            $sort_col = array();
            foreach ($items as $key=> $row) {
                $sort_col[$key] = $row['weighting'];
            }
            array_multisort($sort_col, SORT_DESC, $items);


            return $items;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getNoticeboardEvents', function ($id) {
            $c = new CalendarM($this->_state);
            if($id){
                return ['items' => $c->eventNoticeView($id)];
            }else{
                return null;
            }
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getDefaultSkin', function () {
            $skinID = 0;
            $s = new SkinModel($this->_state);
            $default_skin = $s->getDefaultSkin();
            if(isset($default_skin[0]['id'])){
               $skinID = $default_skin[0]['id'];
            }
            return $skinID;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getEventUsage', function ($id) {
            $obj = new CalendarEvent($this->_state);
            return $obj->getLinkedCalendarDetails($id);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getNotificationListing', function () {
            $m = new CampaignNotificationModel($this->_state);
            return $m->getSentNotifications(1);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getFeedListing', function () {
            $e = new CampaignNotificationModel($this->_state);
            $feed = $e->getPublishedFeeds(1, 10);
            if(isset($feed['rows'])){
                foreach($feed['rows'] as $key=>$value) {
                    $c = new ContentService($this->_state);
                    $message = $c->renderNotificationContent($value['description']);
                    $mPubSet = new PublicationSettings($this->_state);
                    $message = $mPubSet->replaceDateScheduledWildcards($message, $value['lote_created']);
                    $feed['rows'][$key]['description'] = $message;
                }
            }
            return $feed;
        });
        $this->_twig->addFunction($function);
        $function = new \Twig_SimpleFunction('getFeedListingByTag', function ($value) {
            $e = new CampaignNotificationModel($this->_state);
            $data = $e->getPublishedFeedsByTag(1, 7, $value);
            return $data;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getFeedListingByTagValue', function ($value) {
            $e = new CampaignNotificationModel($this->_state);
            $data = $e->getPublishedFeedsByTagValue(1, 7, $value);
            return $data;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getTagsByContext', function ($reference, $phrase = '') {
            $t = new Tag($this->_state);
            return $t->getTagsByContext($reference, $phrase);
        });
        $this->_twig->addFunction($function);

        //Get the data for an object from its identifier
        $function = new \Twig_SimpleFunction('getIdentifier', function ($id) {
//            return $this->_state->getIdentifier()->getIdentifierObject($id);
            return 0;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('max', function ($val1, $val2) {
            return max($val1, $val2);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('min', function ($val1, $val2) {
            return min($val1, $val2);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('DateTimeValue', function ($defaultTime, $format) {
            $d = new \DateTime($defaultTime);
            return $d->format($format);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('constructUrl', function ($parent, $module = '') {

            if (is_array($parent)) {
                if (isset($parent['url'])) {
                    $return = $parent['properties']['url'];
                    if ($parent['properties']['url'] == 'javascript:;') {
                        $string = strtolower(preg_replace('/\s+/', '-', $parent['properties']['name']));
                        $return = '/' . $string;
                    }
                    return $return;
                } else {
                    return null;
                }
            } else {
                $path = $parent;
                $array = explode('/', $path);

                $segments = 3;
                if ($module == 'page') {
                    $segments = 5;
                }
                array_splice($array, 0, $segments);

                $string = '';
                if (!empty($array)) {
                    $string = '/' . implode($array, '/');
                    $string = strtolower(preg_replace('/\s+/', '-', $string));
                }
                return $string;
            }

        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getUniqueId', function () {
            $id = mt_rand(3, 5);
            $id = $id . time();
            return $id;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getUserId', function () {
            $id = $this->_state->getUser()->id;
            return $id;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getUserDetails', function () {
            $details = $this->_state->getUser()->getViewData();
            return $details;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getUserNameById', function ($id) {
            $ue = new UserEntity($this->_state);
            $ue->load($id);
            return $ue->getData();
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getCurrencyList', function () {
            return Currency::getList();
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getCountryList', function () {
            return Country::getList();
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getIndustryList', function () {
            return Industry::getList();
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('base64_decode', function ($var) {
            return base64_decode($var);
        });
        $this->_twig->addFunction($function);

        $filter = new \Twig_SimpleFilter('base64_decode', function ($var) {
            echo base64_decode($var);
        });
        $this->_twig->addFilter($filter);

        $function = new \Twig_SimpleFunction('generateCsrf', function () {
            $m = new Token($this->_state);
            $csrfToken = $m->addToken();
            return $csrfToken['public_key'];
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('base64_encode', function ($var) {
            return base64_encode($var);
        });
        $this->_twig->addFunction($function);

        $filter = new \Twig_SimpleFilter('base64_encode', function ($var) {
            echo base64_encode($var);
        });
        $this->_twig->addFilter($filter);

        $function = new \Twig_SimpleFunction('hasAccess', function ($context, $right = 'view', $ignoreAdmin = false) {
            if (is_array($context)) {
                $result = false;
                foreach ($context as $v) {
                    if ($this->_state->getAccess()->hasAccess($v, $right, $ignoreAdmin)) {
                        $result = true;
                        break;
                    }
                }
            } else {
                $result = $this->_state->getAccess()->hasAccess($context, $right, $ignoreAdmin);
            }
            return $result;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getVisibleAdminGroups', function ($tags = '') {
            $m = new Group($this->_state);
            return $m->getGroups($tags, 'admin');
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getGroups', function ($ids) {
            $m = new Group($this->_state);
            return $m->getGroupList($ids);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getAccessGroups', function ($accessList) {
            $result = [];
            if(is_array($accessList)) {
                $ids = [];
                foreach($accessList as $a) {
                    if($a['access_type']=='group') {
                        $ids[] = $a['access_type_id'];
                    }
                }
                $m = new Group($this->_state);
                $result = $m->getGroupList($ids);
            }
            return $result;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getEventsList', function ($id) {
            $e = new CalendarEvent($this->_state);
            $date = date("Y-m-d");
            return $e->getCalendarEvents($id, $date, '', 10 );
        });
        $this->_twig->addFunction($function);


        $function = new \Twig_SimpleFunction('getAllGroups', function ($tags = '', $kind = '') {
            $m = new Group($this->_state);
            return $m->getGroups($tags, $kind);
        });
        $this->_twig->addFunction($function);

        $filter = new \Twig_SimpleFilter('json_decode', function ($string, $asArray = true) {
            return json_decode($string, $asArray);
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig_SimpleFilter('format_bytes', function ($bytes, $si = true) {
            $unit = $si ? 1000 : 1024;
            if ($bytes <= $unit) {
                return $bytes . " B";
            }
            $exp = intval((log($bytes) / log($unit)));
            $pre = ($si ? "kMGTPE" : "KMGTPE");
            $pre = $pre[$exp - 1] . ($si ? "" : "i");
            return sprintf("%.1f %sB", $bytes / pow($unit, $exp), $pre);
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig_SimpleFilter('format_phone', function ($phone) {
            if (preg_match('/^(61|\+61|0)?([0-9])?([0-9]{8})$/', str_replace(' ', '', $phone), $matches)) {
                $formatCode = $matches[1];
                $areaCode = $matches[2];
                $number = $matches[3];
                if ($areaCode == 4) {
                    $phone = substr($number, 0, 2).' '.substr($number, 2, 3).' '.substr($number, 5, 3);
                } else {
                    $phone = substr($number, 0, 4).' '.substr($number, 4, 4);
                }
                $pre = $formatCode.$areaCode;
                if (!($formatCode == 0 && $areaCode == 4) && !empty($pre)) {
                    $pre = "($pre) ";
                }
                $phone = $pre.$phone;
            }
            return $phone;
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig_SimpleFilter('intOrdinal', function ($number) {
            $ends = array('th','st','nd','rd','th','th','th','th','th','th');
            if ((($number % 100) >= 11) && (($number%100) <= 13))
                return $number. 'th';
            else
                return $number. $ends[$number % 10];
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig_SimpleFilter('format_abn', function ($abn) {
            if (preg_match('/^([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{3})$/', str_replace(' ', '', $abn), $matches)) {
                $abn = implode(' ', array_slice($matches, 1));
            }
            return $abn;
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig_SimpleFilter('format_acn', function ($acn) {
            if (preg_match('/^([0-9]{3})([0-9]{3})([0-9]{3})$/', str_replace(' ', '', $acn), $matches)) {
                $acn = implode(' ', array_slice($matches, 1));
            }
            return $acn;
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig_SimpleFilter('json_encode', function ($string) {
            return json_encode($string);
        });
        $this->_twig->addFilter($filter);

        $function = new \Twig_SimpleFunction('getSystemTreeChildren', function ($reference) {
            $adminNode = false;
            $result = [];
            if ($this->adminTree) {
                $data = json_decode($this->adminTree, true);
                if (is_array($data) && count($data) > 0 && isset($data[0])) {
                    $data = $data[0];
                }
                foreach ($data as $v) {
                    if (isset($v['reference']) && $v['reference'] == $reference) {
                        $adminNode = $v;
                        break;
                    }
                }
                if ($adminNode) {
                    foreach ($adminNode['children'] as $v) {
                        $result[] = $v;
                    }
                }
            }
            return $result;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getAppData', function ($type, $key) {
            $result = [];
            if ($type == 'system') {
                $result = $this->_state->getApps()->getSystem($key);
            } elseif ($type == 'module') {
                $result = $this->_state->getApps()->getModule($key);
            }
            return $result;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getTitle', function ($routeData) {
            $result = '';
            if (isset($routeData['title'])) {
                $result = $routeData['title'];
            }
            if (isset($routeData['_route'])) {
                $result = $routeData['_route'];
            }
            return $result;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('isMultiSite', function () {
            return $this->_state->getSites()->isMultiSite();
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('mimeIsImage', function ($mimeType) {
            return strpos($mimeType, 'image') === 0;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('mimeGetExtension', function ($mimeType, $fileName = '') {
            $result = '';
            if (preg_match('#^image.*#', $mimeType)) {
                $result = 'jpg';
                if (preg_match('#^image/png.*$#', $mimeType)) {
                    $result = 'png';
                } elseif (preg_match('#^image/tif.*$#', $mimeType)) {
                    $result = 'tif';
                } elseif (preg_match('#^image/gif.*$#', $mimeType)) {
                    $result = 'gif';
                }
            } elseif (preg_match('#^application.*xls.*$#', $mimeType)) {
                $result = 'xls';
            } elseif (preg_match('#^application.*rtf.*$#', $mimeType)) {
                $result = 'rtf';
            } elseif (preg_match('#^application.*doc.*$#', $mimeType)) {
                $result = 'doc';
            } elseif (preg_match('#^application/msword.*$#', $mimeType)) {
                $result = 'doc';
            } elseif (preg_match('#^application.*pdf.*$#', $mimeType)) {
                $result = 'pdf';
            } else {
                if ($fileName) {
                    $result = pathinfo($fileName, PATHINFO_EXTENSION);
                }
            }
            return $result;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getSetting', function ($name, $default = null) {
            return $this->_state->getSettings()->get($name, $default);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getSessionVar', function ($namespace, $name, $default = null) {
            return $this->_state->getRequest()->getSession()->get($namespace . $name, $default);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('r', function ($var, $default = null) {
            $route = $this->_state->getRoute();
            if (isset($route[$var])) {
                $result = $route[$var];
            } else {
                $result = $this->_state->getRequest()->get($var, $default);
            }
            return $result;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getEntity', function ($table, $entityId, $includeDeleted = false) {
            $result = false;
            if($entityId) {
                $q = $this->_state->getReadDb()->createQueryBuilder();
                $q->select("*")->from($table, "t")->where("t.id=:entity_id")->setParameter("entity_id", $entityId);
                if (!$includeDeleted) {
                    $q->andWhere("t.lote_deleted is null");
                }
                $result = $q->execute()->fetch(\PDO::FETCH_ASSOC);
            }
            return $result;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('s', function ($name, $default = null) {
            return $this->_state->getSettings()->getSettingOrConfig($name, $default);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('trans', function ($key, $params = []) {
            $route = $this->_state->getRoute();
            if(preg_match_all("/%%([a-zA-Z0-9\_]+)%%/", $key, $matches)) {
                foreach($matches[0] as $k=>$v) {
                    if(array_key_exists($matches[1][$k], $route)) {
                        $key = str_replace($matches[0][$k], $route[$matches[1][$k]], $key);
                    }
                    else {
                        $key = str_replace($matches[0][$k], '', $key);
                    }
                }
            }
            return $this->_state->getTranslator()->trans($key, $params);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('isChaplain', function () {
            return true;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('renderContent', function ($content) {
            $contentService = new ContentService($this->_state);
            return $contentService->renderContent($content);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('renderNotificationContent', function ($content) {
            $contentService = new ContentService($this->_state);
            return $contentService->renderNotificationContent($content, $this->_state->getUser(), null, false);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('renderContentWithDatePlanned', function ($content, $datePlanned, $convertToDisplay = false) {
            $contentService = new PublicationSettings($this->_state);
            return $contentService->renderContentWithDatePlanned($content, $datePlanned, $convertToDisplay);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getBannerByPage', function ($pageId) {
            $modelObj = new ModelAssociationImages($this->_state);
            $bannerIdArr = $modelObj->getBannerByPage($pageId);
            return $bannerIdArr;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getBannerByAssociation', function ($associationId) {
            $modelObj = new ModelAssociationImages($this->_state);
            $bannerIdArr = $modelObj->getBannerByAssociation($associationId);
            return $bannerIdArr;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('isDev', function () {
            return $this->_state->getConfig()->get('lote.dev', false);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('uniqid', function ($prefix = '') {
            return uniqid($prefix);
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('tzGetZone', function () {
            return $this->_state->getTimezone()->getDisplayTimezone();
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('tzGetList', function () {
            return Time::getAllTimezones();
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('tzGetOffset', function () {
            return $this->_state->getTimezone()->getDisplayOffset();
        });
        $this->_twig->addFunction($function);

        $filter = new \Twig_SimpleFilter('tzUtcToDisplay', function ($time, $format = false) {
            $ts = new Timezone($this->_state);
            if ($format == false) {
                $format = $this->_state->getSettings()->get('system.datetime_format', 'jS F Y g:ia');
            }
            return $ts->convertToDisplay($time, $format);
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig_SimpleFilter('tzUtcDateToDisplay', function ($time, $format = false) {
            $ts = new Timezone($this->_state);
            if ($format == false) {
                $format = 'jS F Y';
            }
            return $ts->convertToDisplay($time, $format);
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig_SimpleFilter('tzDateToUtc', function ($time, $format = false, $timezone = false) {
            $ts = new Timezone($this->_state);
            if ($timezone == false) {
                $timezone = $ts->getDisplayTimezone();
            }
            if ($format == false) {
                $format = 'jS F Y g:ia';
            }
            return $ts->convertToUtc($time, $timezone, $format);
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig_SimpleFilter('tzDateToTs', function ($time, $format = false, $timezone = false) {
            $ts = new Timezone($this->_state);
            if ($timezone == false) {
                $timezone = $ts->getDisplayTimezone();
            }
            if ($format == false) {
                $format = 'jS F Y g:ia';
            }
            return $ts->convertToTimestamp($time, $timezone, $format);
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig_SimpleFilter('loteCurrencyFormat', function ($number, $precision = 2) {
            $currencyCode = $this->_state->getSettings()->get('system.currency', 'AUD');

            $prefix = '$';
            if ($currencyCode == "GBP") {
                $prefix = '£';
            }

            return $prefix . number_format($number, $precision, '.', ',');
        });
        $this->_twig->addFilter($filter);

        $function = new \Twig_SimpleFilter('loteCurrencyFormatLabel', function () {
            $currencyCode = $this->_state->getSettings()->get('system.currency', 'AUD');

            $prefix = '$';
            if ($currencyCode == "GBP") {
                $prefix = '\u00A3';//'£';
            }

            return $prefix;
        });
        $this->_twig->addFilter($function);

        $filter = new \Twig_SimpleFilter('loteDateFormat', function ($date, $format = 'Y-m-d g:ia   ') {
            return Date::dateFormat($date, $format);
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig_SimpleFilter('safeDate', function ($date, $format = 'Y-m-d g:ia   ') {
            try {
                $date = Date::dateFormat($date, $format);
            } catch (\Exception $e) {}
            return $date;
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig_SimpleFilter('dateFormatDay', function ($displayDateTime) {
            $tz = new Timezone($this->_state);
            $dtz = new \DateTimeZone($tz->getDisplayTimezone());
            $now = new \DateTime('now', $dtz);
            $now->setTime(0,0,0);
            $then = new \DateTime($displayDateTime);
            $then->setTimezone($dtz);
            $diff = $now->diff($then);
            $diffStr = $diff->format('%R%a');
            switch($diffStr) {
                case '+1':
                    $day = 'Tomorrow';
                    break;
                case "0":
                    $day = $diffStr === '-0' ? 'Yesterday' : 'Today'; // necessary because switch equates -0 and +0
                    break;
                case '-1':case '-2':case '-3':case '-4':case '-5':
                    $day = $then->format('D');
                    break;
                default:
                    $day = $then->format('j M');
            }
            return $day;
        });
        $this->_twig->addFilter($filter);

        $filter = new \Twig_SimpleFilter('cast_to_array', function ($stdClassObject) {
            $response = array();
            foreach ($stdClassObject as $key => $value) {
                $response[] = array($key, $value);
            }
            return $response;
        });
        $this->_twig->addFilter($filter);


        $function = new \Twig_SimpleFunction('getHumanTiming', function ($date) {

            $time = strtotime($date);
            $time = time() - $time; // to get the time since that moment

            $tokens = array(
                31536000 => 'year',
                2592000 => 'month',
                604800 => 'week',
                86400 => 'day',
                3600 => 'hour',
                60 => 'minute',
                1 => 'second'
            );

            foreach ($tokens as $unit => $text) {
                if ($time < $unit) {
                    continue;
                }
                $numberOfUnits = floor($time / $unit);
                return $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '') . ' ago';
            }
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getProjectsAndPages', function () {
            $m = new ModelProject($this->_state);
            $projects = $m->getAll();

            if (!empty($projects)) {
                foreach ($projects as $key => $value) {
                    $pageObj = new PageModel($this->_state);
                    $pageList = $pageObj->getProjectPages($projects[$key]['id']);
                    $projects[$key]['pages'] = $pageList;
                }
            }

            return $projects;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getAllDisplaySuits', function () {
            $m = new ModelDisplay($this->_state);
            $displays = $m->getAllDisplays();
            $groups = $m->getGroupNames();

            $id = isset($_SESSION['control_session_id']) ? $_SESSION['control_session_id'] : 0;
            $obj = new SessionEntity($this->_state);
            $obj->load($id);
            $sessionDetails = $obj->getData();

            $data = [];
            $data['displays'] = $displays;
            $data['groups'] = $groups;
            $data['session_data'] = $sessionDetails;

            return $data;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getCountryName', function ($id) {
            $name = '';
            $obj = new CountryEntity($this->_state);
            if ($obj->load($id)) {
                $data = $obj->getViewData();
                $name = $data['name'];
            }
            return $name;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('getAccountField', function ($key) {
            $field = '';
            $a = new Account($this->_state);
            if($a->loadByField('reference', $this->_state->getSettings()->get('system.reference'))) {
                if(isset($a->getData()[$key])) {
                    $field = $a->getData()[$key];
                }
            }
            return $field;
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('isFramework', function ($site) {
            return $site == $this->_state->getConfig()->getFramework();
        });
        $this->_twig->addFunction($function);

        $function = new \Twig_SimpleFunction('env', function ($env) {
            return getenv($env);
        });
        $this->_twig->addFunction($function);

        $this->_twig->addGlobal('_route', $this->_state->getRoute());

        $this->_twig->addExtension(new Twig_Extensions_Extension_Text());

    }

    /**
     * Get the view debug data
     * @param string $level
     * @return array
     * */
    public function getDebugData($level = LogLevel::DEBUG)
    {
        $result = parent::getDebugData($level);
        $paths = $this->getPaths();
        foreach($paths as $k=>$v) {
            $paths[$k] = str_replace(LOTE_BASE_PATH, "", $v);
        }
        $result['paths'] = $paths;
        return $result;
    }

    /**
     * @return \Twig_Environment
     * */
    public function getTwig()
    {
        return $this->_twig;
    }

    private function getSkin($systemRef, $checkParent = true)
    {
        $route = $this->_state->getRoute();
        $skin = 'default';
        if (isset($this->_configData['_skin'])) {
            $skin = $this->_configData['_skin'];
        } else {
            if (isset($route['_skin'])) {
                $skin = $route['_skin'];
            } elseif (empty($route) && isset($this->_configData['_route_handler']) && isset($this->_configData['_route_handler']['skin'])) {
                $skin = $this->_configData['_route_handler']['skin'];
            } elseif ($systemRef == 'website') {
                $skin = $this->_state->getSites()->getSkin();
            } else {
                if ($checkParent && $parentRef = $this->getParentSystem($this->getSystemRef())) {
                    if ($this->_state->getSettings()->get($parentRef . '.skin', false)) {
                        $skin = $this->_state->getSettings()->get($parentRef . '.skin', 'default');
                    }
                } elseif ($systemSkin = $this->_state->getSettings()->get($this->getSystemRef() . '.skin', false)) {
                    $skin = $systemSkin;
                }
                else {
                    $parentRef = $this->getParentSystem($this->getSystemRef());
                    if($parentSkin = $this->_state->getSettings()->get($parentRef . '.skin', false)) {
                        $skin = $this->_state->getSettings()->get($parentRef . '.skin', 'default');
                    }

                }
            }
        }
        return $skin;
    }

    private function hasCustomSkin()
    {
        $route = $this->_state->getRoute();
        return isset($route['_skin']) && !empty($route['_skin']);
    }

    private function getTheme($systemRef)
    {
        $route = $this->_state->getRoute();
        $theme = 'default';
        if (isset($route['_theme'])) {
            $theme = $route['_theme'];
        } elseif (empty($route) && isset($this->_configData['_route_handler']) && isset($this->_configData['_route_handler']['theme'])) {
            $theme = $this->_configData['_route_handler']['theme'];
        } elseif ($systemRef == 'website') {
            $theme = $this->_state->getSites()->getTheme();
        }
        elseif($appTheme = $this->_state->getSettings()->get("$systemRef.theme")) {
            $theme = $appTheme;
        }
        return $theme;
    }

    private function hasCustomTheme()
    {
        $route = $this->_state->getRoute();
        return isset($route['_theme']) && !empty($route['_theme']);
    }

    public function getPaths()
    {
        $this->paths = [];
        $system = $this->getSystemRef();
        $skin = $this->getSkin($system, false);
        $parentSkin = $this->getSkin($system);

        $this->_state->getApps()->setupViewPath($system, $skin, $this);
        $this->paths[] = LOTE_CORE_PATH . 'view/' . $system . "/{$skin}/";
        $this->paths[] = LOTE_PROJECT_PATH . 'view/' . $system . "/{$skin}/";
        if ($systemConfig = $this->_state->getApps()->getAppOrSystem($system)) {
            if ($parent = $this->getParentSystem($system)) {
                $this->paths[] = LOTE_CORE_PATH . 'view/' . $this->getParentSystem($system) . "/{$parentSkin}/";
            }
            $systemBaseFolder = LOTE_CORE_PATH . 'view/'.$system.'/_base/';
            if(is_dir($systemBaseFolder)) {
                $this->paths[] = $systemBaseFolder;
            }
        }
        $this->paths[] = LOTE_CORE_PATH . 'view/_base/default/';
        $this->addCustomPaths();
        $this->paths = array_unique($this->paths);
        return $this->paths;
    }

    private function getParentSystem($system = '')
    {
        $result = false;
        $route = $this->_state->getRoute();
        if (isset($route['_handler']['parent'])) {
            $result = $route['_handler']['parent'];
        }
        if (!$result && $system) {
            if ($this->statusCode == 404) {
                //set up the parent view
                if ($system == 'crm') {
                    $result = 'admin';
                }
            }
        }
        return $result;
    }

    /**
     * @todo - define default system for the route through configuration
     * */
    protected function renderSetup()
    {
        $route = $this->_state->getRoute();
        $system = $this->getSystemRef();
        $paths = $this->getPaths();
        $paths = $this->removeInvalidPaths($paths);

        $this->_state->getApps()->setupSystem($this->_state, $system);
        $loader = new \Twig_Loader_Filesystem($paths);
        $this->_renderData['_baseUrl'] = $this->getBaseUrl();
        $this->_renderData['_httpHost'] = $this->_state->getSites()->getCurrentSiteUrl();
        if (php_sapi_name() == 'cli') {
            $this->_renderData['_httpHost'] = $this->_state->getSites()->getCurrentSiteUrl();
        } else {
            $this->_renderData['_httpHost'] = $this->_state->getRequest()->getHttpHost();
        }

        if (php_sapi_name() == 'cli') {
            $this->_renderData['_basePath'] = $this->_state->getSites()->getCurrentSiteUrl();
        } else {
            $this->_renderData['_basePath'] = $this->_state->getRequest()->getSchemeAndHttpHost() . $this->_state->getRequest()->getBasePath() . '/';
            $this->_renderData['_requestPath'] = $this->_state->getRequest()->getSchemeAndHttpHost() . $this->_state->getRequest()->getPathInfo();
        }

        $this->_renderData['_route'] = $route;

        /** @todo - what is this about - Amel * */
        $this->_renderData['_path'] = $this->path;

        $this->_renderData['_skinDir'] = $this->getSkinDir(); // $this->_renderData['_basePath'] . "theme/{$system}/{$theme}";
        $this->setupSystemUrl();

        if (php_sapi_name() != 'cli' && $this->_state->getRequest()->headers->get("js-global-hide", false) == true) {
            $this->_renderData['_hideGlobalScripts'] = true;
        }

        if(php_sapi_name() != 'cli') {
            /** @todo - Implement this for CLI as well */
            $urlData = [];
            $urlData['scheme'] = $this->_state->getUrl()->getScheme();
            $urlData['host'] = $this->_state->getUrl()->getHttpHost();
            $urlData['port'] = $this->_state->getRequest()->getPort();
            $urlData['user'] = $this->_state->getRequest()->getUser();
            $urlData['pass'] = $this->_state->getRequest()->getPassword();
            $urlData['path'] = $this->_state->getRequest()->getRequestUri();
            $urlData['query'] = $this->_state->getRequest()->getQueryString();
            $urlData['base'] = $this->_state->getUrl()->getHttpSchemeAndHost();
            $urlData['page'] = $urlData['base'].ltrim(str_replace('?'.$urlData['query'], "", $urlData['path']), '/');
            $urlData['full'] = $this->_state->getUrl()->getHttpSchemeAndHost().ltrim($urlData['path'], '/');
            $this->_renderData['_url'] = $urlData;
        }

        $this->_twig->setLoader($loader);
    }

    private function getSkinDir()
    {
        $system = $this->getSystemRef();
        $skin = $this->getSkin($system);
        $theme = $this->getTheme($system);
        $systemThemeRef = $system;


        if ($systemThemeRef == 'admin' || $this->getParentSystem() == 'admin') {
            $systemThemeRef = '_admin';
        } elseif ($systemThemeRef == 'crm' && $this->statusCode == 404) {
            $systemThemeRef = '_admin';
        }
        //@todo - find an elegant way to do the above, not hard coding...

        $themeBaseDir = $skinDir = $this->_renderData['_basePath'] . 'theme/';

        if ($this->hasCustomTheme() && $this->hasCustomSkin()) {
            $skinDir = $themeBaseDir . "{$theme}/{$skin}/";
        } elseif (is_dir(LOTE_WEB_PATH . "theme/{$systemThemeRef}/".$skin)) {
            $skinDir = $themeBaseDir . "{$systemThemeRef}/".$skin. '/';
        } elseif (isset($this->_configData['system_theme'][$system])) {
            $skinDir = $themeBaseDir . "{$systemThemeRef}/" . $this->_configData['system_theme'][$system] . '/';
        } elseif ($theme != "default" && $systemThemeRef) {
            $skinDir = $themeBaseDir . "{$systemThemeRef}/{$theme}/";
        } else {
            $skinDir = $themeBaseDir . "{$systemThemeRef}/{$skin}/";
        }

        return $skinDir;
    }

    private function removeInvalidPaths($paths)
    {
        $result = [];
        foreach ($paths as $v) {
            if (is_dir($v)) {
                $result[] = $v;
            }
        }
        return $result;
    }

    public function loginRequired()
    {
        if ($this->_state->getRequest()->isXmlHttpRequest()) {
            $this->_state->getResponse()->setStatusCode('401', 'Login Required');
            $this->_state->getResponse()->send();
        } else {
            $requestUri = $this->_state->getRequest()->getRequestUri();
            $url = $this->getSystemLoginRoute($requestUri, $this->_state->getRoute());
            if ($requestUri != '' && $requestUri != '/') {
                $url .= '/redirect/' . base64_encode($requestUri);
            }
            if(strpos($url, '/')!==0) {
                $url = '/'.$url;
            }
            $response = new RedirectResponse($this->_state->getRequest()->getSchemeAndHttpHost() . $url);
            $response->send();
        }
    }

    public function twoFactorRequired()
    {
        if ($this->_state->getRequest()->isXmlHttpRequest()) {
            $this->_state->getResponse()->setStatusCode('401', 'Two step login required for access to this resource.');
            $this->_state->getResponse()->send();
        } else {
            $requestUri = $this->_state->getRequest()->getRequestUri();
            $url = $this->getSystemTwoFactorLoginRoute($requestUri);
            if ($requestUri != '' && $requestUri != '/') {
                $url .= '/' . base64_encode($requestUri);
            }
            if (strpos($url, '/') !== 0) {
                $url = '/' . $url;
            }
            $response = new RedirectResponse($this->_state->getRequest()->getSchemeAndHttpHost() . $url);
            $response->send();
        }
    }

    public function accessDenied()
    {
        if(!$this->_state->getUser()->id) {
            $this->loginRequired();
        }
        else {
            if ($this->_state->getRequest()->isXmlHttpRequest()) {
                $this->success = false;
                $this->_state->getResponse()->setStatusCode('403', 'Access Denied');
                $this->_state->getResponse()->send();
            } else {
                $requestUri = $this->_state->getRequest()->getRequestUri();
                $url = $this->getSystemUnauthorisedRoute($requestUri);
                if(strpos($url, '/')!==0) {
                    $url = '/'.$url;
                }
                $response = new RedirectResponse($this->_state->getRequest()->getSchemeAndHttpHost() . $url);
                $response->send();
            }
        }
    }

    private function getSystemUnauthorisedRoute($requestUri)
    {
        $c = new BaseController($this->_state);
        $systemRef = $this->_state->getSystemByUrl($requestUri);
        if(!$this->_state->getAccess()->hasAccess($systemRef, "view")) {
            $systemRef = "website";
        }
        if($route = $this->_state->getRouter()->get($systemRef.'.access.denied')) {
            $result = $route->compile()->getStaticPrefix();
        }
        else {
            $result = $c->getRouteSystemPrefix($this->_state->getSystemByUrl($requestUri), true) . '/access-denied';
        }
        return $result;
    }

    private function getSystemLoginRoute($requestUri, $route = [])
    {
        $c = new BaseController($this->_state);
        $systemRef = $this->_state->getSystemByUrl($requestUri);
        $isAdminLogin = isset($route['_handler']) &&
                        isset($route['_handler']['system']) &&
                        $route['_handler']['system']=='admin' &&
                        preg_match("#(/)?_admin.*#", $requestUri, $matches);
        if($isAdminLogin) {
            $result = $c->getRouteSystemPrefix("admin") . '/login';
        } elseif (!$result = $this->getAppLoginRoute($this->_state->getRoute())) {
            if ($route = $this->_state->getRouter()->get($systemRef . '.login')) {
                $result = $route->compile()->getStaticPrefix();
            } else {
                $result = $c->getRouteSystemPrefix($this->_state->getSystemByUrl($requestUri), true) . '/login';
            }
        }
        return $result;
    }

    private function getSystemTwoFactorLoginRoute($requestUri)
    {
        $c = new BaseController($this->_state);
        $systemRef = $this->_state->getSystemByUrl($requestUri);
        if ($route = $this->_state->getRouter()->get($systemRef . '.login.two_factor')) {
            $result = $route->compile()->getStaticPrefix();
        } else {
            $result = $c->getRouteSystemPrefix($this->_state->getSystemByUrl($requestUri), true) . '/login/two-factor';
        }
        return $result;
    }

    /** @todo - put this function in a better place */
    private function getAppLoginRoute($routeData)
    {
        $result = false;
        if (isset($routeData['_handler']) && isset($routeData['_handler']['app'])) {
            $app = $routeData['_handler']['app'];
            if ($route = $this->_state->getRouter()->get($app . '.login')) {
                $result = $route->compile()->getStaticPrefix();
            }
        }
        return $result;
    }

    public function redirect($path, $headerCode = 301)
    {
        parent::redirect($path);
        $headers = $this->_state->getResponse()->headers;
        $headers->add(['Location' => $path]);
        $this->_state->getResponse()->setStatusCode($headerCode);
    }

    private function addCustomPaths()
    {
        foreach ($this->customPaths as $v) {
            $this->paths[] = $v;
        }
    }

    public function addCustomPath($path)
    {
        $this->customPaths[] = $path;
        $this->renderSetup();
    }

    public function addChildSystemPath($parentSystem, $system)
    {
        $this->customPaths[] = LOTE_CORE_PATH . 'view/' . $system . '/' . $this->_state->getSettings()->get($parentSystem . '.skin',
                'default') . '/';
    }

    public function addWebPath()
    {
        $this->customPaths[] = $this->paths[] = LOTE_CORE_PATH . 'view/website/' . $this->_state->getSettings()->get('website.skin',
                'default') . '/';
    }

    protected function fullRenderRequired()
    {
        $result = false;
        $route = $this->_state->getRoute();
        if ($this->getSystemRef() == 'website' || (isset($route['_render']) && $route['_render'])) {
            $result = true;
        }
        return $result;
    }

    public function getSourceContext($name)
    {
        // TODO: Implement getSourceContext() method.
    }

    public function getCacheKey($name)
    {
        // TODO: Implement getCacheKey() method.
    }

    public function isFresh($name, $time)
    {
        // TODO: Implement isFresh() method.
    }

    public function exists($name)
    {
        // TODO: Implement exists() method.
    }

    public function getSource($name)
    {
        // TODO: Implement getSource() method.
    }

}