<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\View\Transform\Html;

use Psr\Log\LogLevel;
use SidraBlue\Lote\Service\Content;

class Embed extends Base
{

    public function render()
    {
        if($this->customContent !== false) {
            $result = $this->customContent;
        }
        else {
            $this->renderSetup();
            $result = '';
            if(isset($this->_renderFile)){
                try {
                    $result = $this->_twig->render($this->_renderFile.'.twig', $this->_renderData);
                } catch (\Twig_Error_Loader $e) {
                    $result = "Unable to render view - {$this->_renderFile}";
                    $this->_state->getLoggers()->getMasterLogger("lote")->error($this->_state->accountReference . "-error trying to load view-" . $this->_renderFile, $this->_state->getDebug()->getLogArray(LogLevel::ERROR));
                }
                if($this->fullRenderRequired()) {
                    $c = new Content($this->_state);
                    $result = $c->renderContent($result);
                }
            }
        }
        $result = str_replace("\r", '', $result);
        $result = str_replace("\n", '', $result);
        return 'document.writeln('.json_encode($result).');';
    }

    public function loginRequired()
    {
        return "document.writeln('Login Error');";
    }

    public function accessDenied()
    {
        if($this->_state->getUser()->id) {
            $result = "document.writeln('Access Denied');";
        }
        else {
            $result = "document.writeln('Login Error');";
        }
        return $result;
    }

}