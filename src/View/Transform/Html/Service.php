<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

namespace SidraBlue\Lote\View\Transform\Html;

use SidraBlue\Lote\State\Base as BaseState;
use SidraBlue\Util\Dir;

class Service
{

    public static function renderView(BaseState $state, $viewFile, $viewData, $customPaths = [])
    {
        $view = new Single($state);
        if ($state->getView()->customPaths && is_array($state->getView()->customPaths)) {
            foreach ($state->getView()->customPaths as $p) {
                $view->addCustomPath($p);
            }
        }
        foreach ($customPaths as $p) {
            $view->addCustomPath($p);
        }
        $view->setRenderFile($viewFile);
        $view->addWebPath();
        $view->setData($viewData);
        return $view->render();
    }

    /**
     * @param BaseState $state
     * @param string $content
     * @param array $data
     * @param array $customPaths
     * @return string
     */
    public static function renderString(BaseState $state, $content, $data, $customPaths = [])
    {
        $view = new Single($state);
        if ($state->getView()->customPaths && is_array($state->getView()->customPaths)) {
            foreach ($state->getView()->customPaths as $p) {
                $view->addCustomPath($p);
            }
        }
        foreach ($customPaths as $p) {
            $view->addCustomPath($p);
        }
        $view->addCustomPath(LOTE_TEMP_PATH);

        $tmpFolder = uniqid();
        $tempTwigFile = uniqid();

        Dir::make(LOTE_TEMP_PATH . $tmpFolder);

        $content = str_replace('&lt;', '<', $content);
        $content = str_replace('&gt;', '>', $content);
        file_put_contents(LOTE_TEMP_PATH . $tmpFolder . "/" . $tempTwigFile . '.twig', $content);

        $view->addWebPath();
        $view->setData($data);
        $view->setRenderFile($tmpFolder . "/" . $tempTwigFile);
        // hack for now as we dont want to renderContent just yet so change system from website
        $view->setConfigVar('_route_handler', ['system' => 'cli']);

        $rendered = $view->render();
        unlink(LOTE_TEMP_PATH . $tmpFolder . "/" . $tempTwigFile . '.twig');
        Dir::delete(LOTE_TEMP_PATH . $tmpFolder);

        return $rendered;
    }
}
