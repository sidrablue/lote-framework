<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\View\Transform;

use SidraBlue\Lote\View\Base as Base;

class Json extends Base
{

    public function render()
    {
        if($this->_state->getRequest()->get('_lote_form_module', false) &&
            !$this->_state->getResponse()->headers->set('Content-Type','application/json')) {
            $this->_state->getResponse()->headers->set('Content-Type','text/plain');
        }
        else {
            $this->_state->getResponse()->headers->set('Content-Type','application/json');
        }
        $this->_state->getResponse()->headers->set('Access-Control-Allow-Origin','*');
        return json_encode($this->_renderData);
    }

    public function loginRequired()
    {
        $this->_state->getResponse()->setStatusCode('401', 'Login Required');
        $this->success = false;
        $this->error = 'Login Required';
        $this->_state->getResponse()->setContent($this->render());
        $this->_state->getResponse()->send();
        exit;
    }

    public function accessDenied()
    {
        if($this->_state->getUser()->id) {
            $this->_state->getResponse()->setStatusCode('403', 'Forbidden');
            $this->success = false;
            $this->error = 'Access Denied';
            $this->_state->getResponse()->setContent($this->render());
            $this->_state->getResponse()->send();
            exit;
        }
        else {
            $this->loginRequired();
        }
    }

    public function redirect($path, $headerCode = 301) {
        $this->_renderData['_redirect'] = $path;
        $this->_renderData['_httpCode'] = $headerCode;
    }

}