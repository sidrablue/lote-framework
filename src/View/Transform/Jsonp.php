<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\View\Transform;

use SidraBlue\Lote\View\Base as Base;

class Jsonp extends Base
{

    public function render()
    {
        $this->_state->getResponse()->headers->set('Content-Type','application/json; charset=utf-8');
        $callback = $this->_state->getRequest()->get('callback');
        if($callback) {
            $result = $callback.'('.json_encode($this->_renderData).');';
        }
        else {
            $result = "alert('No callback parameter specified');";
        }
        return $result;
    }

    public function loginRequired()
    {
        $this->_state->getResponse()->setStatusCode('401', 'Login Required');
        $this->_renderData = ['success' => false, 'error' => 'Login Required'];
        $this->_state->getResponse()->setContent($this->render());
        $this->_state->getResponse()->send();
        exit;
    }

    public function accessDenied()
    {
        if($this->_state->getUser()->id) {
            $this->_state->getResponse()->setStatusCode('403', 'Forbidden');
            $this->_renderData = ['success' => false, 'error' => 'Forbidden'];
            $this->_state->getResponse()->setContent($this->render());
            $this->_state->getResponse()->send();
            exit;
        }
        else {
            $this->loginRequired();
        }
    }

}
