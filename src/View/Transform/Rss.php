<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */
namespace SidraBlue\Lote\View\Transform;

use SidraBlue\Lote\View\Base as Base;

class Rss extends Base
{

    public function render()
    {
        $this->_state->getResponse()->headers->set('Content-Type','application/rss+xml');
        return $this->customContent;
    }

}