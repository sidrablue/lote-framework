<?php
/**
 * This file is part of the Lote project by Sidra Blue.
 * http://sidrablue.com/lote/
 */

/**
 * Get the account reference for this lote framework request
 * @access public
 * @param string $default - the default value
 * @return string
 */
function getLoteAccountRef($default = '')
{
    $result = $default;
    if (getenv('ACCOUNT_REF') && php_sapi_name() != 'cli') {
        $result = getenv('ACCOUNT_REF');
    } elseif (defined('ACCOUNT_BASE_URL') && php_sapi_name() != 'cli') {
        $result = trim(str_replace(ACCOUNT_BASE_URL, '', $_SERVER['HTTP_HOST']), '.');
    } elseif (php_sapi_name() == 'cli') {

        $climate = new League\CLImate\CLImate;
        $climate->arguments->add([
                'account' => [
                    'prefix' => 'a',
                    'longPrefix' => 'account',
                    'description' => 'Account',
                    'required' => false
                ]
            ]
        );
        $climate->arguments->parse();
        if ($mode = $climate->arguments->get("account")) {
            $result = $mode;
        }
    } else {
        $c = new \SidraBlue\Lote\Registry\Config();
        return $c->get("lote.mode");
    }
    return $result;
}

function func_get_args_with_keys(array $args, $class = null, $method = null, $includeOptional = false)
{
    if (is_null($class) || is_null($method)) {
        $trace = debug_backtrace()[1];
        $class = $trace['class'];
        $method = $trace['function'];
    }
    $reflection = new \ReflectionMethod($class, $method);

    if (count($args) < $reflection->getNumberOfRequiredParameters()) {
        throw new \RuntimeException("Something went wrong! We had less than the required number of parameters.");
    }

    foreach ($reflection->getParameters() as $param) {
        if (isset($args[$param->getPosition()])) {
            $args[$param->getName()] = $args[$param->getPosition()];
            unset($args[$param->getPosition()]);
        } else {
            if ($includeOptional && $param->isOptional()) {
                $args[$param->getName()] = $param->getDefaultValue();
            }
        }
    }
    return $args;
}

function ddd()
{
    foreach (func_get_args() as $v) {
        dump($v);
    }
    die;
}

function setXDebugLimits($sane = true)
{
    $depth = 4;
    $maxChildren = 256;
    $maxData = 1024;
    if(!$sane) {
        $depth = $maxChildren = $maxData = -1;
    }
    ini_set("xdebug.var_display_max_depth", $depth);
    ini_set("xdebug.var_display_max_children", $maxChildren);
    ini_set("xdebug.var_display_max_data", $maxData);
}
